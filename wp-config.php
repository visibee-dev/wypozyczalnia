<?php
function is_localhost () {
	$environments = array(
		'localhost',
		'127.0.0.1',
		'::1'
	);

	return in_array ($_SERVER['REMOTE_ADDR'], $environments) && file_exists (__DIR__ . '/config-local.php');
}

function is_staging () {
	$environments = array(
		// List of staging hosts, e.g.
		// staging.example.com
		// http://staging.example.com
		'staging.wypozyczalniacykliniarek.com',
		'http://staging.wypozyczalniacykliniarek.com'
	);

	return in_array ($_SERVER['HTTP_HOST'], $environments) && file_exists (__DIR__ . '/config-staging.php');
}

/**
 * Salts and keys for security
 * Generate them at https://api.wordpress.org/secret-key/1.1/salt
 */
define('AUTH_KEY',         'RjiF#TdgNP/~h8!-b@.Yq]bmk-<sW?rY6+cyuo4UJfHlEz[zJ9|}X(OmwCiBP-2l');
define('SECURE_AUTH_KEY',  'R^-?LzRv5s-+pC]`/dws/fHK$x]U7wnhb oJp]3+jXeY,;]0c,`T-8)2D^nme<%v');
define('LOGGED_IN_KEY',    'n|b>JW;hp4I|>d`Z,H5:uHg7+Aa<zVteq:mO!IH++$nOP9[4LV8qS-n{385q@/~c');
define('NONCE_KEY',        '0 d=NuYrDP<#oi|uhT2BJgfUQ0w|f@h~^`LzrYHZF>QOy/0[K8g1x8f}^_{@wulE');
define('AUTH_SALT',        'KsjtyL+6rdd;4 .)3~=]empQXZF/U-CsT%A3h2)*C*;a<E55qZ*2#z|q,!z=!Z0P');
define('SECURE_AUTH_SALT', 'dGhdJSkba.9Tp8.x@=%a Zt5[]-yjoZ]&E[x7AF!q|7rG!Zhz]6~.-<Sf@^!601K');
define('LOGGED_IN_SALT',   'K4h.vV/|_lND|T2WcFs/gP-s+(dp-[K0IY&3%UedgD5cuDBKhq(p%ypr]{RA}X>d');
define('NONCE_SALT',       '7g {ftU$M+-2_0!T^+E&!6[x~^|G4G86d=!HD,OFy%$+C.ZdG9cMyX%;jQT#$wO?');

/**
 * Language
 * Leave blank for American English
 */
define ('WPLANG', '');

/**
 * Load database info
 * You probably don't want to change this
 */
if (is_localhost()) {
	include (__DIR__ . '/config-local.php');
} elseif (is_staging()) {
	include (__DIR__ . '/config-staging.php');
} else {
	include (__DIR__ . '/config-production.php');
}

/**
 * Define wordpress content directory
 * You probably don't want to change this
 */
define ('CONTENT_DIR', '/src');
define ('WP_CONTENT_DIR', __DIR__ . CONTENT_DIR);
define ('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * Define wordpress core directory
 * You probably don't want to change this
 */
define ('MY_WP_CORE_DIR', '/core');
define ('WP_SITEURL', WP_HOME . MY_WP_CORE_DIR);

/**
 * You probably don't want to change this
 */
define ('DB_CHARSET', 'utf8');
define ('DB_COLLATE', '');

/**
 * Absolute path to WordPress core directory
 */
if ( !defined ('ABSPATH') )
	define ('ABSPATH', __DIR__ . MY_WP_CORE_DIR . '/');
require_once (ABSPATH . 'wp-settings.php');
