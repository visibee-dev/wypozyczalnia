(function($) {
	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd   = $('.datepicker_end').pickadate();

		// Picker objects.
		var pickerStart = $inputStart.pickadate('picker'),
			pickerEnd   = $inputEnd.pickadate('picker');

		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;

		var disabled = ebdida_data.disabled;
		var datesToDisable    = [],
			datesToDisableEnd = [];

		var calcMode = ebdida_data.calc_mode;

		if ( ! disabled ) {
			return false;
		}
		
		// Disabled weekdays
		var disabledDays  = disabled.days;

		if ( disabledDays && typeof disabledDays !== 'undefined' ) {

			// On start calendar
			var disableDaysStart = disabledDays.start;
			
			if ( disableDaysStart && typeof disableDaysStart !== 'undefined' ) {

				$.each( disableDaysStart, function( index, day ) {
					datesToDisable.push( parseInt( day ) );
				});

			}

			// On end calendar
			var disabledDaysEnd = disabledDays.end;

			if ( disabledDaysEnd && typeof disabledDaysEnd !== 'undefined' ) {

				$.each( disabledDaysEnd, function( index, day ) {
					datesToDisableEnd.push( day );
				});

			}

		}

		// disabled dates or dateranges
		var disabledDates = disabled.dates;

		if ( disabledDates && typeof disabledDates !== 'undefined' ) {
			
			// On start calendar
			var disableDatesStart = disabledDates.start;

			if ( disableDatesStart  && typeof disabledDates !== 'undefined' ) {

				$.each( disableDatesStart, function( index, date ) {

					start = date[0];

					// If is a daterange
					if ( typeof date[1] !== 'undefined' ) {

						end = date[1];

						disable = {
							from: start,
							to  : end,
							type: 'disabled'
						}

					} else {

						disable = start;
						disable['type'] = 'disabled';
						
					}

					datesToDisable.push( disable );
					
				});

			}

			// On end calendar
			var disabledDatesEnd = disabledDates.end;

			if ( disabledDatesEnd && typeof disabledDatesEnd !== 'undefined' ) {

				$.each( disabledDatesEnd, function( index, date ) {

					start = date[0];

					// If is a daterange
					if ( typeof date[1] !== 'undefined' ) {

						end = date[1];

						disable = {
							from: start,
							to  : end,
							type: 'disabled'
						}

					} else {

						disable = start;
						disable['type'] = 'disabled';
						
					}

					datesToDisableEnd.push( disable );

				});

			}
			
		}

		$('body').on('pickers_init', function(e, variation) {

			// Get already disabled (unavailable) dates (Availability Check)
			alreadyDisabled    = pickerStartItem.disable;
			alreadyDisabledEnd = pickerEndItem.disable;

			// Merge unavailable dates and disabled dates
			toDisable    = alreadyDisabled.concat( datesToDisable );
			toDisableEnd = alreadyDisabledEnd.concat( datesToDisableEnd );

			pickerStartItem.disable = toDisable;
			pickerEndItem.disable   = toDisableEnd;

			return false;
		});

		$('body').on('clear_start_picker', function(e, pickerEndItem) {
			pickerEndItem.disable = toDisableEnd;
			return false;
		});

		$('body').on('clear_end_picker', function(e, pickerStartItem) {
			pickerStartItem.disable = toDisable;
			return false;
		});

	});
})(jQuery);
