<?php

// If uninstall not called from WordPress exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

delete_option( 'ebdida_settings' );
delete_option( 'easy_booking_display_notice_ebdida_license' );

// Delete db entries
global $wpdb;

$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->postmeta WHERE meta_key = %s", "_ebdida_disabled_dates" ) );