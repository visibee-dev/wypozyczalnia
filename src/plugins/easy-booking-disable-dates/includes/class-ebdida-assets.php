<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Disabled_Assets' ) ) :

class Easy_Booking_Disabled_Assets {

	public function __construct() {
        add_action( 'easy_booking_enqueue_additional_scripts', array( $this, 'ebdida_enqueue_scripts' ), 20 );
        add_filter( 'easy_booking_frontend_parameters', array( $this, 'ebdida_frontend_parameters' ), 10, 1 );
	}

	public function ebdida_enqueue_scripts() {
        global $post;

        if ( ! is_product() ) {
            return;
        }

        $post_id = $post->ID;

        $product = get_product( $post_id );

        if ( ! wceb_is_bookable( $product ) ) {
            return;
        }

        // Get Easy Booking: Disable Dates settings
        $ebdd_settings = get_option('ebdida_settings');

        // Get disabled dates for all products
        $dates_to_disable = (array) $ebdd_settings['ebdida_disabled_dates'];

        // Get Easy Booking settings
        $easy_booking_settings = get_option('easy_booking_settings');

        // Calculation mode (Days or Nights)
        $calc_mode = $easy_booking_settings['easy_booking_calc_mode'];

        // Get product type
        $product_type = $product->product_type;

        if ( $product->is_type('variable') || $product->is_type('grouped') || $product->is_type('bundle') ) {

            if ( $product->is_type('variable') || $product->is_type('bundle') ) {
                $parent_disabled_dates = get_post_meta( $post_id, '_ebdida_disabled_dates', true );
            }

            // Get product children
            if( $product->is_type('bundle') ) {

                $bundled = $product->get_bundled_item_ids();
                $children_id = array();

                if ( $bundled ) foreach ( $bundled as $bundled_item_id ) {

                    $bundled_item = $product->get_bundled_item( $bundled_item_id );
                    $_product = $bundled_item->product;

                    if ( $_product->is_type( 'variable' ) ) {
                        $variations = $_product->get_children();

                        foreach ( $variations as $variation_id ) {
                            $children_id[] = $variation_id;
                        }
                    }

                    $children_id[] = $bundled_item->product->id;

                }

            } else {
               $children_id = $product->get_children(); 
            }

            $children_disabled_dates = array();
            if ( $children_id ) foreach ( $children_id as $child_id ) {

                // Get child disabled dates
                $child_disabled_dates = get_post_meta( $child_id, '_ebdida_disabled_dates', true );

                if ( empty( $child_disabled_dates ) ) {
                    $child_disabled_dates = array();
                }

                // Merge parent product disabled dates with variation disabled dates
                if ( isset( $parent_disabled_dates ) && ! empty( $parent_disabled_dates ) ) {
                    $child_disabled_dates = array_merge( $parent_disabled_dates, $child_disabled_dates );
                }

                // Merge with global disabled dates
                $merged_disabled = array_merge( $child_disabled_dates, $dates_to_disable );

                // Format dates and remove duplicated
                $children_disabled_dates[$child_id] = $this->ebdida_format_disabled_dates( $merged_disabled );

            }

            $data = $children_disabled_dates;
            if ( $product->is_type('variable') ) {
                $file_name = 'ebdida-variable-disable-dates';
            } else {
                $file_name = 'ebdida-grouped-disable-dates';
            }

        } else {

            // Get product disabled dates
            $product_disabled_dates = get_post_meta( $post_id, '_ebdida_disabled_dates', true );

            if ( empty( $product_disabled_dates ) ) {
                $product_disabled_dates = array();
            }

            // Merge with global disabled dates
            $data = array_merge( $product_disabled_dates, $dates_to_disable );

            // Format dates and remove duplicated
            $data = $this->ebdida_format_disabled_dates( $data );
            $file_name = 'ebdida-simple-disable-dates';

        }

        wp_enqueue_script(
            'ebdida_disable_dates',
            wceb_get_file_path( '', $file_name, 'js', EBDIDA_PLUGIN_FILE ),
            array('jquery'),
            '1.0',
            true
        );

        wp_localize_script(
            'ebdida_disable_dates',
            'ebdida_data',
            array(
                'calc_mode' => $calc_mode,
                'disabled'  => $data
            )
        );

    }

    /**
    *
    * Gets an array of disabled dates, sorted by type
    *
    * @param array $disabled_dates
    * @param array $check_duplicates
    * @return array $data - Array of disabled dates, sorted by type
    *
    **/
    private function ebdida_format_disabled_dates( $disabled_dates ) {

        if ( ! $disabled_dates || ! is_array( $disabled_dates ) ) {
            return false;
        }

        $easy_booking_settings = get_option('easy_booking_settings');

        $current_year = date('Y');
        $last_year    = $easy_booking_settings['easy_booking_max_year'];
        $years        = array();

        // Get an array of all years from current year to limit year set in the settings
        for ( $i = $current_year; $i <= $last_year; $i++ ) {
            $years[] = $i;
        }

        // Monday or Sunday
        $first_day = $easy_booking_settings['easy_booking_first_day'];

        // Init array
        $data = array(
            'days'  => array(
                'start' => false,
                'end'   => false
            ),
            'dates' => array(
                'start' => false,
                'end'   => false
            ),
        );

        foreach ( $disabled_dates as $disable ) {

            $type     = $disable['type'];
            $disabled = $disable['disabled'];
            $repeat   = $disable['repeat'];
            $calendar = isset( $disable['calendar'] ) ? $disable['calendar'] : 'start_end';

            switch ( $type ) {
                case 'ebdida_day' :

                    // If first weekday is Sunday, add 1 day to each day
                    if ( $first_day == 0 ) {
                        $disabled += 1;
                    }

                    // If after adding 1 day, you get 8, set it to 1 (Sunday => Monday)
                    if ( $disabled == 8 ) {
                        $disabled = 1;
                    }

                    $data['days'][$calendar][] = absint( $disabled );

                break;
                case 'ebdida_date' :

                    if ( $repeat === 'on' ) {
                        $day = explode( '-', $disabled );

                        foreach ( $years as $year ) {
                            $date = $year . '-' . $day[1] . '-' . $day[2];
                            $data['dates'][$calendar][] = $date;
                        }
                        
                    } else {
                        $data['dates'][$calendar][] = $disabled;
                    }
                    
                break;
                case 'ebdida_date_range' :

                    $dates = ebdida_get_dates_from_range( $disabled[0], $disabled[1], false, true );
                    
                    if ( $repeat === 'on' ) {

                        foreach ( $dates as $date ) {
                            $day = explode( '-', $date );

                            foreach ( $years as $year ) {
                                $date = $year . '-' . $day[1] . '-' . $day[2];
                                $data['dates'][$calendar][] = $date;
                            }
                        }

                    } else {

                        foreach ( $dates as $date ) {
                            $data['dates'][$calendar][] = $date;
                        }

                    }
                    
                    
                break;
            }
        }

        if ( $data ) foreach ( $data as $type => $calendars ) {

            if ( ! array_key_exists( 'start', $calendars ) ) {
                $data[$type]['start'] = array();
            }

            if ( ! array_key_exists( 'end', $calendars ) ) {
                $data[$type]['end'] = array();
            }

            // Get all "Start and end" dates and put them into "Start" and "End"
            if ( array_key_exists( 'start_end', $data[$type] ) ) {

                foreach ( $data[$type]['start_end'] as $index => $date ) {
                    $data[$type]['start'][] = $date;
                    $data[$type]['end'][]   = $date;
                    unset( $data[$type]['start_end'][$index] );
                }

            }

            // Remove "Start and end" array once it's empty
            if ( empty( $data[$type]['start_end'] ) ) {
                unset( $data[$type]['start_end'] );
            }

        }

        // Remove duplicates
        foreach ( $data[$type] as $calendar => $dates ) {

            if ( $dates && is_array( $dates ) ) {
                $data[$type][$calendar] = array_unique( $dates );
                usort( $data[$type][$calendar], 'ebdida_sort_dates' );
            }

        }

        // Group dates in dateranges
        foreach ( $data['dates'] as $calendar => $dates ) {
            if ( $dates) {
                $data['dates'][$calendar] = ebdida_group_dates_in_dateranges( $dates );
            }
        }

        return $data;

    }

    /**
    *
    * Pass extra parameters to the main pickadate-custom.js file
    *
    * @param array $params
    * @return array $params
    *
    **/
    function ebdida_frontend_parameters( $params ) {
        $ebdida_settings = get_option('ebdida_settings');
        $allow_disabled = ! isset( $ebdida_settings['ebdida_allow_to_book_disabled'] ) || empty( $ebdida_settings['ebdida_allow_to_book_disabled'] ) ? 'no' : esc_html( $ebdida_settings['ebdida_allow_to_book_disabled'] );

        $params['allow_disabled'] = $allow_disabled;
        return $params;
    }
}

return new Easy_Booking_Disabled_Assets();

endif;