<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_settings_section(
	'ebdida_multisite_settings',
	__( 'Easy Booking : Disable Dates Settings', 'easy_booking_disable_dates' ),
	array( $this, 'ebdida_multisite_settings' ),
	'easy_booking_global_settings'
);

add_settings_field(
	'ebdida_license_key',
	__( 'License Key', 'easy_booking_disable_dates' ),
	array( $this, 'ebdida_multisite_license_key' ),
	'easy_booking_global_settings',
	'ebdida_multisite_settings'
);