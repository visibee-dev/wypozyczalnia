<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

register_setting(
	'ebdida_settings',
	'ebdida_settings', 
	array( $this, 'sanitize_values' )
);

add_settings_section(
	'ebdida_main_settings',
	__( 'Settings', 'easy_booking_disable_dates' ),
	array( $this, 'ebdida_section_general' ),
	'ebdida_settings'
);

// If multisite, save the license key on the network, not the sites.
if ( ! is_multisite() ) {

	add_settings_field(
		'ebdida_license_key',
		__( 'License Key', 'easy_booking_disable_dates' ),
		array( $this, 'ebdida_license_key' ),
		'ebdida_settings',
		'ebdida_main_settings'
	);

}

add_settings_field(
	'ebdida_disabled_dates',
	__( 'Dates to disable', 'easy_booking_disable_dates' ),
	array( $this, 'ebdida_disable_dates_form' ),
	'ebdida_settings',
	'ebdida_main_settings'
);

add_settings_field(
	'ebdida_allow_to_book_disabled',
	__( 'Allow customers to book disabled dates?', 'easy_booking_disable_dates' ),
	array( $this, 'ebdida_allow_to_book_disabled' ),
	'ebdida_settings',
	'ebdida_main_settings'
);