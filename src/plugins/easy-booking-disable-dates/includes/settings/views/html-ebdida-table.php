<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div class="ebdida-disabled">

    <table class="wp-list-table widefat striped">

        <thead>

            <tr>
                <th><?php _e( 'Type', 'easy_booking_disable_dates' ); ?></th>
                <th><?php _e( 'Disable', 'easy_booking_disable_dates' ); ?></th>
                <th><?php _e( 'Repeat?', 'easy_booking_disable_dates' ); ?></th>
                <th><?php _e( 'Calendar', 'easy_booking_disable_dates' ); ?></th>
                <th>&nbsp;</th>
            </tr>

        </thead>

        <tbody class="ebdida-table" data-index="">

			<?php
			
			$i = 0;
        	if ( ! empty( $disabled_dates ) ) :

        		foreach ( $disabled_dates as $disabled_date ) :

	        		$i++;

	        		// Backward compatibility
	        		if ( ! isset( $disabled_date['calendar'] ) ) {
	        			$disabled_date['calendar'] = 'start_end';
	        		}

		        	include( 'html-ebdida-table-row.php' );

				endforeach;

			endif;

			?>

        </tbody>

        <tfoot>

            <tr>

                <th colspan="7">

                    <a href="#" class="button add-disabled-date" data-row="<?php
                        $disabled_date = array(
                            'type' => '',
                            'disabled' => '',
                            'repeat' => '',
                            'calendar' => ''
                        );
                        ob_start();
                        include( 'html-ebdida-table-row.php' );
                        echo esc_attr( ob_get_clean() );
                    ?>">
                    <?php _e( 'Add disabled date', 'easy_booking_disable_dates' ); ?>
                    </a>

                </th>

            </tr>	

        </tfoot>

    </table>

</div>