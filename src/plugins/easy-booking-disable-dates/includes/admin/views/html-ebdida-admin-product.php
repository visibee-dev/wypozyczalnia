<?php $file = isset( $variation ) ? 'html-ebdida-variation-disabled' : 'html-ebdida-disabled'; ?>
<div class="form-field downloadable_files ebdida-disabled">
		
	<label><?php _e( 'Disabled dates', 'easy_booking_disabled' ); ?>:</label>

	<table class="widefat ebdida-table ebdida-disabled-single">

		<thead>

			<tr>
				<th><?php _e( 'Type', 'easy_booking_disable_dates' ); ?></th>
				<th><?php _e( 'Disable', 'easy_booking_disable_dates' ); ?></th>
				<th><?php _e( 'Repeat?', 'easy_booking_disable_dates' ); ?></th>
				<th class="show_if_two_dates"><?php _e( 'Calendar', 'easy_booking_disable_dates' ); ?></th>
				<th>&nbsp;</th>
			</tr>

		</thead>

		<tbody>

			<?php

			$i = 0;

			if ( $disabled_dates ) foreach ( $disabled_dates as $disabled_date ) {
				$i++;

				// Backward compatibility
        		if ( ! isset( $disabled_date['calendar'] ) ) {
        			$disabled_date['calendar'] = 'start_end';
        		}

				include( $file . '.php' );
			}

			?>

		</tbody>

		<tfoot>

			<tr>

				<th colspan="4">
					<a href="#" class="button add-disabled-date" data-row="<?php
                        $disabled_date = array(
							'type'     => 'ebdida_day',
							'disabled' => '',
							'repeat'   => '',
							'calendar' => ''
                        );
                        ob_start();
                        include( $file . '.php' );
                        echo esc_attr( ob_get_clean() );
                    ?>">
                    <?php _e( 'Add disabled date', 'easy_booking_disable_dates' ); ?>
                    </a>
				</th>

			</tr>

		</tfoot>

	</table>
	
</div>