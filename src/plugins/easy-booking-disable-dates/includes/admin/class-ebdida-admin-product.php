<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Disabled_Admin_Product' ) ) :

class Easy_Booking_Disabled_Admin_Product {

	public function __construct() {

		// Filter product types
        if ( WCEB()->allowed_types ) foreach ( WCEB()->allowed_types as $type ) {

            // If product is grouped, the disableddates are set on the children (simple) products
            if ( $type !== 'grouped' ) {

				add_action( 'easy_booking_after_' . $type . '_booking_options', array( $this, 'ebdida_product_disabled_dates' ), 25, 1 );
				add_action( 'woocommerce_process_product_meta_' . $type, array( $this, 'ebdida_save_disabled_dates' ), 10, 1);

			}

		}

		// Variation disabled dates
		add_action( 'easy_booking_after_variation_booking_options', array( $this, 'ebdida_variation_disabled' ), 25, 2 );

		// Save variation disabled dates
		add_action( 'woocommerce_save_product_variation', array( $this, 'ebdida_save_variation_disabled_dates' ), 15, 2 );

	}

	/**
	 *
	 * Single, variable and other product type disabled dates form
	 *
	 */
	public function ebdida_product_disabled_dates( $product ) {
		$disabled_dates = get_post_meta( $product->id, '_ebdida_disabled_dates', true );
		echo '<div class="options_group">';
			include( 'views/html-ebdida-admin-product.php' );
		echo '</div>';
	}

	/**
	 *
	 * Variation disabled dates form
	 *
	 */
	public function ebdida_variation_disabled( $loop, $variation ) {
		$disabled_dates = get_post_meta( $variation->ID, '_ebdida_disabled_dates', true );
		echo '<div class="show_if_variation_bookable">';
			include( 'views/html-ebdida-admin-product.php' );
		echo '</div>';
	}

	/**
	 *
	 * Save single, variable and other product type disabled dates
	 *
	 */
	public function ebdida_save_disabled_dates( $post_id ) {

		// Get disabled dates
		$disabled = isset( $_POST['ebdida_settings'] ) ? $_POST['ebdida_settings'] : array();

		// Format and sanitize values
		$ebdida_disabled_dates = ebdida_sanitize_disabled_dates( $disabled );

		// Update post meta
		update_post_meta( $post_id, '_ebdida_disabled_dates', $ebdida_disabled_dates );

	}

	/**
	 *
	 * Save variation disabled dates
	 *
	 */
	public function ebdida_save_variation_disabled_dates( $variation_id , $v ) {

		// Get disabled dates
		$settings = isset( $_POST['ebdida_settings'] ) ? $_POST['ebdida_settings'] : array();

		$disabled = array();

		$disabled_options = array(
			'var_ebdida_date_type',
			'var_ebdida_disable_day',
			'var_ebdida_disable_date',
			'var_ebdida_disable_daterange_start',
			'var_ebdida_disable_daterange_end',
			'var_ebdida_repeat',
			'var_ebdida_calendar'
		);

		// Get disabled dates for the variation
		foreach ( $disabled_options as $option ) {
			$name = str_replace( 'var_', '', $option );
			$disabled[$name] = isset( $settings[$option][$v] ) ? $settings[$option][$v] : '';
		}

		// Format and sanitize values
		$ebdida_disabled_dates = ebdida_sanitize_disabled_dates( $disabled );
		
		// Update post meta
		update_post_meta( $variation_id, '_ebdida_disabled_dates', $ebdida_disabled_dates );

	}

}

return new Easy_Booking_Disabled_Admin_Product();

endif;