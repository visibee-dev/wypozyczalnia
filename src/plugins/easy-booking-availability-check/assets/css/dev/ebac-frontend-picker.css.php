<?php

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

header("Content-type: text/css; charset: UTF-8");

$background_color = isset( $settings['easy_booking_background_color'] ) ? esc_html( $settings['easy_booking_background_color'] ) : '#FFFFFF';
$main_color = isset( $settings['easy_booking_main_color'] ) ? esc_html( $settings['easy_booking_main_color'] ) : '#0089EC';
$text_color = isset( $settings['easy_booking_text_color'] ) ? esc_html( $settings['easy_booking_text_color'] ) : '#000000';

?>

.picker__day.booked {
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAPElEQVQYV2NkIADOnj37nxGfGpACY2NjRpyKYApAhmBVhKwAqyJ0BRiKsClAUYRLAVwRPgVgRYQUgBQBAKv8I3BUT3C3AAAAAElFTkSuQmCC) repeat;
}

.picker__day--stock {
	position: relative;
	padding: .3125em 50% .3125em 0;
}

.stock-amount {
  box-sizing: border-box;
  height: 100%;
  width: 50%;
  font-size: 0.8em;
  position: absolute;
  top: 0;
  right: 0;
  padding: .525em 0 .525em 0;
  background: <?php echo wceb_adjust_brightness($background_color, -10); ?>;
  text-align: center;
}

.stock-amount.in_stock {
  color: #5fa644;
}

.stock-amount.low {
  color: #e49518;
}

@media (max-width: 480px) {
  .stock-amount {
    display: none;
  }
}

.picker__day--highlighted .stock-amount {
  color: <?php echo $text_color; ?>;
}