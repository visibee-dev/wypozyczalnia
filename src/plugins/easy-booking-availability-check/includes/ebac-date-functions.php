<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
*
* Gets all dates between start and end dates
*
* @param string $start
* @param string $end
* @param bool $past - Get passed dates
* @param bool $offset - Whether to apply "Nights" mode one-day offset or not
* @return array $unix_dates - Array of dates converted to Unix timestamp
*
**/
function ebac_get_dates_from_range( $start, $end, $past = false, $offset = false ) {

	// Get Easy Booking settings
	$easy_booking_settings = get_option('easy_booking_settings');

	 // Calculation mode (Days or Nights)
	$calc_mode = $easy_booking_settings['easy_booking_calc_mode'];

    // If calculation mode is set to "Nights" and offset to true, add one day to start day (as you book the night)
    if ( $offset && $calc_mode === 'nights' ) {
       $start = date( 'Y-m-d', strtotime( $start . ' +1 day' ) );
    }

    $dates = array( $start );

    // Get current date
    $current_date = date( 'Y-m-d' );

    // Convert current date to time
    $day = strtotime( $current_date );

    while ( end( $dates ) < $end ) {
        $dates[] = date( 'Y-m-d', strtotime( end( $dates ) . ' +1 day' ) );
    }

    $unix_dates = array();
    foreach ( $dates as $date ) {

        // Get time
        $unix_date = strtotime( $date );

        // If past is true, add all dates
        if ( $past ) {
            $unix_dates[] = $date;
        } else if ( $unix_date >= $day ) { // Else, add only the one superior to the current day
            $unix_dates[] = $date;
        }

    }

    return $unix_dates;
}

/**
*
* Group an array of dates in dateranges with quantity
*
* @param array $dates
* @return array $dateranges - Array of dateranges
*
**/
function ebac_group_dates_in_dateranges( $dates, $with_qty = false, $format = true ) {

    if ( empty( $dates ) ) {
        return false;
    }

    // Get all dates
    $entries = ( ! $with_qty ) ? array_values( $dates ) : array_keys( $dates );

    // Init variables
    $i      = 0;
    $j      = 0;
    $k      = 0;
    $exists = true;
    $range  = false;

    $dateranges = array();
    while ( $exists ) {

        if ( array_key_exists( $i, $entries ) && ! $range ) {

            $index    = $entries[$i]; // Date

            $start = ( true === $format ) ? ebac_get_formatted_date( $index ) : $index;
            $dateranges[$k][0] = $start;

            if ( $with_qty ) {
                $quantity = $dates[$index]; // Quantity
                $dateranges[$k][2] = $quantity;
            }

        }

        if ( ! array_key_exists( $i+1, $entries ) ) {
            $exists = false;
        }

        $next_index = intval( $i+$j );

        if ( ! array_key_exists( $next_index, $entries ) || ! array_key_exists( $next_index+1, $entries ) ) {

            $exists = false;
            $next_day = false;

        } else {

            // Get next day date
            $next_day = date( 'Y-m-d', strtotime( $entries[$next_index] . ' +1 day' ) );

        }

        // If next entry is next day and has the same quantity
        if ( $exists
            && $entries[$next_index+1] === $next_day
            && ( ( ! $with_qty ) || ( $with_qty && $dates[$entries[$next_index]] === $dates[$entries[$next_index+1]] ) ) ) {

            $end = ( true === $format ) ? ebac_get_formatted_date( $entries[$next_index+1] ) : $entries[$next_index+1];
            $dateranges[$k][1] = $end;
            ksort( $dateranges[$k] );

            $j++;

            $range = true;

        } else {

            $tmp   = $i+1;
            $i     = $tmp+$j;
            $j     = 0;

            $k++;

            $range = false;

        }

    }

    return $dateranges;
}

/**
*
* Formats a date from "yyyy-mm-dd" oe "yyyy,mm,dd" to an array "0 => 'yyyy', 1 => 'mm', 2 => 'dd'"
*
* @param str $date
* @return array $formatted_date
*
**/
function ebac_get_formatted_date( $date ) {

    if ( preg_match( '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/', $date ) ) {
        $disabled_date  = explode( '-', $date );
    } else if ( preg_match( '/^([0-9]{4}\,[0-9]{2}\,[0-9]{2})$/', $date ) ) {
        $disabled_date  = explode( ',', $date );
    } else {
        return false;
    }
    
    $formatted_date = array();

    foreach ( $disabled_date as $date_item ) {

        $formatted_date = array(
            intval( $disabled_date[0] ),
            intval( $disabled_date[1]  - 1 ), // In Javascript months are zero-indexed
            intval( $disabled_date[2] )
        );

    }

    return $formatted_date;

}

/**
*
* Check if date is in the past
*
* @param str $date
* @return bool
*
**/
function ebac_is_passed_date( $date ) {

    $passed = false;

    // Get current day
    $current_date = date( 'Y-m-d' );

    // Convert current day to timestamp
    $day = strtotime( $current_date );

    // Convert date to timestamp
    $time = strtotime( $date );

    if ( $time < $day ) {
        $passed = true;
    }

    return $passed;

}