<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Settings' ) ) :

class Easy_Booking_Availability_Settings {

	private $settings;
	private $global_settings;

	public function __construct() {

		// get plugin options values
		$this->settings = get_option( 'easy_booking_availability_settings' );
		
		// initialize options the first time
		if ( ! $this->settings ) {
		
		    $this->settings = array(
				'easy_booking_availability_license_key' => '',
				'easy_booking_availability_display'     => '',
				'easy_booking_availability_colors'      => '',
				'ebac_unavailability_period'            => 0
		    );

		    add_option( 'easy_booking_availability_settings', $this->settings );

		}

		// Backward_compatibility
		if ( ! isset( $this->settings['ebac_unavailability_period'] ) ) {
			$this->settings['ebac_unavailability_period'] = 0;
			add_option( 'easy_booking_availability_settings', $this->settings );
		}

		if ( is_multisite() ) {
			
			$this->global_settings = get_option( 'easy_booking_global_settings' );

			if ( ! isset( $this->global_settings['easy_booking_availability_license_key'] ) ) {
				$this->global_settings['easy_booking_availability_license_key'] = '';
			}

			update_option( 'easy_booking_global_settings', $this->global_settings );

		}

		add_action( 'admin_menu', array( $this, 'ebac_add_setting_page' ), 10 );
		add_action( 'admin_init', array( $this, 'ebac_settings_init' ) );
		add_action( 'easy_booking_availability_settings_tab', array( $this, 'ebac_display_settings' ), 10 );
		add_action( 'easy_booking_save_settings', array( $this, 'ebac_generate_css' ), 10, 1 );

		$license_set = ! isset( $this->settings['easy_booking_availability_license_key'] ) || empty( $this->settings['easy_booking_availability_license_key'] ) ? false : true;

		if ( get_option( 'easy_booking_display_notice_ebac_license' ) !== '1' && ! $license_set ) {
			update_option( 'easy_booking_display_notice_ebac_license', 0 );
		} else {
			update_option( 'easy_booking_display_notice_ebac_license', '1' );
		}

	}

	/**
	 *
	 * Plugin settings page
	 *
	 */
	public function ebac_add_setting_page() {
		$option_page = add_submenu_page(
			'easy-booking',
			'Availability Check',
			'Availability Check',
			apply_filters( 'easy_booking_settings_capability', 'manage_options' ),
			'easy-booking-availability-check',
			array( $this, 'easy_booking_availability_option_page' )
		);
	}

	/**
	 *
	 * Generate CSS for the calendars when saving Easy Booking settings
	 *
	 */
	public function ebac_generate_css( $settings ) {

		$plugin_dir = plugin_dir_path( EBAC_PLUGIN_FILE );

        $php_file = realpath( $plugin_dir . 'assets/css/dev/ebac-frontend-picker.css.php' );

		$blog_id = '';

        if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			$blog_id = '.' . get_current_blog_id();
        }
        
        $css_file = realpath( $plugin_dir . 'assets/css/ebac-frontend-picker' . $blog_id . '.min.css' );

        if ( $php_file ) {
        	ob_start(); // Capture all output (output buffering)

	        require( $php_file ); // Generate CSS
	        
	        $css = ob_get_clean(); // Get generated CSS (output buffering)
	        $minified_css = wceb_minify_css( $css ); // Minify CSS

	        if ( file_exists( $css_file ) ) {

	        	if ( is_writable( $css_file ) )
	        		file_put_contents( $css_file, $minified_css ); // Save it

	        } else {

		        $created_file = fopen( $plugin_dir . 'assets/css/ebac-frontend-picker' . $blog_id . '.min.css', 'a+' );
		        fwrite( $created_file, $minified_css );
		        fclose( $created_file );

	        }

        }
	}

	/**
	 *
	 * Init settings
	 *
	 */
	public function ebac_settings_init() {

		include_once( 'includes/ebac-settings.php' );

		// Multisite settings
		if ( is_multisite() ) {
			include_once( 'includes/ebac-network-settings.php' );
		}

	}

	public function easy_booking_availability_section_general() {
		echo '';
	}

	/**
	 *
	 * Display settings page content
	 *
	 */
	public function easy_booking_availability_option_page() {
		include_once( 'views/ebac-html-settings.php' );
	}

	/**
	 *
	 * Settings tab
	 *
	 */
	public function ebac_display_settings() { ?>
		<h2><?php _e( 'Easy Booking : Availability Check Settings', 'easy_booking_availability' ); ?></h2>

		<form method="post" action="options.php">

			<?php settings_fields( 'easy_booking_availability_settings' ); ?>
			<?php do_settings_sections( 'easy_booking_availability_settings' ); ?>
			 
			<?php submit_button(); ?>

		</form>
	<?php }

	public function easy_booking_availability_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'easy_booking_availability_license_key',
			'name'              => 'easy_booking_availability_settings[easy_booking_availability_license_key]',
			'value'             => $this->settings['easy_booking_availability_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_availability' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	public function easy_booking_availability_display() {
		wceb_settings_checkbox( array(
			'id'          => 'easy_booking_availability_display',
			'name'        => 'easy_booking_availability_settings[easy_booking_availability_display]',
			'description' => __('Display product availabilities on the front-end. For better rendering, they won\'t be displayed on mobile and for grouped products.', 'easy_booking_availability'),
			'value'       => isset( $this->settings['easy_booking_availability_display'] ) ? 'on' : '',
			'cbvalue'     => 'on'
		));
	}

	public function easy_booking_availability_colors() {
		wceb_settings_checkbox( array(
			'id'          => 'easy_booking_availability_colors',
			'name'        => 'easy_booking_availability_settings[easy_booking_availability_colors]',
			'description' => __('Display high availabilities in green and low availabilities in orange on the front-end. Requires "Display Availabilities".', 'easy_booking_availability'),
			'value'       => isset( $this->settings['easy_booking_availability_colors'] ) ? 'on' : '',
			'cbvalue'     => 'on'
		));
	}

	public function ebac_unavailability_period() {
		wceb_settings_input( array(
			'type'              => 'number',
			'id'                => 'ebac_unavailability_period',
			'name'              => 'easy_booking_availability_settings[ebac_unavailability_period]',
			'value'             => $this->settings['ebac_unavailability_period'],
			'description'       => __( 'Make your products unavailable for x days after the end of a booking. The unavailability period will also be applied before the booked dates to prevent other bookings from overlapping.', 'easy_booking_availability' ),
			'custom_attributes' => array(
				'min' => '0',
				'max' => '30'
			)
		));
	}

	public function easy_booking_availability_init() {
		?>
		
			<button type"button" id="easy_booking_availability_stock_init" class="button">
				<?php _e('Initialize availabilities', 'easy_booking_availability' ); ?>
			</button>

			<p class="easy_booking_availability_response"></p>

		<?php
	}

	public function easy_booking_availability_multisite_settings() {
		do_settings_sections('easy_booking_availability_multisite_settings');
	}

	public function easy_booking_availability_multisite_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'easy_booking_availability_license_key',
			'name'              => 'easy_booking_global_settings[easy_booking_availability_license_key]',
			'value'             => $this->global_settings['easy_booking_availability_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_availability' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	public function sanitize_values( $settings ) {
		
		foreach ( $settings as $key => $value ) {
			$settings[$key] = esc_html( $value );
		}

		return $settings;
	}
}

return new Easy_Booking_Availability_Settings();

endif;