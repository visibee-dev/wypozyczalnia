<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Order' ) ) :

class Easy_Booking_Availability_Order {

	public function __construct() {
        add_filter( 'woocommerce_order_item_quantity', array( $this, 'ebac_prevent_reduce_order_stock' ), 10, 3 );
        add_action( 'woocommerce_reduce_order_stock', array( $this, 'ebac_update_booked_products' ), 10, 1 );
        add_action( 'woocommerce_before_delete_order_item', array( $this, 'ebac_cancel_booking' ), 10, 1 );
        add_action( 'woocommerce_saved_order_items', array( $this, 'ebac_update_order_product' ), 10, 2 );
        add_action( 'woocommerce_restock_refunded_item', array( $this, 'ebac_update_availability_on_refund' ), 10, 4 );
        add_action( 'woocommerce_order_status_cancelled', array( $this, 'ebac_cancelled_order' ), 10, 1 );
    }

    /**
    *
    * Set order item quantity to 0 when reducing stock (so it doesn't reduce stock)
    *
    * @param int $qty - Quantity ordered
    * @param WC_Order $order  - Order object
    * @param array $item - Order item
    *
    **/
    public function ebac_prevent_reduce_order_stock( $qty, $order, $item ) {
        $product = $order->get_product_from_item( $item );

        if ( wceb_is_bookable( $product ) ) {
            $qty = 0;
        }

        return $qty;
    }

    /**
    *
    * Updates amount booked when ordering a bookable product
    *
    * @param int $order_id - order id
    * @param array $posted  - order content
    *
    **/
    public function ebac_update_booked_products( $order ) {

        $ebac_settings  = get_option('easy_booking_availability_settings');
        $unavailability = absint( $ebac_settings['ebac_unavailability_period'] );

        $items = $order->get_items();

        if ( $items ) foreach ( $items as $item ) {
            
            $product        = $order->get_product_from_item( $item );
            $managing_stock = $product->managing_stock();

            if ( wceb_is_bookable( $product ) && $managing_stock ) {

                $product_id   = $item['product_id'];
                $variation_id = $item['variation_id'];

                // Product or variation ID
                $id = ! empty( $variation_id ) || ! $variation_id === '0' ? $variation_id : $product_id;

                $start    = $item['ebs_start_format'];
                $end      = isset( $item['ebs_end_format'] ) ? $item['ebs_end_format'] : $start;
                $quantity = intval( $item['qty'] );

                if ( $unavailability > 0 ) {
                    $start = date( 'Y-m-d', strtotime( $start . ' -' . $unavailability . ' days' ) );
                    $end = date( 'Y-m-d', strtotime( $end . ' +' . $unavailability . ' days' ) );
                }

                // Get product stock for the booked dates, including the current booked quantity
                $booked = ebac_get_new_stock( $id, $start, $end, $quantity );

                // Update post meta
                update_post_meta( $id, '_booking_days', $booked );

                // If is a variation with parent product managing stock
                if ( $managing_stock === 'parent' ) {
                    $parent_booked = ebac_get_new_stock( $product_id, $start, $end, $quantity );
                    update_post_meta( $product_id, '_booking_days', $parent_booked );
                }

                $keep_stock_id = $managing_stock === 'parent' ? $product_id : $id;

                do_action( 'ebac_order_processed', $item, $keep_stock_id, $booked );

            }
        }

    }

    /**
    *
    * Updates amount booked when deleting a bookable product from an order
    *
    * @param int $item_id - item id
    *
    **/
    public function ebac_cancel_booking( $item_id ) {
        $product_id   = wc_get_order_item_meta( $item_id, '_product_id', true );
        $variation_id = wc_get_order_item_meta( $item_id, 'variation_id', true );

        $id = ! empty( $variation_id ) || ! $variation_id === '0' ? $variation_id : $product_id;

        $product = wc_get_product( $id );
        $managing_stock = $product->managing_stock();

        if ( wceb_is_bookable( $product ) && $managing_stock ) {
                
            $start    = wc_get_order_item_meta( $item_id, '_ebs_start_format', true );
            $end      = wc_get_order_item_meta( $item_id, '_ebs_end_format', true );
            $quantity = - wc_get_order_item_meta( $item_id, '_qty', true );

            if ( empty( $end ) ) {
                $end = $start;
            }
            
            $booked = ebac_get_new_stock( $id, $start, $end, $quantity );
            update_post_meta( $id, '_booking_days', $booked );

            if ( $managing_stock === 'parent' ) {
                $parent_booked = ebac_get_new_stock( $product_id, $start, $end, $quantity );
                update_post_meta( $product_id, '_booking_days', $parent_booked );
            }

        }
    }

    /**
    *
    * Updates amount booked when updating order products or saving order
    *
    * @param int $item_id - item id
    * @param array $order_item - additional information about order item
    *
    **/
    public function ebac_update_order_product( $order_id, $posted ) {

        $order = new WC_Order( $order_id );
        $items = $order->get_items();

        if ( $items ) foreach ( $items as $item_id => $item ) {

            $product = $order->get_product_from_item( $item );

            if ( ! $product ) {
                continue;
            }

            $order_item['product_id']   = wc_get_order_item_meta( $item_id, '_product_id' );
            $order_item['variation_id'] = wc_get_order_item_meta( $item_id, '_variation_id' );

            $managing_stock = $product->managing_stock();

            if ( wceb_is_bookable( $product ) && $managing_stock ) {

                $product_id = $order_item['product_id'];
                $id         = empty( $order_item['variation_id'] ) || $order_item['variation_id'] === '0' ? $product_id : $order_item['variation_id'];

                $booked_stock = ebac_get_booked_item_from_order( $id );
                
                if ( $booked_stock ) {
                    update_post_meta( $id, '_booking_days', $booked_stock );
                }

                if ( $managing_stock === 'parent' ) {

                    $parent_booked_stock = ebac_get_booked_item_from_order( $product_id );
                    
                    if ( $parent_booked_stock ) {
                        update_post_meta( $product_id, '_booking_days', $parent_booked_stock );
                    }

                }

                do_action( 'ebac_order_processed', false, $id, $booked_stock );

            }

        }

    }

    public function ebac_update_availability_on_refund( $product_id, $old_stock, $new_quantity, $order ) {
        $this->ebac_update_order_product( $order->id, array() );
    }

    public function ebac_cancelled_order( $order_id ) {
        $this->ebac_update_order_product( $order_id, array() );
    }
    
}

return new Easy_Booking_Availability_Order();

endif;