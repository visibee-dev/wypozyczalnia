<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Admin_Assets' ) ) :

class Easy_Booking_Availability_Admin_Assets {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'ebac_admin_scripts' ), 40 );
    }

    public function ebac_admin_scripts() {

        wp_register_script(
            'easy_booking_availability',
            wceb_get_file_path( 'admin', 'ebac-init', 'js', EBAC_PLUGIN_FILE ),
            array('jquery'),
            '1.0',
            true
        );

        wp_enqueue_script( 'easy_booking_availability' );

        wp_localize_script(
            'easy_booking_availability',
            'init',
            array(
                'ajax_url'     => esc_url( admin_url( 'admin-ajax.php' ) )
            )
        );

	}

}

return new Easy_Booking_Availability_Admin_Assets();

endif;