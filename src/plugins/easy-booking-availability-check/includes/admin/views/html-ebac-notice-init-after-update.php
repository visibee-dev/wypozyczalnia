<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="updated easy-booking-notice">

	<p>

		<?php _e( 'You have just updated Easy Booking: Availability Check to a new version. You need to update the availabilities.', 'easy_booking_availability' ); ?>

		<p>

			<button type="button" class="button" id="easy_booking_availability_stock_init">
				<?php _e( 'Initialize availabilities', 'easy_booking_availability' ); ?>
			</button>

			<span class="easy_booking_availability_response"></span>

		</p>

		<button type="button" class="notice-dismiss easy-booking-notice-close" data-notice="init_after_update"></button>

	</p>

</div>