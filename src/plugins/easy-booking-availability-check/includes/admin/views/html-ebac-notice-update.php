<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="updated easy-booking-notice">

	<p>

		<?php _e( 'This version of Easy Booking: Availability Check requires at least WooCommerce Easy Booking version 1.9. Please update WooCommerce Easy Booking to its latest version.', 'easy_booking_availability' ); ?>

	</p>

</div>