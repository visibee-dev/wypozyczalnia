<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Reports' ) ) :

class Easy_Booking_Availability_Reports {

	public function __construct() {

		add_filter( 'easy_booking_reports_tabs', array( $this, 'ebac_reports_tab' ), 10, 1 );
		add_action( 'easy_booking_load_report_scripts', array( $this, 'ebac_load_report_scripts' ) );
		add_action( 'easy_booking_reports_availabilities', array( $this, 'ebac_reports' ) );

	}

	/**
    *
    * Add an "Availabilities" tab to the reports page
    * @param array $wceb_tabs
    * @return array $wceb_tabs
    *
    **/
	public function ebac_reports_tab( $wceb_tabs ) {
		$wceb_tabs['availabilities'] = __('Availabilities', 'easy_booking_availability');
		return $wceb_tabs;
	}

	/**
    *
    * Load scripts and styles on the reports page
    *
    **/
	public function ebac_load_report_scripts() {

        // Load scripts only on the "Availabilities" tab
        if ( ( ! isset( $_GET['tab'] ) ) || ( isset( $_GET['tab'] ) && $_GET['tab'] !== 'availabilities' ) ) {
            return;
        }

		// JS

		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'wc-enhanced-select' );
		wp_enqueue_script( 'jquery-blockui' );

		wp_register_script(
            'easy_booking_availability_reports',
            wceb_get_file_path( 'admin', 'ebac-reports-functions', 'js', EBAC_PLUGIN_FILE ),
            array('jquery'),
            '1.0',
            true
        );

        wp_enqueue_script( 'easy_booking_availability_reports' );

        $low_stock    = get_option('woocommerce_notify_low_stock_amount');
        $out_of_stock = get_option('woocommerce_notify_no_stock_amount');

        $settings = get_option('easy_booking_settings');
        $max_year = $settings['easy_booking_max_year'];

        wp_localize_script(
            'easy_booking_availability_reports',
            'admin_fc',
            array(
                'ajax_url'     => esc_url( admin_url( 'admin-ajax.php' ) ),
                'low_stock'    => $low_stock,
                'out_of_stock' => $out_of_stock,
                'max_year'     => esc_html( $max_year )
            )
        );

        // CSS

		wp_register_style(
			'easy_booking_availability_reports_picker',
			wceb_get_file_path( 'admin', 'ebac-reports-picker', 'css', EBAC_PLUGIN_FILE ),
			true
		);

        wp_enqueue_style( 'easy_booking_availability_reports_picker' );
        

	}

	/**
    *
    * Display reports
    *
    **/
	public function ebac_reports() {
		include_once( 'views/ebac-html-reports.php' );
	}
}

return new Easy_Booking_Availability_Reports();

endif;