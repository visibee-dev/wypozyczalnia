<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div class="wceb-reports tablenav top">

	<input type="hidden" id="ebac_product_id" name="ebac_product_ids" value="" data-selected="" class="wc-product-search" style="width: 100%;" data-action="ebac_search_bookable_products" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-multiple="false" />

	<button type="button" id="ebac_reports_search" class="button">
		<?php _e( 'Get availabilities', 'easy_booking_availability' ); ?>
	</button>

</div>

<span class="ebac_reports_pickadate"></span>