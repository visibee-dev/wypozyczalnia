<?php

// If uninstall not called from WordPress exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

delete_option( 'easy_booking_availability_settings' );
delete_option( 'easy_booking_availability_imports' );
delete_option( 'easy_booking_display_notice_init' );
delete_option( 'easy_booking_display_notice_init_after_update' );
delete_option( 'easy_booking_display_notice_ebac_license' );

// Delete db entries
global $wpdb;

$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->postmeta WHERE meta_key = %s", "_booking_days" ) );