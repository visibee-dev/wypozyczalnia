"use_strict";

(function($) {
	var Form = {
		init: function() {
			this.cacheDOM();
			this.addEventListeners();

			if (this.isChecked(this.$WITH_DISCOUNT)) {
				this.renderStep2();
			} else {
				this.renderStep1();
			}

			if (this.getRadioBtnVal(this.$METHOD) == 'lakier') {
				this.$CONTAINER_ENAMEL.show();
				this.$CONTAINER_FINISH.show();
			}

			if (this.getRadioBtnVal(this.$ENAMEL) == '2-składnikowy') {
				this.$FINISH_MAT_LABEL.show();
			}
		},
		cacheDOM: function() {
			this.$FORM = $('#materialsCalculatorForm');

			this.$CONTAINER_DISCOUNT = this.$FORM.find('#container-discount');
			this.$CONTAINER_WITH_MATERIALS = this.$FORM.find('#container-with-materials');
			this.$CONTAINER_WITHOUT_MATERIALS = this.$FORM.find('#container-without-materials');

			this.$CONTAINER_ENAMEL = this.$FORM.find('#container-enamel');
			this.$CONTAINER_FINISH = this.$FORM.find('#container-finish');

			this.$FLOOR_SQM = this.$FORM.find('#floorSqm');
			this.$ROOM_NUMBER = this.$FORM.find('#roomNumber');

			this.$WITH_DISCOUNT = this.$FORM.find('input[name="materials"]');

			this.$WOOD = this.$FORM.find('input[name="wood"]');
			this.$METHOD = this.$FORM.find('input[name="method"]');
			this.$ENAMEL = this.$FORM.find('input[name="enamel"]');
			this.$FINISH = this.$FORM.find('input[name="finish"]');

			this.$FINISH_MAT_INPUT = this.$FORM.find('input[name="finish"]#finish-mat');
			this.$FINISH_MAT_LABEL = this.$FORM.find('label[for="finish-mat"]');

			this.$BTN_SELECT = this.$FORM.find('.select-btn');
			this.$BTN_CHANGE = this.$FORM.find('.change-choice-btn');
			this.$BTN_CALCULATE = this.$FORM.find('.calculate-btn');
		},
		addEventListeners: function() {
			this.$BTN_SELECT.click($.proxy(function() {
				setTimeout( this.renderStep2.bind(this), 0 );
			}, this));
			this.$BTN_CHANGE.click($.proxy(function() {
				setTimeout( this.renderStep1.bind(this), 0 );
			}, this));
			this.$BTN_CALCULATE.click($.proxy(function() {
				setTimeout( this.renderStep3.bind(this), 0 );
			}, this));
			this.$METHOD.change($.proxy(function() {
				setTimeout( this.renderMethods.bind(this), 0);
			}, this));
			this.$ENAMEL.change($.proxy(function() {
				setTimeout( this.renderFinishes.bind(this), 0);
			}, this));
		},
		renderStep1: function() {
			this.$CONTAINER_WITH_MATERIALS.hide();
			this.$CONTAINER_WITHOUT_MATERIALS.hide();

			this.$CONTAINER_DISCOUNT.show();
		},
		renderStep2: function() {
			var hasDiscount = this.getRadioBtnVal(this.$WITH_DISCOUNT);

			this.$CONTAINER_DISCOUNT.hide();

			if (hasDiscount == 'TAK') {
				this.$CONTAINER_WITH_MATERIALS.show();
			} else {
				this.$CONTAINER_WITHOUT_MATERIALS.show();
			}
		},
		renderStep3: function() {
			this.validateForm();
		},
		renderMethods: function() {
			var methodVal = this.getRadioBtnVal(this.$METHOD);

			if (methodVal == 'lakier') {
				this.$CONTAINER_ENAMEL.show();
				this.$CONTAINER_FINISH.show();
			} else {
				this.$CONTAINER_ENAMEL.hide();
				this.$CONTAINER_FINISH.hide();
			}
		},
		renderFinishes: function() {
			var methodVal = this.getRadioBtnVal(this.$METHOD);
			var enamelVal = this.getRadioBtnVal(this.$ENAMEL);

			if (methodVal == 'lakier') {
				if (enamelVal == '1-składnikowy') {
					this.uncheckRadios(this.$FINISH_MAT_INPUT);
					this.$FINISH_MAT_LABEL.hide();
				} else {
					this.$FINISH_MAT_LABEL.show();
				}
			}
		},
		isChecked: function($el) {
			var $output = false;

			$el.each(function() {
				if ($(this).is(':checked'))
					$output = true;
			});

			return $output;
		},
		getRadioBtnVal: function($el) {
			var $output;

			$el.each(function() {
				if ($(this).is(':checked'))
					$output = $(this).val();
			});

			return $output ? $output : undefined;
		},
		validateForm: function() {
			var hasError = false;

			this.clearErrors();
			this.$BTN_CALCULATE.attr("disabled","disabled");
			$('body').addClass('wait');

			if ( this.hasVal(this.$FLOOR_SQM) && this.hasVal(this.$ROOM_NUMBER) && this.hasVal(this.$WITH_DISCOUNT) ) {
				if ( this.getRadioBtnVal(this.$WITH_DISCOUNT) == 'TAK' ) {
					if ( this.hasVal(this.$WOOD) && this.hasVal(this.$METHOD) ) {
						if ( this.getRadioBtnVal(this.$METHOD) === 'lakier' ) {
							if ( this.hasVal(this.$ENAMEL) && this.hasVal(this.$FINISH) ) {
							} else hasError = true;
						}
					} else hasError = true;
				}
			} else hasError = true;


			if (hasError) {
				this.displayError();
			} else {
				this.calculateForm();
			}

			this.$BTN_CALCULATE.removeAttr("disabled");
			$('body').removeClass('wait');
		},
		hasVal: function($el) {
			var val = $el.length > 1 ? this.getRadioBtnVal($el) : $el.val();

			if (val === false || val === undefined || val === '') {
				return false;
			} else {
				return true;
			}
		},
		clearErrors: function() {
			this.$FORM.find('.error').animate({opacity: 0}, 500).remove();
		},
		displayError: function() {
			this.$BTN_CALCULATE.after('<p class=\"error\">Uzupełnij wszystkie pola!</p>');
			this.$FORM.find('.error').animate({opacity: 1}, 500)
		},
		calculateForm: function() {
			if ( this.getRadioBtnVal(this.$WITH_DISCOUNT) != 'TAK' ) {
				this.uncheckRadios(this.$WOOD);
				this.uncheckRadios(this.$METHOD);
				this.uncheckRadios(this.$ENAMEL);
				this.uncheckRadios(this.$FINISH);
			} else if ( this.getRadioBtnVal(this.$METHOD) === 'olej' ) {
				this.uncheckRadios(this.$ENAMEL);
				this.uncheckRadios(this.$FINISH);
			} else if ( this.getRadioBtnVal(this.$METHOD) === 'lakier' && this.getRadioBtnVal(this.$ENAMEL) === '1-składnikowy' ) {
				this.uncheckRadios(this.$FINISH_MAT_INPUT);
			}

			this.$FORM.submit();
		},
		uncheckRadios: function($el) {
			$el.prop('checked', false);
		},
	}

	$(document).ready(function () {
		Form.init();
	});

})(jQuery);
