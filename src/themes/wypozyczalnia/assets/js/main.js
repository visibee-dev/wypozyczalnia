var WYPOZYCZALNIA = {
	API: {}
};


/**
 * Submit dostawa form
 */
(function($)
{
	$(document).ready(function() {
		var $dostawa = $('.reservation__step-dostawa');
		var $submit = $dostawa.find('a.next');
		var $form = $dostawa.find('form');

		$submit.click(function(event) {
			event.preventDefault();
			$form.submit();
		});

	});
})(jQuery);


/**
 * Submit rezerwacja form
 */
(function($)
{
	$(document).ready(function() {
		var $rezerwacja = $('.reservation__step-rezerwacja');
		var $submit = $rezerwacja.find('a.next');
		var $form = $rezerwacja.find('form');

		$submit.click(function(event) {
			event.preventDefault();
			validateForm($form);
		});

		function validateForm($form) {
			var inputsToValidate = [
				'input[name="date"]:checked'
			];
			var isValid = true;

			$form.find('.error').remove();

			for (var i = 0; i < inputsToValidate.length; i++) {
				var selector = inputsToValidate[i];

				if ( !$(selector).val() ) {
					isValid = false;
				}
			}

			if (isValid) {
				$form.submit();
			} else {
				displayEror();
			}


			function displayEror() {
				$form.append('<p class=\"error\">Uzupełnij wszystkie pola!</p>');
				$form.find('.error').animate({opacity: 1}, 500);
			}
		}
	});
})(jQuery);


/**
 * Init datepicker
 */
(function($)
{

	$(document).ready(function() {
		$('.datepicker').datepicker();

		$('#custom-date-input').focus(function() {
			$('#custom-date-radio').prop('checked', true);
		});

		$btn = $('#show-custom-date');
		$customDate = $('.custom-date-input-container');

		$btn.click(function(event) {
			event.preventDefault();
			$customDate.show();
		})

		$.datepicker.regional['pl'] = {
			closeText: "Zamknij",
			prevText: "&#x3C;Poprzedni",
			nextText: "Następny&#x3E;",
			currentText: "Dziś",
			monthNames: [ "styczeń","luty","marzec","kwiecień","maj","czerwiec",
			"lipiec","sierpień","wrzesień","październik","listopad","grudzień" ],
			monthNamesShort: [ "sty","lut","mar","kwi","maj","cze",
			"lip","sie","wrz","paź","lis","gru" ],
			dayNames: [ "niedziela","poniedziałek","wtorek","środa","czwartek","piątek","sobota" ],
			dayNamesShort: [ "nd","pon","wt","śr","czw","pt","sob" ],
			dayNamesMin: [ "nd","pn","wt","śr","cz","pt","sb" ],
			weekHeader: "Tydz",
			dateFormat: "D dd M yy",
			// dateFormat: "yy-mm-dd",
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ""
		};
 		$.datepicker.setDefaults($.datepicker.regional['pl']);
	});

})(jQuery);



/**
 * Show notif
 */
(function($)
{
	$(document).ready(function() {
		$notifs = $('.notif');

		$notifs.click(function() {
			$title = $(this).data('ntitle');
			$content = $(this).data('ncontent');

			displayNotif($title, $content);
		});

		function displayNotif($title, $content) {
			$html = '<div class=\"notif-popup\">' + 
				'<div class=\"wrap\">' +
					'<div class=\"notif-content\">';
					if ($title)
						$html += '<h3>' + $title + '</h3>';
					if ($content)
						$html += '<p>' + $content + '</p>';
					$html += '<a class=\"notif-close\"></a>' +
					'</div>' +
				'</div>' +
			'</div>';

			$('body').append($html);

			$popup = $('.notif-popup');
			$close = $popup.find('.notif-close');

			$close.click(function() {
				$popup.remove();
			});
		}

	});
})(jQuery);


/**
 * Toggle product images
 */
(function($)
{
	$(document).ready(function() {
		var $imgLinks = $('.woocommerce .images .zoom');
		var $imgContainer = $('.woocommerce .woocommerce-main-image');
		var $imgContainerWidth = $imgContainer.width();


		$imgLinks.click(function(event) {
			event.preventDefault();

			insertImg(getSrc($(this)))

		});

		function getSrc($element) {
			var srcset = $element.find('img').attr('srcset').split(', ');
			var src = $element.find('img').attr('src');
			var minSize = Infinity;
			var maxSize = 0;
			var maxSizeSrc;

			for (var i = 0; i < srcset.length; i++) {
				var item = srcset[i];
				var img = item.split(' ');
				var url = img[0];
				var size = img[1];

				size = size.replace('\D', '');
				size = parseInt(size, 10);

				if ((size >= $imgContainerWidth) && (size < minSize)) {
					minSize = size;
					src = url;
				}

				if (size > maxSize) {
					maxSize = size;
					maxSizeSrc = url;
				}
			}

			if (maxSize && maxSize < $imgContainerWidth) {
				src = maxSizeSrc;
			}

			return src;
		}

		function insertImg(src) {
			var img = '<img src="' + src + '">';

			$imgContainer.html(img);
		}
	});
})(jQuery);


/**
 * Scale proportionaly all the iframes
 */
(function($)
{
	$(document).ready(function() {
		var $iframes = $('iframe');

		setAspectRatios();
		rescaleIframes();

		$(window).resize(function() {
			rescaleIframes()}
		);

		function setAspectRatios() {
			$iframes.each(function() {
				var height = $(this).height();
				var width = $(this).width();
				var aspectRatio = height / width;

				$(this).data('aspectRatio', aspectRatio);
			});
		}

		function rescaleIframes() {
			$iframes.each(function() {
				var width = $(this).width();
				var aspectRatio = $(this).data('aspectRatio');
				var newHeight, newWidth;

				$(this).width('100%');

				newHeight = $(this).width();
				newWidth = Math.round(newHeight * aspectRatio);

				$(this).height(newWidth);
			});
		}
	});
})(jQuery);

/**
 * Header Nav Menu toggle
 */
(function($)
{	
	$(document).ready(function() {
		var $btnToggle = $('#header-nav-toggle');
		var $btnClose = $('#toggle-menu-close, .toggle-menu .wrap-bg');
		var $menu = $('.toggle-menu');

		$btnToggle.click(function() {
			$menu.toggleClass('is-active');
		});

		$btnClose.click(function() {
			$menu.removeClass('is-active');
		})
	});
	
})(jQuery);

/**
 * Funkcja wyświetlająca odpowiedni format obrazka w zależności od ekranu
 */
(function($)
{
	$(document).ready(function() {
		// Vars
		var sections = $('[data-background]');
		var cachedScreenWidth = 0;

		/**
		 * [getMatchedImageSize description]
		 * @return {[type]} [description]
		 */
		var getMatchedImageSize = function(val) {
			var split = val.split('-');
			var postfix = '-'+split[split.length - 1];

			return val.replace(postfix, '');
		};

		/**
		 * [getMatchedScreenWidth description]
		 * @return {[type]} [description]
		 */
		var getMatchedScreenWidth = function() {
			var breakpoints = SITE.backgrounds.breakpoints;
			var matchedWidth = breakpoints[breakpoints.length - 1];

			$.each(breakpoints, function(i, formatWidth)
		    {
				if( formatWidth >= $(window).width() )
				{
					matchedWidth = formatWidth;
					return false;
				}
			});

			return matchedWidth;
		};

		/**
		 * [reloadBackgroundsFormats description]
		 * @return {[type]} [description]
		 */
		var reloadBackgroundsFormats = function() {
			var matchedWidth = getMatchedScreenWidth();
			if( cachedScreenWidth == matchedWidth ) { return; }

			// Zmiana cache formatu
			cachedScreenWidth = matchedWidth;

			sections.each(function()
			{
				var section = $(this);
				var background = section.data('background');
				var matchedImageSize = '';
				var url = background.url;

				$.each(background.sizes, function(key, val)
				{
					if( val == matchedWidth )
					{
						matchedImageSize = getMatchedImageSize( key );

						if( typeof background.sizes[matchedImageSize] != 'undefined' )
						{
							url = background.sizes[matchedImageSize];
							return false;
						}
					}
				});

				section.css({
					backgroundImage: 'url('+url+')'
				});
			});
		};

		/**
		 * [onResize description]
		 * @return {[type]} [description]
		 */
		var onResize = function() {
			reloadBackgroundsFormats();
		};

		// Init
		onResize();

		// Events
		$(window).on('resize', onResize);
	});
})(jQuery);

//# sourceMappingURL=main.js.map

//# sourceMappingURL=main.js.map
