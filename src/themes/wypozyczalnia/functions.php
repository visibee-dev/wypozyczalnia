<?php

$theme = array(
	'name' => 'Wypożyczalnia',
	'version' => '1.0.3'
);

include_once( 'functions/utils.php' );
include_once( 'functions/api.php' );

include_once( 'functions/theme.php' );
include_once( 'functions/filters.php' );
include_once( 'functions/shortcodes.php' );

?>