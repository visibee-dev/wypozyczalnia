<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charser'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri() . '/img/apple-touch-icon.png'; ?>">
        <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri() . '/img/favicon-32x32.png'; ?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri() . '/img/favicon-16x16.png'; ?>" sizes="16x16">
        <link rel="manifest" href="<?php echo get_stylesheet_directory_uri() . '/img/manifest.json'; ?>">
        <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri() . '/img/safari-pinned-tab.svg'; ?>" color="#c13538">
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() . '/img/favicon.ico'; ?>">
        <meta name="msapplication-config" content="<?php echo get_stylesheet_directory_uri() . '/img/browserconfig.xml'; ?>">
        <meta name="theme-color" content="#ffffff">

        <?php wp_head(); ?>
    </head>
    <body <?php echo body_class(); ?>>

    <?php
	get_component('common/repeater-options', array(
		'type' => 'header',
	));
	?>

	<?php get_component('common/header'); ?>