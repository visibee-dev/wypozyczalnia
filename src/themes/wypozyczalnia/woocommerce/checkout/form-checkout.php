<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="row">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col-xs-12  col-sm-8 col-sm-offset-2" id="customer_details">
			<?php do_action( 'woocommerce_checkout_billing' ); ?>

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
		</div>

	<?php endif; ?>

	</div>

	<div class="row podsumowanie-row">

		<h1 class="col-xs-12"><?php _e( 'Your order', 'woocommerce' ); ?></h1>

		<div class="col-xs-12 col-sm-6">

			<?php if (isset($_SESSION['calc'])) : ?>

			<div class="rezerwacja">

				<?php $calc = $_SESSION['calc']; ?>

				<h3>Rezerwacja</h3>

				<div class="white-box">

					Rezerwacja cykliniarki dla lokalizacji Warszawa

					<p>od <strong><?php echo $calc->get_polish_date($calc->delivery['start_date']); ?></strong><br>
					do <strong><?php echo $calc->get_polish_date($calc->delivery['end_date']); ?></strong><br>
					łącznie: <strong><?php echo $calc->get_rental_length(); ?></strong>
					</p>

				</div>

			</div>

			<?php endif; ?>



			<?php do_action( 'woocommerce_checkout_shipping' ); ?>
		</div>

		<div class="col-xs-12 col-sm-6">

			<h3 id="order_review_heading">Podsumowanie</h3>

			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order white-box">

				<?php if (isset($calc)) : ?>

					<?php $calc->the_edit_form(false); ?>


				<?php endif; ?>

				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>

			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

		</div>

	</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
