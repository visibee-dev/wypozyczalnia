<?php

function custom_theme_setup() {
	global $theme;

	// Set timezone to Warsaw
	date_default_timezone_set('Europe/Warsaw');

	// Register nav menus
	register_nav_menu( 'header-nav', __( 'Header Menu', $theme['name'] ) );
	register_nav_menu( 'footer-nav', __( 'Footer Menu', $theme['name'] ) );

	// Add thumbnail support
	add_theme_support( 'post-thumbnails' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style( 'editor-style-shared' );

	// Add image sizes
	add_image_size('section-background-xs', 768, 9999, FALSE); // @2x:  x , @1x:  x
	add_image_size('section-background-sm', 992, 9999, FALSE); // @2x:  x , @1x:  x
	add_image_size('section-background-md', 1200, 9999, FALSE); // @2x:  x , @1x:  x
	add_image_size('section-background-lg', 1600, 9999, FALSE); // @2x:  x , @1x:  x
	add_image_size('section-background-xxl', 1920, 9999, FALSE); // @2x:  x , @1x:  x

	// Add translations support
	load_theme_textdomain( $theme['name'], get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );


function get_site_config() {
	return array(
		'backgrounds' => array(
			'breakpoints' => array(
				'xs' => 768,
				'sm' => 992,
				'md' => 1200,
				'lg' => 1600,
				'xl' => 1920
			),
		),
	);
}


function enqueue_scripts_and_styles() {
	global $theme;

	wp_enqueue_style( 'main', get_stylesheet_uri(), array(), $theme['version'] );
	wp_enqueue_style( 'font-lato', 'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700&amp;subset=latin-ext', $theme['version']);
	wp_enqueue_style( 'jquery-ui', './../node_modules/jquery-ui-dist/jquery-ui.min.css', $theme['version'] );


	wp_enqueue_script( 'main-js', get_stylesheet_directory_uri().'/assets/js/main.js', array( 'jquery' ), $theme['version'], true );
	wp_enqueue_script( 'calc-js', get_stylesheet_directory_uri().'/assets/js/calc.js', array( 'jquery' ), $theme['version'], true );
	wp_enqueue_script( 'jquery-ui', './../node_modules/jquery-ui-dist/jquery-ui.min.js', array( 'jquery' ), $theme['version'], true );
	wp_enqueue_script( 'slick-carousel', './../node_modules/slick-carousel/slick/slick.min.js', array( 'jquery' ), $theme['version'], true );
	wp_localize_script( 'main-js', 'SITE', get_site_config() );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_and_styles' );


if ( function_exists( 'acf_add_options_page' ) ) {

	$parent = acf_add_options_page( array(
		'page_title' => 'Wypożyczalnia Options',
		'menu_title' => 'Wypożyczalnia',
		'menu_slug' => 'wypozyczalnia',
		'icon_url' => 'dashicons-store',
		'redirect' => true
	) );

	acf_add_options_sub_page( array(
		'page_title' => 'Haeder Settings',
		'menu_title' => 'Header',
		'menu_slug' => $parent['menu_slug'] . '-header',
		'parent_slug' => $parent['menu_slug']
	) );

	acf_add_options_sub_page( array(
		'page_title' => 'Footer Settings',
		'menu_title' => 'Footer',
		'menu_slug' => $parent['menu_slug'] . '-footer',
		'parent_slug' => $parent['menu_slug']
	) );

	acf_add_options_sub_page( array(
		'page_title' => 'FAQ Settings',
		'menu_title' => 'FAQ',
		'menu_slug' => $parent['menu_slug'] . '-faq',
		'parent_slug' => $parent['menu_slug']
	) );
}

?>