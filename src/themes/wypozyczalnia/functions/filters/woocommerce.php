<?php

// Removes Woo classes from body...
add_filter( 'body_class', function( $classes ) {
	$remove_classes = array(
		'woocommerce',
		'woocommerce-page'
	);
	$classes = array_diff($classes, $remove_classes);

	return $classes;
} );

// ...and adding them intro Woo Container
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
function my_theme_wrapper_start() {
	echo
	'<section id="woocommerce woocommerce-page" class="woocommerce section section--white">'.
		'<div class="container">'.
			'<div class="row">';
}
function my_theme_wrapper_end() {
	echo
			'</div>'.
		'</div>'.
	'</section>';
}

// Remove Woo styles one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

// Let Woo know, that theme is configured
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Modify Woo Actions
// Single Product Summary

// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'mywoo_get_description', 7 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );


// Products Listing

// add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_after_shop_loop_item', 'mywoo_buttons_div_open', 6 );
add_action( 'woocommerce_after_shop_loop_item', 'mywoo_more_info_link', 7 );
// add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'mywoo_buttons_div_close', 15 );


// Display all products on archive page
add_action( 'pre_get_posts', 'mywoo_custom_query' );
function mywoo_custom_query( $query ) {
	if( !is_admin() && $query->is_main_query() && is_post_type_archive('product') ) {
		$query->set('posts_per_page', '-1');
	}
}



function mywoo_jednostka_miary() {
	global $product;
	$jednostka = $product->get_attribute( 'jednostka-miary' );

	if ($jednostka && $jednostka !== '')
		return "<span class='jednostka'> / " . $jednostka . "</span>";
}

function mywoo_pre_price() {
	global $product;
	$pre_price = $product->get_attribute( 'pre-price' );
	if ($pre_price && $pre_price !== '')
		return "<span class='pre-price'>" . $pre_price . " " . "</span>";
}

function mywoo_get_description() {
	wc_get_template( 'single-product/tabs/description.php' );
}

function mywoo_buttons_div_open() {
	echo '<div class="buttons">';
}

function mywoo_more_info_link() {
	if (is_archive()) {
		echo '<a href="'.get_permalink().'" class="button more_info_button">Szczegóły</a>';
	}
}

function mywoo_buttons_div_close() {
	echo '</div>';
}

function mywoo_the_product_thumbnail($post) {
	$image_size = apply_filters( 'single_product_archive_thumbnail_size', '' );

	if ( has_post_thumbnail($post) ) {
		$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
		echo get_the_post_thumbnail( $post, $image_size, array(
			'title' => $props['title'],
			'alt' => $props['alt'],
		) );
	} elseif ( wc_placeholder_img_src() ) {
		echo wc_placeholder_img( $image_size );
	}
}



// Adds subtitle info to product name in cart
add_filter('woocommerce_cart_item_name', 'mywoo_add_subtitle_to_item_name', 1, 3);
function mywoo_add_subtitle_to_item_name($product_name, $cart_item, $cart_item_key ) {
	$product_id = $cart_item['product_id'];

	if ($subtitle = get_field('subtitle', $product_id)) {
		$product_name .= "<p class='subtitle'>" . $subtitle . "</p>";
	}

	return $product_name;
}

add_filter('woocommerce_cart_item_quantity', 'mywoo_remove_quantity_if_not_important', 10, 3);
function mywoo_remove_quantity_if_not_important($product_quantity, $cart_item_key, $cart_item) {
	$product_id = intval($cart_item['product_id']);
	$limited_ids = get_field('limited_ids', 'option');

	if ( !in_array($product_id, $limited_ids) ){
		return $product_quantity;
	} else {
		return '';
	}
}

add_filter( 'woocommerce_cart_item_remove_link', 'mywoo_remove_delete_btn_if_not_important', 10, 2 );
function mywoo_remove_delete_btn_if_not_important($sprintf, $cart_item_key) {
	preg_match('/data-product_id="(\d+)/', $sprintf, $matches);

	$product_id = intval($matches[1]);
	$limited_ids = get_field('limited_ids', 'option');

	if ( !in_array($product_id, $limited_ids) ) {
		return $sprintf;
	} else {
		return '';
	}
};

// Adds limited class in cart
add_filter('woocommerce_cart_item_class', 'mywoo_add_product_name_class', 10, 3);
function mywoo_add_product_name_class($class, $cart_item, $cart_item_key) {
	$product_id = intval($cart_item['product_id']);
	$limited_ids = get_field('limited_ids', 'option');

	if ( in_array($product_id, $limited_ids) ) {
		$class .= ' cart_item--limited';
	}

	return $class;
}

add_filter('woocommerce_get_price_html', 'mywoo_show_discounted_rental_price_on_product_page', 10, 3);
function mywoo_show_discounted_rental_price_on_product_page( $price_html, $product ) {
	$product_id = intval($product->id);
	$rental_id = intval( get_field('rental_id', 'option') );

	if ($rental_id && $product_id === $rental_id) {
		$product_price = intval($product->price);
		$discount = intval( get_field('rental_discount', 'option') );
		$from_price = $product_price * ( 1 - ($discount / 100) );

		$price_html = str_replace($product_price, $from_price, $price_html);
	}

    return $price_html;
};

add_filter('woocommerce_cart_item_subtotal', 'mywoo_display_discounted_price', 10, 3);
function mywoo_display_discounted_price( $price_html, $cart_item, $cart_item_key ) {
	$rental_id = intval( get_field('rental_id', 'option') );
	$product_id = intval( $cart_item['product_id'] );

	if ($rental_id === $product_id) {
		if ( calc_isset() ) {
			$calc = $_SESSION['calc'];

			if ($calc->is_discounted()) {
				$product_price = $cart_item['data']->price;
				$discount = 1 - ( intval( get_field('rental_discount', 'option') ) / 100);
				$un_discount = 1 / $discount;
				$price_before_discount = $product_price * $un_discount;

				$price_html = "<del>" . $price_before_discount . ",00 zł</del>" . $price_html;
			}
		}

	}

	return $price_html;
};

add_filter('woocommerce_checkout_login_message', 'filter_woocommerce_checkout_login_message', 10, 1);
function filter_woocommerce_checkout_login_message($msg) {
    $msg = "Wypełnij poniższy formularz";
    return $msg;
};

add_filter('woocommerce_checkout_fields', 'filter_woocommerce_checkout_fields', 10, 2);
function filter_woocommerce_checkout_fields( $fields ) {

    unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_state']);

	unset($fields['shipping']['billing_company']);
	unset($fields['shipping']['billing_country']);
	unset($fields['shipping']['billing_address_2']);
	unset($fields['shipping']['billing_state']);

	foreach ($fields as $key_section => $sections) {
		foreach ($sections as $key_field => $field) {

			if ( !isset($field['placeholder']) ) {
				$fields[$key_section][$key_field]['placeholder'] = $field['label'];
			}

			$fields[$key_section][$key_field]['required'] = true;
			$fields[$key_section][$key_field]['class'][] = 'no-label';
		}
	}

	unset($fields['order']['order_comments']['placeholder']);
	$fields['order']['order_comments']['required'] = false;
	$fields['billing']['billing_address_1']['placeholder'] = 'Ulica, nr domu, nr lokalu';
	$fields['shipping']['billing_address_1']['placeholder'] = 'Ulica, nr domu, nr lokalu';

    return $fields;
};

add_filter('woocommerce_checkout_fields', 'reorder_fields', 11);
function reorder_fields($fields) {
	$order = array(
		'first_name',
		'last_name',
		'email',
		'phone',
		'address_1',
		'postcode',
		'city'
	);
	foreach($order as $field) {
		if (isset($fields['billing']['billing_' . $field])) {
			$billing_fields['billing_' . $field] = $fields['billing']['billing_' . $field];
		}
		if (isset($fields['shipping']['shipping_' . $field])) {
			$shipping_fields['shipping_' . $field] = $fields['shipping']['shipping_' . $field];
		}
	}

	$fields['billing'] = $billing_fields;
	$fields['shipping'] = $shipping_fields;

	return $fields;
}

add_filter('woocommerce_checkout_fields', 'add_custom_checkboxes', 12);
function add_custom_checkboxes($fields) {
	if (have_rows('checkboxes', 'option')) :

		while (have_rows('checkboxes', 'option')) : the_row();

			$id = get_sub_field('id');
			$required = get_sub_field('required');
			$label = $required ? '<span class="required-star">*</span> ' . get_sub_field('label') : get_sub_field('label');

			$unique_name = 'billing_checkbox_' . $id;

			$fields['billing'][$unique_name] = array(
				'type' => 'checkbox',
				'label' => $label,
				'required' => $required
			);

			$fields['billing'][$unique_name]['class'][] = 'form-row';
			$fields['billing'][$unique_name]['class'][] = 'form-row-checkbox';

		endwhile;

	endif;

	return $fields;
}

add_action('woocommerce_checkout_process', 'process_custom_checkboxes');
function process_custom_checkboxes() {
    if (have_rows('checkboxes', 'option')) :

		while (have_rows('checkboxes', 'option')) : the_row();

			$id = get_sub_field('id');
			$required = get_sub_field('required');
			$unique_name = 'billing_checkbox_' . $id;
			$name = get_sub_field('name');

			if ($required && !$_POST[$unique_name]) {
				wc_add_notice('Pole <strong>'. $name . '</strong> jest wymagane.', 'error');
			}

		endwhile;

	endif;
}

add_action('woocommerce_checkout_update_order_meta', 'add_custom_checkboxes_to_order_meta');
function add_custom_checkboxes_to_order_meta( $order_id ) {
	if (have_rows('checkboxes', 'option')) :

		while (have_rows('checkboxes', 'option')) : the_row();

			$id = get_sub_field('id');
			$label = get_sub_field('label');

			$unique_name = 'billing_checkbox_' . $id;

			if (!$_POST[$unique_name]) {
				update_post_meta( $order_id, $label, esc_attr($_POST[$unique_name]));
			}

		endwhile;

	endif;
}

add_filter('woocommerce_form_field_checkbox', 'output_proper_checkbox', 10, 4 );
function output_proper_checkbox($field, $key, $args, $value) {
	$css = '';
	$id = $args['id'];
	$label = $args['label'];
	$css_classes = $args['class'];

	foreach ($css_classes as $css_class) {
		$css .= $css_class . ' ';
	}

	$field = '<p class="' . $css . '" id="' . $id . '_field' . '">' .
		'<input type="checkbox" class="input-checkbox" name="' . $id . '" id="' . $id . '" value="1">' .
		'<label for="'. $id .'">' .
			$label .
		'</label>' .
	'</p>';

    return $field;
}

add_action('woocommerce_checkout_update_order_meta', 'add_reservation_details_to_order', 10, 1);
function add_reservation_details_to_order( $order_id ) {
	$order = new WC_Order($order_id);
	$rental_id = intval( get_field('rental_id', 'option') );

	if ( calc_isset() && order_has_product( $order_id, $rental_id ) ) {
		$calc = $_SESSION['calc'];

		$note = $calc->order_note();

		$order->add_order_note( $note );

		unset($_SESSION['calc']);
	}


}

function order_has_product( $order_id, $product_id ){
	$order = new WC_Order( $order_id );
	$items = $order->get_items();

	foreach ( $items as $item ) {
		$item_id = $item['product_id'];
		if ( $item_id == $product_id ) {
			return true;
		}
	}

	return false;
}

add_action('woocommerce_email_order_meta', 'add_reservation_details_to_email', 10, 3);
function add_reservation_details_to_email( $order, $sent_to_admin = true, $plain_text = false ) {

	if ( !$sent_to_admin ) {
		return;
	}

	$notes = array();

	$args = array(
		'post_id' => $order->id,
		'approve' => 'approve',
		'type' => '' // You don't need type as orders get only order notes as comments.
	);

	// Remove the filter excluding the comments from queries
	remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

	// You get all notes for the order /public and private/
	$notes = get_comments( $args );

	// Add the filter again.
	add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

	echo '<h2>';
		_e( 'Order Notes', 'woocommerce' );
	echo '</h2>';
	echo '<ul class="order_notes">';
	if ( $notes ) {
		foreach( $notes as $note ) {
			$note_classes = get_comment_meta( $note->comment_ID, 'is_customer_note', true ) ? array( 'customer-note', 'note' ) : array( 'note' );
			?>
			<li rel="<?php echo absint($note->comment_ID); ?>" class="<?php echo implode(' ', $note_classes); ?>">
				<div class="note_content">
					<?php echo wpautop( wptexturize( wp_kses_post( $note->comment_content ) ) ); ?>
				</div>
			</li>
		<?php
		}
	} else {
		echo '<li class="no-order-comment">There are no order notes</li>';
	}
	echo '</ul>';
}

add_action( 'wp_footer', 'update_cart_with_ajax' );
function update_cart_with_ajax() {
	if (is_cart()) :
	?>
	<script>
		jQuery('div.woocommerce').on('change', '.qty', function(){
			jQuery("[name='update_cart']").trigger("click");
		});
	</script>
	<?php
	endif;
}

add_filter('template_include', 'set_session_cookie_in_templates');
function set_session_cookie_in_templates($template) {
	$page_templates = array(
		'koszyk-page.php',
	);

	$template_parts = explode('/', $template);
	$template_name = $template_parts[count($template_parts) - 1];

	if (in_array($template_name, $page_templates)  && !is_user_logged_in()) {
		global $woocommerce;
		$woocommerce->session->set_customer_session_cookie(true);
	}

	return $template;
}

add_filter( 'woocommerce_email_styles', 'remove_email_styles_for_admin' );
function remove_email_styles_for_admin( $css ) {
	$css .= '#wrapper { padding: 0; }'
		. '#body_content table td { padding: 10px; }'
		. '#body_content table td td { padding: 10px; }'
		. '#body_content table td th { padding: 10px; }'
		. '#header_wrapper { padding: 10px; }'
		. '#template_footer #credit { padding: 10px; }'
		. 'table { width: 100% !important; }';

	return $css;
}

function woo_in_cart($product_id, $quantity = 1) {
	global $woocommerce;

	foreach($woocommerce->cart->get_cart() as $key => $item ) {

		if ( $product_id == $item['product_id'] && $quantity <= $item['quantity'] ) {
			return true;
		}
	}

	return false;
}

function calc_is_sander_in_cart() {
	$sander_id = intval( get_field('rental_id', 'option') );
	if ( woo_in_cart($sander_id) ) {
		return true;
	}

	return false;
}

function calc_isset() {
	return isset($_SESSION['calc']) ? $_SESSION['calc'] : false;
}

function add_deposit_fee() {
	global $woocommerce;

	if ( calc_is_sander_in_cart() && calc_isset() ) {
		$calc = $_SESSION['calc'];

		$deposit_id = get_field('deposit_id', 'option');
		$_deposit = wc_get_product( $deposit_id );

		$surcharge = $_deposit->get_price();

		$woocommerce->cart->add_fee( $_deposit->post->post_title, $surcharge, false, '' );
	}
}
add_action( 'woocommerce_cart_calculate_fees', 'add_deposit_fee' );

function validate_cart_after_being_updated() {
	global $woocommerce;

	if ( !calc_is_sander_in_cart() && is_cart() ) {
		$link = '';

		if ( have_rows('breadcrumbs', 'option') ) {
			the_row();
			$page = get_sub_field('page');
			$link = get_permalink( $page->ID );
		}

		$btn = '<a href="' . $link .'" class="btn btn--fill">zarezerwuj cykliniarkę</a>';
		wc_add_notice( 'Aby dokonać zakupu ' . $btn );
	}
}
add_action( 'woocommerce_cart_updated', 'validate_cart_after_being_updated' );

add_filter( 'woocommerce_account_menu_items', 'custom_woocommerce_account_menu_items' );
function custom_woocommerce_account_menu_items( $items ) {
	if ( isset( $items['downloads'] ) ) unset( $items['downloads'] );
	return $items;
}

add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
	global $woocommerce;

	if ( isset( $_GET['empty-cart'] ) ) {
		$woocommerce->cart->empty_cart();
	}
}

add_filter( 'manage_edit-shop_order_columns', 'shop_order_columns' );
function shop_order_columns( $columns ) {
	$columns = ( is_array( $columns ) ) ? $columns : array();

	if ( isset( $columns['order_date'] ) )
		unset( $columns['order_date'] );
	if ( !isset( $columns['reservation_start'] ) )
		$columns['reservation_start'] = 'Rozpoczęcie najmu';
	if ( !isset( $columns['reservation_end'] ) )
		$columns['reservation_end'] = 'Koniec najmu';

	return $columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'add_reservation_custom_column' );
function add_reservation_custom_column( $column ) {
	global $post, $the_order;

	$has_reservation_meta = get_post_meta( $post->ID, '_reservation_start' ) && get_post_meta( $post->ID, '_reservation_end' );

	if ( !$has_reservation_meta ) {
		$items = $the_order->get_items();

		foreach ( $items as $item ) {
			if ( $item['product_id'] == intval( get_field('rental_id', 'option') ) ) {
				if ( !update_post_meta( $post->ID, '_reservation_start', $item['ebs_start_format'] ) ) {
					add_post_meta( $post->ID, '_reservation_start', $item['ebs_start_format'], true );
				}

				if ( !update_post_meta( $post->ID, '_reservation_end', $item['ebs_end_format'] ) ) {
					add_post_meta( $post->ID, '_reservation_end', $item['ebs_end_format'], true );
				}
			}
		}
	}

	switch ( $column ) {
		case 'reservation_start':
			$start = get_post_meta( $post->ID, '_reservation_start' );
			echo array_shift( $start );
			break;

		case 'reservation_end':
			$end = get_post_meta( $post->ID, '_reservation_end' );
			echo array_shift( $end );
			break;
	}
}

add_filter( 'manage_edit-shop_order_sortable_columns', 'make_reservation_columns_sortable' );
function make_reservation_columns_sortable( $columns ) {
	$columns['reservation_start'] = '_reservation_start';
	$columns['reservation_end'] = '_reservation_end';

	return $columns;
}


add_filter( 'request', 'reservation_start_column_orderby' );
function reservation_start_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'reservation_start' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => '_reservation_start',
			'orderby' => 'meta_value_num'
		) );
	}

	return $vars;
}

add_filter( 'request', 'reservation_end_column_orderby' );
function reservation_end_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'reservation_end' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => 'reservation_end',
			'orderby' => 'meta_value_num'
		) );
	}

	return $vars;
}

?>
