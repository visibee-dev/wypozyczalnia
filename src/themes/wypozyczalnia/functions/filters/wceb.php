<?php

// Change button on Product Listing
add_filter('easy_booking_loop_add_to_cart_link', 'link_to_reservation_form');
function link_to_reservation_form($link) {
	if (have_rows('breadcrumbs', 'option')) : the_row();
		$post_object = get_sub_field('page');
		$url = get_permalink($post_object);
	endif;
	return '<a href="' . $url . '" rel="nofollow" class="button btn btn--primary">Rezerwuj</a>';
}

?>