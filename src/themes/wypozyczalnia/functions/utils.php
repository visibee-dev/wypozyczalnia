<?php

function format_link($link)
{
	if( ! $link) { return ''; }

	return strpos($link, 'https://') !== FALSE ?  $link : 'http://'.str_replace('http://', '', $link);
}


function get_page_template_by_ID($id)
{
	return get_post_meta( $id, '_wp_page_template', true );
}


function get_page_link_by_path($path)
{
	$page = get_page_by_path($path);
	return $page->ID ? get_permalink($page->ID) : "";
}


function get_ID_by_slug($page_slug)
{
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}


function get_page_children_all($pageID)
{
	// Set up the objects needed
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));

	return get_page_children( $pageID, $all_wp_pages );
}


function the_slug($echo=true)
{
	if(is_404())
	{
		$slug = __('Error');
	}
	else
	{
		$slug = basename(get_permalink());
	}
	do_action('before_slug', $slug);
	$slug = apply_filters('slug_filter', $slug);
	if( $echo ) echo $slug;
	do_action('after_slug', $slug);
	return $slug;
}


function get_the_slug()
{
	return the_slug($echo=FALSE);
}


function get_current_user_name()
{
	global $current_user;
	$current_user = wp_get_current_user();
	return $current_user->user_login;
}


function is_ancestor($post_id)
{
    global $wp_query;
    $ancestors = $wp_query->post->ancestors;
    if ( in_array($post_id, $ancestors) ) {
        return true;
    } else {
        return false;
    }
}


function get_nav_menu_id_from_slug($menu_slug)
{
	$locations = get_nav_menu_locations();

	if (isset($locations[$menu_slug]))
	{
	    return $locations[$menu_slug];
	}

	return NULL;
}


function assign_parent_terms($post_id)
{
	global $post;

	if( ! is_object($post) ) { return; }

	if($post->post_type != 'product')
		return $post_id;

	// get all assigned terms
	$terms = wp_get_post_terms($post_id, 'product_cat' );
	foreach($terms as $term){
		while($term->parent != 0 && !has_term( $term->parent, 'product_cat', $post )){
			// move upward until we get to 0 level terms
			wp_set_post_terms($post_id, array($term->parent), 'product_cat', true);
			$term = get_term($term->parent, 'product_cat');
		}
	}
}
add_action('save_post', 'assign_parent_terms');


function pagination() {
	if (!isset($_GET['page'])){
		$_GET['page']=1;
	}
}
// add_action('init', 'pagination');


function strposa($haystack, $needle, $offset=0)
{
    if( ! is_array($needle)) { $needle = array($needle); }
    foreach($needle as $query)
    {
        if(strpos($haystack, $query, $offset) !== false) { return true; } // stop on first true result
    }
    return false;
}


function get_file_ext($filename)
{
	return substr(strrchr($filename, '.'), 1);
}


function get_component( $slug, array $args = array() )
{
	global $wp_query;
	$slugs = array();
	$slugs[] = 'components/'.$slug;
	$slugs[] = 'components/'.$slug.'/index';

	foreach($slugs as $slug)
	{
		$_template_file = locate_template("{$slug}.php", false, false);

		if( $_template_file) { break; }
	}

	extract($args, EXTR_OVERWRITE);

	if( $_template_file )
	{
		require($_template_file);
	}
}


foreach( glob( get_template_directory() . '/components/*' ) as $view )
{
	foreach( glob( $view . '/*' ) as $component )
	{
		$functions_file = $component . '/functions.php';

		if( file_exists( $functions_file ) )
		{
			include( $functions_file );
		}
	}
}


function contains($needle, $haystack) {
    return strpos($haystack, $needle) !== false;
}

add_action('init', 'myStartSession', 1);
function myStartSession() {
	if(!session_id()) {
		session_start();
	}
}
