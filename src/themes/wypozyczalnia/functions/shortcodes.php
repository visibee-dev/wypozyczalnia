<?php

function register_shortcodes() {
	add_shortcode('notif', 'the_notif');
}
add_action( 'init', 'register_shortcodes');
add_filter('the_title', 'do_shortcode');

function the_notif($atts, $content = null) {

	extract(shortcode_atts(array(
		'ntitle' => '',
		'ncontent' => '',
		'nafter' => false
	), $atts));

	$css = $nafter ? 'notif--after' : 'notif--before';

	return "<span class='notif {$css}' data-ntitle='{$ntitle}' data-ncontent='{$ncontent}'>{$content}</span>";
}