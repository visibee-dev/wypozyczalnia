<?php

include_once( 'filters/woocommerce.php' );
include_once( 'filters/wceb.php' );

function custom_wp_title( $title, $sep ) {
	global $paged, $page, $theme;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', $theme['name'] ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'custom_wp_title', 10, 2 );


function callback($buffer) {
	return $buffer;
}


function buffer_start() {
	ob_start("callback");
}
add_action('init', 'buffer_start');


function buffer_end() {
	ob_end_flush();
}
add_action('wp_footer', 'buffer_end');

?>