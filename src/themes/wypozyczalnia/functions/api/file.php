<?php

class File {
	public $name;
	public $url;
	public $title;
	public $icon_url;

	private $file;
	private $icon_extensions = array(
		'jpeg',
		'jpg',
		'pdf',
		'png',
	);

	public function __construct($file) {
		$this->file = $file;

		$this->url = $this->file['url'];
		$this->title = $this->file['title'];
		$this->name = $this->get_name();
		$this->icon_url = $this->get_icon_url();
	}

	private function get_name() {
		return $this->file['title'].".".$this->get_extension();
	}

	private function get_extension() {
		$file_name = $this->file['filename'];
		$file_name = explode(".", $file_name);

		$file_extension = $file_name[ count($file_name) - 1 ];

		return $file_extension;
	}

	private function get_icon_url() {
		$icon_url = get_stylesheet_directory_uri().'/img/mime/';
		$file_subtype = $this->get_mime_subtype();
		

		if (in_array($file_subtype, $this->icon_extensions)) {
			$icon_url .= $file_subtype.'.png';
		} else {
			$icon_url .= 'default'.'.png';
		}

		return $icon_url;
	}

	private function get_mime_subtype() {
		$mime_type = $this->file["mime_type"];
		$mime_type = explode("/", $mime_type);

		$file_subtype = $mime_type[1];

		return $file_subtype;
	}
}

?>