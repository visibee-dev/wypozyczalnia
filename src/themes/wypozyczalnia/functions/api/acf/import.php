<?php

// customize ACF path
add_filter( 'acf/settings/path', 'custom_acf_settings_path') ;
function custom_acf_settings_path( $path ) {

	// update path
	$path = get_stylesheet_directory() . '/api/acf/';

	// return
	return $path;
}

// Customize ACF dir
add_filter( 'acf/settings/dir', 'custom_acf_settings_dir' );
function custom_acf_settings_dir( $dir ) {

	// update path
	$dir = get_stylesheet_directory_uri() . '/api/acf/';

	// return
	return $dir;
}

// Include ACF
include_once( get_stylesheet_directory() . '/api/acf/acf.php' );

?>
