<?php

class Calculator {
// Dzienna wydajność cykliniarki w m2
	private $EFF_1R = 20;		// dla jednego pomieszczenia
	private $EFF_MR = 20;		// dla większej ilości pomieszczeń

// Zmienne dt. ustawiania i konwersji dat
	private $months_en = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
	private $months_pl = array('sty', 'lut', 'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrz', 'paź', 'lis', 'gru');
	private $days_en = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
	private $days_pl = array('pon', 'wt', 'śr', 'czw', 'pt', 'sob', 'nd');

	public $value = array();
	public $rental = array();
	public $materials = array();

	public $materials_to_buy = array();

	public $delivery = array();

	public function __construct() {

		$this->set_values();
		$this->set_rentals();
		$this->set_materials();

		$this->set_materials_to_buy();
		
	}

	public function the_available_dates($from = null, $count = 5) {
		$start = $from ? $from : date('Y-m-d');
		$current_date = date('Y-m-d');
		$current_time = intval( date('G') );
		$after_closing = $current_time < 14 ? false : true;
		$length = $this->rental['length'] * 24 * 60 * 60; // of rent in seconds
		$ONE_DAY = 1 * 24 * 60 * 60; // in seconds

		$displayed = 0;

		if ($length > 0 && $start && isset($this->rental['id'])) {

			$disabled = $this->get_disabled_dates($this->rental['id']);

			echo "<div class='availabilities row flexbox-container flexbox-wrap'>";

			while ($displayed < $count) {
				$end = date('Y-m-d', strtotime($start) + $length);

				$start_pl = $this->get_polish_date($start);
				$start_weekday_pl = explode(' ', $start_pl)[0];

				if (
					ebac_get_product_stock( $this->rental['id'], null, $start, $end) > 0
					&& !in_array($start, $disabled['dates'])
					&& !in_array($start_weekday_pl, $disabled['days'])
					&& (
						( $current_date == $start && !$after_closing)
						|| $current_date != $start
						)
					) {
					$this->the_date_input($start);
					$displayed++;
				}

				$start = date('Y-m-d', strtotime($start) + $ONE_DAY);
			}

			echo "</div>";

		} else {
			get_component('common/steps/error');
		}
	}

	public function get_disabled_dates($product_id) {
	// Get disabled dates from global & product settings
		$ebdd_settings = get_option('ebdida_settings');
		$global_disabled_dates = (array) $ebdd_settings['ebdida_disabled_dates'];
		$product_disabled_dates = get_post_meta( $product_id, '_ebdida_disabled_dates', true );
		if ( empty($product_disabled_dates) ) {
			$product_disabled_dates = array();
		}
		$disabled_dates = array_merge($product_disabled_dates, $global_disabled_dates);

	// Get global settings - the last year available to book
		$easy_booking_settings = get_option('easy_booking_settings');

		$current_year = date('Y');
		$last_year = $easy_booking_settings['easy_booking_max_year'];
		$years = array();
		for ( $i = $current_year; $i <= $last_year; $i++ ) {
			$years[] = $i;
		}

		$output = array(
			'days' => array(),
			'dates' => array()
		);

		foreach ($disabled_dates as $disabled_date) {
			$type = $disabled_date['type'];
			$repeat = ($disabled_date['repeat'] === 'on') ? true : false;
			$disabled = $disabled_date['disabled'];

			if ($type === 'ebdida_date') {
				if ($repeat) {
					$date = explode('-', $disabled);
					$month = $date[1];
					$day = $date[2];

					foreach ($years as $year) {
						$output['dates'][] = $year . '-' . $month . '-' . $day;
					}
				} else {
					$output['dates'][] = $disabled;
				}

			} elseif ($type === 'ebdida_date_range') {
				$start = $disabled[0];
				$end = $disabled[1];
				$dates_in_range = ebdida_get_dates_from_range($start, $end);

				if ($repeat) {
					$mm_dd = array();

					foreach ($dates_in_range as $date_in_range) {
						$date = explode('-', $date_in_range);
						$mm_dd[] = $date[1] . '-' . $date[2];
					}

					foreach ($years as $year) {
						foreach ($mm_dd as $date) 
							$output['dates'][] = $year . '-' . $date;
					}
				}

			} elseif ($type === 'ebdida_day') {
				$day_no = $disabled - 1;
				$day_pl = $this->days_pl[$day_no];

				if (!in_array($day_pl, $output['days'])) {
					$output['days'][] = $day_pl;
				}
			}
		}

		return $output;
	}

	private function the_date_input($date) {
		echo "<div class='availability col-xs-12 col-sm-4'>";
			echo "<input
				type='radio'
				name='date'
				id='{$date}'
				value='{$date}'";
				$this->check_delivery_field('start_date', $date);
			echo ">";
			echo "<label for='{$date}'>" . $this->get_polish_date($date) . "</label>";
		echo "</div>";
	}

	public function set_delivery($start_date, $duration = 1) {
		$DAY = 24 * 60 * 60;
		$start_offset = strtotime($start_date);
		$end_offset = strtotime($start_date) + $duration * $DAY;
		$end_day = date('D', $end_offset);

		if ( $end_day == 'Sun' ) {
			$end_offset += $DAY;
		}

		$this->delivery['start_date'] = $start_date;
		$this->delivery['end_date'] = date('Y-m-d', $end_offset);
	}

	public function check_delivery_field($key, $value) {
		if ( isset($this->delivery[$key]) && $this->delivery[$key] == $value )
			echo "checked";
	}

	public function to_normal_date($date) {
		$date = explode(' ', $date);

		$day = $date[1];

		foreach ($this->months_pl as $key => $value) {
			if ($date[2] === $value) {
				$month = $key;
			}
		}

		if ($month < 10) {
			$month = (string) $month;
			$month = '0' . $month;
		}

		$year = $date[3];

		return $year . '-' . $month . '-' . $day;
	}

	public function get_polish_date($date) {
		$date_en = date('D d M', strtotime($date));
		$date_pl = str_replace($this->months_en, $this->months_pl, $date_en);
		$date_pl = str_replace($this->days_en, $this->days_pl, $date_pl);

		return $date_pl;
	}

	public function set_values() {

		$this->value['rooms'] = isset($_GET['rooms']) ? $_GET['rooms'] : null;
		$this->value['sqm'] = isset($_GET['sqm']) ? $_GET['sqm'] : null;
		$this->value['materials'] = isset($_GET['materials']) ? $_GET['materials'] : null;
		$this->value['wood'] = isset($_GET['wood']) ? $_GET['wood'] : null;
		$this->value['method'] = isset($_GET['method']) ? $_GET['method'] : null;
		$this->value['enamel'] = isset($_GET['enamel']) ? $_GET['enamel'] : null;
		$this->value['finish'] = isset($_GET['finish']) ? $_GET['finish'] : null;

	}

	public function set_rentals() {

		$this->rental['id'] = intval( get_field('rental_id', 'option') );
		$_product = wc_get_product( $this->rental['id'] );

		$this->rental['price'] = $_product->get_regular_price();
		$this->rental['deposit_id'] = get_field('deposit_id', 'option');

		$eff = $this->value['rooms'] > 1 ? $this->EFF_MR : $this->EFF_1R;
		$this->rental['length'] = ceil($this->value['sqm'] / $eff);

		$this->rental['discount'] = get_field('rental_discount', 'option') / 100;

	}

	public function set_materials() {

		$this->materials = array();

		if (have_rows('wydajnosci', 'option')) : while (have_rows('wydajnosci', 'option')) : the_row();

			$data_set = explode(';', get_sub_field('zestaw'));

			$method = isset($data_set[0]) ? $data_set[0] : null;
			$type = isset($data_set[1])? $data_set[1] : null;

			if ($type && isset($data_set[2])) {
				$type .= ';' . $data_set[2];
			}

			$variants = array();

			if (have_rows('produkty')) : while (have_rows('produkty')) : the_row();

				$id = get_sub_field('produkt');
				$_product = wc_get_product( $id );

				$price = $_product->get_regular_price();
				$efficiency = get_sub_field('wydajnosc');
				$name = get_the_title( $id );

				$variants[] = array(
					'name' => $name,
					'id' => $id,
					'price' => $price,
					'efficiency' => $efficiency,
					'ratio' => $price / $efficiency,
				);

			endwhile; endif;

			usort($variants, function($a, $b) {
				return $a['ratio'] - $b['ratio'];
			});

			if ($type)
				$this->materials[$method][$type][] = $variants;
			else
				$this->materials[$method][] = $variants;

		endwhile; endif;
	}

	public function set_materials_to_buy() {

		$this->materials_to_buy = array();

		$chosen_sqm = $this->value['sqm'];
		$chosen_method = $this->value['method'];
		$chosen_subtype = '';
		$has_materials = ($this->value['materials'] === 'TAK') ? true : false;

		if ($chosen_method === 'lakier') {
			$chosen_subtype = $this->value['enamel'] . ';' . $this->value['finish'];
		} elseif ($chosen_method === 'olej') {
			$chosen_subtype = $this->value['wood'];
		}

		foreach ($this->materials as $method => $subarray) {
			if ($method === 'always') {
				foreach ($subarray as $products) {
					$this->add_products($products);
				}
			}
			elseif ($method === 'all' && $has_materials) {
				foreach ($subarray as $products) {
					$this->add_products($products);
				}	
			} elseif ($method === $chosen_method && $has_materials) {
				foreach ($subarray as $subtype => $subarray) {
					if ($subtype === 'all' || $subtype === $chosen_subtype) {
						foreach ($subarray as $products) {
							$this->add_products($products);
						}
					}
				}
			}
		}
	}

	private function add_products($products) {
		$length = count($products);
		$sqm = $this->value['sqm'];

		if ($length === 1) {
			$product = $products[0];
			$this->add_product($product);
		} else {
			$tmp_sqm = $sqm;
			$i = 0;

			while ($i < $length && $tmp_sqm > 0) {
				$product = $products[$i];
				$quantity = 0;

				if ($i === 0) {
					while ($tmp_sqm > $product['efficiency']) {
						$tmp_sqm -= $product['efficiency'];
						$quantity++;
					}
					$this->add_product($product, $quantity);
				} else {
					$previous_product = $products[$i - 1];
					$quantity = ceil($tmp_sqm / $product['efficiency']);

					if ($quantity * $product['price'] > $previous_product['price']) {
						$this->add_product($previous_product, 1);
						$tmp_sqm -= $previous_product['efficiency'];
					} else {
						if ($i !== ($length - 1)) {
							$quantity = floor($tmp_sqm / $product['efficiency']);
						}
						$this->add_product($product, $quantity);
						$tmp_sqm -= $product['efficiency'] * $quantity;
					}
				}

				$i++;
			}

			if ($tmp_sqm > 0) {
				$product = $products[$i - 1];
				$this->add_product($product, 1);
			}
		}
	}

	private function add_product($product, $quantity = false) {
		$quantity = $quantity ? 1 : ceil($this->value['sqm'] / $product['efficiency']);
		$id = $product['id'];

		if (!isset($this->materials_to_buy[$id])) {
			$this->materials_to_buy[$id] = array();
		}

		if (!isset($this->materials_to_buy[$id]['quantity'])) {
			$this->materials_to_buy[$id]['quantity'] = 0;
		}

		$this->materials_to_buy[$id]['quantity'] += $quantity;
		$this->materials_to_buy[$id]['price'] = $product['price'];
	}

	public function is_valid() {

		if ( $this->value['sqm'] && $this->value['rooms'] && $this->value['materials'] ) {
			if ( $this->value['materials'] === 'TAK' ) {
				if ( $this->value['wood'] && $this->value['method'] ) {
					if ( $this->value['method'] === 'lakier' ) {
						if ( $this->value['enamel'] && $this->value['finish'] ) {
							return true;
						} else return false;
					} else return true;
				} else return false;
			} else return true;
		} else return false;

	}

	public function the_value($arg, $value = NULL) {
		$radios = array('materials', 'wood', 'method', 'enamel', 'finish');

		if ( $this->value[$arg] ) {
			if ( in_array($arg, $radios) ) {
				if ( $value == $this->value[$arg] )
					echo 'checked';
			} else {
				echo 'value="' . $this->value[$arg] . '"';
			}
		}
	}

	public function the_costs() {

		$this->the_cost_labels();
	}

	private function the_cost_labels() {
		echo "<div class='costs'>";

			$this->the_rental_label();

			$this->the_cost_label('Rezerwacja na:', $this->get_rental_length());

			if ($this->value['materials'] === 'TAK') {

				$this->the_cost_label('Cena materiałów:', $this->get_materials_cost(), array(
					'title' => 'Cena materiałów',
					'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam lobortis, purus nec blandit consequat, diam mauris dapibus libero, vitae aliquam sem purus a felis.',
				));

			}

			// $this->the_cost_label('Koszt dostawy:', $this->price_format(220));

			$this->the_cost_label('Koszt wynajmu:', $this->get_rental_price(), array(
				'title' => 'Koszt wynajmu',
				'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam lobortis, purus nec blandit consequat, diam mauris dapibus libero, vitae aliquam sem purus a felis.',
			));

		echo "</div>";
	}

	private function the_rental_label() {
		$img = get_the_post_thumbnail($this->rental['id']);

		echo '<div class="cost-label cost-label--rental">';

			echo '<div class="col-xs-8 col-xs-offset-2 col-sm-offset-0 col-sm-4">' . $img . '</div>';

			echo '<div class="description-1 col-xs-12 col-sm-4">' . get_field('rental_description_1', 'option') . '</div>';

			echo '<div class="description-2 col-xs-12 col-sm-4">' . get_field('rental_description_2', 'option') . '</div>';

		echo '</div>';
		$img;
	}

	private function the_cost_label($label, $value, $notif = NULL) {
		$notif_data = $this->get_notif_data($notif);
		$notif_class = $this->get_notif_class($notif, true);

		echo "<div class='cost-label {$notif_class}' {$notif_data}>";

			echo "<label class='name col-xs-12 col-sm-6'>{$label}</label>";

			echo "<span class='value col-xs-12 col-sm-6'>{$value}</span>";

		echo "</div>";
	}

	public function get_rental_length() {
		if ($this->rental['length'] > 1)
			return $this->rental['length'] . ' dni';
		else
			return $this->rental['length'] . ' dzień';
	}

	private function get_materials_cost() {
		$cost = 0;

		foreach ($this->materials_to_buy as $material) {
			$cost += $material['price'] * $material['quantity'];
		}

		return $this->price_format($cost);
	}

	private function get_rental_price() {
		$normal_price = $this->rental['price'] * $this->rental['length'];
		if ($this->value['materials'] === 'TAK') {
			$discounted_price = $normal_price * (1 - $this->rental['discount']);
			return '<del>' . $this->price_format($normal_price) . '</del> <ins>' . $this->price_format($discounted_price) . '</ins>';
		}
		else {
			return $this->price_format($normal_price);
		}
	}

	public function is_discounted() {
		return ($this->value['materials'] === 'TAK') ? true : false;
	}

	private function get_notif_data($notif) {
		$notif_data = '';

		if ($notif) {

			if ($notif['content'])
				$notif_data .= " data-ncontent='{$notif['content']}'";
			if ($notif['title'])
				$notif_data .= " data-ntitle='{$notif['title']}'";
		}

		return $notif_data;
	}

	private function get_notif_class($notif, $after) {
		$notif_class = '';

		if ($notif) {
			$notif_class = 'notif';
			if ($after)
				$notif_class .= ' notif--after';
		}

		return $notif_class;
	}

	public function the_edit_form($edit = true) {

		echo "<form action='" . get_permalink() . "' method='get' class='edit'>";

			echo "Wycena obliczona na podstawie podanych parametrów:";

			echo "<div class='labels'>";
		
			$this->the_edit_fields();

			echo "</div>";


		if ($edit) {

			$this->the_hidden_input('edit_mode', 1);

			$this->the_edit_btn();

		}

		echo "</form>";
	}

	private function the_edit_fields() {

		foreach ($this->value as $key => $value) {

			if ($value && $key) {
				$this->the_label($key, $value);
				$this->the_hidden_input($key, $value);
			}

		}

	}

	private function the_label($key, $value) {

		echo "<label class='block' for='input-{$key}'>" . $this->get_label($key) . ": " . "<span class='value'>" . $value . "</span>" . "</label>";
	}

	private function the_hidden_input($key, $value) {

		echo "<input type='hidden' name='{$key}' value='{$value}' id='input-{$key}'>";
	}

	private function the_edit_btn($text = "Edytuj parametry") {

		echo "<input type='submit' value='{$text}' class='edit-btn'>" . " " . "<i class='icon olowek'></i>";
	}

	public function order_note() {
		$note = '';

		foreach ($this->value as $key => $value) {

			if ($value && $key) {
				$note .= $this->get_label($key) . ': ' . '<strong>' . $value . '</strong>' . '<br>';
			}

		}

		return $note;
	}

	private function get_label($key) {

		switch ($key) {
			case 'sqm':
				return 'Metraż [m<sup>2</sup>]';
				break;
			case 'rooms':
				return 'Pomieszczenia';
				break;
			case 'materials':
				return 'Komplet materiałów (rabat 50% na cenę wynajmu)';
				break;
			case 'wood':
				return 'Rodzaj drewna';
				break;
			case 'method':
				return 'Metoda';
				break;
			case 'enamel':
				return 'Lakier';
				break;
			case 'finish':
				return 'Wykończenie';
				break;
		}

	}

	public function price_format($price) {
		return number_format($price, 2, ',', ' ') . ' zł';
	}


}

?>
