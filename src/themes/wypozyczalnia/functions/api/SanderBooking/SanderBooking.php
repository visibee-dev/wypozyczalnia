<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'SanderBooking' ) ) :

/**
 * Main Kalkulator Class.
 */
final class SanderBooking {
	/**
	 * The single instance of the class.
	 *
	 * @var SanderBooking
	 */
	protected static $_instance = null;

	/**
	 * Main SanderBooking Instance.
	 *
	 * Ensures only one instance of SanderBooking is loaded or can be loaded.
	 *
	 * @return SanderBooking - Main instance.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * SanderBooking Constructor.
	 */
	public function __construct() {
		$this->includes();
		$this->init_acf_options_pages();
		$this->init_hooks();
	}

	/**
	 * Include required core files.
	 */
	public function includes() {
		include_once( 'includes/SB_Calculator.php' );
		include_once( 'includes/SB_Sander.php' );
		include_once( 'includes/SB_Reservation.php' );
	}

	/**
	 * Init ACF options pages.
	 */
	public function init_acf_options_pages() {
		if ( function_exists( 'acf_add_options_page' ) ) {

			// Add parent
			$parent = acf_add_options_page( array(
				'page_title' => 'SanderBooking Options',
				'menu_title' => 'SanderBooking',
				'menu_slug' => 'sb',
				'icon_url' => 'dashicons-calendar',
				'redirect' => true
			) );

			// Sander Options
			acf_add_options_sub_page( array(
				'page_title' => 'Sander Settings',
				'menu_title' => 'Sander',
				'menu_slug' => 'sb-sander',
				'parent_slug' => $parent['menu_slug']
			) );

			// Calculator Options
			acf_add_options_sub_page( array(
				'page_title' => 'Calculator Settings',
				'menu_title' => 'Calculator',
				'menu_slug' => 'sb-calculator',
				'parent_slug' => $parent['menu_slug']
			) );

			// Reservation Options
			acf_add_options_sub_page( array(
				'page_title' => 'Reservation Settings',
				'menu_title' => 'Reservation',
				'menu_slug' => 'sb-reservation',
				'parent_slug' => $parent['menu_slug']
			) );
		}
	}

	/**
	 * Hook into actions and filters.
	 */
	private function init_hooks() {
		add_action( 'init', array( $this, 'init' ), 0 );
	}

	/**
	 * Init SanderBooking when WordPress Initialises.
	 */
	public function init() {
		// Set locale for Time
		setlocale (LC_TIME, "pl_PL");
	}

	/**
	 * Get Calculator Class.
	 * @return SB_Calculator
	 */
	public function calculator() {
		return SB_Calculator::instance();
	}

	/**
	 * Get Sander Class.
	 * @return SB_Sander
	 */
	public function sander() {
		return SB_Sander::instance();
	}

	/**
	 * Get Reservation Class.
	 * @return SB_Reservation
	 */
	public function reservation() {
		return SB_Reservation::instance();
	}
}

endif;

/**
 * Main instance of SanderBooking.
 *
 * Returns the main instance of SB to prevent the need to use globals.
 *
 * @return SanderBooking
 */
function SB() {
	return SanderBooking::instance();
}

$GLOBALS['SanderBooking'] = SB();
