<?php

if (have_rows('content')) : while (have_rows('content')) : the_row();

	$show = get_sub_field('show');

	if ($show) :

		while (the_flexible_field('layout')) :

			$class = get_row_layout('layout');

			$name = 'common/contents/' . $class;

			get_component($name);

		endwhile;
	endif;

endwhile; endif;

?>