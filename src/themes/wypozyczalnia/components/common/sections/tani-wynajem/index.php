<div class="container">
	<div class="tani-wynajem">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-offset-7">
				<div class="wrap flexbox-container flexbox-wrap">
					<?php the_sub_field('content'); ?>
				</div>
			</div>
		</div>
	</div>
</div>