</div> <!-- END .row.flexbox-container -->
<div class="row">
	<form class="availability-form">
		<div class="col-xs-12 col-sm-8">
			<input type="text" name="city-or-city-code" placeholder="<?php the_sub_field('input_label'); ?>">
		</div>
		<div class="col-xs-12 col-sm-4">
			<input type="submit" class="btn btn--primary" value="<?php the_sub_field('search_label'); ?>">
		</div>
	</form>
</div>