<?php if (have_rows('faqs', 'options')) : ?>

<div class="col-xs-12">
	<div class="faqs">

	<?php while (have_rows('faqs', 'options')) : the_row(); ?>
		<?php if (get_sub_field('show_on_home')) : ?>
		<div class="faq">
			<p class="question"><?php the_sub_field('question'); ?></p>
			<p class="answear"><?php the_sub_field('answear'); ?></p>
		</div>
		<?php endif; ?>
	<?php endwhile; ?>

	</div>
</div>

<script>
(function($){

	$(document).ready(function() {
		$('.faqs').slick({

		});
	});

})(jQuery);
</script>

<?php endif; ?>