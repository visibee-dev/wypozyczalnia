<?php if (have_rows('reviews')) : ?>

	<div class="reviews">

	<?php while (have_rows('reviews')) : the_row(); ?>

		<div class="col-xs-12 col-sm-4">
			<div class="review">
				<figure class="photo">
					<?php $photo = get_sub_field('img'); ?>
					<img src="<?php echo $photo['url']; ?>" alt="<?php the_sub_field('name'); ?>">
				</figure>
				<div class="text"><?php the_sub_field('content'); ?></div>
				<div class="name"><?php the_sub_field('name'); ?></div>
			</div>
		</div>

	<?php endwhile; ?>

	</div>

<?php endif; ?>