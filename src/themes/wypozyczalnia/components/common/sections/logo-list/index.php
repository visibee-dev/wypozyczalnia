<?php

$label = $args['label'];
$repeater = $args['repeater'];

?>

<div class="col-xs-12 col-sm-6">
	<div class="logo-list flexbox-container flexbox-wrap-md">

	<?php if (get_sub_field($label)) : ?>

		<label class="label"><?php the_sub_field($label); ?></label>

	<?php endif; ?>

	<?php if (have_rows($repeater)) : ?>

		<div class="logos flexbox-container">

		<?php while (have_rows($repeater)): the_row(); ?>

			<?php $logo = get_sub_field('img'); ?>
			<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">

		<?php endwhile; ?>

		</div>

	<?php endif; ?>

	</div>
</div>