<?php if (have_rows('files')) : ?>
	
	<div class="files col-xs-12 flexbox-container flexbox-wrap">
	
	<?php while (have_rows('files')) : the_row(); ?>
		
		<?php $file = new File(get_sub_field('file')); ?>

		<figure class="file">
			<a href="<?php echo $file->url; ?>" target="_blank" title="<?php echo $file->title; ?>">
				<img src="<?php echo $file->icon_url; ?>" alt="<?php echo $file->name; ?>">
				<figcaption><?php echo $file->name; ?></figcaption>
			</a>
		</figure>
	
	<?php endwhile; ?>
	
	</div>

<?php endif; ?>