<?php if (have_rows('step')) : $counter = 0; ?>
	<div class="col-xs-12">
		<section class="jak-to-dziala">
			<div class="flexbox-container flexbox-wrap">

			<?php while (have_rows('step')) : the_row(); $counter++; ?>

				<div class="item step-<?php echo $counter; ?> col-xs-12 col-sm-4">

				<?php if ($link = get_sub_field('link')) : ?>

					<a class="item__link" href="<?php echo $link; ?>">

				<?php endif; ?>

						<figure class="item__icon">
							<i class="icon <?php the_sub_field('icon'); ?>"></i>
						</figure>
						<div class="label"><?php the_sub_field('label'); ?></div>

				<?php if ($link) : ?>

					</a>

				<?php endif; ?>

				</div>

				<div class="arrow arrow-<?php echo $counter; ?> col-xs-12 col-sm-4">
					<i class="icon strzalka-rog"></i>
				</div>				

			<?php endwhile; ?>

			<div class="blank col-xs-12 col-sm-4"></div>

			</div>
		</section>
	</div>
<?php endif; ?>