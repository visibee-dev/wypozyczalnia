<div class="col-xs-12 col-sm-6">
	<figure class="media">
	<?php if (get_sub_field('image')) : ?>
		<?php $image = get_sub_field('image'); ?>
		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	<?php elseif (get_sub_field('iframe')) : ?>
		<?php the_sub_field('iframe'); ?>
	<?php endif; ?>
	<?php if (get_sub_field('caption')) : ?>
		<figcaption><?php the_sub_field('caption'); ?></figcaption>
	<?php endif; ?>
	</figure>
</div>