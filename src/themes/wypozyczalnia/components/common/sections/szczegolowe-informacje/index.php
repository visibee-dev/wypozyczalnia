<div class="columns">
	<div class="col-xs-12 col-sm-5 col-border col-border--after">
		<?php the_sub_field('col-left'); ?>
	</div>

	<div class="col-xs-12 col-sm-7 col-border col-border--before">
		<?php the_sub_field('col-right'); ?>
	</div>
</div>