<?php if (have_rows('products')) : ?>

<div class="col-xs-12 woocommerce">
	<div class="products row flexbox-container flexbox-wrap flexbox-stretch">

	<?php while (have_rows('products')) : the_row(); ?>
		
		<?php $post_object = get_sub_field('product'); ?>

		<?php if ($post_object) : ?>

			<?php

			$post = $post_object;
			setup_postdata($post);

			?>
			
			<div class="product col-xs-12 col-sm-4">

				<a href="<?php echo get_post_permalink($post); ?>">

					<?php mywoo_the_product_thumbnail($post); ?>

					<h3><?php echo $post->post_title; ?></h3>

				</a>

				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

			</div>

			<?php wp_reset_postdata(); ?>

		<?php endif; ?>

	<?php endwhile; ?>

	</div>
</div>

<script>
(function($){

	$(document).ready(function() {
		$('.products').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
			{
				breakpoint: SITE.backgrounds.breakpoints['sm'],
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			},
			]
		});
	});

})(jQuery);
</script>

<?php endif; ?>
