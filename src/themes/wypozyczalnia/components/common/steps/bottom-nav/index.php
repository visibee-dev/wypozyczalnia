<?php

global $previousStep;
global $currentStep;
global $nextStep;
global $error;

$from_reservation =  ( isset($_POST['from-reservation']) && $_POST['from-reservation'] == '1') ? true : false;

function nav_can_be_shown() {
	global $currentStep;

	if ($currentStep !== 'wycena') {
		return true;
	} elseif (
		isset($_SESSION['calc'])
		&& $_SESSION['calc']->is_valid()
		&& (
			!isset($_GET['edit_mode'])
			|| $_GET['edit_mode'] != '1'
		)
		) {
		return true;
	} else {
		return false;
	}
}

?>

<?php if ( ($previousStep || $nextStep) && nav_can_be_shown() ) : ?>

	<div class="col-xs-12">
		<div class="reservation__nav">
			<div class="buttons">
			<?php if ($previousStep && ($currentStep !== 'koszyk' || $from_reservation) ) : ?>
				<a class="btn btn--fill prev" href="<?php echo $previousStep['url']; ?>">Poprzedni krok</a>
			<?php endif; ?>
			<?php if ($currentStep === 'koszyk') : ?>
				<a href="<?php echo get_post_type_archive_link( 'product' ); ?>" class="btn btn--fill prev">Powrót do sklepu</a>
				<a href="?empty-cart">Wyczyść koszyk</a>
			<?php endif; ?>
			<?php if (!$error && $nextStep) : ?>
				<a class="btn btn--fill btn--primary next" href="<?php echo $nextStep['url']; ?>">
					<?php echo $nextStep['label']; ?>
				</a>
			<?php endif; ?>
			</div>
		</div>
	</div>

<?php endif; ?>
