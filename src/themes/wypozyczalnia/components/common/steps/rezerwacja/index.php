<div class="col-xs-12 col-sm-10 col-sm-offset-1">

	<?php
		
	if ( !isset($_SESSION['calc']) ) :

		$_SESSION['calc'] = new Calculator();

	endif;

	$calc = $_SESSION['calc'];

	global $nextStep;
	
	?>

	<form class="rezerwacja-daty" action="<?php echo $nextStep['url']; ?>" method="post">

		<div class="custom-date row">

			<p class="custom-date-intro col-xs-12">Wybierz jeden z dostępnych terminów dla miasta Warszawa lub <a href="#" id="show-custom-date"><span>podaj inny termin</span> <i class="icon kalendarz"></i></a></p>

			<div class="availability custom-date-input-container col-xs-12 col-sm-10 col-sm-offset-1">
				<input
					type="radio"
					name="date"
					id="custom-date-radio"
					value="custom"
					<?php $calc->check_delivery_field('start_date', 'custom') ?>
				>

				<label for="custom-date-radio" class="no-padding">

					<input id="custom-date-input" type="text" name="custom-date" class="datepicker" placeholder="Wprowadź datę">

				</label>
			</div>

			<input type="hidden" name="from-reservation" value="1">

		</div>

		<?php $calc->the_available_dates(); ?>

	</form>

</div>

<?php


?>
