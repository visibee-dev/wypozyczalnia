<?php $calc = $_SESSION['calc']; ?>

<form id="materialsCalculatorForm" action="<?php the_permalink(); ?>" method="get">
	<input id="floorSqm" type="number" name="sqm" placeholder="wpisz metraż podłogi do cyklinowania (m2)" min="1" <?php $calc->the_value('sqm'); ?>>
	<input id="roomNumber" type="number" name="rooms" placeholder="wpisz liczbę pomieszczeń" min="1" <?php $calc->the_value('rooms'); ?>>

	<div id="container-discount" class="row">
		<div class="col-xs-12">
			<p class="label">Wybierz wariant wynajmu:</p>
		</div>

		<div class="col-xs-6 hidde-input">

			<label for="discount-true" class="select-btn">
				<div class="label-block">
					<p class="name">Z kompletem materiałów</p>
					<img src="<?php echo get_stylesheet_directory_uri() . '/img/all-materials.jpg'; ?>">
					<p class="price-label">Cena wynajmu:</p>
					<p class="price"><del>100,00 zł</del> 50,00 zł / doba</p>
				</div>

				<a class="btn btn--fill select-btn">Wybieram</a>
			</label>

			<input id="discount-true" type="radio" name="materials" value="TAK" <?php $calc->the_value('materials', 'TAK'); ?>>
		</div>
		<div class="col-xs-6 hidde-input">

			<label for="discount-false" class="select-btn">
				<div class="label-block">
					<p class="name">Bez chemii</p>
					<img src="<?php echo get_stylesheet_directory_uri() . '/img/no-materials.jpg'; ?>">
					<p class="price-label">Cena wynajmu:</p>
					<p class="price">100,00 zł / doba</p>
				</div>

				<a class="btn btn--fill select-btn">Wybieram</a>
			</label>

			<input id="discount-false" type="radio" name="materials" value="NIE" <?php $calc->the_value('materials', 'NIE'); ?>>
		</div>
	</div>

	<div id="container-without-materials" class="row">
		<div class="col-xs-12">
			<p class="label">Wybrałeś wynajem cykliniarki bez chemii (<a class="change-choice-btn">zmień wybór</a>).</p>

			<div class="calculate">
				<button type="button" class="btn btn--fill calculate-btn">Oblicz koszt</button>
			</div>
		</div>
	</div>

	<div id="container-with-materials" class="row">
		<div class="col-xs-12">
			<p class="label">Wybrałeś wynajem cykliniarki z kompletem materiałów (<a class="change-choice-btn">zmień wybór</a>).</p>
		</div>
		<div class="col-xs-12">

			<div class="wood" id="container-wood">
				<div class="flexbox-container flexbox-wrap">
					<p class="label-small">Wybierz rodzaj drewna podłogi:</p>
					<div class="align-left">
						<input name="wood" id="wood-iglaste" type="radio" value="iglaste" <?php $calc->the_value('wood', 'iglaste'); ?>>
						<label for="wood-iglaste">iglaste (np. sosna, świerk)</label>

						<input name="wood" id="wood-lisciaste" type="radio" value="liściaste" <?php $calc->the_value('wood', 'liściaste'); ?>>
						<label for="wood-lisciaste">liściaste (np. dąb, buk, jesion)</label>

						<input name="wood" id="wood-egzotyczne" type="radio" value="egzotyczne" <?php $calc->the_value('wood', 'egzotyczne'); ?>>
						<label for="wood-egzotyczne">egzotyczne (np. merbau, bambus)</label>
					</div>
				</div>
			</div>

			<div class="method" id="container-method">
				<p class="label-small notif" data-ntitle="Metoda" data-ncontent="Lorem ipsum dolor sit amet">Wybierz metodę:</p>

				<input name="method" id="method-olej" type="radio" value="olej" <?php $calc->the_value('method', 'olej'); ?>>
				<label for="method-olej">olej</label>

				<input name="method" id="method-lakier" type="radio" value="lakier" <?php $calc->the_value('method', 'lakier'); ?>>
				<label for="method-lakier">lakier</label>
			</div>

			<div class="enamel" id="container-enamel">
				<p class="label-small notif" data-ntitle="Lakier" data-ncontent="Lorem ipsum dolor sit amet">Wybierz lakier:</p>

				<input name="enamel" id="enamel-1" type="radio" value="1-składnikowy" <?php $calc->the_value('enamel', '1-składnikowy'); ?>>
				<label for="enamel-1">1-składnikowy</label>

				<input name="enamel" id="enamel-2" type="radio" value="2-składnikowy" <?php $calc->the_value('enamel', '2-składnikowy'); ?>>
				<label for="enamel-2">2-składnikowy</label>
			</div>

			<div class="finish" id="container-finish">
				<p class="label-small notif" data-ntitle="Wykończenie" data-ncontent="Lorem ipsum dolor sit amet">Wybierz wykończenie:</p>

				<input name="finish" id="finish-mat" type="radio" value="mat" <?php $calc->the_value('finish', 'mat'); ?>>
				<label for="finish-mat">mat</label>

				<input name="finish" id="finish-polmat" type="radio" value="półmat" <?php $calc->the_value('finish', 'półmat'); ?>>
				<label for="finish-polmat">półmat</label>

				<input name="finish" id="finish-polysk" type="radio" value="połysk" <?php $calc->the_value('finish', 'połysk'); ?>>
				<label for="finish-polysk">połysk</label>
			</div>

			<input type="hidden" name="update" value="true">

			<div class="calculate">
				<button type="button" class="btn btn--fill calculate-btn">Oblicz koszt</button>
			</div>

		</div>
	</div>
</form>
