<div class="col-xs-12 col-sm-8 col-sm-offset-2">

	<?php

	if ( !isset($_SESSION['calc']) || ( isset($_GET['update']) && $_GET['update'] == 'true' ) ) :

		$_SESSION['calc'] = new Calculator();

	endif;

	$calc = $_SESSION['calc'];

	if ( !$calc->is_valid() || ( isset($_GET['edit_mode']) && $_GET['edit_mode'] == 1 ) ) :

		get_component('common/steps/wycena-ankieta');

	else :

		get_component('common/steps/wycena-obliczenia');

	endif;

	?>

</div>
