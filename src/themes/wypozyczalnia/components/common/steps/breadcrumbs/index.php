<?php if (have_rows('breadcrumbs', 'option')) : ?>

	<?php

	global $previousStep;
	global $currentStep;
	global $nextStep;
	
	$previousStep = false;
	$currentStep = false;
	$nextStep = false;

	$step = strtolower($args['step']);
	$beforeCurrent = true;
	$afterCurrent = false;

	?>

	<nav class="reservation__breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<ul>

					<?php while (have_rows('breadcrumbs', 'option')) : the_row();

						$post_object = get_sub_field('page');

						$title = get_sub_field('title');

						if ($post_object) :

							$next_button_label = get_sub_field('next_button_label');

							if (strtolower(get_sub_field('label')) === $step) {
								$class = 'class="is-current"';
								$beforeCurrent = false;
								$afterCurrent = true;
								$currentStep = $step;

								$nextStep['label'] = $next_button_label;
								$nextStep['url'] =  get_permalink($post_object);
							} elseif ($beforeCurrent) {
								$class = 'class="is-previous"';
								$previousStep = array(
									'url' => get_permalink($post_object),
								);
							} elseif ($afterCurrent) {
								$nextStep['url'] =  get_permalink($post_object);
								$afterCurrent = false;
								$class = '';
							} else {
								$class = '';
							}

							?>

							<?php if ($beforeCurrent): ?>
								<a href="<?php echo get_permalink($post_object); ?>">
							<?php endif; ?>
									<li <?php echo $class; ?>>
										<?php echo $title; ?>
									</li>
							<?php if ($beforeCurrent): ?>
								</a>
							<?php endif; ?>	
						

						<?php endif; ?>

					<?php endwhile; ?>

					</ul>
					
				</div>
			</div>
		</div>
	</nav>

<?php endif; ?>
