<?php if (have_rows('pp-produkty', 'options')) : ?>

<div class="pp woocommerce col-xs-12 col-sm-10 col-sm-offset-1">
	<div class="products row flexbox-container flexbox-wrap flexbox-stretch">
		<div class="pp__intro col-xs-12"><?php the_field('pp-intro', 'options'); ?></div>

		<?php while (have_rows('pp-produkty', 'options')) : the_row(); ?>

			<?php $post_object = get_sub_field('product'); ?>

			<?php if ($post_object) : ?>

				<?php

				$post = $post_object;
				setup_postdata($post);
				$_product = wc_get_product( $post->ID );

				?>
				
				<div class="product col-xs-6 col-sm-3">
					<div class="product__wrap">

						<a href="<?php echo get_post_permalink($post); ?>">

							<?php mywoo_the_product_thumbnail($post); ?>

							<h3><?php echo $post->post_title; ?></h3>

						</a>

						<?php if ( $price_html = $_product->get_price_html() ) : ?>
							<span class="price"><?php echo mywoo_pre_price() . $price_html . mywoo_jednostka_miary(); ?></span>
						<?php endif; ?>
						
						<?php woocommerce_template_single_add_to_cart(); ?>

					</div>

				</div>

				<?php wp_reset_postdata(); ?>

			<?php endif; ?>

		<?php endwhile; ?>
	</div>
</div>

<?php endif; ?>
