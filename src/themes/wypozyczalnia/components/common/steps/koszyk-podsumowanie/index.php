<div class="col-xs-12 col-sm-10 col-sm-offset-1">

	<?php

	global $woocommerce;

	if (isset($_POST['from-reservation']) && $_POST['from-reservation'] == '1' && isset($_SESSION['calc'])) {
		$calc = $_SESSION['calc'];

// Wyczyść koszyk przed dodaniem produktów z rezerwacji
		$woocommerce->cart->empty_cart();


// Dodaj do koszyka cykliniarkę
		$duration = $calc->rental['length'];
		$start_date = $_POST['date'] == 'custom' ? $calc->to_normal_date($_POST['custom-date']) : $_POST['date'];

		$calc->set_delivery( $start_date, $duration);

		$end_date = $calc->delivery['end_date'];
		$rental_id = $calc->rental['id'];
		$price = $calc->is_discounted() ? $calc->rental['price'] * (1 - $calc->rental['discount']) : $calc->rental['price'];

		$data = array(
			'duration'   => $duration,
			'start_date' => $calc->get_polish_date($start_date),
			'end_date'   => $calc->get_polish_date($end_date),
			'start'      => $start_date,
			'end'        => $end_date,
			'new_price'  => $price * $duration,
		);

		WC()->session->__unset( 'booking' );

		$booking_data = array();
		$booking_data[$rental_id] = $data;

		WC()->session->set( 'booking', $booking_data );

		$woocommerce->cart->add_to_cart($rental_id);


// Dodaj do koszyka powiązane produkty
		foreach ($calc->materials_to_buy as $id => $item) {
			$woocommerce->cart->add_to_cart($id, $item['quantity']);
		}
	}

	while (have_posts()) : the_post();
		
		the_content();

	endwhile;

?>

</div>
