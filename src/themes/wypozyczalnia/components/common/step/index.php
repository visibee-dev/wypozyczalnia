<?php

$step = strtolower($args['step']);

get_component('common/steps/breadcrumbs', array(
	'step' => $step,
));

$class = $step ? 'reservation__step-' . $step : '';

$name = 'common/steps/' . $step;

?>

<section class="reservation__step <?php echo $class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php get_component('common/contents/title'); ?>	
			</div>

			<?php get_component($name); ?>

			<?php get_component('common/steps/bottom-nav'); ?>
		</div>
	</div>
</section>