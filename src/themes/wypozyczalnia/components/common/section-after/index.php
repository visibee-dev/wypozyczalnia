<?php if ($args['content_after']) : ?>
<div class="col-xs-12">
	<div class="content-after-section">
		<?php echo $args['content_after']; ?>
	</div>
</div>
<?php endif; ?>