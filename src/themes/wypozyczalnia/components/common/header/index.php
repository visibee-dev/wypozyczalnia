<?php
	$css = !is_front_page() ? 'inverted' : '';
?>
<header class="header <?php echo $css; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="flexbox-container flexbox-wrap-sm">
					<div class="flexbox-item">
						<?php get_component('header/logo'); ?>
					</div>

					<div class="flexbox-item header__contact">
						<?php get_component('header/contact'); ?>
					</div>

					<div class="flexbox-item">
						<?php get_component('header/nav-toggle'); ?>
					</div>

					<div class="flexbox-item header__cart-summary">
						<?php get_component('header/cart-summary'); ?>
					</div>
				</div>

			</div>
		</div>
	</div>
</header>

<?php get_component('header/menu'); ?>
