<?php

$type = $args['type'] ? '--' . $args['type'] : '';

if (have_rows('content' . $type, 'option')) : while (have_rows('content' . $type, 'option')) : the_row();

	$show = get_sub_field('show');

	if ($show) :
		$section_intro = get_sub_field('section_intro');
		$bg_modifier = get_sub_field('section_bg');
		$bg_img = contains('img', $bg_modifier) ? get_sub_field('section_bg_img') : false;
		$size = 'full';
		$content_after = get_sub_field('content_after');

		while (the_flexible_field('layout')) :

			$class = get_row_layout('layout');

			$name = 'common/sections/' . $class;

			if ($class === 'media') {
				$size = 'half';
			} elseif ($class === 'none') {
				$name = false;
			}

			get_component('common/section', $args = array(
				'name' => $name,
				'class' => $class.'-container',
				'intro_args' => array(
					'size' => $size,
					'content' => $section_intro,
				),
				'bg_modifier' => $bg_modifier,
				'bg_img' => $bg_img,
				'after_args' => array(
					'content_after' => $content_after,
				),
			));

		endwhile;
	endif;

endwhile; endif;

?>
