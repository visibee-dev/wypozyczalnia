<?php if ($args['content']) : ?>

	<?php if ($args['size'] === 'half') : ?>

	<div class="col-xs-12 col-sm-6">
		<div class="section__intro section__intro--small">

	<?php else : ?>

	<div class="col-xs-12">
		<div class="section__intro">

	<?php endif; ?>

			<?php echo $args['content']; ?>
		</div>
	</div>

<?php endif; ?>