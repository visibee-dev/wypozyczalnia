<?php if (have_rows('columns')) : ?>

<section class="contents contents--kolumny">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-offset-1">
				<div class="row flexbox-container flexbox-wrap flexbox-stretch flexbox-center">

				<?php while (have_rows('columns')) : the_row(); ?>

					<div class="column col-xs-12 <?php the_sub_field('size'); ?>">

						<?php the_sub_field('content'); ?>

					</div>

				<?php endwhile; ?>

				</div>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>