<?php $img = get_sub_field('image'); ?>

<?php if ($img) : ?>

	<section class="contents contents--zdjecie">
		<div class="container-fluid">
			<div class="row">

				<figure class="zdjecie">
					<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">

				<?php if (get_sub_field('caption')) : ?>
					<div class="caption">
						<div class="container">
							<div class="row">
								<figcaption class="col-xs-12 col-sm-5 col-sm-offset-5"><?php the_sub_field('caption'); ?></figcaption>
							</div>
						</div>
					</div>
				<?php endif; ?>

				</figure>

			</div>
		</div>
	</section>

<?php endif; ?>