<section class="contents contents--tekst">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<section class="content">
					<?php the_sub_field('content'); ?>
				</section>
			</div>
		</div>
	</div>
</section>