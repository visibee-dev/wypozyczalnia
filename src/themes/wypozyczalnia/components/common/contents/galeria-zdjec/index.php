<?php if (have_rows('images')) : ?>

<section class="contents contents--galeria-zdjec">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="row galeria-zdjec flexbox-container">

				<?php while (have_rows('images')) : the_row(); ?>

					<?php $img = get_sub_field('image'); ?>

					<figure class="col-xs-6 col-sm-4 zdjecie">
						<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
						<figcaption><?php the_sub_field('caption'); ?></figcaption>
					</figure>

				<?php endwhile; ?>

				</div>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>