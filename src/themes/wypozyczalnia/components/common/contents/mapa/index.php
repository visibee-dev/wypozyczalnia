<?php if ($map = get_sub_field('map')) : ?>

<section class="contents contents--mapa">
	<div class="container-flex">
		<div class="map" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>">
			<div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>" data-type="logo"></div>
		</div>
	</div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF5eTMm7VZAT_Us47cCqvPbtUVO9-HWJc"></script>
<script type="text/javascript">
(function($) {

	var $map;

	function initMap() {
		var $el = $('.contents--mapa .map');
		var $markers = $el.find('.marker');

		var lat = $el.data('lat');
		var lng = $el.data('lng');

		var markerBase = '<?php echo get_stylesheet_directory_uri(); ?>/img/';
		var markers = {
			logo: {
				icon: markerBase + 'marker-logo.png'
			},
		};
       	
		$map = new google.maps.Map( $el[0], {
			zoom		: 15,
			center		: new google.maps.LatLng(lat, lng),
			mapTypeId	: google.maps.MapTypeId.ROADMAP,
			scrollwheel : false
		});

		$markers.each(function() {
			var lat = $(this).data('lat');
			var lng = $(this).data('lng');
			var type = $(this).data('type');

			var location = {
				position: new google.maps.LatLng(lat, lng),
				type: type
			}

			addMarker(location);
		});

		function addMarker(location) {
			var marker = new google.maps.Marker({
				position: location.position,
				icon: markers[location.type].icon,
				map: $map
			});
		}
	}

	$(document).ready(function() {
		initMap();
	});
})(jQuery);
</script>


<?php endif; ?>
