<?php if (have_rows('faqs', 'options')) : ?>

<section class="contents contents--faq-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="row faq-page flexbox-container">

				<?php while (have_rows('faqs', 'options')) : the_row(); ?>

					<div class="faq">
						<p class="question"><?php the_sub_field('question'); ?></p>
						<p class="answear"><?php the_sub_field('answear'); ?></p>
					</div>

				<?php endwhile; ?>

				</div>
			</div>
		</div>
	</div>
</section>

<script>
(function($){

	$(document).ready(function() {
		$('.faq-page .faq').click(function(event) {
			$(this).toggleClass('active').children('.answear').toggle(300);
		});
	});

})(jQuery);
</script>

<?php endif; ?>