<?php

$bg_modifier = $args["bg_modifier"] ? "section--".$args["bg_modifier"] : "";

if ($bg_modifier === 'section--red') {
	$bg_modifier .= ' inverted';
} elseif ($bg_modifier === 'section--img-dark') {
	$bg_modifier .= ' section--img';
}

$bg_data = $args["bg_img"] ? "data-background='".json_encode($args["bg_img"])."'" : "";

?>

<section class="section <?php echo $bg_modifier.' '.$args['class']; ?>" <?php echo $bg_data; ?>>
	<div class="container">
		<div class="row flexbox-container flexbox-wrap">
		<?php get_component('common/section-intro', $args['intro_args'] ? $args['intro_args'] : array()); ?>

		<?php get_component($args['name']); ?>

		<?php if ($args['after_args']) get_component('common/section-after', $args['after_args']); ?>
		</div>
	</div>
</section>