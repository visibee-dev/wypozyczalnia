<div class="row">
	<div class="col-xs-12">

	<?php
	wp_nav_menu( array(
		'menu'	=> 'footer',
		'container'	=> 'nav',
		'container_class'	=> 'nav nav--menu nav--footer',
		'container_id'	=> 'footer-nav'
	));
	?>
	
	</div>
</div>