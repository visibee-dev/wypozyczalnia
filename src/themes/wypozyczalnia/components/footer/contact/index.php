<?php if (have_rows('contact')) : ?>
	<div class="footer-contact row flexbox-container flexbox-wrap-xs">
	<?php while (have_rows('contact')) : the_row(); ?>
		<div class="col-xs-12 col-sm-4">
			<div class="item">
				<figure class="item__icon">
					<i class="icon <?php the_sub_field('icon'); ?>"></i>
				</figure>
				<div class="label"><?php the_sub_field('label'); ?></div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php endif; ?>