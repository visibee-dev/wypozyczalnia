<section class="single">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ): the_post(); ?>

					<?php get_component('common/content'); ?>

				<?php endwhile; ?>
			
			<?php else: ?>

				<?php get_component('common/content-error'); ?>

			<?php endif; ?>
			
			</div>
		</div>
	</div>
</section>