<section class="toggle-menu">
	<div class="wrap-nav">

		<a id="toggle-menu-close" class="nav__toggle nav__toggle--close link">
			<span class="inline-block">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</span>
		</a>

		<?php
		wp_nav_menu( array(
			'menu'	=> 'header',
			'container'	=> 'nav',
			'container_class'	=> 'nav nav--menu nav--header',
			'container_id'	=> 'header-nav'
		));
		?>

	</div>
	<div class="wrap-bg"></div>
</section>