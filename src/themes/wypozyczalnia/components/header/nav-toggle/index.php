<a id="header-nav-toggle" class="nav__toggle link">
	<span class="nav-text h1 inline-block">Menu</span>
	<span class="inline-block">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</span>
</a>
