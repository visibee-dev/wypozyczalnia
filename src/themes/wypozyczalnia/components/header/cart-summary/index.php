<?php
	$items = WC()->cart->get_cart_contents_count();
	$total = WC()->cart->get_cart_total();
?>

<a
		class="nav__cart-summary"
		href="<?php echo wc_get_cart_url(); ?>"
		title="<?php _e( 'View your shopping cart' ); ?>">
	<i class="icon cart-icon"></i> Koszyk <?php if ($items > 0) echo '(' . $items . ')'; ?>
</a> / <a
			href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"
			class="nav__cart-summary">Moje Konto</a>
