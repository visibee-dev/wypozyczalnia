<?php

$logo_src = get_stylesheet_directory_uri() . "/img/";

if ( !is_front_page() ) {
	$logo_src .= "logo-inverted.png";
} else {
	$logo_src .= "logo.png";
}

?>

<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
	<img class="header__logo" src="<?php echo $logo_src; ?>">
</a>
