<?php

if (have_rows('content')) : while (have_rows('content')) : the_row();

	$show = get_sub_field('show');

	if ($show) :

		while (the_flexible_field('layout')) :

			$class = get_row_layout('layout');

			if ($class === 'tekst') {
				$name = 'common/media';
			} elseif ($class === 'zdjecie') {
				$name = 'common/media';
			} elseif ($class === 'galeria_zdjec') {
				$name = 'common/media';
			} 

			get_component('common/section', $args = array(
				'name' => $name,
				'class' => $class.'-container',
			));

		endwhile;
	endif;

endwhile; endif;

?>