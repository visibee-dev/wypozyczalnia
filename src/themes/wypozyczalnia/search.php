<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<?php get_header(); ?>
<?

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query["s"] = $_GET['s'];
	}
}
$wp_query = new WP_Query([
		's'=>get_search_query(),
		'post_type' => 'any',
    'suppress_filters' => TRUE,
    'posts_per_page' => '-1'
	]);
// echo '<pre>'; var_dump($wp_query->posts); echo '</pre>';



?>
<section class="search_section">
  <div class="container">
  	<?php if (sizeof($wp_query->posts)>0) { ?>
		  <p class="section__title">Wyniki wyszukiwania dla frazy: <b><?php the_search_query(); ?> </b></p>

		<?php  foreach ($wp_query->posts as $post) {
      switch ($post->post_type){
        case 'product':
          $title='Produkt';
        break;
        case 'recipe':
          $title='Przepis';
        break;
        case 'page':
          $title='Strona';
        break;
        case 'shop':
          $title='Sklep';
        break;
        default :
          $title='Post';
        break;
      }
      ?>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
        <div class="c-content-media-1" style="min-height:200px">
          <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"><?php echo $title ?></div>
              <a href="<?php echo get_permalink($post->ID) ?>" class="c-title c-font-uppercase c-font-bold c-theme-on-hover"><?php echo $post->post_title ?></a>
          <div class="c-date"> <?php echo date( 'Y/m/d', strtotime($post->post_date)) ?> </div>
        </div>
      </div>
    <?php }
		}
		else
			echo '  <p class="section__title">Brak wyników wyszukiwania</p>';
		?>
  </div>
</section>


<?php get_footer(); ?>
