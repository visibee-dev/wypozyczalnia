<?php // Template Name: Contact Page ?>

<?php
	
	get_header();

	$name = 'common/sections/' . 'contact';
	$class = 'contact';

	$section_intro = '<h1>' . get_the_title() . '</h1>';
	$size = 'full';


	get_component('common/section', $args = array(
		'name' => $name,
		'class' => $class.'-container',
		'intro_args' => array(
			'size' => $size,
			'content' => $section_intro,
		),
		// 'bg_modifier' => $bg_modifier,
		// 'bg_img' => $bg_img,
		// 'after_args' => array(
		// 	'content_after' => $content_after,
		// ),
	));

	get_footer();

?>