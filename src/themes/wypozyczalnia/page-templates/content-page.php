<?php // Template Name: Content Page ?>

<?php
	
	get_header();

	if (have_posts()) : while (have_posts()):

		the_post();

		get_component('common/contents/title');

		get_component('common/content');

	endwhile; else :

		get_component('common/contents/error');

	endif;

	get_footer();

?>