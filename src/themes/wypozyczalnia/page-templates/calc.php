<?php // Template Name: Calculator ?>

<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css" type="text/css" media="all">
	<!-- <script type="text/javascript" src="calculator.js"></script> -->
	<title>Kalkulator</title>
	<style>
		.for-wood, .for-materials, .for-enamel { display: none; }
		.materials .for-materials { display: block; }
		.materials.wood .for-wood { display: block; }
		.materials.enamel .for-enamel { display: block; }


		form > div {
			margin-top: 1em;
		}

		div:not(div.inline) label {
			display: block;
		}
		input, select {
			padding: 0.5em;
			margin-top: 0.5em;
		}

		.pricing {
			padding: 1em;
			background-color: #f5f5f5;
		}

		table {
			width: 100%;
		}
		table td {
			padding: 0.5em;
			border: 1px solid black;
			margin: 0;
		}

		form, .pricing {
			width: 50%;
			float: left;
			margin: 0;
			box-sizing: border-box;
		}

		.container {
			width: 100%;
			max-width: 1200px;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="container">
		<form id="materialsCalculatorForm">
			<div>
				<label for="floorSqm">Wpisz metraż podłogi do cyklinowania (m2):</label>
				<input id="floorSqm" type="number" placeholder="50" value="50">
			</div>
			<div>
				<label for="roomNumber">Wpisz liczbę pomieszczeń:</label>
				<input id="roomNumber" type="number" placeholder="2" value="2">
			</div>
			<div class="inline">
				<input id="discount" type="checkbox" value="0.5">
				<label for="discount">Chcę kupić komplet materiałów oraz otrzymać 50% zniżki na wynajem.</label>
			</div>
			<div class="for-materials">
				<label for="finishType">Wybierz rodzaj wykończenia:</label>
				<select id="finishType">
					<option value="olej">olej</option>
					<option value="lakier">lakier</option>
				</select>
			</div>
			<div class="for-wood">
				<label for="woodType">Wybierz rodzaj drewna:</label>
				<select id="woodType">
					<option value="iglaste, 3">iglaste</option>
					<option value="lisciaste, 2">liściaste</option>
					<option value="egzotyczne, 2">egzotyczne</option>
				</select>
			</div>
			<div class="for-enamel">
				<label for="enamelType">Wybierz rodzaj lakieru:</label>
				<select id="enamelType">
					<option value="mat, 2">mat - 2 składnikowy</option>
					<option value="polmat, 1">półmat - 1 składnikowy</option>
					<option value="polmat, 2">półmat - 2 składnikowy</option>
					<option value="polysk, 1">połysk - 1 składnikowy</option>
					<option value="polysk, 2">połysk - 2 składnikowy</option>
				</select>
			</div>
			<div>
				<button id="calculate" type="button">Wyceń</button>
			</div>
		</form>
		<div class="pricing">
			<h2>Wycena materiałów:</h2>
			<div id="output"></div>
		</div>
	</div>

	<script type="text/javascript">
		"use_strict";

		var Form = {
			init: function() {
				this._cacheDOM();
				this._addEventListeners();
				this._render();
			},
			_cacheDOM: function() {
				this.FORM = document.getElementById('materialsCalculatorForm');
				this.FLOOR_SQM = document.getElementById('floorSqm');
				this.ROOM_NUMBER = document.getElementById('roomNumber');
				this.ALL_MATERIALS = document.getElementById('allMaterials');
				this.FINISH_TYPE = document.getElementById('finishType');
				this.WOOD_TYPE = document.getElementById('woodType');
				this.ENAMEL_TYPE = document.getElementById('enamelType');
				this.BUTTON = document.getElementById('calculate');
			},
			_addEventListeners: function() {
				this.FORM.addEventListener('change', this._render.bind(this));
				// this.FORM.addEventListener('change', Calculator._render.bind(Calculator));
				this.BUTTON.addEventListener('click', Calculator._render.bind(Calculator));
			},
			_render: function() {
				switch (this.ALL_MATERIALS.checked) {
					case true:
						CSS.addClass(this.FORM, 'materials');
						break;
					case false:
						CSS.removeClass(this.FORM, 'materials');
						break;
				}

				switch (this.FINISH_TYPE.value) {
					case 'olej':
						CSS.removeClass(this.FORM, 'enamel');
						CSS.addClass(this.FORM, 'wood');
						break;
					case 'lakier':
						CSS.removeClass(this.FORM, 'wood');
						CSS.addClass(this.FORM, 'enamel');
						break;
					default:
						CSS.removeClass(this.FORM, 'enamel');
						CSS.removeClass(this.FORM, 'wood');
						break;
				}
			},
		}

		var Calculator = {
			init: function() {
				this._setConsts();
				this._render();
			},
			_setConsts: function() {
				this.EFF_1R = 25;		// [m2] Wydajność maszyny przy 1 pomieszczeniu
				this.EFF_MR = 20;		// [m2] Wydajność maszyny przy 2+ pomieszczeniach
				this.RENT_PRICE = 100;	// [zł/doba] Cena wynajmu maszyny
				this.OUTPUT = document.getElementById('output');	// DOM element for outputing calculations
			},
			_setVars: function() {
				this.wantsMaterials = Form.ALL_MATERIALS.checked;	// [boolean] Czy klient chce materiały?
				this.rent_multiplier = this.wantsMaterials ? 1 - Form.ALL_MATERIALS.value : 1; // [%] Mnożnik ceny wynajmu maszyny. Zależny od tego, czy klient chce wszystkie materiały
				this.sqm = Form.FLOOR_SQM.value;	// [m2] Powierzchnia pracy
				this.days = Form.ROOM_NUMBER.value > 1 ? Math.ceil(this.sqm / this.EFF_MR) : Math.ceil(this.sqm / this.EFF_1R);	// [d] Liczba dni wynajmu
				this.finishType = Form.FINISH_TYPE.value;
				this.productType = this.finishType === 'olej' ? Form.WOOD_TYPE.value : Form.ENAMEL_TYPE.value;
				this.sumup = 0;
			},
			_render: function() {
				this._setVars();
				this._setProducts();
				this._displayHTML();
			},
			_displayRent: function() {
				var output = '';
				var price_single = this.RENT_PRICE;
				var price_total = this.days * this.RENT_PRICE;

				Calculator.sumup += price_total * this.rent_multiplier;

				if (this.rent_multiplier !== 1) {
					price_single = '<del>' + price_single + '</del> ' + '<ins>' + price_single * this.rent_multiplier + '</ins>';
					price_total = '<del>' + price_total + '</del> ' + '<ins>' + price_total * this.rent_multiplier + '</ins>';
				}

				return '<tr>' +
						'<td>Koszt wynajmu maszyny</td>' + 
						'<td>' + price_single + ' zł' + '</td>' +
						'<td>' + this.days + ' dni' + '</td>' +
						'<td>' + price_total + ' zł' + '</td>' + 
					'</tr>';
			},
			_displayProducts: function() {
				if (!this.wantsMaterials) {
					return '';
				} else {
					var content = '';

					if (this.finishType === 'lakier') {
						content += this.PROD_PODKLAD.displayCost();
						switch (this.productType) {
							case 'polmat, 1':
							case 'polysk, 1':
								content += this.PROD_LAKIER_1S.displayCost();
								break;
							default:
								content += this.PROD_LAKIER_2S.displayCost();
						}
					} else if (this.finishType === 'olej') {
						switch (this.productType) {
							case 'iglaste, 3':
								content += this.PROD_OLEJ_3W.displayOilCost();
								break;
							default:
								content += this.PROD_OLEJ_2W.displayOilCost();
						}
					}

					content += this.PROD_WALEK.displayCost() +
						this.PROD_SZPACHLA.displayCost() +
						this.PROD_KRAZKI.displayCost();

					return content;
				}
			},
			_displayHTML: function() {
				var content =
					'<table>' +
						'<thead>' + 
							'<tr>' +
								'<th>Nazwa</th>' +
								'<th>Cena jednostkowa</th>' +
								'<th>Ilość</th>' +
								'<th>Wartość</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>' +
							this._displayRent() +
							this._displayProducts() +
						'</tbody>' +
						'<tfoot>' +
							'<tr>' +
								'<td colspan=3>Łącznie</td>' +
								'<td>' + this.sumup + ' zł</td>' +
							'</tr>' +
						'</tfoot>' +
					'</table>';
				this.OUTPUT.innerHTML = content;
			},
			_setProducts: function() {
				this.PROD_WALEK = new Product('Wałek 1 szt.', 40, Infinity);
				this.PROD_SZPACHLA = new Product('Szpachla 5l', 200, 50);
				this.PROD_KRAZKI = new Product('Krażki 16 szt.', 64, 7);
				this.PROD_PODKLAD = new Product('Szpachla 5l', 200, 50);
				this.PROD_LAKIER_2S = new Product('Lakier 2-składnikowy 5l', 450, 15);
				this.PROD_LAKIER_1S = new Product('Lakier 1-składnikowy 5l', 300, 15);
				this.PROD_OLEJ_2W = new Oil('Olej', [280, 120], 7.2, [2.5, 1]);
				this.PROD_OLEJ_3W = new Oil('Olej', [280, 120], 5.6, [2.5, 1]);
			}

		}

		var Product = function(name, price, efficiency) {
			this.name = name;
			this.efficiency = efficiency;
			this.price = price;
			this.amount = this.efficiency === Infinity ? 1 : Math.ceil(Calculator.sqm / this.efficiency);	// Product efficiency for 1 unit of measure. If EFF is Inifity, 1 unit of measure is enough for all the work. 

			this.displayCost = function(name, price, amount) {
				name = name ? name : this.name;
				price = price ? price : this.price;
				amount = amount ? amount : this.amount;

				Calculator.sumup += price * amount;

				return '<tr>' +
						'<td>' + name + '</td>' + 
						'<td>' + price + ' zł' + '</td>' +
						'<td>' + amount + ' opak.' + '</td>' +
						'<td>' + amount * price + ' zł' + '</td>' + 
					'</tr>';
			}
		}

		function Oil(name, price, efficiency, volume) {
			Product.call(this, name, price, efficiency);
			this.amount = this.efficiency === Infinity ? 1 : Calculator.sqm / this.efficiency;
			this.amount = this.amount.toFixed(1);
			
			this.displayOilCost = function() {
				var array = [];
				var content = '';

				for (var i = 0; i < volume.length; i++) {
					var items;

					if (i === volume.length - 1) {
						items = Math.ceil(this.amount / volume[i]); 
					} else {
						items = Math.floor(this.amount / volume[i]);
					}

					this.amount -= items * volume[i];

					array.push({
						name: name + ' ' + volume[i] + 'l',
						price: price[i],
						amount: items,
						volume: volume[i],
					})
				}

				for (var i = array.length - 1; i >= 0; i--) {
					if (i > 0 && array[i].volume * array[i].amount > array[i - 1].volume) {
						array[i].amount = 0;
						array[i - 1].amount += 1;
					}
					if (array[i].amount > 0) {
						content += this.displayCost(array[i].name, array[i].price, array[i].amount);
					}
				}

				return content;
			}
		};

		Oil.prototype = Object.create(Product.prototype);


		var CSS = {
			regex: function(cls) {
				return new RegExp('(\\s|^)' + cls + '(\\s|$)');
			},
			hasClass: function(el, cls) {
				return el.className.match(this.regex(cls));
			},
			addClass: function(el, cls) {
				if (!this.hasClass(el, cls)) {
					el.className += ' ' + cls;
				}
			},
			removeClass: function(el, cls) {
				if (this.hasClass(el, cls)) {
					el.className = el.className.replace(this.regex(cls), '');
				}
			},
			toggleClass: function(el, cls) {
				if (this.hasClass(el, cls)) {
					this.removeClass(el, cls);
				} else {
					this.addClass(el, cls);
				}
			}
		}

		window.onload = function() {
			Form.init();
			Calculator.init();
		}

	</script>
</body>
</html>