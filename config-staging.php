<?php
/**
 * This is a sample `config-*.php` file
 * Use it to generate your
 * * `config-local.php`
 * * `config-staging.php`
 * * `config-production.php`
 * Note: `config-local.php` isn't tracked with git
 */
define ('DB_NAME', 'wypozyc3_staging');
define ('DB_USER', 'wypozyc3_wypo'); // Probably 'root'
define ('DB_PASSWORD', 'ynmo1DkhsZ'); // Probably empty ''
define ('DB_HOST', 'localhost'); // Proabably 'localhost'

/**
 * Define your home URL
 */
define ('WP_HOME', 'http://staging.wypozyczalniacykliniarek.com');

/**
 * Database tables prefix
 * You probably want it to be more complex, e.g. 'wpexample123_'
 */
$table_prefix = 'wpwyp2810161505_';

/**
 * Hide errors
 * You probably want to comment this in your `config-local.php` file
 */
ini_set ('display_errors', 0);
define ('WP_DEBUG_DISPLAY', false);

/**
 * Debug mode
 * You probably want to uncomment this in your `config-local.php` file
 */
// define ('SAVEQUERIES', true);
// define ('WP_DEBUG', true);

/**
 * Loging errors
 */
ini_set('log_errors', 'On');
ini_set('error_log', '/home/wypozyc3/domains/staging.wypozyczalniacykliniarek.com/public_html/php-errors.log'); // /full/path/to/php-errors.log
