var project = 'cheminum',
	src = './src/themes',
	assets = src + 'assets/',
	dist = src;


module.exports = {
	path: {
		src: src,
		dist: dist
	},

	tasks: {
		php: {
			htmlmin: {
				collapseWhitespace: true,
				removeAttributeQuotes: true,
				removeComments: true,
				minifyJS: true
			},
			extensions: ['php', 'html']
		},
		scripts: {
			bundles: {
				main: ['main'],
				vendors: ['vendors']
			},
			borwserSync: {
				match: '**/*.js'
			},
			extensions: ['js', 'ts']
		},
		styles: {
			borwserSync: {
				match: '**/*.css'
			},
			cssnano: {
				autoprefixer: {
					add: true,
					browsers: ['> 1%']
				}
			},
			sass: {},
			extensions: ['sass', 'scss']
		},
		watch: {
		}
	}
}
