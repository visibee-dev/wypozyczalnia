var config = require('../config');

var options = config.tasks.scripts;
if (!options) return;

var browserSync = require('browser-sync'),
	gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		camelize: true
	});

var task = function () {
	var src = config.path.src + '/assets/js/**/*.{' + options.extensions + '}';
		dest = config.path.dist;

	return gulp.src(src)
		.pipe(plugins.if(!global.dist, plugins.sourcemaps.init()))
		.pipe(plugins.babel())
		.pipe(plugins.concat('theme.js'))
		.pipe(plugins.uglify())
		.pipe(plugins.rename({
			suffix: '.min'
		}))
		.pipe(plugins.if(!global.dist, plugins.sourcemaps.write('.')))
		.pipe(gulp.dest(dest))
		.pipe(browserSync.stream(options.browserSync));
}

gulp.task('scripts', task);
module.exports = task;
