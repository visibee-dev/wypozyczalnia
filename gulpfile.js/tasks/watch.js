var config = require('../config');

var options = config.tasks.watch;
if (!options) return;

var browserSync = require('browser-sync'),
	gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		camelize: true
	});

var task = function () {
	var src = {
			styles: config.path.src + '/**/*.{' + config.tasks.styles.extensions + '}',
			scripts: config.path.src + '/**/*.{' + config.tasks.scripts.extensions + '}'
		},
		dest = config.path.dist;

	browserSync.init();

	gulp.watch(src.styles, ['styles']);
	gulp.watch(src.scripts, ['scripts']);
}

gulp.task('watch', task);
module.exports = task;
