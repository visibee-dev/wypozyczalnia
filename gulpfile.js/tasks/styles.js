var config = require('../config');

var options = config.tasks.styles;
if (!options) return;

var browserSync = require('browser-sync'),
	gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		camelize: true
	});

var task = function () {
	var src = config.path.src + '/**/*.{' + options.extensions + '}',
		dest = config.path.dist;

	return gulp.src(src)
		.pipe(plugins.if(!global.dist, plugins.sourcemaps.init()))
		.pipe(plugins.sassGlob())
		.pipe(plugins.sass(options.sass))
		.pipe(plugins.cssnano(options.cssnano))
		.pipe(plugins.if(!global.dist, plugins.sourcemaps.write('.')))
		.pipe(gulp.dest(dest))
		.pipe(browserSync.stream(options.browserSync));
}

gulp.task('styles', task);
module.exports = task;
