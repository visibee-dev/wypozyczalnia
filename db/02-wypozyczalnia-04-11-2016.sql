# WordPress MySQL database migration
#
# Generated: Friday 4. November 2016 17:25 UTC
# Hostname: localhost
# Database: `wypozyczalnia`
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wpwyp2810161505_commentmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_commentmeta`;


#
# Table structure of table `wpwyp2810161505_commentmeta`
#

CREATE TABLE `wpwyp2810161505_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_commentmeta`
#

#
# End of data contents of table `wpwyp2810161505_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_comments`
#

DROP TABLE IF EXISTS `wpwyp2810161505_comments`;


#
# Table structure of table `wpwyp2810161505_comments`
#

CREATE TABLE `wpwyp2810161505_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_comments`
#
INSERT INTO `wpwyp2810161505_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_comments`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_links`
#

DROP TABLE IF EXISTS `wpwyp2810161505_links`;


#
# Table structure of table `wpwyp2810161505_links`
#

CREATE TABLE `wpwyp2810161505_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_links`
#

#
# End of data contents of table `wpwyp2810161505_links`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_options`
#

DROP TABLE IF EXISTS `wpwyp2810161505_options`;


#
# Table structure of table `wpwyp2810161505_options`
#

CREATE TABLE `wpwyp2810161505_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_options`
#
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wypozyczalnia', 'yes'),
(2, 'home', 'http://localhost/wypozyczalnia', 'yes'),
(3, 'blogname', 'Wypożyczalnia Cykliniarek', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'patryk@visibee.pl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:88:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=5&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:37:"tinymce-advanced/tinymce-advanced.php";i:1;s:47:"tinymce-custom-styles/tinymce-custom-styles.php";i:2;s:31:"wp-migrate-db/wp-migrate-db.php";i:3;s:23:"wp-smushit/wp-smush.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:104:"E:\\wamp\\www\\wypozyczalnia/wp-content/plugins/black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1;s:67:"E:\\wamp\\www\\wypozyczalnia/wp-content/themes/wypozyczalnia/style.css";i:2;s:81:"E:\\wamp\\www\\wypozyczalnia/wp-content/themes/wypozyczalnia/editor-style-shared.css";i:3;s:74:"E:\\wamp\\www\\wypozyczalnia/wp-content/themes/wypozyczalnia/editor-style.css";i:4;s:0:"";}', 'no'),
(40, 'template', 'wypozyczalnia', 'yes'),
(41, 'stylesheet', 'wypozyczalnia', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:47:"tinymce-custom-styles/tinymce-custom-styles.php";s:13:"tcs_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '5', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'wpwyp2810161505_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'cron', 'a:4:{i:1478308223;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1478351445;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1478362338;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(116, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1477660736;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(136, 'current_theme', '', 'yes'),
(137, 'theme_mods_wypozyczalnia', 'a:1:{i:0;b:0;}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'recently_activated', 'a:1:{s:59:"black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1478195638;}', 'yes'),
(145, 'wdev-frash', 'a:3:{s:7:"plugins";a:1:{s:23:"wp-smushit/wp-smush.php";i:1477660862;}s:5:"queue";a:2:{s:32:"7de3619981caadc55f30a002bfb299f6";a:4:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:5:"email";s:7:"show_at";i:1477660862;s:6:"sticky";b:1;}s:32:"fc50097023d0d34c5a66f6cddcf77694";a:3:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:4:"rate";s:7:"show_at";i:1478360540;}}s:4:"done";a:0:{}}', 'no'),
(146, 'wp-smush-version', '2.4.5', 'no'),
(147, 'wp-smush-skip-redirect', '1', 'no'),
(148, 'wp-smush-install-type', 'new', 'no'),
(151, 'wp-smush-hide_upgrade_notice', '1', 'no'),
(185, 'tadv_settings', 'a:6:{s:9:"toolbar_1";s:119:"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo,redo";s:9:"toolbar_2";s:110:"styleselect,removeformat,media,image,wp_more,table,wp_help,strikethrough,underline,wp_code,code,indent,outdent";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"options";s:27:"menubar,advlist,contextmenu";s:7:"plugins";s:35:"table,advlist,importcss,contextmenu";}', 'yes'),
(186, 'tadv_admin_settings', 'a:2:{s:7:"options";s:9:"importcss";s:16:"disabled_editors";s:0:"";}', 'yes'),
(187, 'tadv_version', '4000', 'yes'),
(190, 'wp-smush-hide_update_info', '1', 'no'),
(193, 'tcs_addstyledrop', 'a:5:{i:0;a:7:{s:5:"title";s:5:"intro";s:5:"block";s:1:"p";s:7:"classes";s:5:"intro";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:1;a:7:{s:5:"title";s:6:"medium";s:5:"block";s:1:"p";s:7:"classes";s:6:"medium";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:2;a:7:{s:5:"title";s:5:"small";s:5:"block";s:1:"p";s:7:"classes";s:5:"small";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:3;a:7:{s:5:"title";s:2:"lg";s:5:"block";s:1:"p";s:7:"classes";s:2:"lg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:4;a:7:{s:5:"title";s:3:"xlg";s:5:"block";s:1:"p";s:7:"classes";s:3:"xlg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}}', 'yes'),
(196, 'tcs_locstyle', 'themes_directory', 'yes'),
(201, 'widget_black-studio-tinymce', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;

#
# End of data contents of table `wpwyp2810161505_options`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_postmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_postmeta`;


#
# Table structure of table `wpwyp2810161505_postmeta`
#

CREATE TABLE `wpwyp2810161505_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_postmeta`
#
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1478192562:1'),
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1478280123:1'),
(6, 5, '_wp_page_template', 'page-templates/content-page.php'),
(7, 4, '_wp_trash_meta_status', 'draft'),
(8, 4, '_wp_trash_meta_time', '1478193005'),
(9, 4, '_wp_desired_post_slug', ''),
(10, 2, '_wp_trash_meta_status', 'publish'),
(11, 2, '_wp_trash_meta_time', '1478193007'),
(12, 2, '_wp_desired_post_slug', 'sample-page') ;

#
# End of data contents of table `wpwyp2810161505_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_posts`
#

DROP TABLE IF EXISTS `wpwyp2810161505_posts`;


#
# Table structure of table `wpwyp2810161505_posts`
#

CREATE TABLE `wpwyp2810161505_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_posts`
#
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', '', 0, 'http://localhost/wypozyczalnia/?p=1', 0, 'post', '', 1),
(2, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/wypozyczalnia/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2016-11-03 17:10:07', '2016-11-03 17:10:07', '', 0, 'http://localhost/wypozyczalnia/?page_id=2', 0, 'page', '', 0),
(4, 1, '2016-11-03 16:12:25', '2016-11-03 16:12:25', '', 'Podstrona opisowa', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2016-11-03 17:10:05', '2016-11-03 17:10:05', '', 0, 'http://localhost/wypozyczalnia/?page_id=4', 0, 'page', '', 0),
(5, 1, '2016-11-03 16:13:12', '2016-11-03 16:13:12', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'publish', 'closed', 'closed', '', 'strona-opisowa', '', '', '2016-11-04 17:24:00', '2016-11-04 17:24:00', '', 0, 'http://localhost/wypozyczalnia/?page_id=5', 0, 'page', '', 0),
(6, 1, '2016-11-03 16:13:12', '2016-11-03 16:13:12', '', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 16:13:12', '2016-11-03 16:13:12', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2016-11-03 16:29:11', '2016-11-03 16:29:11', 'Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 16:29:11', '2016-11-03 16:29:11', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2016-11-03 17:10:05', '2016-11-03 17:10:05', '', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2016-11-03 17:10:05', '2016-11-03 17:10:05', '', 4, 'http://localhost/wypozyczalnia/2016/11/03/4-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2016-11-03 17:10:07', '2016-11-03 17:10:07', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/wypozyczalnia/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2016-11-03 17:10:07', '2016-11-03 17:10:07', '', 2, 'http://localhost/wypozyczalnia/2016/11/03/2-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2016-11-03 17:18:17', '2016-11-03 17:18:17', 'Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z <span class="intro">przygotowaniem</span> podłogi.\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 17:18:17', '2016-11-03 17:18:17', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2016-11-03 17:18:54', '2016-11-03 17:18:54', 'Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 17:18:54', '2016-11-03 17:18:54', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2016-11-03 17:19:16', '2016-11-03 17:19:16', '<span class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</span>\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 17:19:16', '2016-11-03 17:19:16', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2016-11-03 17:19:38', '2016-11-03 17:19:38', '<span class="intro medium">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</span>\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-03 17:19:38', '2016-11-03 17:19:38', '', 5, 'http://localhost/wypozyczalnia/2016/11/03/5-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2016-11-03 17:40:36', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-11-03 17:40:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wypozyczalnia/?page_id=15', 0, 'page', '', 0),
(16, 1, '2016-11-03 17:55:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-11-03 17:55:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/wypozyczalnia/?p=16', 0, 'post', '', 0),
(17, 1, '2016-11-04 15:48:40', '2016-11-04 15:48:40', '<span class="intro medium h1">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</span>\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 15:48:40', '2016-11-04 15:48:40', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2016-11-04 16:03:54', '2016-11-04 16:03:54', 'Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.\r\n\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:03:54', '2016-11-04 16:03:54', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2016-11-04 16:05:55', '2016-11-04 16:05:55', '<p class="medium h2">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:05:55', '2016-11-04 16:05:55', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2016-11-04 16:08:03', '2016-11-04 16:08:03', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:08:03', '2016-11-04 16:08:03', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2016-11-04 16:11:10', '2016-11-04 16:11:10', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\nOgólne warunki umowy najmu/dzierżawy', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:11:10', '2016-11-04 16:11:10', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2016-11-04 16:12:57', '2016-11-04 16:12:57', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<p class="h3">II. Postanowienia ogólne</p>\r\n<p class="h3"></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:12:57', '2016-11-04 16:12:57', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2016-11-04 16:14:12', '2016-11-04 16:14:12', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<p class="h3">II. Postanowienia ogólne</p>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2,', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:14:12', '2016-11-04 16:14:12', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2016-11-04 16:15:51', '2016-11-04 16:15:51', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<p class="h3">II. Postanowienia ogólne</p>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<p class="h2">Główne zalety</p>\r\n\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają transport</li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:15:51', '2016-11-04 16:15:51', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2016-11-04 16:17:07', '2016-11-04 16:17:07', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<p class="h3">II. Postanowienia ogólne</p>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<p class="h2">Główne zalety</p>\r\n\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają transport</li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:17:07', '2016-11-04 16:17:07', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2016-11-04 16:47:42', '2016-11-04 16:47:42', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają transport</li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:47:42', '2016-11-04 16:47:42', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2016-11-04 17:18:28', '2016-11-04 17:18:28', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\n\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\n<p style="text-align: left;">Align left</p>\n<p style="text-align: center;">Align center</p>\n<p style="text-align: right;">Align right</p>\n<p style="text-align: justify;">Jusftify</p>\n<p style="text-align: justify;"><del>Strikethrough</del></p>\n<p style="text-align: justify;"><span style="text-decoration: underline;">Underline</span></p>\n<p style="text-align: justify;">Increase i</p>\n\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\n<h3>II. Postanowienia ogólne</h3>\n<strong> §2</strong>\n\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\n\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\n\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\n\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\n\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\n<h2>Główne zalety</h2>\n<ul>\n 	<li>Silnik o dużej mocy</li>\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\n 	<li>Kompatybilna z Bona Power Drive</li>\n</ul>\n<table>\n<tbody>\n<tr>\n<td><strong>Model:</strong></td>\n<td>Bona FlexiSand 1.5</td>\n<td>Bona FlexiSand 1.9</td>\n</tr>\n<tr>\n<td><strong>Waga:</strong></td>\n<td>38 kg</td>\n<td>50 kg</td>\n</tr>\n<tr>\n<td><strong>Moc:</strong></td>\n<td>1,5 kW</td>\n<td>1,9 kW</td>\n</tr>\n</tbody>\n</table>\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-autosave-v1', '', '', '2016-11-04 17:18:28', '2016-11-04 17:18:28', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-autosave-v1/', 0, 'revision', '', 0),
(28, 1, '2016-11-04 16:50:47', '2016-11-04 16:50:47', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają transport</li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 16:50:47', '2016-11-04 16:50:47', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2016-11-04 17:09:03', '2016-11-04 17:09:03', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Strona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:09:03', '2016-11-04 17:09:03', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2016-11-04 17:14:16', '2016-11-04 17:14:16', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:14:16', '2016-11-04 17:14:16', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2016-11-04 17:18:47', '2016-11-04 17:18:47', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<p style="text-align: left;">Align left</p>\r\n<p style="text-align: center;">Align center</p>\r\n<p style="text-align: right;">Align right</p>\r\n<p style="text-align: justify;">Jusftify</p>\r\n<p style="text-align: justify;"><del>Strikethrough</del></p>\r\n<p style="text-align: justify;"><span style="text-decoration: underline;">Underline</span></p>\r\n<p style="text-align: justify; padding-left: 30px;">Increase indent</p>\r\n<p style="text-align: justify;">Decresea indent</p>\r\n<p style="text-align: justify;"><code>Code</code></p>\r\n\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:18:47', '2016-11-04 17:18:47', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2016-11-04 17:20:17', '2016-11-04 17:20:17', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<p style="text-align: left;"></p>\r\n<code>Code some code</code>\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:20:17', '2016-11-04 17:20:17', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0) ;
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(33, 1, '2016-11-04 17:20:36', '2016-11-04 17:20:36', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\n<code>Code some code</code>\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:20:36', '2016-11-04 17:20:36', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2016-11-04 17:24:00', '2016-11-04 17:24:00', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-11-04 17:24:00', '2016-11-04 17:24:00', '', 5, 'http://localhost/wypozyczalnia/2016/11/04/5-revision-v1/', 0, 'revision', '', 0) ;

#
# End of data contents of table `wpwyp2810161505_posts`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_relationships`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_relationships`;


#
# Table structure of table `wpwyp2810161505_term_relationships`
#

CREATE TABLE `wpwyp2810161505_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_relationships`
#
INSERT INTO `wpwyp2810161505_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_taxonomy`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_taxonomy`;


#
# Table structure of table `wpwyp2810161505_term_taxonomy`
#

CREATE TABLE `wpwyp2810161505_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_taxonomy`
#
INSERT INTO `wpwyp2810161505_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1) ;

#
# End of data contents of table `wpwyp2810161505_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_termmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_termmeta`;


#
# Table structure of table `wpwyp2810161505_termmeta`
#

CREATE TABLE `wpwyp2810161505_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_termmeta`
#

#
# End of data contents of table `wpwyp2810161505_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_terms`
#

DROP TABLE IF EXISTS `wpwyp2810161505_terms`;


#
# Table structure of table `wpwyp2810161505_terms`
#

CREATE TABLE `wpwyp2810161505_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_terms`
#
INSERT INTO `wpwyp2810161505_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0) ;

#
# End of data contents of table `wpwyp2810161505_terms`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_usermeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_usermeta`;


#
# Table structure of table `wpwyp2810161505_usermeta`
#

CREATE TABLE `wpwyp2810161505_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_usermeta`
#
INSERT INTO `wpwyp2810161505_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin_wypozycczalnia'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wpwyp2810161505_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wpwyp2810161505_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'black_studio_tinymce_widget'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:2:{s:64:"66f3984cc62f770630fcf1fb03035d85de5bea560c668298af7b781cbb22a2c9";a:4:{s:10:"expiration";i:1478869845;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";s:5:"login";i:1477660245;}s:64:"33237267088c5c3caec2c6fe5c69077736ba8ec35cf8570dc9e3f66c7558caaf";a:4:{s:10:"expiration";i:1478870677;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";s:5:"login";i:1477661077;}}'),
(15, 1, 'wpwyp2810161505_dashboard_quick_press_last_post_id', '3'),
(16, 1, 'wpwyp2810161505_user-settings', 'editor=tinymce'),
(17, 1, 'wpwyp2810161505_user-settings-time', '1478192702') ;

#
# End of data contents of table `wpwyp2810161505_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_users`
#

DROP TABLE IF EXISTS `wpwyp2810161505_users`;


#
# Table structure of table `wpwyp2810161505_users`
#

CREATE TABLE `wpwyp2810161505_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_users`
#
INSERT INTO `wpwyp2810161505_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin_wypozyczalnia', '$P$BqCt3f5NgafK1P8Ci3qrqDTmBA1bAP0', 'admin_wypozyczalnia', 'patryk@visibee.pl', '', '2016-10-28 13:10:21', '', 0, 'admin_wypozycczalnia') ;

#
# End of data contents of table `wpwyp2810161505_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

