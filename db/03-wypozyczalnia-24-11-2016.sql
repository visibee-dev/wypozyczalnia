# WordPress MySQL database migration
#
# Generated: Thursday 24. November 2016 11:51 UTC
# Hostname: localhost
# Database: `wypozyczalnia`
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wpwyp2810161505_commentmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_commentmeta`;


#
# Table structure of table `wpwyp2810161505_commentmeta`
#

CREATE TABLE `wpwyp2810161505_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_commentmeta`
#

#
# End of data contents of table `wpwyp2810161505_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_comments`
#

DROP TABLE IF EXISTS `wpwyp2810161505_comments`;


#
# Table structure of table `wpwyp2810161505_comments`
#

CREATE TABLE `wpwyp2810161505_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_comments`
#
INSERT INTO `wpwyp2810161505_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_comments`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_links`
#

DROP TABLE IF EXISTS `wpwyp2810161505_links`;


#
# Table structure of table `wpwyp2810161505_links`
#

CREATE TABLE `wpwyp2810161505_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_links`
#

#
# End of data contents of table `wpwyp2810161505_links`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_options`
#

DROP TABLE IF EXISTS `wpwyp2810161505_options`;


#
# Table structure of table `wpwyp2810161505_options`
#

CREATE TABLE `wpwyp2810161505_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_options`
#
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wypozyczalniacykliniarek.com', 'yes'),
(2, 'home', 'http://wypozyczalniacykliniarek.com', 'yes'),
(3, 'blogname', 'Wypożyczalnia Cykliniarek', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'patryk@visibee.pl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:85:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=52&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:31:"cookie-notice/cookie-notice.php";i:1;s:37:"tinymce-advanced/tinymce-advanced.php";i:2;s:47:"tinymce-custom-styles/tinymce-custom-styles.php";i:3;s:31:"wp-migrate-db/wp-migrate-db.php";i:4;s:23:"wp-smushit/wp-smush.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:91:"\\public_html/wp-content/plugins/black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1;s:54:"\\public_html/wp-content/themes/wypozyczalnia/style.css";i:2;s:68:"\\public_html/wp-content/themes/wypozyczalnia/editor-style-shared.css";i:3;s:61:"\\public_html/wp-content/themes/wypozyczalnia/editor-style.css";i:4;s:0:"";}', 'no'),
(40, 'template', 'wypozyczalnia', 'yes'),
(41, 'stylesheet', 'wypozyczalnia', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:47:"tinymce-custom-styles/tinymce-custom-styles.php";s:13:"tcs_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '52', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'wpwyp2810161505_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'cron', 'a:4:{i:1479993023;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1479993045;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1480003938;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(116, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1477660736;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(136, 'current_theme', '', 'yes'),
(137, 'theme_mods_wypozyczalnia', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:6:"header";i:2;s:10:"header-nav";i:2;}}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'recently_activated', 'a:1:{s:59:"black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1478195638;}', 'yes'),
(145, 'wdev-frash', 'a:3:{s:7:"plugins";a:1:{s:23:"wp-smushit/wp-smush.php";i:1477660862;}s:5:"queue";a:2:{s:32:"7de3619981caadc55f30a002bfb299f6";a:4:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:5:"email";s:7:"show_at";i:1477660862;s:6:"sticky";b:1;}s:32:"fc50097023d0d34c5a66f6cddcf77694";a:3:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:4:"rate";s:7:"show_at";i:1480074236;}}s:4:"done";a:0:{}}', 'no'),
(146, 'wp-smush-version', '2.4.5', 'no'),
(147, 'wp-smush-skip-redirect', '1', 'no'),
(148, 'wp-smush-install-type', 'new', 'no'),
(151, 'wp-smush-hide_upgrade_notice', '1', 'no'),
(185, 'tadv_settings', 'a:6:{s:9:"toolbar_1";s:141:"formatselect,bold,italic,blockquote,subscript,superscript,bullist,numlist,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo,redo";s:9:"toolbar_2";s:110:"styleselect,removeformat,media,image,wp_more,table,wp_help,strikethrough,underline,wp_code,code,indent,outdent";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"options";s:27:"menubar,advlist,contextmenu";s:7:"plugins";s:35:"table,advlist,importcss,contextmenu";}', 'yes'),
(186, 'tadv_admin_settings', 'a:2:{s:7:"options";s:9:"importcss";s:16:"disabled_editors";s:0:"";}', 'yes'),
(187, 'tadv_version', '4000', 'yes'),
(190, 'wp-smush-hide_update_info', '1', 'no'),
(193, 'tcs_addstyledrop', 'a:5:{i:0;a:7:{s:5:"title";s:5:"intro";s:5:"block";s:1:"p";s:7:"classes";s:5:"intro";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:1;a:7:{s:5:"title";s:6:"medium";s:5:"block";s:1:"p";s:7:"classes";s:6:"medium";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:2;a:7:{s:5:"title";s:5:"small";s:5:"block";s:1:"p";s:7:"classes";s:5:"small";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:3;a:7:{s:5:"title";s:2:"lg";s:5:"block";s:1:"p";s:7:"classes";s:2:"lg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:4;a:7:{s:5:"title";s:3:"xlg";s:5:"block";s:1:"p";s:7:"classes";s:3:"xlg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}}', 'yes'),
(196, 'tcs_locstyle', 'themes_directory', 'yes'),
(201, 'widget_black-studio-tinymce', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(253, 'cookie_notice_options', 'a:18:{s:12:"message_text";s:27:"Ta strona używa ciasteczek";s:11:"accept_text";s:2:"Ok";s:8:"see_more";s:3:"yes";s:12:"see_more_opt";a:4:{s:4:"text";s:12:"Więcej info";s:9:"link_type";s:4:"page";s:2:"id";i:45;s:4:"link";s:0:"";}s:11:"link_target";s:6:"_blank";s:11:"refuse_text";s:2:"No";s:11:"refuse_code";s:0:"";s:16:"on_scroll_offset";i:100;s:4:"time";s:5:"month";s:16:"script_placement";s:6:"footer";s:8:"position";s:6:"bottom";s:11:"hide_effect";s:4:"fade";s:9:"css_style";s:4:"none";s:6:"colors";a:2:{s:4:"text";s:7:"#000000";s:3:"bar";s:7:"#ffffff";}s:10:"refuse_opt";s:2:"no";s:9:"on_scroll";s:2:"no";s:19:"deactivation_delete";s:2:"no";s:9:"translate";b:0;}', 'no'),
(254, 'cookie_notice_version', '1.2.36.1', 'no'),
(265, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(329, 'acf_version', '5.4.8', 'yes'),
(354, 'category_children', 'a:0:{}', 'yes') ;

#
# End of data contents of table `wpwyp2810161505_options`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_postmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_postmeta`;


#
# Table structure of table `wpwyp2810161505_postmeta`
#

CREATE TABLE `wpwyp2810161505_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=964 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_postmeta`
#
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1478192562:1'),
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1478344580:1'),
(6, 5, '_wp_page_template', 'page-templates/content-page.php'),
(7, 4, '_wp_trash_meta_status', 'draft'),
(8, 4, '_wp_trash_meta_time', '1478193005'),
(9, 4, '_wp_desired_post_slug', ''),
(10, 2, '_wp_trash_meta_status', 'publish'),
(11, 2, '_wp_trash_meta_time', '1478193007'),
(12, 2, '_wp_desired_post_slug', 'sample-page'),
(13, 35, '_wp_attached_file', '2016/11/cykliniarki.jpg'),
(14, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:825;s:6:"height";i:455;s:4:"file";s:23:"2016/11/cykliniarki.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"cykliniarki-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"cykliniarki-300x165.jpg";s:5:"width";i:300;s:6:"height";i:165;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"cykliniarki-768x424.jpg";s:5:"width";i:768;s:6:"height";i:424;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(15, 35, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.8365642069187098;s:5:"bytes";i:7317;s:11:"size_before";i:93370;s:10:"size_after";i:86053;s:4:"time";d:0.20000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:4:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:9.8800000000000008;s:5:"bytes";i:349;s:11:"size_before";i:3533;s:10:"size_after";i:3184;s:4:"time";d:0.040000000000000001;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:8.9299999999999997;s:5:"bytes";i:695;s:11:"size_before";i:7784;s:10:"size_after";i:7089;s:4:"time";d:0.02;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:11.65;s:5:"bytes";i:3324;s:11:"size_before";i:28532;s:10:"size_after";i:25208;s:4:"time";d:0.059999999999999998;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:5.5099999999999998;s:5:"bytes";i:2949;s:11:"size_before";i:53521;s:10:"size_after";i:50572;s:4:"time";d:0.080000000000000002;}}}'),
(16, 45, '_edit_last', '1'),
(17, 45, '_wp_page_template', 'default'),
(18, 45, '_edit_lock', '1478346498:1'),
(28, 48, '_edit_last', '1'),
(29, 48, '_wp_page_template', 'page-templates/calc.php'),
(30, 48, '_edit_lock', '1478539322:1'),
(31, 52, '_edit_last', '1'),
(32, 52, '_edit_lock', '1479988132:1'),
(33, 52, '_wp_page_template', 'page-templates/home-page.php'),
(34, 54, '_menu_item_type', 'post_type'),
(35, 54, '_menu_item_menu_item_parent', '0'),
(36, 54, '_menu_item_object_id', '52'),
(37, 54, '_menu_item_object', 'page'),
(38, 54, '_menu_item_target', ''),
(39, 54, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(40, 54, '_menu_item_xfn', ''),
(41, 54, '_menu_item_url', ''),
(42, 54, '_menu_item_orphaned', '1479904201'),
(43, 55, '_menu_item_type', 'post_type'),
(44, 55, '_menu_item_menu_item_parent', '0'),
(45, 55, '_menu_item_object_id', '48'),
(46, 55, '_menu_item_object', 'page'),
(47, 55, '_menu_item_target', ''),
(48, 55, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(49, 55, '_menu_item_xfn', ''),
(50, 55, '_menu_item_url', ''),
(51, 55, '_menu_item_orphaned', '1479904204'),
(52, 56, '_menu_item_type', 'post_type'),
(53, 56, '_menu_item_menu_item_parent', '0'),
(54, 56, '_menu_item_object_id', '45'),
(55, 56, '_menu_item_object', 'page'),
(56, 56, '_menu_item_target', ''),
(57, 56, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(58, 56, '_menu_item_xfn', ''),
(59, 56, '_menu_item_url', ''),
(60, 56, '_menu_item_orphaned', '1479904205'),
(61, 57, '_edit_last', '1'),
(62, 57, '_wp_page_template', 'default'),
(63, 57, '_edit_lock', '1479904094:1'),
(64, 59, '_edit_last', '1'),
(65, 59, '_edit_lock', '1479904127:1'),
(66, 59, '_wp_page_template', 'default'),
(67, 60, '_edit_last', '1'),
(68, 60, '_wp_page_template', 'default'),
(69, 60, '_edit_lock', '1479904128:1'),
(70, 61, '_edit_last', '1'),
(71, 61, '_wp_page_template', 'default'),
(72, 61, '_edit_lock', '1479904129:1'),
(73, 62, '_edit_last', '1'),
(74, 62, '_wp_page_template', 'default'),
(75, 62, '_edit_lock', '1479904125:1'),
(76, 67, '_menu_item_type', 'post_type'),
(77, 67, '_menu_item_menu_item_parent', '0'),
(78, 67, '_menu_item_object_id', '62'),
(79, 67, '_menu_item_object', 'page'),
(80, 67, '_menu_item_target', ''),
(81, 67, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(82, 67, '_menu_item_xfn', ''),
(83, 67, '_menu_item_url', ''),
(85, 68, '_menu_item_type', 'post_type'),
(86, 68, '_menu_item_menu_item_parent', '0'),
(87, 68, '_menu_item_object_id', '61'),
(88, 68, '_menu_item_object', 'page'),
(89, 68, '_menu_item_target', ''),
(90, 68, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(91, 68, '_menu_item_xfn', ''),
(92, 68, '_menu_item_url', ''),
(94, 69, '_menu_item_type', 'post_type'),
(95, 69, '_menu_item_menu_item_parent', '0'),
(96, 69, '_menu_item_object_id', '60'),
(97, 69, '_menu_item_object', 'page'),
(98, 69, '_menu_item_target', ''),
(99, 69, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(100, 69, '_menu_item_xfn', ''),
(101, 69, '_menu_item_url', ''),
(103, 70, '_menu_item_type', 'post_type'),
(104, 70, '_menu_item_menu_item_parent', '0'),
(105, 70, '_menu_item_object_id', '59'),
(106, 70, '_menu_item_object', 'page'),
(107, 70, '_menu_item_target', ''),
(108, 70, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(109, 70, '_menu_item_xfn', ''),
(110, 70, '_menu_item_url', ''),
(112, 71, '_menu_item_type', 'post_type'),
(113, 71, '_menu_item_menu_item_parent', '0') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(114, 71, '_menu_item_object_id', '57'),
(115, 71, '_menu_item_object', 'page'),
(116, 71, '_menu_item_target', ''),
(117, 71, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(118, 71, '_menu_item_xfn', ''),
(119, 71, '_menu_item_url', ''),
(121, 72, '_menu_item_type', 'post_type'),
(122, 72, '_menu_item_menu_item_parent', '0'),
(123, 72, '_menu_item_object_id', '52'),
(124, 72, '_menu_item_object', 'page'),
(125, 72, '_menu_item_target', ''),
(126, 72, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(127, 72, '_menu_item_xfn', ''),
(128, 72, '_menu_item_url', ''),
(130, 73, '_edit_last', '1'),
(131, 73, '_edit_lock', '1479987991:1'),
(132, 80, '_wp_attached_file', '2016/11/fig.png'),
(133, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1048;s:6:"height";i:562;s:4:"file";s:15:"2016/11/fig.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"fig-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:15:"fig-300x161.png";s:5:"width";i:300;s:6:"height";i:161;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:15:"fig-768x412.png";s:5:"width";i:768;s:6:"height";i:412;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:16:"fig-1024x549.png";s:5:"width";i:1024;s:6:"height";i:549;s:9:"mime-type";s:9:"image/png";}s:21:"section-background-sm";a:4:{s:4:"file";s:15:"fig-992x532.png";s:5:"width";i:992;s:6:"height";i:532;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(134, 81, '_wp_attached_file', '2016/11/main.jpg'),
(135, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1550;s:6:"height";i:820;s:4:"file";s:16:"2016/11/main.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"main-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"main-300x159.jpg";s:5:"width";i:300;s:6:"height";i:159;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"main-768x406.jpg";s:5:"width";i:768;s:6:"height";i:406;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"main-1024x542.jpg";s:5:"width";i:1024;s:6:"height";i:542;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:16:"main-992x525.jpg";s:5:"width";i:992;s:6:"height";i:525;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:17:"main-1200x635.jpg";s:5:"width";i:1200;s:6:"height";i:635;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(136, 81, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:2.9325190503700034;s:5:"bytes";i:7462;s:11:"size_before";i:254457;s:10:"size_after";i:246995;s:4:"time";d:0.34000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:6:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:4.0499999999999998;s:5:"bytes";i:232;s:11:"size_before";i:5726;s:10:"size_after";i:5494;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:2.9399999999999999;s:5:"bytes";i:263;s:11:"size_before";i:8945;s:10:"size_after";i:8682;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:2.8799999999999999;s:5:"bytes";i:1135;s:11:"size_before";i:39421;s:10:"size_after";i:38286;s:4:"time";d:0.059999999999999998;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:2.8500000000000001;s:5:"bytes";i:1762;s:11:"size_before";i:61841;s:10:"size_after";i:60079;s:4:"time";d:0.040000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:2.98;s:5:"bytes";i:1765;s:11:"size_before";i:59160;s:10:"size_after";i:57395;s:4:"time";d:0.080000000000000002;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:2.8999999999999999;s:5:"bytes";i:2305;s:11:"size_before";i:79364;s:10:"size_after";i:77059;s:4:"time";d:0.12;}}}'),
(137, 80, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:1973097;s:10:"size_after";i:1973097;s:4:"time";d:1.9199999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:5:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:46174;s:10:"size_after";i:46174;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:87908;s:10:"size_after";i:87908;s:4:"time";d:0.16;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:447161;s:10:"size_after";i:447161;s:4:"time";d:0.34999999999999998;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:712145;s:10:"size_after";i:712145;s:4:"time";d:0.67000000000000004;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:679709;s:10:"size_after";i:679709;s:4:"time";d:0.70999999999999996;}}}'),
(138, 52, 'content_0_section_intro', ''),
(139, 52, '_content_0_section_intro', 'field_5835d59224057'),
(140, 52, 'content_0_content_0_image', ''),
(141, 52, '_content_0_content_0_image', 'field_5835d5b924059'),
(142, 52, 'content_0_content_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(143, 52, '_content_0_content_0_iframe', 'field_5835d5ea2405b'),
(144, 52, 'content_0_content_0_caption', ''),
(145, 52, '_content_0_content_0_caption', 'field_5835d5ca2405a'),
(146, 52, 'content_0_content', 'a:1:{i:0;s:5:"media";}'),
(147, 52, '_content_0_content', 'field_5835d59b24058'),
(148, 52, 'content', '4'),
(149, 52, '_content', 'field_5835d51f24055'),
(168, 52, 'content_0_layout', 'a:1:{i:0;s:12:"tani-wynajem";}'),
(169, 52, '_content_0_layout', 'field_5835d59b24058'),
(190, 52, 'content_1_section_intro', '<h1>Jak to działa?</h1>\r\nLorem ipsum dolor sit amet enim. Etiam ullamcorper convallis ac, laoreet enim.\r\nSuspendisse a pellentesque dui, non felis.'),
(191, 52, '_content_1_section_intro', 'field_5835d59224057'),
(192, 52, 'content_1_layout_0_image', ''),
(193, 52, '_content_1_layout_0_image', 'field_5835d5b924059'),
(194, 52, 'content_1_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(195, 52, '_content_1_layout_0_iframe', 'field_5835d5ea2405b'),
(196, 52, 'content_1_layout_0_caption', ''),
(197, 52, '_content_1_layout_0_caption', 'field_5835d5ca2405a'),
(198, 52, 'content_1_layout', 'a:1:{i:0;s:13:"jak-to-dziala";}'),
(199, 52, '_content_1_layout', 'field_5835d59b24058'),
(320, 52, 'content_0_section_bg', 'img'),
(321, 52, '_content_0_section_bg', 'field_5836c16c062d1'),
(322, 52, 'content_1_section_bg', 'black'),
(323, 52, '_content_1_section_bg', 'field_5836c16c062d1'),
(392, 52, 'content_2_section_bg', 'white'),
(393, 52, '_content_2_section_bg', 'field_5836c16c062d1'),
(394, 52, 'content_2_section_intro', '<h1>Każdy może\r\ncyklinować</h1>\r\nNasze maszyny są tak proste w obsłudze, że nie\r\npotrzebujesz nawet specjalistycznego szkolenia.\r\nWystarczy Ci krótki instruktarz, który\r\nprzygotowaliśmy w formie wideo na YouTube.\r\n\r\n<span class="btn--fill btn btn--primary">Dowiedz się więcej</span>'),
(395, 52, '_content_2_section_intro', 'field_5835d59224057'),
(396, 52, 'content_2_layout_0_image', ''),
(397, 52, '_content_2_layout_0_image', 'field_5835d5b924059'),
(398, 52, 'content_2_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(399, 52, '_content_2_layout_0_iframe', 'field_5835d5ea2405b'),
(400, 52, 'content_2_layout_0_caption', ''),
(401, 52, '_content_2_layout_0_caption', 'field_5835d5ca2405a'),
(402, 52, 'content_2_layout', 'a:1:{i:0;s:5:"media";}'),
(403, 52, '_content_2_layout', 'field_5835d59b24058'),
(636, 52, 'content_0_section_bg_img', '81'),
(637, 52, '_content_0_section_bg_img', 'field_5836c1c4062d2'),
(638, 52, 'content_1_layout_0_step_0_icon', 'mouse'),
(639, 52, '_content_1_layout_0_step_0_icon', 'field_5836c4722cf71'),
(640, 52, 'content_1_layout_0_step_0_label', 'Zarezerwuj online'),
(641, 52, '_content_1_layout_0_step_0_label', 'field_5836c4db2cf72'),
(642, 52, 'content_1_layout_0_step_1_icon', 'trolley'),
(643, 52, '_content_1_layout_0_step_1_icon', 'field_5836c4722cf71'),
(644, 52, 'content_1_layout_0_step_1_label', 'Kurier dostarczy cykliniarkę pod Twój dom'),
(645, 52, '_content_1_layout_0_step_1_label', 'field_5836c4db2cf72'),
(646, 52, 'content_1_layout_0_step_2_icon', 'cykliniarka'),
(647, 52, '_content_1_layout_0_step_2_icon', 'field_5836c4722cf71'),
(648, 52, 'content_1_layout_0_step_2_label', 'Wycyklinuj podłogę'),
(649, 52, '_content_1_layout_0_step_2_label', 'field_5836c4db2cf72'),
(650, 52, 'content_1_layout_0_step_3_icon', 'truck'),
(651, 52, '_content_1_layout_0_step_3_icon', 'field_5836c4722cf71'),
(652, 52, 'content_1_layout_0_step_3_label', 'Kurier odbierze cykliniarkę od Ciebie'),
(653, 52, '_content_1_layout_0_step_3_label', 'field_5836c4db2cf72'),
(654, 52, 'content_1_layout_0_step', '4'),
(655, 52, '_content_1_layout_0_step', 'field_5836c4452cf70'),
(656, 52, 'content_3_section_bg', 'white'),
(657, 52, '_content_3_section_bg', 'field_5836c16c062d1'),
(658, 52, 'content_3_section_intro', '<h1>Zaoszczędzisz\r\nnawet 400zł*</h1>\r\nSamodzielny remont podłogi oznacza,\r\nże sporo pieniędzy zostanie w Twojej kieszeni.\r\nCyklinowanie przez fachowaca pokoju\r\no powierzchni 10 m<sup>2</sup> to koszt ok. 400 zł.'),
(659, 52, '_content_3_section_intro', 'field_5835d59224057'),
(660, 52, 'content_3_layout_0_image', '80'),
(661, 52, '_content_3_layout_0_image', 'field_5835d5b924059'),
(662, 52, 'content_3_layout_0_iframe', ''),
(663, 52, '_content_3_layout_0_iframe', 'field_5835d5ea2405b'),
(664, 52, 'content_3_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(665, 52, '_content_3_layout_0_caption', 'field_5835d5ca2405a'),
(666, 52, 'content_3_layout', 'a:1:{i:0;s:5:"media";}'),
(667, 52, '_content_3_layout', 'field_5835d59b24058'),
(740, 52, 'content_0_layout_0_content', '<h1>Tani\r\nwynajem od\r\n<strong>50zł\r\n</strong>za dobę</h1>\r\n<span class="btn btn--primary btn--fill">Sprawdź ceny</span>'),
(741, 52, '_content_0_layout_0_content', 'field_5836ca8eb9160') ;

#
# End of data contents of table `wpwyp2810161505_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_posts`
#

DROP TABLE IF EXISTS `wpwyp2810161505_posts`;


#
# Table structure of table `wpwyp2810161505_posts`
#

CREATE TABLE `wpwyp2810161505_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_posts`
#
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', '', 0, 'http://wypozyczalniacykliniarek.com/?p=1', 0, 'post', '', 1),
(2, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://wypozyczalniacykliniarek.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2016-11-03 17:10:07', '2016-11-03 17:10:07', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=2', 0, 'page', '', 0),
(4, 1, '2016-11-03 16:12:25', '2016-11-03 16:12:25', '', 'Podstrona opisowa', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2016-11-03 17:10:05', '2016-11-03 17:10:05', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=4', 0, 'page', '', 0),
(5, 1, '2016-11-03 16:13:12', '2016-11-03 16:13:12', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\n<img class="alignnone wp-image-35 size-full" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg" alt="cykliniarki" width="825" height="455" />\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<a class="btn btn--primary btn--fill" href="#">Button</a>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>', 'Podstrona opisowa', '', 'publish', 'closed', 'closed', '', 'strona-opisowa', '', '', '2016-11-04 17:48:10', '2016-11-04 17:48:10', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=5', 0, 'page', '', 0),
(35, 1, '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 'cykliniarki', '', 'inherit', 'open', 'closed', '', 'cykliniarki', '', '', '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 5, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 'Strona o ciasteczkach', '', 'publish', 'closed', 'closed', '', 'strona-o-ciasteczkach', '', '', '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=45', 0, 'page', '', 0),
(48, 1, '2016-11-07 17:22:44', '2016-11-07 17:22:44', '', 'Kalkulator', '', 'publish', 'closed', 'closed', '', 'kalkulator', '', '', '2016-11-07 17:24:08', '2016-11-07 17:24:08', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=48', 0, 'page', '', 0),
(51, 1, '2016-11-23 09:52:52', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-11-23 09:52:52', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=51', 0, 'post', '', 0),
(52, 1, '2016-11-23 10:00:37', '2016-11-23 10:00:37', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2016-11-24 11:47:00', '2016-11-24 11:47:00', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=52', 0, 'page', '', 0),
(54, 1, '2016-11-23 12:30:01', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:01', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2016-11-23 12:30:03', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:03', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=55', 1, 'nav_menu_item', '', 0),
(56, 1, '2016-11-23 12:30:05', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:05', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 'O nas', '', 'publish', 'closed', 'closed', '', 'o-nas', '', '', '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=57', 0, 'page', '', 0),
(59, 1, '2016-11-23 12:30:45', '2016-11-23 12:30:45', '', 'Cykliniarki', '', 'publish', 'closed', 'closed', '', 'cykliniarki', '', '', '2016-11-23 12:30:45', '2016-11-23 12:30:45', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=59', 0, 'page', '', 0),
(60, 1, '2016-11-23 12:30:51', '2016-11-23 12:30:51', '', 'Produkty', '', 'publish', 'closed', 'closed', '', 'produkty', '', '', '2016-11-23 12:30:51', '2016-11-23 12:30:51', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=60', 0, 'page', '', 0),
(61, 1, '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 'Kontakt', '', 'publish', 'closed', 'closed', '', 'kontakt', '', '', '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=61', 0, 'page', '', 0),
(62, 1, '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 'Zarezerwuj', '', 'publish', 'closed', 'closed', '', 'zarezerwuj', '', '', '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=62', 0, 'page', '', 0),
(67, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=67', 6, 'nav_menu_item', '', 0),
(68, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=68', 5, 'nav_menu_item', '', 0),
(69, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=69', 4, 'nav_menu_item', '', 0),
(70, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=70', 3, 'nav_menu_item', '', 0),
(71, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=71', 2, 'nav_menu_item', '', 0),
(72, 1, '2016-11-23 12:31:52', '2016-11-23 12:31:52', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2016-11-23 12:31:52', '2016-11-23 12:31:52', '', 0, 'http://wypozyczalniacykliniarek.com/?p=72', 1, 'nav_menu_item', '', 0),
(73, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"page";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Main', 'main', 'publish', 'closed', 'closed', '', 'group_5835d5033efe3', '', '', '2016-11-24 11:48:29', '2016-11-24 11:48:29', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=73', 0, 'acf-field-group', '', 0),
(74, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:19:"field_5835d59b24058";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5835d51f24055', '', '', '2016-11-24 11:48:29', '2016-11-24 11:48:29', '', 73, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=74', 0, 'acf-field', '', 0),
(75, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_5835d59224057', '', '', '2016-11-24 11:48:29', '2016-11-24 11:48:29', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=75', 2, 'acf-field', '', 0),
(76, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:3:{i:0;a:6:{s:3:"key";s:13:"5835d5a00eafe";s:5:"label";s:5:"Media";s:4:"name";s:5:"media";s:7:"display";s:5:"table";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"5836c41b2cf6f";s:5:"label";s:15:"Jak to działa?";s:4:"name";s:13:"jak-to-dziala";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:2;a:6:{s:3:"key";s:13:"5836ca79b915f";s:5:"label";s:12:"Tani wynajem";s:4:"name";s:12:"tani-wynajem";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_5835d59b24058', '', '', '2016-11-24 11:44:56', '2016-11-24 11:44:56', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=76', 3, 'acf-field', '', 0),
(77, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:16:{s:4:"type";s:5:"image";s:12:"instructions";s:17:"Dodaj zdjęcie...";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5835d5b924059', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=77', 0, 'acf-field', '', 0),
(78, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:34:"... lub kod iframe do pliku wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Iframe', 'iframe', 'publish', 'closed', 'closed', '', 'field_5835d5ea2405b', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=78', 1, 'acf-field', '', 0),
(79, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:27:"Podpis pod zdjęciem/wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_5835d5ca2405a', '', '', '2016-11-24 10:46:23', '2016-11-24 10:46:23', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=79', 2, 'acf-field', '', 0),
(80, 1, '2016-11-23 17:49:26', '2016-11-23 17:49:26', '', 'fig', '', 'inherit', 'open', 'closed', '', 'fig', '', '', '2016-11-24 09:26:12', '2016-11-24 09:26:12', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/fig.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2016-11-23 17:49:30', '2016-11-23 17:49:30', '', 'main', '', 'inherit', 'open', 'closed', '', 'main', '', '', '2016-11-24 11:06:42', '2016-11-24 11:06:42', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/main.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:4:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:3:"red";s:3:"red";s:3:"img";s:3:"img";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_5836c16c062d1', '', '', '2016-11-24 10:33:08', '2016-11-24 10:33:08', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=89', 0, 'acf-field', '', 0),
(90, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_5836c16c062d1";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_5836c1c4062d2', '', '', '2016-11-24 10:33:08', '2016-11-24 10:33:08', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=90', 1, 'acf-field', '', 0),
(94, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836c41b2cf6f";s:9:"collapsed";s:0:"";s:3:"min";i:4;s:3:"max";i:4;s:6:"layout";s:3:"row";s:12:"button_label";s:8:"Add Step";}', 'Step', 'step', 'publish', 'closed', 'closed', '', 'field_5836c4452cf70', '', '', '2016-11-24 10:54:45', '2016-11-24 10:54:45', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=94', 0, 'acf-field', '', 0),
(95, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:12:{s:9:"logo-mark";s:9:"logo-mark";s:11:"cykliniarka";s:11:"cykliniarka";s:5:"email";s:5:"email";s:5:"mouse";s:5:"mouse";s:6:"pencil";s:6:"pencil";s:5:"phone";s:5:"phone";s:3:"pin";s:3:"pin";s:4:"safe";s:4:"safe";s:4:"star";s:4:"star";s:7:"trolley";s:7:"trolley";s:5:"truck";s:5:"truck";s:10:"truck-mapa";s:10:"truck-mapa";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_5836c4722cf71', '', '', '2016-11-24 10:47:15', '2016-11-24 10:47:15', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=95', 0, 'acf-field', '', 0),
(96, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";i:2;s:9:"new_lines";s:2:"br";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_5836c4db2cf72', '', '', '2016-11-24 10:54:45', '2016-11-24 10:54:45', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=96', 1, 'acf-field', '', 0),
(101, 1, '2016-11-24 11:10:28', '2016-11-24 11:10:28', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836ca79b915f";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5836ca8eb9160', '', '', '2016-11-24 11:10:28', '2016-11-24 11:10:28', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=101', 0, 'acf-field', '', 0) ;

#
# End of data contents of table `wpwyp2810161505_posts`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_relationships`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_relationships`;


#
# Table structure of table `wpwyp2810161505_term_relationships`
#

CREATE TABLE `wpwyp2810161505_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_relationships`
#
INSERT INTO `wpwyp2810161505_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(67, 2, 0),
(68, 2, 0),
(69, 2, 0),
(70, 2, 0),
(71, 2, 0),
(72, 2, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_taxonomy`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_taxonomy`;


#
# Table structure of table `wpwyp2810161505_term_taxonomy`
#

CREATE TABLE `wpwyp2810161505_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_taxonomy`
#
INSERT INTO `wpwyp2810161505_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6) ;

#
# End of data contents of table `wpwyp2810161505_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_termmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_termmeta`;


#
# Table structure of table `wpwyp2810161505_termmeta`
#

CREATE TABLE `wpwyp2810161505_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_termmeta`
#

#
# End of data contents of table `wpwyp2810161505_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_terms`
#

DROP TABLE IF EXISTS `wpwyp2810161505_terms`;


#
# Table structure of table `wpwyp2810161505_terms`
#

CREATE TABLE `wpwyp2810161505_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_terms`
#
INSERT INTO `wpwyp2810161505_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Header', 'header', 0) ;

#
# End of data contents of table `wpwyp2810161505_terms`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_usermeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_usermeta`;


#
# Table structure of table `wpwyp2810161505_usermeta`
#

CREATE TABLE `wpwyp2810161505_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_usermeta`
#
INSERT INTO `wpwyp2810161505_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin_wypozycczalnia'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wpwyp2810161505_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wpwyp2810161505_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'black_studio_tinymce_widget'),
(13, 1, 'show_welcome_panel', '1'),
(15, 1, 'wpwyp2810161505_dashboard_quick_press_last_post_id', '51'),
(16, 1, 'wpwyp2810161505_user-settings', 'editor=tinymce&libraryContent=browse'),
(17, 1, 'wpwyp2810161505_user-settings-time', '1478280645'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'nav_menu_recently_edited', '2'),
(21, 1, 'session_tokens', 'a:1:{s:64:"aa57f493d92a48619a50e126985e37ac1fbd9880a383fbb94f04246e3b187ecc";a:4:{s:10:"expiration";i:1481131709;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";s:5:"login";i:1479922109;}}'),
(22, 1, 'acf_user_settings', 'a:1:{s:29:"collapsed_field_5835d51f24055";s:3:"2,3";}') ;

#
# End of data contents of table `wpwyp2810161505_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_users`
#

DROP TABLE IF EXISTS `wpwyp2810161505_users`;


#
# Table structure of table `wpwyp2810161505_users`
#

CREATE TABLE `wpwyp2810161505_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_users`
#
INSERT INTO `wpwyp2810161505_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin_wypozyczalnia', '$P$BqCt3f5NgafK1P8Ci3qrqDTmBA1bAP0', 'admin_wypozyczalnia', 'patryk@visibee.pl', '', '2016-10-28 13:10:21', '', 0, 'admin_wypozycczalnia') ;

#
# End of data contents of table `wpwyp2810161505_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

