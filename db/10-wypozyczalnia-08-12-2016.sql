# WordPress MySQL database migration
#
# Generated: Thursday 8. December 2016 18:36 UTC
# Hostname: localhost
# Database: `wypozyczalnia`
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wpwyp2810161505_commentmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_commentmeta`;


#
# Table structure of table `wpwyp2810161505_commentmeta`
#

CREATE TABLE `wpwyp2810161505_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_commentmeta`
#

#
# End of data contents of table `wpwyp2810161505_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_comments`
#

DROP TABLE IF EXISTS `wpwyp2810161505_comments`;


#
# Table structure of table `wpwyp2810161505_comments`
#

CREATE TABLE `wpwyp2810161505_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_comments`
#
INSERT INTO `wpwyp2810161505_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(3, 354, 'WooCommerce', '', '', '', '2016-12-08 17:58:49', '2016-12-08 17:58:49', 'Czekanie na przelew Status zamówienia zmieniony z Oczekujące na płatność na Wstrzymane (oczekujące na płatność).', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 354, 'WooCommerce', '', '', '', '2016-12-08 17:58:52', '2016-12-08 17:58:52', 'Ilość produktu 315 została zmniejszona z 10 do 10.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 355, 'WooCommerce', '', '', '', '2016-12-08 18:01:05', '2016-12-08 18:01:05', 'Czekanie na przelew Status zamówienia zmieniony z Oczekujące na płatność na Wstrzymane (oczekujące na płatność).', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(6, 355, 'WooCommerce', '', '', '', '2016-12-08 18:01:08', '2016-12-08 18:01:08', 'Ilość produktu 315 została zmniejszona z 10 do 10.', 0, '1', 'WooCommerce', 'order_note', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_comments`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_links`
#

DROP TABLE IF EXISTS `wpwyp2810161505_links`;


#
# Table structure of table `wpwyp2810161505_links`
#

CREATE TABLE `wpwyp2810161505_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_links`
#

#
# End of data contents of table `wpwyp2810161505_links`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_options`
#

DROP TABLE IF EXISTS `wpwyp2810161505_options`;


#
# Table structure of table `wpwyp2810161505_options`
#

CREATE TABLE `wpwyp2810161505_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_options`
#
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wypozyczalniacykliniarek.com', 'yes'),
(2, 'home', 'http://wypozyczalniacykliniarek.com', 'yes'),
(3, 'blogname', 'Wypożyczalnia Cykliniarek', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'patryk@visibee.pl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:192:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:11:"produkty/?$";s:27:"index.php?post_type=product";s:41:"produkty/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:36:"produkty/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:28:"produkty/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:57:"kategoria-produktu/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:52:"kategoria-produktu/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:33:"kategoria-produktu/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:45:"kategoria-produktu/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:27:"kategoria-produktu/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:53:"tag-produktu/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:48:"tag-produktu/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:29:"tag-produktu/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:41:"tag-produktu/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:23:"tag-produktu/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"produkt/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"produkt/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"produkt/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produkt/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produkt/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"produkt/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"produkt/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"produkt/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"produkt/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"produkt/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"produkt/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"produkt/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"produkt/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"produkt/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"produkt/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"produkt/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"produkt/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"produkt/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"produkt/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produkt/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produkt/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"produkt/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"product_variation/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"product_variation/([^/]+)/embed/?$";s:50:"index.php?product_variation=$matches[1]&embed=true";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"product_variation/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"product_variation/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"shop_order_refund/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"shop_order_refund/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"shop_order_refund/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"shop_order_refund/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"shop_order_refund/([^/]+)/embed/?$";s:50:"index.php?shop_order_refund=$matches[1]&embed=true";s:38:"shop_order_refund/([^/]+)/trackback/?$";s:44:"index.php?shop_order_refund=$matches[1]&tb=1";s:46:"shop_order_refund/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&paged=$matches[2]";s:53:"shop_order_refund/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&cpage=$matches[2]";s:43:"shop_order_refund/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?shop_order_refund=$matches[1]&wc-api=$matches[3]";s:49:"shop_order_refund/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"shop_order_refund/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"shop_order_refund/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?shop_order_refund=$matches[1]&page=$matches[2]";s:34:"shop_order_refund/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"shop_order_refund/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"shop_order_refund/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"shop_order_refund/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=52&cpage=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:31:"cookie-notice/cookie-notice.php";i:1;s:67:"easy-booking-availability-check/easy-booking-availability-check.php";i:2;s:57:"easy-booking-disable-dates/easy-booking-disable-dates.php";i:3;s:37:"tinymce-advanced/tinymce-advanced.php";i:4;s:47:"tinymce-custom-styles/tinymce-custom-styles.php";i:5;s:60:"woocommerce-easy-booking-system/woocommerce-easy-booking.php";i:6;s:27:"woocommerce/woocommerce.php";i:7;s:31:"wp-migrate-db/wp-migrate-db.php";i:8;s:23:"wp-smushit/wp-smush.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:91:"\\public_html/wp-content/plugins/black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1;s:54:"\\public_html/wp-content/themes/wypozyczalnia/style.css";i:2;s:68:"\\public_html/wp-content/themes/wypozyczalnia/editor-style-shared.css";i:3;s:61:"\\public_html/wp-content/themes/wypozyczalnia/editor-style.css";i:4;s:0:"";}', 'no'),
(40, 'template', 'wypozyczalnia', 'yes'),
(41, 'stylesheet', 'wypozyczalnia', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:47:"tinymce-custom-styles/tinymce-custom-styles.php";s:13:"tcs_uninstall";s:24:"booking-system/dopbs.php";s:15:"DOPBSPUninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '52', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'wpwyp2810161505_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:131:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'cron', 'a:8:{i:1481241600;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1481244448;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1481245823;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1481287648;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1481289045;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1481299938;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1483617600;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(116, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1477660736;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(136, 'current_theme', '', 'yes'),
(137, 'theme_mods_wypozyczalnia', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:6:"header";i:2;s:10:"header-nav";i:2;s:10:"footer-nav";i:3;}}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'recently_activated', 'a:2:{s:66:"booking-and-rental-system-woocommerce/redq-rental-and-bookings.php";i:1480955545;s:24:"booking-system/dopbs.php";i:1480948646;}', 'yes'),
(145, 'wdev-frash', 'a:3:{s:7:"plugins";a:1:{s:23:"wp-smushit/wp-smush.php";i:1477660862;}s:5:"queue";a:1:{s:32:"fc50097023d0d34c5a66f6cddcf77694";a:4:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:4:"rate";s:7:"show_at";i:1481027691;s:6:"sticky";b:1;}}s:4:"done";a:1:{i:0;a:6:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:5:"email";s:7:"show_at";i:1477660862;s:5:"state";s:6:"ignore";s:4:"hash";s:32:"fc50097023d0d34c5a66f6cddcf77694";s:10:"handled_at";i:1480941295;}}}', 'no'),
(146, 'wp-smush-version', '2.4.5', 'no'),
(147, 'wp-smush-skip-redirect', '1', 'no'),
(148, 'wp-smush-install-type', 'new', 'no'),
(151, 'wp-smush-hide_upgrade_notice', '1', 'no'),
(185, 'tadv_settings', 'a:6:{s:9:"toolbar_1";s:141:"formatselect,bold,italic,blockquote,subscript,superscript,bullist,numlist,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo,redo";s:9:"toolbar_2";s:110:"styleselect,removeformat,media,image,wp_more,table,wp_help,strikethrough,underline,wp_code,code,indent,outdent";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"options";s:27:"menubar,advlist,contextmenu";s:7:"plugins";s:35:"table,advlist,importcss,contextmenu";}', 'yes'),
(186, 'tadv_admin_settings', 'a:2:{s:7:"options";s:9:"importcss";s:16:"disabled_editors";s:0:"";}', 'yes'),
(187, 'tadv_version', '4000', 'yes'),
(190, 'wp-smush-hide_update_info', '1', 'no'),
(193, 'tcs_addstyledrop', 'a:5:{i:0;a:7:{s:5:"title";s:5:"intro";s:5:"block";s:1:"p";s:7:"classes";s:5:"intro";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:1;a:7:{s:5:"title";s:6:"medium";s:5:"block";s:1:"p";s:7:"classes";s:6:"medium";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:2;a:7:{s:5:"title";s:5:"small";s:5:"block";s:1:"p";s:7:"classes";s:5:"small";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:3;a:7:{s:5:"title";s:2:"lg";s:5:"block";s:1:"p";s:7:"classes";s:2:"lg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:4;a:7:{s:5:"title";s:3:"xlg";s:5:"block";s:1:"p";s:7:"classes";s:3:"xlg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}}', 'yes'),
(196, 'tcs_locstyle', 'themes_directory', 'yes'),
(201, 'widget_black-studio-tinymce', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(253, 'cookie_notice_options', 'a:18:{s:12:"message_text";s:27:"Ta strona używa ciasteczek";s:11:"accept_text";s:2:"Ok";s:8:"see_more";s:3:"yes";s:12:"see_more_opt";a:4:{s:4:"text";s:12:"Więcej info";s:9:"link_type";s:4:"page";s:2:"id";i:45;s:4:"link";s:0:"";}s:11:"link_target";s:6:"_blank";s:11:"refuse_text";s:2:"No";s:11:"refuse_code";s:0:"";s:16:"on_scroll_offset";i:100;s:4:"time";s:5:"month";s:16:"script_placement";s:6:"footer";s:8:"position";s:6:"bottom";s:11:"hide_effect";s:4:"fade";s:9:"css_style";s:4:"none";s:6:"colors";a:2:{s:4:"text";s:7:"#000000";s:3:"bar";s:7:"#ffffff";}s:10:"refuse_opt";s:2:"no";s:9:"on_scroll";s:2:"no";s:19:"deactivation_delete";s:2:"no";s:9:"translate";b:0;}', 'no'),
(254, 'cookie_notice_version', '1.2.36.1', 'no'),
(265, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(329, 'acf_version', '5.4.8', 'yes'),
(354, 'category_children', 'a:0:{}', 'yes'),
(431, 'woocommerce_default_country', 'PL', 'yes'),
(432, 'woocommerce_allowed_countries', 'specific', 'yes'),
(433, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(434, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"PL";}', 'yes'),
(435, 'woocommerce_ship_to_countries', 'specific', 'yes'),
(436, 'woocommerce_specific_ship_to_countries', 'a:1:{i:0;s:2:"PL";}', 'yes'),
(437, 'woocommerce_default_customer_address', 'base', 'yes'),
(438, 'woocommerce_calc_taxes', 'yes', 'yes'),
(439, 'woocommerce_demo_store', 'no', 'yes'),
(440, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(441, 'woocommerce_currency', 'PLN', 'yes'),
(442, 'woocommerce_currency_pos', 'right', 'yes'),
(443, 'woocommerce_price_thousand_sep', '.', 'yes'),
(444, 'woocommerce_price_decimal_sep', ',', 'yes'),
(445, 'woocommerce_price_num_decimals', '0', 'yes'),
(446, 'woocommerce_weight_unit', 'kg', 'yes'),
(447, 'woocommerce_dimension_unit', 'cm', 'yes'),
(448, 'woocommerce_enable_review_rating', 'no', 'yes'),
(449, 'woocommerce_review_rating_required', 'yes', 'no'),
(450, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(451, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(452, 'woocommerce_shop_page_id', '60', 'yes'),
(453, 'woocommerce_shop_page_display', '', 'yes'),
(454, 'woocommerce_category_archive_display', '', 'yes'),
(455, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(456, 'woocommerce_cart_redirect_after_add', 'yes', 'yes'),
(457, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(458, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(459, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:1;}', 'yes'),
(460, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:1;}', 'yes'),
(461, 'woocommerce_enable_lightbox', 'no', 'yes'),
(462, 'woocommerce_manage_stock', 'yes', 'yes'),
(463, 'woocommerce_hold_stock_minutes', '', 'no'),
(464, 'woocommerce_notify_low_stock', 'yes', 'no'),
(465, 'woocommerce_notify_no_stock', 'yes', 'no'),
(466, 'woocommerce_stock_email_recipient', 'patryk@visibee.pl', 'no'),
(467, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(468, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(469, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(470, 'woocommerce_stock_format', 'no_amount', 'yes'),
(471, 'woocommerce_file_download_method', 'force', 'no'),
(472, 'woocommerce_downloads_require_login', 'no', 'no'),
(473, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(474, 'woocommerce_prices_include_tax', 'yes', 'yes'),
(475, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(476, 'woocommerce_shipping_tax_class', '', 'yes'),
(477, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(478, 'woocommerce_tax_classes', 'Reduced Rate\r\nZero Rate', 'yes'),
(479, 'woocommerce_tax_display_shop', 'incl', 'yes'),
(480, 'woocommerce_tax_display_cart', 'incl', 'no'),
(481, 'woocommerce_price_display_suffix', '', 'yes'),
(482, 'woocommerce_tax_total_display', 'itemized', 'no'),
(483, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(484, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(485, 'woocommerce_ship_to_destination', 'billing', 'no'),
(486, 'woocommerce_enable_coupons', 'yes', 'yes'),
(487, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(488, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(489, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(490, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(491, 'woocommerce_cart_page_id', '191', 'yes'),
(492, 'woocommerce_checkout_page_id', '192', 'yes'),
(493, 'woocommerce_terms_page_id', '142', 'no'),
(494, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(495, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(496, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(497, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(498, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(499, 'woocommerce_myaccount_page_id', '193', 'yes'),
(500, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(501, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(502, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(503, 'woocommerce_registration_generate_username', 'yes', 'no'),
(504, 'woocommerce_registration_generate_password', 'no', 'no') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(505, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(506, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(507, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(508, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(509, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(510, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(511, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(512, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(513, 'woocommerce_email_from_name', 'Wypożyczalnia Cykliniarek', 'no'),
(514, 'woocommerce_email_from_address', 'patryk@visibee.pl', 'no'),
(515, 'woocommerce_email_header_image', '', 'no'),
(516, 'woocommerce_email_footer_text', 'Wypożyczalnia Cykliniarek - Powered by WooCommerce', 'no'),
(517, 'woocommerce_email_base_color', '#557da1', 'no'),
(518, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(519, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(520, 'woocommerce_email_text_color', '#505050', 'no'),
(521, 'woocommerce_api_enabled', 'yes', 'yes'),
(525, 'woocommerce_db_version', '2.6.8', 'yes'),
(526, 'woocommerce_version', '2.6.8', 'yes'),
(527, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(530, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(531, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(532, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(533, 'widget_woocommerce_price_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(534, 'widget_woocommerce_product_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(535, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(536, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(537, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(538, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(539, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(540, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(541, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(545, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(553, 'woocommerce_allow_tracking', 'no', 'yes'),
(571, 'product_cat_children', 'a:0:{}', 'yes'),
(596, 'woocommerce_bacs_settings', 'a:5:{s:7:"enabled";s:3:"yes";s:5:"title";s:20:"Direct Bank Transfer";s:11:"description";s:173:"Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.";s:12:"instructions";s:173:"Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.";s:15:"account_details";s:0:"";}', 'yes'),
(597, 'woocommerce_bacs_accounts', 'a:1:{i:0;a:6:{s:12:"account_name";s:0:"";s:14:"account_number";s:0:"";s:9:"bank_name";s:0:"";s:9:"sort_code";s:0:"";s:4:"iban";s:0:"";s:3:"bic";s:0:"";}}', 'yes'),
(599, 'woocommerce_cheque_settings', 'a:4:{s:7:"enabled";s:2:"no";s:5:"title";s:14:"Check Payments";s:11:"description";s:98:"Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.";s:12:"instructions";s:98:"Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.";}', 'yes'),
(601, 'woocommerce_cod_settings', 'a:6:{s:7:"enabled";s:3:"yes";s:5:"title";s:16:"Cash on Delivery";s:11:"description";s:28:"Pay with cash upon delivery.";s:12:"instructions";s:28:"Pay with cash upon delivery.";s:18:"enable_for_methods";s:0:"";s:18:"enable_for_virtual";s:3:"yes";}', 'yes'),
(603, 'woocommerce_paypal_settings', 'a:18:{s:7:"enabled";s:2:"no";s:5:"title";s:6:"PayPal";s:11:"description";s:85:"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.";s:5:"email";s:17:"patryk@visibee.pl";s:8:"testmode";s:2:"no";s:5:"debug";s:2:"no";s:8:"advanced";s:0:"";s:14:"receiver_email";s:17:"patryk@visibee.pl";s:14:"identity_token";s:0:"";s:14:"invoice_prefix";s:3:"WC-";s:13:"send_shipping";s:2:"no";s:16:"address_override";s:2:"no";s:13:"paymentaction";s:4:"sale";s:10:"page_style";s:0:"";s:11:"api_details";s:0:"";s:12:"api_username";s:19:"admin_wypozyczalnia";s:12:"api_password";s:18:"G$Bu1UCLw!xI0tmMjB";s:13:"api_signature";s:0:"";}', 'yes'),
(605, 'woocommerce_gateway_order', 'a:4:{s:4:"bacs";i:0;s:6:"cheque";i:1;s:3:"cod";i:2;s:6:"paypal";i:3;}', 'yes'),
(643, 'options_content_0_show', '1', 'no'),
(644, '_options_content_0_show', 'field_583c0ba566b90', 'no'),
(645, 'options_content_0_section_bg', 'red', 'no'),
(646, '_options_content_0_section_bg', 'field_583c0ba566f83', 'no'),
(647, 'options_content_0_section_intro', '<h1>Skontaktuj się z nami</h1>', 'no'),
(648, '_options_content_0_section_intro', 'field_583c0ba56774c', 'no'),
(649, 'options_content_0_layout_0_contact_0_icon', 'email', 'no'),
(650, '_options_content_0_layout_0_contact_0_icon', 'field_583c0ba6c5ecd', 'no'),
(651, 'options_content_0_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>', 'no'),
(652, '_options_content_0_layout_0_contact_0_label', 'field_583c0ba6c629f', 'no'),
(653, 'options_content_0_layout_0_contact_1_icon', 'telefon', 'no'),
(654, '_options_content_0_layout_0_contact_1_icon', 'field_583c0ba6c5ecd', 'no'),
(655, 'options_content_0_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>', 'no'),
(656, '_options_content_0_layout_0_contact_1_label', 'field_583c0ba6c629f', 'no'),
(657, 'options_content_0_layout_0_contact_2_icon', 'pinezka', 'no'),
(658, '_options_content_0_layout_0_contact_2_icon', 'field_583c0ba6c5ecd', 'no'),
(659, 'options_content_0_layout_0_contact_2_label', 'Radzymińska 98,\r\n03-574 Warszawa', 'no'),
(660, '_options_content_0_layout_0_contact_2_label', 'field_583c0ba6c629f', 'no'),
(661, 'options_content_0_layout_0_contact', '3', 'no'),
(662, '_options_content_0_layout_0_contact', 'field_583c0ba5ed167', 'no'),
(663, 'options_content_0_layout_0_partners_label', 'Nasz partner:', 'no'),
(664, '_options_content_0_layout_0_partners_label', 'field_583c0ba5ed552', 'no'),
(665, 'options_content_0_layout_0_partners_0_img', '138', 'no'),
(666, '_options_content_0_layout_0_partners_0_img', 'field_583c0ba7362fa', 'no'),
(667, 'options_content_0_layout_0_partners', '1', 'no'),
(668, '_options_content_0_layout_0_partners', 'field_583c0ba5ed955', 'no'),
(669, 'options_content_0_layout_0_cooperators_label', 'Współpracujemy z:', 'no'),
(670, '_options_content_0_layout_0_cooperators_label', 'field_583c0ba5edd30', 'no'),
(671, 'options_content_0_layout_0_cooperators_0_img', '140', 'no'),
(672, '_options_content_0_layout_0_cooperators_0_img', 'field_583c0ba7849e0', 'no'),
(673, 'options_content_0_layout_0_cooperators_1_img', '139', 'no'),
(674, '_options_content_0_layout_0_cooperators_1_img', 'field_583c0ba7849e0', 'no'),
(675, 'options_content_0_layout_0_cooperators', '2', 'no'),
(676, '_options_content_0_layout_0_cooperators', 'field_583c0ba5ee119', 'no'),
(677, 'options_content_0_layout', 'a:1:{i:0;s:6:"footer";}', 'no'),
(678, '_options_content_0_layout', 'field_583c0ba567b49', 'no'),
(679, 'options_content', '1', 'no'),
(680, '_options_content', 'field_583c0ba55100f', 'no'),
(712, 'WPLANG', 'pl_PL', 'yes'),
(718, 'woocommerce_permalinks', 'a:4:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:0:"";}', 'yes'),
(775, 'pa_pre-price_children', 'a:0:{}', 'yes'),
(813, 'options_content--footer_0_show', '1', 'no'),
(814, '_options_content--footer_0_show', 'field_583c0ba566b90', 'no'),
(815, 'options_content--footer_0_section_bg', 'red', 'no'),
(816, '_options_content--footer_0_section_bg', 'field_583c0ba566f83', 'no'),
(817, 'options_content--footer_0_section_intro', '<h1>Skontaktuj się z nami</h1>', 'no'),
(818, '_options_content--footer_0_section_intro', 'field_583c0ba56774c', 'no'),
(819, 'options_content--footer_0_layout_0_contact_0_icon', 'email', 'no'),
(820, '_options_content--footer_0_layout_0_contact_0_icon', 'field_583c0ba6c5ecd', 'no'),
(821, 'options_content--footer_0_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>', 'no'),
(822, '_options_content--footer_0_layout_0_contact_0_label', 'field_583c0ba6c629f', 'no'),
(823, 'options_content--footer_0_layout_0_contact_1_icon', 'telefon', 'no'),
(824, '_options_content--footer_0_layout_0_contact_1_icon', 'field_583c0ba6c5ecd', 'no'),
(825, 'options_content--footer_0_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>', 'no'),
(826, '_options_content--footer_0_layout_0_contact_1_label', 'field_583c0ba6c629f', 'no'),
(827, 'options_content--footer_0_layout_0_contact_2_icon', 'pinezka', 'no'),
(828, '_options_content--footer_0_layout_0_contact_2_icon', 'field_583c0ba6c5ecd', 'no'),
(829, 'options_content--footer_0_layout_0_contact_2_label', 'Radzymińska 98,\r\n03-574 Warszawa', 'no'),
(830, '_options_content--footer_0_layout_0_contact_2_label', 'field_583c0ba6c629f', 'no') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(831, 'options_content--footer_0_layout_0_contact', '3', 'no'),
(832, '_options_content--footer_0_layout_0_contact', 'field_583c0ba5ed167', 'no'),
(833, 'options_content--footer_0_layout_0_partners_label', 'Nasz partner:', 'no'),
(834, '_options_content--footer_0_layout_0_partners_label', 'field_583c0ba5ed552', 'no'),
(835, 'options_content--footer_0_layout_0_partners_0_img', '138', 'no'),
(836, '_options_content--footer_0_layout_0_partners_0_img', 'field_583c0ba7362fa', 'no'),
(837, 'options_content--footer_0_layout_0_partners', '1', 'no'),
(838, '_options_content--footer_0_layout_0_partners', 'field_583c0ba5ed955', 'no'),
(839, 'options_content--footer_0_layout_0_cooperators_label', 'Współpracujemy z:', 'no'),
(840, '_options_content--footer_0_layout_0_cooperators_label', 'field_583c0ba5edd30', 'no'),
(841, 'options_content--footer_0_layout_0_cooperators_0_img', '140', 'no'),
(842, '_options_content--footer_0_layout_0_cooperators_0_img', 'field_583c0ba7849e0', 'no'),
(843, 'options_content--footer_0_layout_0_cooperators_1_img', '139', 'no'),
(844, '_options_content--footer_0_layout_0_cooperators_1_img', 'field_583c0ba7849e0', 'no'),
(845, 'options_content--footer_0_layout_0_cooperators', '2', 'no'),
(846, '_options_content--footer_0_layout_0_cooperators', 'field_583c0ba5ee119', 'no'),
(847, 'options_content--footer_0_layout', 'a:1:{i:0;s:6:"footer";}', 'no'),
(848, '_options_content--footer_0_layout', 'field_583c0ba567b49', 'no'),
(849, 'options_content--footer_0_content_after', '', 'no'),
(850, '_options_content--footer_0_content_after', 'field_584011ac7b85c', 'no'),
(851, 'options_content--footer', '1', 'no'),
(852, '_options_content--footer', 'field_583c0ba55100f', 'no'),
(853, 'options_content--header', '1', 'no'),
(854, '_options_content--header', 'field_584042ea01135', 'no'),
(855, 'options_contact', '<a href="tel:+48668387017">668 387 017</a>\r\n<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalniacykliniarek.com</a>', 'no'),
(856, '_options_contact', 'field_584044ab994e9', 'no'),
(857, 'options_content--header_0_show', '1', 'no'),
(858, '_options_content--header_0_show', 'field_584042ea1aba6', 'no'),
(859, 'options_content--header_0_section_bg', 'red', 'no'),
(860, '_options_content--header_0_section_bg', 'field_584042ea1afed', 'no'),
(861, 'options_content--header_0_section_intro', '', 'no'),
(862, '_options_content--header_0_section_intro', 'field_584042ea1b7c5', 'no'),
(863, 'options_content--header_0_layout_0_content', '<strong>UWAGA!</strong> Aktualnie jesteśmy w fazie testów i działamy tylko na terenie miasta Warszawy.', 'no'),
(864, '_options_content--header_0_layout_0_content', 'field_58404456994e6', 'no'),
(865, 'options_content--header_0_layout', 'a:1:{i:0;s:12:"notification";}', 'no'),
(866, '_options_content--header_0_layout', 'field_584042ea1bb70', 'no'),
(867, 'options_content--header_0_content_after', '', 'no'),
(868, '_options_content--header_0_content_after', 'field_584042ea1bf50', 'no'),
(913, 'widget_dopbspwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(914, 'DOPBSP_db_version', '2.196', 'yes'),
(915, 'DOPBSP_db_version_api_keys', '1', 'yes'),
(916, 'DOPBSP_db_version_calendars', '1', 'yes'),
(917, 'DOPBSP_db_version_coupons', '1', 'yes'),
(918, 'DOPBSP_db_version_days', '1', 'yes'),
(919, 'DOPBSP_db_version_days_available', '1', 'yes'),
(920, 'DOPBSP_db_version_days_unavailable', '1', 'yes'),
(921, 'DOPBSP_db_version_discounts', '1', 'yes'),
(922, 'DOPBSP_db_version_discounts_items', '1', 'yes'),
(923, 'DOPBSP_db_version_discounts_items_rules', '1', 'yes'),
(924, 'DOPBSP_db_version_emails', '1', 'yes'),
(925, 'DOPBSP_db_version_emails_translation', '1', 'yes'),
(926, 'DOPBSP_db_version_extras', '1', 'yes'),
(927, 'DOPBSP_db_version_extras_groups', '1.001', 'yes'),
(928, 'DOPBSP_db_version_extras_groups_items', '1.001', 'yes'),
(929, 'DOPBSP_db_version_fees', '1', 'yes'),
(930, 'DOPBSP_db_version_forms', '1', 'yes'),
(931, 'DOPBSP_db_version_forms_fields', '1.001', 'yes'),
(932, 'DOPBSP_db_version_forms_select_options', '1', 'yes'),
(933, 'DOPBSP_db_version_languages', '1', 'yes'),
(934, 'DOPBSP_db_version_locations', '1', 'yes'),
(935, 'DOPBSP_db_version_models', '1', 'yes'),
(936, 'DOPBSP_db_version_reservations', '1.004', 'yes'),
(937, 'DOPBSP_db_version_rules', '1', 'yes'),
(938, 'DOPBSP_db_version_settings', '1', 'yes'),
(939, 'DOPBSP_db_version_settings_calendar', '1', 'yes'),
(940, 'DOPBSP_db_version_settings_notifications', '1', 'yes'),
(941, 'DOPBSP_db_version_settings_payment', '1', 'yes'),
(942, 'DOPBSP_db_version_translation', '1', 'yes'),
(943, 'DOPBSP_db_version_woocommerce', '1.002', 'yes'),
(977, 'pa_jednostka-miary_children', 'a:0:{}', 'yes'),
(1014, 'easy_booking_settings', 'a:16:{s:27:"easy_booking_calendar_theme";s:7:"classic";s:29:"easy_booking_background_color";s:7:"#FFFFFF";s:23:"easy_booking_main_color";s:7:"#bf1f24";s:23:"easy_booking_text_color";s:7:"#000000";s:22:"easy_booking_calc_mode";s:4:"days";s:17:"post_all_bookable";s:1:"1";s:18:"easy_booking_dates";s:3:"two";s:21:"easy_booking_duration";s:4:"days";s:28:"easy_booking_custom_duration";i:1;s:24:"easy_booking_booking_min";s:1:"0";s:24:"easy_booking_booking_max";s:1:"0";s:33:"easy_booking_first_available_date";s:1:"0";s:21:"easy_booking_max_year";i:2021;s:22:"easy_booking_first_day";s:1:"1";s:25:"easy_booking_all_bookable";s:2:"no";s:21:"easy_booking_year_max";i:2021;}', 'yes'),
(1015, 'easy_booking_data_needs_update', '1', 'yes'),
(1027, 'easy_booking_display_notice_wceb-addons', '1', 'yes'),
(1032, 'easy_booking_availability_settings', 'a:3:{s:37:"easy_booking_availability_license_key";s:23:"849-FYoY-XAM3-Fh8B-VQst";s:33:"easy_booking_availability_display";s:2:"on";s:26:"ebac_unavailability_period";s:1:"0";}', 'yes'),
(1033, 'easy_booking_display_notice_ebac_license', '1', 'yes'),
(1034, 'easy_booking_availability_imports', 'a:0:{}', 'yes'),
(1035, 'ebac_imports_backward_compatibility', '1', 'yes'),
(1036, 'ebdida_settings', 'a:2:{s:18:"ebdida_license_key";s:23:"848-JZbx-Ypzc-Gpm4-eoGs";s:21:"ebdida_disabled_dates";a:0:{}}', 'yes'),
(1037, 'easy_booking_display_notice_ebdida_license', '1', 'yes'),
(1038, 'easy_booking_display_notice_init', '1', 'yes'),
(1039, 'easy_booking_display_notice_init_after_update', '1', 'yes'),
(1099, 'options_breadcrumbs_0_page', '339', 'no'),
(1100, '_options_breadcrumbs_0_page', 'field_5848521aef954', 'no'),
(1101, 'options_breadcrumbs_0_label', 'wycena', 'no'),
(1102, '_options_breadcrumbs_0_label', 'field_58485267548ae', 'no'),
(1103, 'options_breadcrumbs_1_page', '62', 'no'),
(1104, '_options_breadcrumbs_1_page', 'field_5848521aef954', 'no'),
(1105, 'options_breadcrumbs_1_label', 'rezerwacja', 'no'),
(1106, '_options_breadcrumbs_1_label', 'field_58485267548ae', 'no'),
(1107, 'options_breadcrumbs_2_page', '191', 'no'),
(1108, '_options_breadcrumbs_2_page', 'field_5848521aef954', 'no'),
(1109, 'options_breadcrumbs_2_label', 'koszyk', 'no'),
(1110, '_options_breadcrumbs_2_label', 'field_58485267548ae', 'no'),
(1111, 'options_breadcrumbs_3_page', '192', 'no'),
(1112, '_options_breadcrumbs_3_page', 'field_5848521aef954', 'no'),
(1113, 'options_breadcrumbs_3_label', 'dostawa', 'no'),
(1114, '_options_breadcrumbs_3_label', 'field_58485267548ae', 'no'),
(1115, 'options_breadcrumbs_4_page', '191', 'no'),
(1116, '_options_breadcrumbs_4_page', 'field_5848521aef954', 'no'),
(1117, 'options_breadcrumbs_4_label', 'podsumowanie', 'no') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1118, '_options_breadcrumbs_4_label', 'field_58485267548ae', 'no'),
(1119, 'options_breadcrumbs', '5', 'no'),
(1120, '_options_breadcrumbs', 'field_5848520aef953', 'no') ;

#
# End of data contents of table `wpwyp2810161505_options`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_postmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_postmeta`;


#
# Table structure of table `wpwyp2810161505_postmeta`
#

CREATE TABLE `wpwyp2810161505_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=10083 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_postmeta`
#
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1480949369:1'),
(6, 5, '_wp_page_template', 'page-templates/content-page.php'),
(13, 35, '_wp_attached_file', '2016/11/cykliniarki.jpg'),
(14, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:825;s:6:"height";i:455;s:4:"file";s:23:"2016/11/cykliniarki.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"cykliniarki-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"cykliniarki-300x165.jpg";s:5:"width";i:300;s:6:"height";i:165;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"cykliniarki-768x424.jpg";s:5:"width";i:768;s:6:"height";i:424;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(15, 35, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.8365642069187098;s:5:"bytes";i:7317;s:11:"size_before";i:93370;s:10:"size_after";i:86053;s:4:"time";d:0.20000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:4:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:9.8800000000000008;s:5:"bytes";i:349;s:11:"size_before";i:3533;s:10:"size_after";i:3184;s:4:"time";d:0.040000000000000001;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:8.9299999999999997;s:5:"bytes";i:695;s:11:"size_before";i:7784;s:10:"size_after";i:7089;s:4:"time";d:0.02;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:11.65;s:5:"bytes";i:3324;s:11:"size_before";i:28532;s:10:"size_after";i:25208;s:4:"time";d:0.059999999999999998;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:5.5099999999999998;s:5:"bytes";i:2949;s:11:"size_before";i:53521;s:10:"size_after";i:50572;s:4:"time";d:0.080000000000000002;}}}'),
(16, 45, '_edit_last', '1'),
(17, 45, '_wp_page_template', 'default'),
(18, 45, '_edit_lock', '1478346498:1'),
(28, 48, '_edit_last', '1'),
(29, 48, '_wp_page_template', 'page-templates/calc.php'),
(30, 48, '_edit_lock', '1478539322:1'),
(31, 52, '_edit_last', '1'),
(32, 52, '_edit_lock', '1481029522:1'),
(33, 52, '_wp_page_template', 'page-templates/home-page.php'),
(34, 54, '_menu_item_type', 'post_type'),
(35, 54, '_menu_item_menu_item_parent', '0'),
(36, 54, '_menu_item_object_id', '52'),
(37, 54, '_menu_item_object', 'page'),
(38, 54, '_menu_item_target', ''),
(39, 54, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(40, 54, '_menu_item_xfn', ''),
(41, 54, '_menu_item_url', ''),
(42, 54, '_menu_item_orphaned', '1479904201'),
(43, 55, '_menu_item_type', 'post_type'),
(44, 55, '_menu_item_menu_item_parent', '0'),
(45, 55, '_menu_item_object_id', '48'),
(46, 55, '_menu_item_object', 'page'),
(47, 55, '_menu_item_target', ''),
(48, 55, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(49, 55, '_menu_item_xfn', ''),
(50, 55, '_menu_item_url', ''),
(51, 55, '_menu_item_orphaned', '1479904204'),
(52, 56, '_menu_item_type', 'post_type'),
(53, 56, '_menu_item_menu_item_parent', '0'),
(54, 56, '_menu_item_object_id', '45'),
(55, 56, '_menu_item_object', 'page'),
(56, 56, '_menu_item_target', ''),
(57, 56, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(58, 56, '_menu_item_xfn', ''),
(59, 56, '_menu_item_url', ''),
(60, 56, '_menu_item_orphaned', '1479904205'),
(61, 57, '_edit_last', '1'),
(62, 57, '_wp_page_template', 'default'),
(63, 57, '_edit_lock', '1479904094:1'),
(64, 59, '_edit_last', '1'),
(65, 59, '_edit_lock', '1480955382:1'),
(66, 59, '_wp_page_template', 'page-templates/content-page.php'),
(67, 60, '_edit_last', '1'),
(68, 60, '_wp_page_template', 'default'),
(69, 60, '_edit_lock', '1480510803:1'),
(70, 61, '_edit_last', '1'),
(71, 61, '_wp_page_template', 'default'),
(72, 61, '_edit_lock', '1479904129:1'),
(73, 62, '_edit_last', '1'),
(74, 62, '_wp_page_template', 'default'),
(75, 62, '_edit_lock', '1479904125:1'),
(76, 67, '_menu_item_type', 'post_type'),
(77, 67, '_menu_item_menu_item_parent', '0'),
(78, 67, '_menu_item_object_id', '62'),
(79, 67, '_menu_item_object', 'page'),
(80, 67, '_menu_item_target', ''),
(81, 67, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(82, 67, '_menu_item_xfn', ''),
(83, 67, '_menu_item_url', ''),
(85, 68, '_menu_item_type', 'post_type'),
(86, 68, '_menu_item_menu_item_parent', '0'),
(87, 68, '_menu_item_object_id', '61'),
(88, 68, '_menu_item_object', 'page'),
(89, 68, '_menu_item_target', ''),
(90, 68, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(91, 68, '_menu_item_xfn', ''),
(92, 68, '_menu_item_url', ''),
(94, 69, '_menu_item_type', 'post_type'),
(95, 69, '_menu_item_menu_item_parent', '0'),
(96, 69, '_menu_item_object_id', '60'),
(97, 69, '_menu_item_object', 'page'),
(98, 69, '_menu_item_target', ''),
(99, 69, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(100, 69, '_menu_item_xfn', ''),
(101, 69, '_menu_item_url', ''),
(103, 70, '_menu_item_type', 'post_type'),
(104, 70, '_menu_item_menu_item_parent', '0'),
(105, 70, '_menu_item_object_id', '59'),
(106, 70, '_menu_item_object', 'page'),
(107, 70, '_menu_item_target', ''),
(108, 70, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(109, 70, '_menu_item_xfn', ''),
(110, 70, '_menu_item_url', ''),
(112, 71, '_menu_item_type', 'post_type'),
(113, 71, '_menu_item_menu_item_parent', '0'),
(114, 71, '_menu_item_object_id', '57'),
(115, 71, '_menu_item_object', 'page'),
(116, 71, '_menu_item_target', ''),
(117, 71, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(118, 71, '_menu_item_xfn', ''),
(119, 71, '_menu_item_url', ''),
(121, 72, '_menu_item_type', 'post_type'),
(122, 72, '_menu_item_menu_item_parent', '0'),
(123, 72, '_menu_item_object_id', '52') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(124, 72, '_menu_item_object', 'page'),
(125, 72, '_menu_item_target', ''),
(126, 72, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(127, 72, '_menu_item_xfn', ''),
(128, 72, '_menu_item_url', ''),
(130, 73, '_edit_last', '1'),
(131, 73, '_edit_lock', '1480941940:1'),
(132, 80, '_wp_attached_file', '2016/11/fig.png'),
(133, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1048;s:6:"height";i:562;s:4:"file";s:15:"2016/11/fig.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"fig-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:15:"fig-300x161.png";s:5:"width";i:300;s:6:"height";i:161;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:15:"fig-768x412.png";s:5:"width";i:768;s:6:"height";i:412;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:16:"fig-1024x549.png";s:5:"width";i:1024;s:6:"height";i:549;s:9:"mime-type";s:9:"image/png";}s:21:"section-background-sm";a:4:{s:4:"file";s:15:"fig-992x532.png";s:5:"width";i:992;s:6:"height";i:532;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(137, 80, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:1973097;s:10:"size_after";i:1973097;s:4:"time";d:1.9199999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:5:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:46174;s:10:"size_after";i:46174;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:87908;s:10:"size_after";i:87908;s:4:"time";d:0.16;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:447161;s:10:"size_after";i:447161;s:4:"time";d:0.34999999999999998;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:712145;s:10:"size_after";i:712145;s:4:"time";d:0.67000000000000004;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:679709;s:10:"size_after";i:679709;s:4:"time";d:0.70999999999999996;}}}'),
(138, 52, 'content_0_section_intro', ''),
(139, 52, '_content_0_section_intro', 'field_5835d59224057'),
(140, 52, 'content_0_content_0_image', ''),
(141, 52, '_content_0_content_0_image', 'field_5835d5b924059'),
(142, 52, 'content_0_content_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(143, 52, '_content_0_content_0_iframe', 'field_5835d5ea2405b'),
(144, 52, 'content_0_content_0_caption', ''),
(145, 52, '_content_0_content_0_caption', 'field_5835d5ca2405a'),
(146, 52, 'content_0_content', 'a:1:{i:0;s:5:"media";}'),
(147, 52, '_content_0_content', 'field_5835d59b24058'),
(148, 52, 'content', '10'),
(149, 52, '_content', 'field_5835d51f24055'),
(168, 52, 'content_0_layout', 'a:1:{i:0;s:18:"sprawdz-dostepnosc";}'),
(169, 52, '_content_0_layout', 'field_5835d59b24058'),
(190, 52, 'content_1_section_intro', ''),
(191, 52, '_content_1_section_intro', 'field_5835d59224057'),
(192, 52, 'content_1_layout_0_image', ''),
(193, 52, '_content_1_layout_0_image', 'field_5835d5b924059'),
(194, 52, 'content_1_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(195, 52, '_content_1_layout_0_iframe', 'field_5835d5ea2405b'),
(196, 52, 'content_1_layout_0_caption', ''),
(197, 52, '_content_1_layout_0_caption', 'field_5835d5ca2405a'),
(198, 52, 'content_1_layout', 'a:1:{i:0;s:12:"tani-wynajem";}'),
(199, 52, '_content_1_layout', 'field_5835d59b24058'),
(320, 52, 'content_0_section_bg', 'red'),
(321, 52, '_content_0_section_bg', 'field_5836c16c062d1'),
(322, 52, 'content_1_section_bg', 'img'),
(323, 52, '_content_1_section_bg', 'field_5836c16c062d1'),
(392, 52, 'content_2_section_bg', 'black'),
(393, 52, '_content_2_section_bg', 'field_5836c16c062d1'),
(394, 52, 'content_2_section_intro', '<h1>Jak to działa?</h1>\r\nLorem ipsum dolor sit amet enim. Etiam ullamcorper convallis ac, laoreet enim.\r\nSuspendisse a pellentesque dui, non felis.'),
(395, 52, '_content_2_section_intro', 'field_5835d59224057'),
(396, 52, 'content_2_layout_0_image', ''),
(397, 52, '_content_2_layout_0_image', 'field_5835d5b924059'),
(398, 52, 'content_2_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(399, 52, '_content_2_layout_0_iframe', 'field_5835d5ea2405b'),
(400, 52, 'content_2_layout_0_caption', ''),
(401, 52, '_content_2_layout_0_caption', 'field_5835d5ca2405a'),
(402, 52, 'content_2_layout', 'a:1:{i:0;s:13:"jak-to-dziala";}'),
(403, 52, '_content_2_layout', 'field_5835d59b24058'),
(636, 52, 'content_0_section_bg_img', '112'),
(637, 52, '_content_0_section_bg_img', 'field_5836c1c4062d2'),
(638, 52, 'content_1_layout_0_step_0_icon', 'mouse'),
(639, 52, '_content_1_layout_0_step_0_icon', 'field_5836c4722cf71'),
(640, 52, 'content_1_layout_0_step_0_label', 'Zarezerwuj online'),
(641, 52, '_content_1_layout_0_step_0_label', 'field_5836c4db2cf72'),
(642, 52, 'content_1_layout_0_step_1_icon', 'trolley'),
(643, 52, '_content_1_layout_0_step_1_icon', 'field_5836c4722cf71'),
(644, 52, 'content_1_layout_0_step_1_label', 'Kurier dostarczy cykliniarkę pod Twój dom'),
(645, 52, '_content_1_layout_0_step_1_label', 'field_5836c4db2cf72'),
(646, 52, 'content_1_layout_0_step_2_icon', 'cykliniarka'),
(647, 52, '_content_1_layout_0_step_2_icon', 'field_5836c4722cf71'),
(648, 52, 'content_1_layout_0_step_2_label', 'Wycyklinuj podłogę'),
(649, 52, '_content_1_layout_0_step_2_label', 'field_5836c4db2cf72'),
(650, 52, 'content_1_layout_0_step_3_icon', 'truck'),
(651, 52, '_content_1_layout_0_step_3_icon', 'field_5836c4722cf71'),
(652, 52, 'content_1_layout_0_step_3_label', 'Kurier odbierze cykliniarkę od Ciebie'),
(653, 52, '_content_1_layout_0_step_3_label', 'field_5836c4db2cf72'),
(654, 52, 'content_1_layout_0_step', '4'),
(655, 52, '_content_1_layout_0_step', 'field_5836c4452cf70'),
(656, 52, 'content_3_section_bg', 'white'),
(657, 52, '_content_3_section_bg', 'field_5836c16c062d1'),
(658, 52, 'content_3_section_intro', '<h1>Każdy może\r\ncyklinować</h1>\r\nNasze maszyny są tak proste w obsłudze, że nie\r\npotrzebujesz nawet specjalistycznego szkolenia.\r\nWystarczy Ci krótki instruktarz, który\r\nprzygotowaliśmy w formie wideo na YouTube.\r\n\r\n<span class="btn--fill btn btn--primary">Dowiedz się więcej</span>'),
(659, 52, '_content_3_section_intro', 'field_5835d59224057'),
(666, 52, 'content_3_layout', 'a:1:{i:0;s:5:"media";}'),
(667, 52, '_content_3_layout', 'field_5835d59b24058'),
(740, 52, 'content_0_layout_0_content', '<h1>Tani\r\nwynajem od\r\n<strong>50zł\r\n</strong>za dobę</h1>\r\n<span class="btn btn--primary btn--fill">Sprawdź ceny</span>'),
(741, 52, '_content_0_layout_0_content', 'field_5836ca8eb9160'),
(964, 52, 'content_4_section_bg', 'img-dark'),
(965, 52, '_content_4_section_bg', 'field_5836c16c062d1'),
(966, 52, 'content_4_section_intro', '<h1>Opinie o wypożyczalni</h1>\r\n&nbsp;\r\n\r\n<img class="alignnone size-medium wp-image-166" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/ocena-300x72.png" alt="ocena" width="300" height="72" />\r\n<p class="lg">Ocena 4,7 na 5 gwiazdek</p>'),
(967, 52, '_content_4_section_intro', 'field_5835d59224057'),
(968, 52, 'content_4_layout_0_image', '80'),
(969, 52, '_content_4_layout_0_image', 'field_5835d5b924059'),
(970, 52, 'content_4_layout_0_iframe', ''),
(971, 52, '_content_4_layout_0_iframe', 'field_5835d5ea2405b'),
(972, 52, 'content_4_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(973, 52, '_content_4_layout_0_caption', 'field_5835d5ca2405a'),
(974, 52, 'content_4_layout', 'a:1:{i:0;s:6:"opinie";}'),
(975, 52, '_content_4_layout', 'field_5835d59b24058'),
(1056, 107, '_wp_attached_file', '2016/11/mapa.png'),
(1057, 107, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:622;s:6:"height";i:602;s:4:"file";s:16:"2016/11/mapa.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"mapa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"mapa-300x290.png";s:5:"width";i:300;s:6:"height";i:290;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1058, 107, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:23.692348901767886;s:5:"bytes";i:19459;s:11:"size_before";i:82132;s:10:"size_after";i:62673;s:4:"time";d:0.52000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:30.75;s:5:"bytes";i:3534;s:11:"size_before";i:11491;s:10:"size_after";i:7957;s:4:"time";d:0.089999999999999997;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:31.289999999999999;s:5:"bytes";i:11245;s:11:"size_before";i:35941;s:10:"size_after";i:24696;s:4:"time";d:0.14999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:13.49;s:5:"bytes";i:4680;s:11:"size_before";i:34700;s:10:"size_after";i:30020;s:4:"time";d:0.28000000000000003;}}}'),
(1379, 112, '_wp_attached_file', '2016/11/main.jpg'),
(1380, 112, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3100;s:6:"height";i:1640;s:4:"file";s:16:"2016/11/main.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"main-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"main-300x159.jpg";s:5:"width";i:300;s:6:"height";i:159;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"main-768x406.jpg";s:5:"width";i:768;s:6:"height";i:406;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"main-1024x542.jpg";s:5:"width";i:1024;s:6:"height";i:542;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:16:"main-992x525.jpg";s:5:"width";i:992;s:6:"height";i:525;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:17:"main-1200x635.jpg";s:5:"width";i:1200;s:6:"height";i:635;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-lg";a:4:{s:4:"file";s:17:"main-1600x846.jpg";s:5:"width";i:1600;s:6:"height";i:846;s:9:"mime-type";s:10:"image/jpeg";}s:22:"section-background-xxl";a:4:{s:4:"file";s:18:"main-1920x1016.jpg";s:5:"width";i:1920;s:6:"height";i:1016;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1461, 112, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.0412934075178208;s:5:"bytes";i:16486;s:11:"size_before";i:542072;s:10:"size_after";i:525586;s:4:"time";d:0.52000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:8:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:4.0499999999999998;s:5:"bytes";i:232;s:11:"size_before";i:5723;s:10:"size_after";i:5491;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:3.1400000000000001;s:5:"bytes";i:283;s:11:"size_before";i:8999;s:10:"size_after";i:8716;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:2.8999999999999999;s:5:"bytes";i:1154;s:11:"size_before";i:39752;s:10:"size_after";i:38598;s:4:"time";d:0.029999999999999999;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:2.8700000000000001;s:5:"bytes";i:1802;s:11:"size_before";i:62737;s:10:"size_after";i:60935;s:4:"time";d:0.10000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:2.9500000000000002;s:5:"bytes";i:1765;s:11:"size_before";i:59851;s:10:"size_after";i:58086;s:4:"time";d:0.070000000000000007;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:2.8199999999999998;s:5:"bytes";i:2269;s:11:"size_before";i:80550;s:10:"size_after";i:78281;s:4:"time";d:0.059999999999999998;}s:21:"section-background-lg";O:8:"stdClass":5:{s:7:"percent";d:2.96;s:5:"bytes";i:3647;s:11:"size_before";i:123211;s:10:"size_after";i:119564;s:4:"time";d:0.11;}s:22:"section-background-xxl";O:8:"stdClass":5:{s:7:"percent";d:3.3100000000000001;s:5:"bytes";i:5334;s:11:"size_before";i:161249;s:10:"size_after";i:155915;s:4:"time";d:0.13;}}}'),
(1469, 138, '_wp_attached_file', '2016/11/logo-domalux.png'),
(1470, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:404;s:6:"height";i:120;s:4:"file";s:24:"2016/11/logo-domalux.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"logo-domalux-150x120.png";s:5:"width";i:150;s:6:"height";i:120;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"logo-domalux-300x89.png";s:5:"width";i:300;s:6:"height";i:89;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1471, 139, '_wp_attached_file', '2016/11/logo-dotpay.png'),
(1472, 139, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:181;s:6:"height";i:54;s:4:"file";s:23:"2016/11/logo-dotpay.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"logo-dotpay-150x54.png";s:5:"width";i:150;s:6:"height";i:54;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1473, 140, '_wp_attached_file', '2016/11/logo-tba.png'),
(1474, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:68;s:4:"file";s:20:"2016/11/logo-tba.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1475, 139, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:31.899981057018373;s:5:"bytes";i:1684;s:11:"size_before";i:5279;s:10:"size_after";i:3595;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:2:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:24.300000000000001;s:5:"bytes";i:494;s:11:"size_before";i:2033;s:10:"size_after";i:1539;s:4:"time";d:0.02;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:36.659999999999997;s:5:"bytes";i:1190;s:11:"size_before";i:3246;s:10:"size_after";i:2056;s:4:"time";d:0.02;}}}'),
(1476, 138, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:25.958337016411797;s:5:"bytes";i:8810;s:11:"size_before";i:33939;s:10:"size_after";i:25129;s:4:"time";d:0.26000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:23.329999999999998;s:5:"bytes";i:1257;s:11:"size_before";i:5389;s:10:"size_after";i:4132;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:29.02;s:5:"bytes";i:4597;s:11:"size_before";i:15840;s:10:"size_after";i:11243;s:4:"time";d:0.080000000000000002;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:23.260000000000002;s:5:"bytes";i:2956;s:11:"size_before";i:12710;s:10:"size_after";i:9754;s:4:"time";d:0.14999999999999999;}}}'),
(1477, 140, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:33.14805457301668;s:5:"bytes";i:1312;s:11:"size_before";i:3958;s:10:"size_after";i:2646;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:1:{s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:33.149999999999999;s:5:"bytes";i:1312;s:11:"size_before";i:3958;s:10:"size_after";i:2646;s:4:"time";d:0.029999999999999999;}}}'),
(1478, 52, 'content_5_section_bg', 'red'),
(1479, 52, '_content_5_section_bg', 'field_5836c16c062d1'),
(1480, 52, 'content_5_section_intro', '<h1>Dostarczamy pod Twój dom w <del>całej Polsce!<ins>Warszawie!</ins></del></h1>\r\nJuż wkrótce wygodna dostawa kurierem do każdej miejscowości na terenie całego kraju już w 24 godziny*.\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone size-full wp-image-253" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa-warszawa.png" alt="mapa-warszawa" width="622" height="602" />\r\n\r\n&nbsp;'),
(1481, 52, '_content_5_section_intro', 'field_5835d59224057'),
(1482, 52, 'content_5_layout_0_contact_0_icon', 'email'),
(1483, 52, '_content_5_layout_0_contact_0_icon', 'field_583710110e6cd'),
(1484, 52, 'content_5_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(1485, 52, '_content_5_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(1486, 52, 'content_5_layout_0_contact_1_icon', 'phone'),
(1487, 52, '_content_5_layout_0_contact_1_icon', 'field_583710110e6cd'),
(1488, 52, 'content_5_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(1489, 52, '_content_5_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(1490, 52, 'content_5_layout_0_contact_2_icon', 'pin'),
(1491, 52, '_content_5_layout_0_contact_2_icon', 'field_583710110e6cd'),
(1492, 52, 'content_5_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(1493, 52, '_content_5_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(1494, 52, 'content_5_layout_0_contact', '3'),
(1495, 52, '_content_5_layout_0_contact', 'field_583710060e6cc'),
(1496, 52, 'content_5_layout_0_partners_label', 'Nasz partner:'),
(1497, 52, '_content_5_layout_0_partners_label', 'field_583710740e6cf'),
(1498, 52, 'content_5_layout_0_partners_0_img', '138'),
(1499, 52, '_content_5_layout_0_partners_0_img', 'field_583710df0e6d2'),
(1500, 52, 'content_5_layout_0_partners', '1'),
(1501, 52, '_content_5_layout_0_partners', 'field_583710830e6d0'),
(1502, 52, 'content_5_layout_0_cooperators_label', 'Współpracujemy z:'),
(1503, 52, '_content_5_layout_0_cooperators_label', 'field_583710f20e6d3'),
(1504, 52, 'content_5_layout_0_cooperators_0_img', '140'),
(1505, 52, '_content_5_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(1506, 52, 'content_5_layout_0_cooperators_1_img', '139'),
(1507, 52, '_content_5_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(1508, 52, 'content_5_layout_0_cooperators', '2'),
(1509, 52, '_content_5_layout_0_cooperators', 'field_583711200e6d4'),
(1510, 52, 'content_5_layout', 'a:1:{i:0;s:4:"none";}'),
(1511, 52, '_content_5_layout', 'field_5835d59b24058'),
(1626, 145, '_menu_item_type', 'post_type'),
(1627, 145, '_menu_item_menu_item_parent', '0'),
(1628, 145, '_menu_item_object_id', '61'),
(1629, 145, '_menu_item_object', 'page'),
(1630, 145, '_menu_item_target', ''),
(1631, 145, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1632, 145, '_menu_item_xfn', ''),
(1633, 145, '_menu_item_url', ''),
(1635, 142, '_edit_last', '1'),
(1636, 142, '_wp_page_template', 'default'),
(1637, 142, 'content', ''),
(1638, 142, '_content', 'field_5835d51f24055'),
(1641, 142, '_edit_lock', '1480004480:1'),
(1642, 143, '_edit_last', '1'),
(1643, 143, '_wp_page_template', 'default'),
(1644, 143, 'content', ''),
(1645, 143, '_content', 'field_5835d51f24055'),
(1648, 143, '_edit_lock', '1480004481:1'),
(1649, 144, '_edit_last', '1'),
(1650, 144, '_wp_page_template', 'default'),
(1651, 144, 'content', ''),
(1652, 144, '_content', 'field_5835d51f24055'),
(1655, 144, '_edit_lock', '1480004438:1'),
(1656, 149, '_edit_last', '1'),
(1657, 149, '_wp_page_template', 'default'),
(1658, 149, 'content', ''),
(1659, 149, '_content', 'field_5835d51f24055'),
(1662, 149, '_edit_lock', '1480004482:1'),
(1663, 151, '_menu_item_type', 'post_type'),
(1664, 151, '_menu_item_menu_item_parent', '0'),
(1665, 151, '_menu_item_object_id', '149'),
(1666, 151, '_menu_item_object', 'page'),
(1667, 151, '_menu_item_target', ''),
(1668, 151, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1669, 151, '_menu_item_xfn', ''),
(1670, 151, '_menu_item_url', ''),
(1672, 152, '_menu_item_type', 'post_type'),
(1673, 152, '_menu_item_menu_item_parent', '0'),
(1674, 152, '_menu_item_object_id', '144'),
(1675, 152, '_menu_item_object', 'page'),
(1676, 152, '_menu_item_target', ''),
(1677, 152, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1678, 152, '_menu_item_xfn', ''),
(1679, 152, '_menu_item_url', ''),
(1681, 153, '_menu_item_type', 'post_type'),
(1682, 153, '_menu_item_menu_item_parent', '0'),
(1683, 153, '_menu_item_object_id', '143'),
(1684, 153, '_menu_item_object', 'page'),
(1685, 153, '_menu_item_target', ''),
(1686, 153, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1687, 153, '_menu_item_xfn', ''),
(1688, 153, '_menu_item_url', ''),
(1690, 154, '_menu_item_type', 'post_type'),
(1691, 154, '_menu_item_menu_item_parent', '0'),
(1692, 154, '_menu_item_object_id', '142'),
(1693, 154, '_menu_item_object', 'page'),
(1694, 154, '_menu_item_target', ''),
(1695, 154, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1696, 154, '_menu_item_xfn', ''),
(1697, 154, '_menu_item_url', ''),
(1927, 52, 'content_1_section_bg_img', '112') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1928, 52, '_content_1_section_bg_img', 'field_5836c1c4062d2'),
(1929, 52, 'content_1_layout_0_content', '<h1>Tani\r\nwynajem od\r\n<strong>50zł\r\n</strong>za dobę</h1>\r\n<span class="btn btn--primary btn--fill">Sprawdź ceny</span>'),
(1930, 52, '_content_1_layout_0_content', 'field_5836ca8eb9160'),
(1931, 52, 'content_2_layout_0_step_0_icon', 'myszka'),
(1932, 52, '_content_2_layout_0_step_0_icon', 'field_5836c4722cf71'),
(1933, 52, 'content_2_layout_0_step_0_label', 'Zarezerwuj online'),
(1934, 52, '_content_2_layout_0_step_0_label', 'field_5836c4db2cf72'),
(1935, 52, 'content_2_layout_0_step_1_icon', 'wozek'),
(1936, 52, '_content_2_layout_0_step_1_icon', 'field_5836c4722cf71'),
(1937, 52, 'content_2_layout_0_step_1_label', 'Kurier dostarczy cykliniarkę pod Twój dom'),
(1938, 52, '_content_2_layout_0_step_1_label', 'field_5836c4db2cf72'),
(1939, 52, 'content_2_layout_0_step_2_icon', 'cykliniarka'),
(1940, 52, '_content_2_layout_0_step_2_icon', 'field_5836c4722cf71'),
(1941, 52, 'content_2_layout_0_step_2_label', 'Wycyklinuj podłogę'),
(1942, 52, '_content_2_layout_0_step_2_label', 'field_5836c4db2cf72'),
(1943, 52, 'content_2_layout_0_step_3_icon', 'ciezarowka'),
(1944, 52, '_content_2_layout_0_step_3_icon', 'field_5836c4722cf71'),
(1945, 52, 'content_2_layout_0_step_3_label', 'Kurier odbierze cykliniarkę od Ciebie'),
(1946, 52, '_content_2_layout_0_step_3_label', 'field_5836c4db2cf72'),
(1947, 52, 'content_2_layout_0_step', '4'),
(1948, 52, '_content_2_layout_0_step', 'field_5836c4452cf70'),
(1949, 52, 'content_3_layout_0_image', ''),
(1950, 52, '_content_3_layout_0_image', 'field_5835d5b924059'),
(1951, 52, 'content_3_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(1952, 52, '_content_3_layout_0_iframe', 'field_5835d5ea2405b'),
(1953, 52, 'content_3_layout_0_caption', ''),
(1954, 52, '_content_3_layout_0_caption', 'field_5835d5ca2405a'),
(1955, 52, 'content_5_layout_0_image', '80'),
(1956, 52, '_content_5_layout_0_image', 'field_5835d5b924059'),
(1957, 52, 'content_5_layout_0_iframe', ''),
(1958, 52, '_content_5_layout_0_iframe', 'field_5835d5ea2405b'),
(1959, 52, 'content_5_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(1960, 52, '_content_5_layout_0_caption', 'field_5835d5ca2405a'),
(1961, 52, 'content_6_section_bg', 'white'),
(1962, 52, '_content_6_section_bg', 'field_5836c16c062d1'),
(1963, 52, 'content_6_section_intro', '<h1>Zaoszczędzisz\r\nnawet 400zł*</h1>\r\nSamodzielny remont podłogi oznacza,\r\nże sporo pieniędzy zostanie w Twojej kieszeni.\r\nCyklinowanie przez fachowaca pokoju\r\no powierzchni 10 m<sup>2</sup> to koszt ok. 400 zł.'),
(1964, 52, '_content_6_section_intro', 'field_5835d59224057'),
(1965, 52, 'content_6_layout_0_contact_0_icon', 'email'),
(1966, 52, '_content_6_layout_0_contact_0_icon', 'field_583710110e6cd'),
(1967, 52, 'content_6_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(1968, 52, '_content_6_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(1969, 52, 'content_6_layout_0_contact_1_icon', 'phone'),
(1970, 52, '_content_6_layout_0_contact_1_icon', 'field_583710110e6cd'),
(1971, 52, 'content_6_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(1972, 52, '_content_6_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(1973, 52, 'content_6_layout_0_contact_2_icon', 'pin'),
(1974, 52, '_content_6_layout_0_contact_2_icon', 'field_583710110e6cd'),
(1975, 52, 'content_6_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(1976, 52, '_content_6_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(1977, 52, 'content_6_layout_0_contact', '3'),
(1978, 52, '_content_6_layout_0_contact', 'field_583710060e6cc'),
(1979, 52, 'content_6_layout_0_partners_label', 'Nasz partner:'),
(1980, 52, '_content_6_layout_0_partners_label', 'field_583710740e6cf'),
(1981, 52, 'content_6_layout_0_partners_0_img', '138'),
(1982, 52, '_content_6_layout_0_partners_0_img', 'field_583710df0e6d2'),
(1983, 52, 'content_6_layout_0_partners', '1'),
(1984, 52, '_content_6_layout_0_partners', 'field_583710830e6d0'),
(1985, 52, 'content_6_layout_0_cooperators_label', 'Współpracujemy z:'),
(1986, 52, '_content_6_layout_0_cooperators_label', 'field_583710f20e6d3'),
(1987, 52, 'content_6_layout_0_cooperators_0_img', '140'),
(1988, 52, '_content_6_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(1989, 52, 'content_6_layout_0_cooperators_1_img', '139'),
(1990, 52, '_content_6_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(1991, 52, 'content_6_layout_0_cooperators', '2'),
(1992, 52, '_content_6_layout_0_cooperators', 'field_583711200e6d4'),
(1993, 52, 'content_6_layout', 'a:1:{i:0;s:5:"media";}'),
(1994, 52, '_content_6_layout', 'field_5835d59b24058'),
(2723, 52, 'content_0_layout_0_input_label', 'Wpisz miasto lub kod pocztowy...'),
(2724, 52, '_content_0_layout_0_input_label', 'field_583837c3a4e29'),
(2725, 52, 'content_0_layout_0_search_label', 'Sprawdź dostępność'),
(2726, 52, '_content_0_layout_0_search_label', 'field_583837cfa4e2a'),
(2913, 165, '_wp_attached_file', '2016/11/opinie.jpg'),
(2914, 165, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3100;s:6:"height";i:1608;s:4:"file";s:18:"2016/11/opinie.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"opinie-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"opinie-300x156.jpg";s:5:"width";i:300;s:6:"height";i:156;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"opinie-768x398.jpg";s:5:"width";i:768;s:6:"height";i:398;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:19:"opinie-1024x531.jpg";s:5:"width";i:1024;s:6:"height";i:531;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:18:"opinie-992x515.jpg";s:5:"width";i:992;s:6:"height";i:515;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:19:"opinie-1200x622.jpg";s:5:"width";i:1200;s:6:"height";i:622;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-lg";a:4:{s:4:"file";s:19:"opinie-1600x830.jpg";s:5:"width";i:1600;s:6:"height";i:830;s:9:"mime-type";s:10:"image/jpeg";}s:22:"section-background-xxl";a:4:{s:4:"file";s:19:"opinie-1920x996.jpg";s:5:"width";i:1920;s:6:"height";i:996;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2915, 165, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.9658033693738997;s:5:"bytes";i:20592;s:11:"size_before";i:258505;s:10:"size_after";i:237913;s:4:"time";d:0.56999999999999995;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:8:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.0599999999999996;s:5:"bytes";i:184;s:11:"size_before";i:3633;s:10:"size_after";i:3449;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";i:4;s:5:"bytes";i:231;s:11:"size_before";i:5782;s:10:"size_after";i:5551;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:3.3700000000000001;s:5:"bytes";i:679;s:11:"size_before";i:20163;s:10:"size_after";i:19484;s:4:"time";d:0.02;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:5.2300000000000004;s:5:"bytes";i:1575;s:11:"size_before";i:30090;s:10:"size_after";i:28515;s:4:"time";d:0.059999999999999998;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:5.0599999999999996;s:5:"bytes";i:1463;s:11:"size_before";i:28932;s:10:"size_after";i:27469;s:4:"time";d:0.029999999999999999;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:6.4299999999999997;s:5:"bytes";i:2396;s:11:"size_before";i:37236;s:10:"size_after";i:34840;s:4:"time";d:0.059999999999999998;}s:21:"section-background-lg";O:8:"stdClass":5:{s:7:"percent";d:9.2400000000000002;s:5:"bytes";i:5272;s:11:"size_before";i:57037;s:10:"size_after";i:51765;s:4:"time";d:0.16;}s:22:"section-background-xxl";O:8:"stdClass":5:{s:7:"percent";d:11.619999999999999;s:5:"bytes";i:8792;s:11:"size_before";i:75632;s:10:"size_after";i:66840;s:4:"time";d:0.22;}}}'),
(2916, 166, '_wp_attached_file', '2016/11/ocena.png'),
(2917, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:310;s:6:"height";i:74;s:4:"file";s:17:"2016/11/ocena.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ocena-150x74.png";s:5:"width";i:150;s:6:"height";i:74;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"ocena-300x72.png";s:5:"width";i:300;s:6:"height";i:72;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2918, 166, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:27.085417824717435;s:5:"bytes";i:7309;s:11:"size_before";i:26985;s:10:"size_after";i:19676;s:4:"time";d:0.12000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:25.149999999999999;s:5:"bytes";i:1018;s:11:"size_before";i:4047;s:10:"size_after";i:3029;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:27.260000000000002;s:5:"bytes";i:4248;s:11:"size_before";i:15584;s:10:"size_after";i:11336;s:4:"time";d:0.040000000000000001;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:27.780000000000001;s:5:"bytes";i:2043;s:11:"size_before";i:7354;s:10:"size_after";i:5311;s:4:"time";d:0.070000000000000007;}}}'),
(2919, 52, 'content_4_section_bg_img', '165'),
(2920, 52, '_content_4_section_bg_img', 'field_5836c1c4062d2'),
(2921, 52, 'content_6_layout_0_image', '80'),
(2922, 52, '_content_6_layout_0_image', 'field_5835d5b924059'),
(2923, 52, 'content_6_layout_0_iframe', ''),
(2924, 52, '_content_6_layout_0_iframe', 'field_5835d5ea2405b'),
(2925, 52, 'content_6_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(2926, 52, '_content_6_layout_0_caption', 'field_5835d5ca2405a'),
(2927, 52, 'content_7_section_bg', 'black'),
(2928, 52, '_content_7_section_bg', 'field_5836c16c062d1'),
(2929, 52, 'content_7_section_intro', '<h1>FAQ</h1>'),
(2930, 52, '_content_7_section_intro', 'field_5835d59224057'),
(2931, 52, 'content_7_layout_0_contact_0_icon', 'email'),
(2932, 52, '_content_7_layout_0_contact_0_icon', 'field_583710110e6cd'),
(2933, 52, 'content_7_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(2934, 52, '_content_7_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(2935, 52, 'content_7_layout_0_contact_1_icon', 'telefon'),
(2936, 52, '_content_7_layout_0_contact_1_icon', 'field_583710110e6cd'),
(2937, 52, 'content_7_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(2938, 52, '_content_7_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(2939, 52, 'content_7_layout_0_contact_2_icon', 'pinezka'),
(2940, 52, '_content_7_layout_0_contact_2_icon', 'field_583710110e6cd'),
(2941, 52, 'content_7_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2942, 52, '_content_7_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(2943, 52, 'content_7_layout_0_contact', '3'),
(2944, 52, '_content_7_layout_0_contact', 'field_583710060e6cc'),
(2945, 52, 'content_7_layout_0_partners_label', 'Nasz partner:'),
(2946, 52, '_content_7_layout_0_partners_label', 'field_583710740e6cf'),
(2947, 52, 'content_7_layout_0_partners_0_img', '138'),
(2948, 52, '_content_7_layout_0_partners_0_img', 'field_583710df0e6d2'),
(2949, 52, 'content_7_layout_0_partners', '1'),
(2950, 52, '_content_7_layout_0_partners', 'field_583710830e6d0'),
(2951, 52, 'content_7_layout_0_cooperators_label', 'Współpracujemy z:'),
(2952, 52, '_content_7_layout_0_cooperators_label', 'field_583710f20e6d3'),
(2953, 52, 'content_7_layout_0_cooperators_0_img', '140'),
(2954, 52, '_content_7_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(2955, 52, 'content_7_layout_0_cooperators_1_img', '139'),
(2956, 52, '_content_7_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(2957, 52, 'content_7_layout_0_cooperators', '2'),
(2958, 52, '_content_7_layout_0_cooperators', 'field_583711200e6d4'),
(2959, 52, 'content_7_layout', 'a:1:{i:0;s:3:"faq";}'),
(2960, 52, '_content_7_layout', 'field_5835d59b24058'),
(3645, 176, '_wp_attached_file', '2016/11/photo.jpg'),
(3646, 176, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:200;s:6:"height";i:200;s:4:"file";s:17:"2016/11/photo.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"photo-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(3647, 176, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:5.8220293000542593;s:5:"bytes";i:1073;s:11:"size_before";i:18430;s:10:"size_after";i:17357;s:4:"time";d:0.059999999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:2:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9399999999999999;s:5:"bytes";i:194;s:11:"size_before";i:4923;s:10:"size_after";i:4729;s:4:"time";d:0.040000000000000001;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:6.5099999999999998;s:5:"bytes";i:879;s:11:"size_before";i:13507;s:10:"size_after";i:12628;s:4:"time";d:0.02;}}}'),
(3648, 52, 'content_4_layout_0_reviews_0_img', '176'),
(3649, 52, '_content_4_layout_0_reviews_0_img', 'field_58385679f0113'),
(3650, 52, 'content_4_layout_0_reviews_0_content', 'Chcialbym podziekować za wynajem cykliniraki. Byłem bardzo zadwolony z maszyn oraz ze współpracy z państwem. W najblizszym czasie ponownie się zgłoszę.'),
(3651, 52, '_content_4_layout_0_reviews_0_content', 'field_58385681f0114'),
(3652, 52, 'content_4_layout_0_reviews_0_name', 'Marcin Filipek'),
(3653, 52, '_content_4_layout_0_reviews_0_name', 'field_58385693f0115'),
(3654, 52, 'content_4_layout_0_reviews_1_img', '176'),
(3655, 52, '_content_4_layout_0_reviews_1_img', 'field_58385679f0113'),
(3656, 52, 'content_4_layout_0_reviews_1_content', 'Chciałam Pańswu bardzo podziękować za wynajem sprzętu. Spełniło ono swoje zadanie należycie. Z pewnością będę korzystała z Państwa usług.'),
(3657, 52, '_content_4_layout_0_reviews_1_content', 'field_58385681f0114'),
(3658, 52, 'content_4_layout_0_reviews_1_name', 'Lucyna Piechowska'),
(3659, 52, '_content_4_layout_0_reviews_1_name', 'field_58385693f0115'),
(3660, 52, 'content_4_layout_0_reviews_2_img', '176'),
(3661, 52, '_content_4_layout_0_reviews_2_img', 'field_58385679f0113'),
(3662, 52, 'content_4_layout_0_reviews_2_content', 'Bardzo dziękuję za możliwość skorzystania z Państwa oferty. Cykliniarka spisała się bardzo dobrze dzięki czemu cała praca przebiegła wyjatkowo szybko i przyjemnie. '),
(3663, 52, '_content_4_layout_0_reviews_2_content', 'field_58385681f0114'),
(3664, 52, 'content_4_layout_0_reviews_2_name', 'Janusz Nowakowski'),
(3665, 52, '_content_4_layout_0_reviews_2_name', 'field_58385693f0115'),
(3666, 52, 'content_4_layout_0_reviews', '3'),
(3667, 52, '_content_4_layout_0_reviews', 'field_5838565af0112'),
(3668, 52, 'content_4_layout_0_content', '<a class="btn btn--primary" href="#">Zobacz wszystkie opinie</a>'),
(3669, 52, '_content_4_layout_0_content', 'field_5838569ff0116'),
(4420, 52, 'content_7_layout_0_faqs_0_question', 'Jak długo muszę czekać na maszynę po rezerwacji?'),
(4421, 52, '_content_7_layout_0_faqs_0_question', 'field_583863f942729'),
(4422, 52, 'content_7_layout_0_faqs_0_answear', 'Po instruktażu i otrzymaniu stosownych porad praktycznych KAŻDY jest w stanie w ciągu dwóch dni, po kilku godzinach pracy, do 200 złotych doprowadzić swoją podłogę o powierzchni około 50 m<sup>2</sup> do perfekcji.  Nie musisz wynajmować żadnych cykliniarzy, firm,  nikt nie będzie łazil Ci po Twoim mieszkaniu, nie musisz parzyć nikomu kawy, sprzątać po kimś ubikacji - wszystko to zrobisz sam. Obsługa tych cykliniarek to przyjemność. '),
(4423, 52, '_content_7_layout_0_faqs_0_answear', 'field_583863ff4272a'),
(4424, 52, 'content_7_layout_0_faqs_1_question', 'Lorem ipsum?'),
(4425, 52, '_content_7_layout_0_faqs_1_question', 'field_583863f942729'),
(4426, 52, 'content_7_layout_0_faqs_1_answear', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.'),
(4427, 52, '_content_7_layout_0_faqs_1_answear', 'field_583863ff4272a'),
(4428, 52, 'content_7_layout_0_faqs', '2'),
(4429, 52, '_content_7_layout_0_faqs', 'field_583863ae42723'),
(4430, 52, 'content_7_layout_0_content', '<a class="btn btn--primary" href="#">Zobacz wszystkie pytania</a>'),
(4431, 52, '_content_7_layout_0_content', 'field_583863ae42727'),
(4432, 52, 'content_8_section_bg', 'white'),
(4433, 52, '_content_8_section_bg', 'field_5836c16c062d1'),
(4434, 52, 'content_8_section_intro', '<h1>U nas kupisz wszystko</h1>\r\nNie trać czasu na chodzenie po marketach. Zaopatrzymy Ciebie w niezbędne papiery ścierne, lakiery i wałki.\r\nSprzedajemy wszystkie produkty potrzebne do odswieżenia podlogi.'),
(4435, 52, '_content_8_section_intro', 'field_5835d59224057'),
(4436, 52, 'content_8_layout_0_contact_0_icon', 'email'),
(4437, 52, '_content_8_layout_0_contact_0_icon', 'field_583710110e6cd'),
(4438, 52, 'content_8_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(4439, 52, '_content_8_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(4440, 52, 'content_8_layout_0_contact_1_icon', 'telefon'),
(4441, 52, '_content_8_layout_0_contact_1_icon', 'field_583710110e6cd'),
(4442, 52, 'content_8_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(4443, 52, '_content_8_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(4444, 52, 'content_8_layout_0_contact_2_icon', 'pinezka'),
(4445, 52, '_content_8_layout_0_contact_2_icon', 'field_583710110e6cd'),
(4446, 52, 'content_8_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(4447, 52, '_content_8_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(4448, 52, 'content_8_layout_0_contact', '3'),
(4449, 52, '_content_8_layout_0_contact', 'field_583710060e6cc'),
(4450, 52, 'content_8_layout_0_partners_label', 'Nasz partner:'),
(4451, 52, '_content_8_layout_0_partners_label', 'field_583710740e6cf'),
(4452, 52, 'content_8_layout_0_partners_0_img', '138'),
(4453, 52, '_content_8_layout_0_partners_0_img', 'field_583710df0e6d2'),
(4454, 52, 'content_8_layout_0_partners', '1'),
(4455, 52, '_content_8_layout_0_partners', 'field_583710830e6d0'),
(4456, 52, 'content_8_layout_0_cooperators_label', 'Współpracujemy z:'),
(4457, 52, '_content_8_layout_0_cooperators_label', 'field_583710f20e6d3'),
(4458, 52, 'content_8_layout_0_cooperators_0_img', '140'),
(4459, 52, '_content_8_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(4460, 52, 'content_8_layout_0_cooperators_1_img', '139'),
(4461, 52, '_content_8_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(4462, 52, 'content_8_layout_0_cooperators', '2'),
(4463, 52, '_content_8_layout_0_cooperators', 'field_583711200e6d4'),
(4464, 52, 'content_8_layout', 'a:1:{i:0;s:4:"none";}'),
(4465, 52, '_content_8_layout', 'field_5835d59b24058'),
(5358, 52, 'content_9_layout_0_contact_0_icon', 'email'),
(5359, 52, '_content_9_layout_0_contact_0_icon', 'field_583710110e6cd'),
(5360, 52, 'content_9_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(5361, 52, '_content_9_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(5362, 52, 'content_9_layout_0_contact_1_icon', 'telefon'),
(5363, 52, '_content_9_layout_0_contact_1_icon', 'field_583710110e6cd'),
(5364, 52, 'content_9_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(5365, 52, '_content_9_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(5366, 52, 'content_9_layout_0_contact_2_icon', 'pinezka'),
(5367, 52, '_content_9_layout_0_contact_2_icon', 'field_583710110e6cd') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(5368, 52, 'content_9_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(5369, 52, '_content_9_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(5370, 52, 'content_9_layout_0_contact', '3'),
(5371, 52, '_content_9_layout_0_contact', 'field_583710060e6cc'),
(5372, 52, 'content_9_layout_0_partners_label', 'Nasz partner:'),
(5373, 52, '_content_9_layout_0_partners_label', 'field_583710740e6cf'),
(5374, 52, 'content_9_layout_0_partners_0_img', '138'),
(5375, 52, '_content_9_layout_0_partners_0_img', 'field_583710df0e6d2'),
(5376, 52, 'content_9_layout_0_partners', '1'),
(5377, 52, '_content_9_layout_0_partners', 'field_583710830e6d0'),
(5378, 52, 'content_9_layout_0_cooperators_label', 'Współpracujemy z:'),
(5379, 52, '_content_9_layout_0_cooperators_label', 'field_583710f20e6d3'),
(5380, 52, 'content_9_layout_0_cooperators_0_img', '140'),
(5381, 52, '_content_9_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(5382, 52, 'content_9_layout_0_cooperators_1_img', '139'),
(5383, 52, '_content_9_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(5384, 52, 'content_9_layout_0_cooperators', '2'),
(5385, 52, '_content_9_layout_0_cooperators', 'field_583711200e6d4'),
(5718, 52, 'content_0_show', '1'),
(5719, 52, '_content_0_show', 'field_58397608ccf61'),
(5720, 52, 'content_1_show', '1'),
(5721, 52, '_content_1_show', 'field_58397608ccf61'),
(5722, 52, 'content_2_show', '1'),
(5723, 52, '_content_2_show', 'field_58397608ccf61'),
(5724, 52, 'content_3_show', '1'),
(5725, 52, '_content_3_show', 'field_58397608ccf61'),
(5726, 52, 'content_4_show', '1'),
(5727, 52, '_content_4_show', 'field_58397608ccf61'),
(5728, 52, 'content_5_show', '1'),
(5729, 52, '_content_5_show', 'field_58397608ccf61'),
(5730, 52, 'content_6_show', '1'),
(5731, 52, '_content_6_show', 'field_58397608ccf61'),
(5732, 52, 'content_7_show', '1'),
(5733, 52, '_content_7_show', 'field_58397608ccf61'),
(5734, 52, 'content_8_show', '0'),
(5735, 52, '_content_8_show', 'field_58397608ccf61'),
(6088, 194, '_edit_last', '1'),
(6089, 194, '_edit_lock', '1480949345:1'),
(6090, 194, '_visibility', 'visible'),
(6091, 194, '_stock_status', 'instock'),
(6092, 194, 'total_sales', '3'),
(6093, 194, '_downloadable', 'no'),
(6094, 194, '_virtual', 'no'),
(6095, 194, '_tax_status', 'taxable'),
(6096, 194, '_tax_class', ''),
(6097, 194, '_purchase_note', ''),
(6098, 194, '_featured', 'no'),
(6099, 194, '_weight', ''),
(6100, 194, '_length', ''),
(6101, 194, '_width', ''),
(6102, 194, '_height', ''),
(6103, 194, '_sku', ''),
(6104, 194, '_product_attributes', 'a:2:{s:12:"pa_pre-price";a:6:{s:4:"name";s:12:"pa_pre-price";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:0;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";i:0;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(6105, 194, '_regular_price', '40'),
(6106, 194, '_sale_price', ''),
(6107, 194, '_sale_price_dates_from', ''),
(6108, 194, '_sale_price_dates_to', ''),
(6109, 194, '_price', '40'),
(6110, 194, '_sold_individually', ''),
(6111, 194, '_manage_stock', 'no'),
(6112, 194, '_backorders', 'no'),
(6113, 194, '_stock', ''),
(6114, 194, '_upsell_ids', 'a:0:{}'),
(6115, 194, '_crosssell_ids', 'a:0:{}'),
(6116, 194, '_product_version', '2.6.8'),
(6117, 194, '_product_image_gallery', '195,80,165'),
(6118, 194, '_wc_rating_count', 'a:0:{}'),
(6119, 194, '_wc_review_count', '0'),
(6120, 194, '_wc_average_rating', '0'),
(6121, 195, '_wp_attached_file', '2016/11/produkt-podklad.jpg'),
(6122, 195, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:392;s:6:"height";i:392;s:4:"file";s:27:"2016/11/produkt-podklad.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"produkt-podklad-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"produkt-podklad-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"produkt-podklad-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"produkt-podklad-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(6123, 195, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.3030439138997334;s:5:"bytes";i:1912;s:11:"size_before";i:57886;s:10:"size_after";i:55974;s:4:"time";d:0.15999999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:5:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9300000000000002;s:5:"bytes";i:162;s:11:"size_before";i:4126;s:10:"size_after";i:3964;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:3.54;s:5:"bytes";i:371;s:11:"size_before";i:10475;s:10:"size_after";i:10104;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9199999999999999;s:5:"bytes";i:208;s:11:"size_before";i:5302;s:10:"size_after";i:5094;s:4:"time";d:0.029999999999999999;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:10104;s:10:"size_after";i:10104;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:4.2000000000000002;s:5:"bytes";i:1171;s:11:"size_before";i:27879;s:10:"size_after";i:26708;s:4:"time";d:0.089999999999999997;}}}'),
(6124, 194, '_thumbnail_id', '195'),
(6125, 191, '_edit_lock', '1481195386:1'),
(6126, 200, '_wp_attached_file', '2016/11/KT-DX-CAPON-EXTRA-02-2010.pdf'),
(6127, 194, 'content_0_show', '1'),
(6128, 194, '_content_0_show', 'field_58397608ccf61'),
(6129, 194, 'content_0_section_bg', 'black-pure'),
(6130, 194, '_content_0_section_bg', 'field_5836c16c062d1'),
(6131, 194, 'content_0_section_intro', '<h1>Szczegółowe informacje</h1>'),
(6132, 194, '_content_0_section_intro', 'field_5835d59224057'),
(6133, 194, 'content_0_layout_0_col-left', '<h2>Zalety</h2>\r\n<ul>\r\n 	<li>Neutralizuje olejki eteryczne, garbniki i żywice</li>\r\n 	<li>Podkreśla strukturę drewna</li>\r\n 	<li>Nieznacznie wpływa na barwę drewna</li>\r\n 	<li>Szybkoschnący</li>\r\n</ul>\r\n<h2>Dane techniczne</h2>\r\n<ul>\r\n 	<li>Ilość wartstw: 1-2</li>\r\n 	<li>Wydajność z litra: ok. 10m<sup>2</sup> - pędzel, wałek; 30-40m<sup>2</sup> - szpachla parkieciarska</li>\r\n 	<li>Nakładanie warstwy po: 2-4h</li>\r\n 	<li>Rozcieńczalnik: do wyrobów nitrocelulozowych lub poliuretanowych</li>\r\n 	<li>Opakowanie: 5L</li>\r\n</ul>'),
(6134, 194, '_content_0_layout_0_col-left', 'field_583c065c0a8f3'),
(6135, 194, 'content_0_layout_0_col-right', '<h2>Sposób przygotowania</h2>\r\n<strong>Etap 1 - Przygotowanie podłoża</strong>\r\n\r\nPowierzchnia przeznaczona do lakierowania powinna być czysta, sucha, bez pozostałości np. past woskowych i środków nabłyszczających. Stare powłoki lakierowe należy usunąć. Rysy i szczeliny wypełnić szpachlą. Przed lakierowaniem podłoga powinna być wyszlifowana papierem ściernym o granulacji 100 -120 a następnie dokładnie oczyszczona z pyłu.\r\n\r\n<strong>Etap 2 - Lakierowanie</strong>\r\n\r\nPrzed użyciem podkład dokładnie wymieszaj w opakowaniu. Wyrób nie wymaga rozcieńczania. Podkład nakładaj 1-krotnie przy pomocy pędzla lub wałka, 2-krotnie przy pomocy szpachli parkieciarskiej. W przypadku lakierowania drewna egzotycznego i gatunków problematycznych, aby uzyskać odpowiedni poziom odcięcia, zaleca się aplikację wałkiem lub pędzlem z nakładem 100-120 g/m<sup>2</sup>. W przypadku podłóg wykonanych z drewna bukowego, do aplikacji podkładu zalecane jest użycie wałka. Po wyschnięciu (2-4h) można nakładać lakiery nawierzchniowe bez konieczności matowienia.'),
(6136, 194, '_content_0_layout_0_col-right', 'field_583c06a10a8f4'),
(6137, 194, 'content_0_layout', 'a:1:{i:0;s:22:"szczegolowe-informacje";}'),
(6138, 194, '_content_0_layout', 'field_5835d59b24058'),
(6139, 194, 'content_1_show', '1'),
(6140, 194, '_content_1_show', 'field_58397608ccf61'),
(6141, 194, 'content_1_section_bg', 'black-pure'),
(6142, 194, '_content_1_section_bg', 'field_5836c16c062d1'),
(6143, 194, 'content_1_section_intro', '<h1>Do pobrania</h1>'),
(6144, 194, '_content_1_section_intro', 'field_5835d59224057'),
(6145, 194, 'content_1_layout_0_files_0_file', '200'),
(6146, 194, '_content_1_layout_0_files_0_file', 'field_583c06e00a8f7'),
(6147, 194, 'content_1_layout_0_files_1_file', '195'),
(6148, 194, '_content_1_layout_0_files_1_file', 'field_583c06e00a8f7'),
(6149, 194, 'content_1_layout_0_files', '2'),
(6150, 194, '_content_1_layout_0_files', 'field_583c06ce0a8f6'),
(6151, 194, 'content_1_layout', 'a:1:{i:0;s:11:"do-pobrania";}') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(6152, 194, '_content_1_layout', 'field_5835d59b24058'),
(6153, 194, 'content', '2'),
(6154, 194, '_content', 'field_5835d51f24055'),
(6155, 201, '_edit_lock', '1481134828:1'),
(6156, 201, '_edit_last', '1'),
(6502, 60, 'content', ''),
(6503, 60, '_content', 'field_5835d51f24055'),
(6506, 244, '_edit_last', '1'),
(6507, 244, '_edit_lock', '1480528923:1'),
(6508, 244, '_visibility', 'visible'),
(6509, 244, '_stock_status', 'instock'),
(6510, 244, '_downloadable', 'no'),
(6511, 244, '_virtual', 'no'),
(6512, 244, '_tax_status', 'taxable'),
(6513, 244, '_tax_class', ''),
(6514, 244, '_purchase_note', ''),
(6515, 244, '_featured', 'no'),
(6516, 244, '_weight', ''),
(6517, 244, '_length', ''),
(6518, 244, '_width', ''),
(6519, 244, '_height', ''),
(6520, 244, '_product_attributes', 'a:1:{s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(6521, 244, '_regular_price', '40'),
(6522, 244, '_sale_price', ''),
(6523, 244, '_sale_price_dates_from', ''),
(6524, 244, '_sale_price_dates_to', ''),
(6525, 244, '_price', '40'),
(6526, 244, '_sold_individually', ''),
(6527, 244, '_manage_stock', 'no'),
(6528, 244, '_backorders', 'no'),
(6529, 244, '_stock', ''),
(6530, 244, '_upsell_ids', 'a:0:{}'),
(6531, 244, '_crosssell_ids', 'a:0:{}'),
(6532, 244, '_product_version', '2.6.8'),
(6533, 244, '_product_image_gallery', '195,80,165'),
(6534, 244, '_thumbnail_id', '195'),
(6535, 244, 'content_0_show', '1'),
(6536, 244, '_content_0_show', 'field_58397608ccf61'),
(6537, 244, 'content_0_section_bg', 'black-pure'),
(6538, 244, '_content_0_section_bg', 'field_5836c16c062d1'),
(6539, 244, 'content_0_section_intro', '<h1>Szczegółowe informacje</h1>'),
(6540, 244, '_content_0_section_intro', 'field_5835d59224057'),
(6541, 244, 'content_0_layout_0_col-left', '<h2>Zalety</h2>\r\n<ul>\r\n 	<li>Neutralizuje olejki eteryczne, garbniki i żywice</li>\r\n 	<li>Podkreśla strukturę drewna</li>\r\n 	<li>Nieznacznie wpływa na barwę drewna</li>\r\n 	<li>Szybkoschnący</li>\r\n</ul>\r\n<h2>Dane techniczne</h2>\r\n<ul>\r\n 	<li>Ilość wartstw: 1-2</li>\r\n 	<li>Wydajność z litra: ok. 10m<sup>2</sup> - pędzel, wałek; 30-40m<sup>2</sup> - szpachla parkieciarska</li>\r\n 	<li>Nakładanie warstwy po: 2-4h</li>\r\n 	<li>Rozcieńczalnik: do wyrobów nitrocelulozowych lub poliuretanowych</li>\r\n 	<li>Opakowanie: 5L</li>\r\n</ul>'),
(6542, 244, '_content_0_layout_0_col-left', 'field_583c065c0a8f3'),
(6543, 244, 'content_0_layout_0_col-right', '<h2>Sposób przygotowania</h2>\r\n<strong>Etap 1 - Przygotowanie podłoża</strong>\r\n\r\nPowierzchnia przeznaczona do lakierowania powinna być czysta, sucha, bez pozostałości np. past woskowych i środków nabłyszczających. Stare powłoki lakierowe należy usunąć. Rysy i szczeliny wypełnić szpachlą. Przed lakierowaniem podłoga powinna być wyszlifowana papierem ściernym o granulacji 100 -120 a następnie dokładnie oczyszczona z pyłu.\r\n\r\n<strong>Etap 2 - Lakierowanie</strong>\r\n\r\nPrzed użyciem podkład dokładnie wymieszaj w opakowaniu. Wyrób nie wymaga rozcieńczania. Podkład nakładaj 1-krotnie przy pomocy pędzla lub wałka, 2-krotnie przy pomocy szpachli parkieciarskiej. W przypadku lakierowania drewna egzotycznego i gatunków problematycznych, aby uzyskać odpowiedni poziom odcięcia, zaleca się aplikację wałkiem lub pędzlem z nakładem 100-120 g/m<sup>2</sup>. W przypadku podłóg wykonanych z drewna bukowego, do aplikacji podkładu zalecane jest użycie wałka. Po wyschnięciu (2-4h) można nakładać lakiery nawierzchniowe bez konieczności matowienia.'),
(6544, 244, '_content_0_layout_0_col-right', 'field_583c06a10a8f4'),
(6545, 244, 'content_0_layout', 'a:1:{i:0;s:22:"szczegolowe-informacje";}'),
(6546, 244, '_content_0_layout', 'field_5835d59b24058'),
(6547, 244, 'content_1_show', '1'),
(6548, 244, '_content_1_show', 'field_58397608ccf61'),
(6549, 244, 'content_1_section_bg', 'black-pure'),
(6550, 244, '_content_1_section_bg', 'field_5836c16c062d1'),
(6551, 244, 'content_1_section_intro', '<h1>Do pobrania</h1>'),
(6552, 244, '_content_1_section_intro', 'field_5835d59224057'),
(6553, 244, 'content_1_layout_0_files_0_file', '200'),
(6554, 244, '_content_1_layout_0_files_0_file', 'field_583c06e00a8f7'),
(6555, 244, 'content_1_layout_0_files_1_file', '195'),
(6556, 244, '_content_1_layout_0_files_1_file', 'field_583c06e00a8f7'),
(6557, 244, 'content_1_layout_0_files', '2'),
(6558, 244, '_content_1_layout_0_files', 'field_583c06ce0a8f6'),
(6559, 244, 'content_1_layout', 'a:1:{i:0;s:11:"do-pobrania";}'),
(6560, 244, '_content_1_layout', 'field_5835d59b24058'),
(6561, 244, 'content', '2'),
(6562, 244, '_content', 'field_5835d51f24055'),
(6569, 244, 'total_sales', '1'),
(6570, 244, '_sku', ''),
(6913, 52, 'content_0_content_after', ''),
(6914, 52, '_content_0_content_after', 'field_58400d1d4d1e5'),
(6915, 52, 'content_1_content_after', ''),
(6916, 52, '_content_1_content_after', 'field_58400d1d4d1e5'),
(6917, 52, 'content_2_content_after', ''),
(6918, 52, '_content_2_content_after', 'field_58400d1d4d1e5'),
(6919, 52, 'content_3_content_after', ''),
(6920, 52, '_content_3_content_after', 'field_58400d1d4d1e5'),
(6921, 52, 'content_4_content_after', '<a class="btn btn--primary" href="#">Zobacz wszystkie opinie</a>'),
(6922, 52, '_content_4_content_after', 'field_58400d1d4d1e5'),
(6923, 52, 'content_5_content_after', '<a class="btn--primary btn" href="#">Szczegóły dostawy</a>\r\n<p class="xsmall">* czas dostawy do mniejszych miejscowości może wynieść do 48h</p>\r\n<p class="xsmall">Dostawa kurierem Tba EXPRESS wraz ze zwrotem w cenie od 200 do 220 zł.</p>'),
(6924, 52, '_content_5_content_after', 'field_58400d1d4d1e5'),
(6925, 52, 'content_6_content_after', ''),
(6926, 52, '_content_6_content_after', 'field_58400d1d4d1e5'),
(6927, 52, 'content_7_content_after', '<a class="btn btn--primary" href="#">Zobacz wszystkie pytania</a>'),
(6928, 52, '_content_7_content_after', 'field_58400d1d4d1e5'),
(6929, 52, 'content_8_content_after', ''),
(6930, 52, '_content_8_content_after', 'field_58400d1d4d1e5'),
(6931, 52, 'content_9_show', '1'),
(6932, 52, '_content_9_show', 'field_58397608ccf61'),
(6933, 52, 'content_9_section_bg', 'white'),
(6934, 52, '_content_9_section_bg', 'field_5836c16c062d1'),
(6935, 52, 'content_9_section_intro', '<h1>U nas kupisz wszystko</h1>\r\nNie trać czasu na chodzenie po marketach. Zaopatrzymy Ciebie w niezbędne papiery ścierne, lakiery i wałki. Sprzedajemy wszystkie produkty potrzebne do odswieżenia podlogi.'),
(6936, 52, '_content_9_section_intro', 'field_5835d59224057'),
(6937, 52, 'content_9_layout_0_products_0_product', '194'),
(6938, 52, '_content_9_layout_0_products_0_product', 'field_58400cc8fde33'),
(6939, 52, 'content_9_layout_0_products_1_product', '244'),
(6940, 52, '_content_9_layout_0_products_1_product', 'field_58400cc8fde33'),
(6941, 52, 'content_9_layout_0_products', '2'),
(6942, 52, '_content_9_layout_0_products', 'field_58400ca2fde31'),
(6943, 52, 'content_9_layout', 'a:1:{i:0;s:8:"produkty";}'),
(6944, 52, '_content_9_layout', 'field_5835d59b24058'),
(6945, 52, 'content_9_content_after', '<a class="btn btn--primary btn--fill" href="#">Zobacz pozostałe produkty</a>'),
(6946, 52, '_content_9_content_after', 'field_58400d1d4d1e5') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(8075, 253, '_wp_attached_file', '2016/11/mapa-warszawa.png'),
(8076, 253, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:622;s:6:"height";i:602;s:4:"file";s:25:"2016/11/mapa-warszawa.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"mapa-warszawa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:25:"mapa-warszawa-300x290.png";s:5:"width";i:300;s:6:"height";i:290;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"mapa-warszawa-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"mapa-warszawa-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:25:"mapa-warszawa-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(8077, 253, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:30.82600095031728;s:5:"bytes";i:91474;s:11:"size_before";i:296743;s:10:"size_after";i:205269;s:4:"time";d:1.5599999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:6:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:30.940000000000001;s:5:"bytes";i:4099;s:11:"size_before";i:13250;s:10:"size_after";i:9151;s:4:"time";d:0.040000000000000001;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:35.399999999999999;s:5:"bytes";i:14663;s:11:"size_before";i:41422;s:10:"size_after";i:26759;s:4:"time";d:0.17999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:32.5;s:5:"bytes";i:5727;s:11:"size_before";i:17621;s:10:"size_after";i:11894;s:4:"time";d:0.080000000000000002;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:34.159999999999997;s:5:"bytes";i:14433;s:11:"size_before";i:42250;s:10:"size_after";i:27817;s:4:"time";d:0.35999999999999999;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:33.210000000000001;s:5:"bytes";i:45733;s:11:"size_before";i:137715;s:10:"size_after";i:91982;s:4:"time";d:0.62;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:15.33;s:5:"bytes";i:6819;s:11:"size_before";i:44485;s:10:"size_after";i:37666;s:4:"time";d:0.28000000000000003;}}}'),
(9206, 257, '_edit_lock', '1481134822:1'),
(9207, 257, '_edit_last', '1'),
(9208, 276, '_edit_lock', '1480955379:1'),
(9209, 276, '_edit_last', '1'),
(9210, 5, 'content_0_show', '1'),
(9211, 5, '_content_0_show', 'field_5845620a97f9d'),
(9212, 5, 'content_0_layout_0_content', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\n<img class="alignnone wp-image-35 size-full" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg" alt="cykliniarki" width="825" height="455" />\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<a class="btn btn--primary btn--fill" href="#">Button</a>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>'),
(9213, 5, '_content_0_layout_0_content', 'field_584562c22e459'),
(9214, 5, 'content_0_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9215, 5, '_content_0_layout', 'field_584562a62e458'),
(9216, 5, 'content', '1'),
(9217, 5, '_content', 'field_5845620a7c5f4'),
(9226, 315, '_edit_last', '1'),
(9227, 315, '_edit_lock', '1480959890:1'),
(9228, 315, '_visibility', 'visible'),
(9229, 315, '_stock_status', 'instock'),
(9230, 315, 'pricing_type', 'general_pricing'),
(9231, 315, 'general_price', '100'),
(9232, 315, 'hourly_price', '100'),
(9233, 315, 'redq_rental_availability', 'a:2:{i:0;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:17:"Grudzień 1, 2016";s:2:"to";s:18:"Grudzień 31, 2017";s:8:"rentable";s:2:"no";}i:1;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:10:"12/06/2016";s:2:"to";s:10:"12/15/2016";s:8:"rentable";s:2:"no";}}'),
(9234, 315, 'redq_attributes', 'a:0:{}'),
(9235, 315, 'redq_all_data', 'a:6:{s:12:"pricing_type";s:15:"general_pricing";s:15:"general_pricing";s:3:"100";s:14:"hourly_pricing";s:3:"100";s:18:"block_rental_dates";N;s:19:"rental_availability";a:1:{i:0;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:17:"Grudzień 1, 2016";s:2:"to";s:18:"Grudzień 31, 2017";s:8:"rentable";s:2:"no";}}s:10:"attributes";a:0:{}}'),
(9236, 315, 'total_sales', '4'),
(9237, 315, '_downloadable', 'no'),
(9238, 315, '_virtual', 'no'),
(9239, 315, '_tax_status', 'taxable'),
(9240, 315, '_tax_class', ''),
(9241, 315, '_purchase_note', ''),
(9242, 315, '_featured', 'no'),
(9243, 315, '_weight', ''),
(9244, 315, '_length', ''),
(9245, 315, '_width', ''),
(9246, 315, '_height', ''),
(9247, 315, '_sku', ''),
(9248, 315, '_product_attributes', 'a:2:{s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:12:"pa_pre-price";a:6:{s:4:"name";s:12:"pa_pre-price";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(9249, 315, '_regular_price', '100'),
(9250, 315, '_sale_price', ''),
(9251, 315, '_sale_price_dates_from', ''),
(9252, 315, '_sale_price_dates_to', ''),
(9253, 315, '_price', '100'),
(9254, 315, '_sold_individually', ''),
(9255, 315, '_manage_stock', 'yes'),
(9256, 315, '_backorders', 'no'),
(9257, 315, '_stock', '10'),
(9258, 315, '_upsell_ids', 'a:0:{}'),
(9259, 315, '_crosssell_ids', 'a:0:{}'),
(9260, 315, '_product_version', '2.6.8'),
(9261, 315, '_product_image_gallery', ''),
(9262, 315, 'content', ''),
(9263, 315, '_content', 'field_5835d51f24055'),
(9273, 194, 'redq_rental_availability', ''),
(9274, 244, 'redq_rental_availability', ''),
(9306, 318, '_wp_attached_file', '2016/11/cykliniarki-1.jpg'),
(9307, 318, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1550;s:6:"height";i:827;s:4:"file";s:25:"2016/11/cykliniarki-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"cykliniarki-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"cykliniarki-1-300x160.jpg";s:5:"width";i:300;s:6:"height";i:160;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:25:"cykliniarki-1-768x410.jpg";s:5:"width";i:768;s:6:"height";i:410;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:26:"cykliniarki-1-1024x546.jpg";s:5:"width";i:1024;s:6:"height";i:546;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"cykliniarki-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"cykliniarki-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:25:"cykliniarki-1-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-xs";a:4:{s:4:"file";s:25:"cykliniarki-1-768x410.jpg";s:5:"width";i:768;s:6:"height";i:410;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:25:"cykliniarki-1-992x529.jpg";s:5:"width";i:992;s:6:"height";i:529;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:26:"cykliniarki-1-1200x640.jpg";s:5:"width";i:1200;s:6:"height";i:640;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9308, 318, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.2828893468508298;s:5:"bytes";i:18192;s:11:"size_before";i:554146;s:10:"size_after";i:535954;s:4:"time";d:0.43000000000000005;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:10:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.3300000000000001;s:5:"bytes";i:402;s:11:"size_before";i:7544;s:10:"size_after";i:7142;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:4.5;s:5:"bytes";i:586;s:11:"size_before";i:13017;s:10:"size_after";i:12431;s:4:"time";d:0.029999999999999999;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:3.77;s:5:"bytes";i:2321;s:11:"size_before";i:61512;s:10:"size_after";i:59191;s:4:"time";d:0.050000000000000003;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:3.6499999999999999;s:5:"bytes";i:3553;s:11:"size_before";i:97376;s:10:"size_after";i:93823;s:4:"time";d:0.040000000000000001;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.04;s:5:"bytes";i:505;s:11:"size_before";i:10022;s:10:"size_after";i:9517;s:4:"time";d:0.01;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:4.0800000000000001;s:5:"bytes";i:911;s:11:"size_before";i:22343;s:10:"size_after";i:21432;s:4:"time";d:0.02;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:3.2599999999999998;s:5:"bytes";i:2161;s:11:"size_before";i:66262;s:10:"size_after";i:64101;s:4:"time";d:0.040000000000000001;}s:21:"section-background-xs";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:59191;s:10:"size_after";i:59191;s:4:"time";d:0.10000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:3.6000000000000001;s:5:"bytes";i:3334;s:11:"size_before";i:92497;s:10:"size_after";i:89163;s:4:"time";d:0.080000000000000002;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:3.5499999999999998;s:5:"bytes";i:4419;s:11:"size_before";i:124382;s:10:"size_after";i:119963;s:4:"time";d:0.050000000000000003;}}}'),
(9309, 320, '_wp_attached_file', '2016/11/bona-1.jpg'),
(9310, 320, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-1-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-1-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9311, 321, '_wp_attached_file', '2016/11/bona-2.jpg'),
(9312, 321, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-2-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-2-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9313, 320, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.3711911357340725;s:5:"bytes";i:1610;s:11:"size_before";i:25270;s:10:"size_after";i:23660;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.4400000000000004;s:5:"bytes";i:404;s:11:"size_before";i:6272;s:10:"size_after";i:5868;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.6900000000000004;s:5:"bytes";i:393;s:11:"size_before";i:6911;s:10:"size_after";i:6518;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:6.7300000000000004;s:5:"bytes";i:813;s:11:"size_before";i:12087;s:10:"size_after";i:11274;s:4:"time";d:0.01;}}}'),
(9314, 322, '_wp_attached_file', '2016/11/bona-3.jpg'),
(9315, 322, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-3-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-3-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9316, 321, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.981246552675124;s:5:"bytes";i:1447;s:11:"size_before";i:18130;s:10:"size_after";i:16683;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3200000000000003;s:5:"bytes";i:293;s:11:"size_before";i:4633;s:10:"size_after";i:4340;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.9900000000000002;s:5:"bytes";i:345;s:11:"size_before";i:4934;s:10:"size_after";i:4589;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:9.4499999999999993;s:5:"bytes";i:809;s:11:"size_before";i:8563;s:10:"size_after";i:7754;s:4:"time";d:0.01;}}}'),
(9317, 323, '_wp_attached_file', '2016/11/bona-4.jpg'),
(9318, 323, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-4-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-4-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9319, 322, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7966089413810593;s:5:"bytes";i:1876;s:11:"size_before";i:27602;s:10:"size_after";i:25726;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3099999999999996;s:5:"bytes";i:430;s:11:"size_before";i:6819;s:10:"size_after";i:6389;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3799999999999999;s:5:"bytes";i:472;s:11:"size_before";i:7401;s:10:"size_after";i:6929;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.2800000000000002;s:5:"bytes";i:974;s:11:"size_before";i:13382;s:10:"size_after";i:12408;s:4:"time";d:0.01;}}}'),
(9320, 324, '_wp_attached_file', '2016/11/bona-5.jpg'),
(9321, 324, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-5-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-5-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9322, 323, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7761053128441811;s:5:"bytes";i:1292;s:11:"size_before";i:19067;s:10:"size_after";i:17775;s:4:"time";d:0.050000000000000003;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4199999999999999;s:5:"bytes";i:260;s:11:"size_before";i:4794;s:10:"size_after";i:4534;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4699999999999998;s:5:"bytes";i:282;s:11:"size_before";i:5157;s:10:"size_after";i:4875;s:4:"time";d:0.029999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.2300000000000004;s:5:"bytes";i:750;s:11:"size_before";i:9116;s:10:"size_after";i:8366;s:4:"time";d:0.01;}}}'),
(9323, 325, '_wp_attached_file', '2016/11/bona-6.jpg'),
(9324, 325, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-6-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-6-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9325, 324, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7911532045417857;s:5:"bytes";i:1262;s:11:"size_before";i:18583;s:10:"size_after";i:17321;s:4:"time";d:0.050000000000000003;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4299999999999997;s:5:"bytes";i:241;s:11:"size_before";i:4442;s:10:"size_after";i:4201;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.0099999999999998;s:5:"bytes";i:245;s:11:"size_before";i:4893;s:10:"size_after";i:4648;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.3900000000000006;s:5:"bytes";i:776;s:11:"size_before";i:9248;s:10:"size_after";i:8472;s:4:"time";d:0.02;}}}'),
(9326, 325, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.9683315351907087;s:5:"bytes";i:1098;s:11:"size_before";i:15757;s:10:"size_after";i:14659;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.7800000000000002;s:5:"bytes";i:198;s:11:"size_before";i:3423;s:10:"size_after";i:3225;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.1200000000000001;s:5:"bytes";i:217;s:11:"size_before";i:4236;s:10:"size_after";i:4019;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.4299999999999997;s:5:"bytes";i:683;s:11:"size_before";i:8098;s:10:"size_after";i:7415;s:4:"time";d:0.02;}}}'),
(9327, 326, '_wp_attached_file', '2016/11/bona.jpg'),
(9328, 326, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1550;s:6:"height";i:582;s:4:"file";s:16:"2016/11/bona.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"bona-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"bona-300x113.jpg";s:5:"width";i:300;s:6:"height";i:113;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"bona-768x288.jpg";s:5:"width";i:768;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"bona-1024x384.jpg";s:5:"width";i:1024;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"bona-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:16:"bona-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:16:"bona-600x582.jpg";s:5:"width";i:600;s:6:"height";i:582;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-xs";a:4:{s:4:"file";s:16:"bona-768x288.jpg";s:5:"width";i:768;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:16:"bona-992x372.jpg";s:5:"width";i:992;s:6:"height";i:372;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:17:"bona-1200x451.jpg";s:5:"width";i:1200;s:6:"height";i:451;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9329, 326, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.9277077631327804;s:5:"bytes";i:22430;s:11:"size_before";i:571071;s:10:"size_after";i:548641;s:4:"time";d:0.45000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:10:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.1299999999999999;s:5:"bytes";i:489;s:11:"size_before";i:7974;s:10:"size_after";i:7485;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:7.4299999999999997;s:5:"bytes";i:1060;s:11:"size_before";i:14267;s:10:"size_after";i:13207;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:5.4500000000000002;s:5:"bytes";i:3461;s:11:"size_before";i:63500;s:10:"size_after";i:60039;s:4:"time";d:0.029999999999999999;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:4.4699999999999998;s:5:"bytes";i:4452;s:11:"size_before";i:99635;s:10:"size_after";i:95183;s:4:"time";d:0.11;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.6399999999999997;s:5:"bytes";i:605;s:11:"size_before";i:10727;s:10:"size_after";i:10122;s:4:"time";d:0.029999999999999999;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:4.6100000000000003;s:5:"bytes";i:1062;s:11:"size_before";i:23039;s:10:"size_after";i:21977;s:4:"time";d:0.01;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:3.2400000000000002;s:5:"bytes";i:2215;s:11:"size_before";i:68450;s:10:"size_after";i:66235;s:4:"time";d:0.040000000000000001;}s:21:"section-background-xs";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:60039;s:10:"size_after";i:60039;s:4:"time";d:0.040000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:4.3600000000000003;s:5:"bytes";i:4153;s:11:"size_before";i:95225;s:10:"size_after";i:91072;s:4:"time";d:0.080000000000000002;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:3.8500000000000001;s:5:"bytes";i:4933;s:11:"size_before";i:128215;s:10:"size_after";i:123282;s:4:"time";d:0.070000000000000007;}}}'),
(9330, 327, '_wp_attached_file', '2016/11/cyk-1.jpg'),
(9331, 327, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-1-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-1-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9332, 328, '_wp_attached_file', '2016/11/cyk-2.jpg'),
(9333, 328, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-2-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-2-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9334, 329, '_wp_attached_file', '2016/11/cyk-3.jpg'),
(9335, 329, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-3-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-3-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9336, 327, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.4370171170017505;s:5:"bytes";i:1508;s:11:"size_before";i:23427;s:10:"size_after";i:21919;s:4:"time";d:0.080000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.21;s:5:"bytes";i:282;s:11:"size_before";i:5411;s:10:"size_after";i:5129;s:4:"time";d:0.029999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4400000000000004;s:5:"bytes";i:328;s:11:"size_before";i:6028;s:10:"size_after";i:5700;s:4:"time";d:0.029999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.4900000000000002;s:5:"bytes";i:898;s:11:"size_before";i:11988;s:10:"size_after";i:11090;s:4:"time";d:0.02;}}}'),
(9337, 328, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.1406680968433962;s:5:"bytes";i:1864;s:11:"size_before";i:26104;s:10:"size_after";i:24240;s:4:"time";d:0.060000000000000005;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.1699999999999999;s:5:"bytes";i:369;s:11:"size_before";i:5983;s:10:"size_after";i:5614;s:4:"time";d:0.040000000000000001;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.96;s:5:"bytes";i:398;s:11:"size_before";i:6682;s:10:"size_after";i:6284;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.1600000000000001;s:5:"bytes";i:1097;s:11:"size_before";i:13439;s:10:"size_after";i:12342;s:4:"time";d:0.01;}}}'),
(9338, 330, '_wp_attached_file', '2016/11/cyk-4.jpg'),
(9339, 330, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-4-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-4-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9340, 329, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.1048221932601008;s:5:"bytes";i:1908;s:11:"size_before";i:26855;s:10:"size_after";i:24947;s:4:"time";d:0.10000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";i:6;s:5:"bytes";i:365;s:11:"size_before";i:6083;s:10:"size_after";i:5718;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.0099999999999998;s:5:"bytes";i:412;s:11:"size_before";i:6857;s:10:"size_after";i:6445;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.1300000000000008;s:5:"bytes";i:1131;s:11:"size_before";i:13915;s:10:"size_after";i:12784;s:4:"time";d:0.080000000000000002;}}}'),
(9341, 331, '_wp_attached_file', '2016/11/cyk-5.jpg'),
(9342, 331, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-5-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-5-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9343, 330, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.9226737815045976;s:5:"bytes";i:1453;s:11:"size_before";i:20989;s:10:"size_after";i:19536;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4500000000000002;s:5:"bytes";i:268;s:11:"size_before";i:4921;s:10:"size_after";i:4653;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.5;s:5:"bytes";i:292;s:11:"size_before";i:5313;s:10:"size_after";i:5021;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.3000000000000007;s:5:"bytes";i:893;s:11:"size_before";i:10755;s:10:"size_after";i:9862;s:4:"time";d:0.01;}}}'),
(9344, 332, '_wp_attached_file', '2016/11/cyk-6.jpg'),
(9345, 332, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-6-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-6-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9346, 331, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7720289599152395;s:5:"bytes";i:1534;s:11:"size_before";i:22652;s:10:"size_after";i:21118;s:4:"time";d:0.090000000000000011;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.0999999999999996;s:5:"bytes";i:320;s:11:"size_before";i:5249;s:10:"size_after";i:4929;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.5899999999999999;s:5:"bytes";i:323;s:11:"size_before";i:5777;s:10:"size_after";i:5454;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.6600000000000001;s:5:"bytes";i:891;s:11:"size_before";i:11626;s:10:"size_after";i:10735;s:4:"time";d:0.070000000000000007;}}}'),
(9347, 332, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.5010158472165784;s:5:"bytes";i:1846;s:11:"size_before";i:24610;s:10:"size_after";i:22764;s:4:"time";d:0.070000000000000007;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3700000000000001;s:5:"bytes";i:382;s:11:"size_before";i:6001;s:10:"size_after";i:5619;s:4:"time";d:0.029999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.5599999999999996;s:5:"bytes";i:433;s:11:"size_before";i:6596;s:10:"size_after";i:6163;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.5800000000000001;s:5:"bytes";i:1031;s:11:"size_before";i:12013;s:10:"size_after";i:10982;s:4:"time";d:0.029999999999999999;}}}'),
(9348, 59, 'content_0_show', '1'),
(9349, 59, '_content_0_show', 'field_5845620a97f9d'),
(9350, 59, 'content_0_layout_0_content', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9351, 59, '_content_0_layout_0_content', 'field_584562c22e459'),
(9352, 59, 'content_0_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9353, 59, '_content_0_layout', 'field_584562a62e458'),
(9354, 59, 'content_1_show', '1'),
(9355, 59, '_content_1_show', 'field_5845620a97f9d'),
(9356, 59, 'content_1_layout_0_image', '318'),
(9357, 59, '_content_1_layout_0_image', 'field_584562f32e45b'),
(9358, 59, 'content_1_layout_0_capiton', ''),
(9359, 59, '_content_1_layout_0_capiton', 'field_584563072e45c'),
(9360, 59, 'content_1_layout', 'a:1:{i:0;s:7:"zdjecie";}'),
(9361, 59, '_content_1_layout', 'field_584562a62e458'),
(9362, 59, 'content_2_show', '1'),
(9363, 59, '_content_2_show', 'field_5845620a97f9d'),
(9364, 59, 'content_2_layout_0_images_0_image', '320'),
(9365, 59, '_content_2_layout_0_images_0_image', 'field_584564252e45f'),
(9366, 59, 'content_2_layout_0_images_0_caption', 'Large wheels for easy transportation'),
(9367, 59, '_content_2_layout_0_images_0_caption', 'field_584564322e460'),
(9368, 59, 'content_2_layout_0_images_1_image', '321'),
(9369, 59, '_content_2_layout_0_images_1_image', 'field_584564252e45f'),
(9370, 59, 'content_2_layout_0_images_1_caption', 'Thermal and power overload fuse ensures operational safety'),
(9371, 59, '_content_2_layout_0_images_1_caption', 'field_584564322e460'),
(9372, 59, 'content_2_layout_0_images_2_image', '322'),
(9373, 59, '_content_2_layout_0_images_2_image', 'field_584564252e45f'),
(9374, 59, 'content_2_layout_0_images_2_caption', 'Folding and stepless shaft adjustment for easy handling'),
(9375, 59, '_content_2_layout_0_images_2_caption', 'field_584564322e460'),
(9376, 59, 'content_2_layout_0_images_3_image', '323'),
(9377, 59, '_content_2_layout_0_images_3_image', 'field_584564252e45f'),
(9378, 59, 'content_2_layout_0_images_3_caption', 'Fuly sealed system keeps indoor air perfectly clean'),
(9379, 59, '_content_2_layout_0_images_3_caption', 'field_584564322e460'),
(9380, 59, 'content_2_layout_0_images_4_image', '324'),
(9381, 59, '_content_2_layout_0_images_4_image', 'field_584564252e45f'),
(9382, 59, 'content_2_layout_0_images_4_caption', 'Pressure release valve for easy filter clearing'),
(9383, 59, '_content_2_layout_0_images_4_caption', 'field_584564322e460'),
(9384, 59, 'content_2_layout_0_images_5_image', '325'),
(9385, 59, '_content_2_layout_0_images_5_image', 'field_584564252e45f'),
(9386, 59, 'content_2_layout_0_images_5_caption', 'Unique 2-step cyclonic filter separation process'),
(9387, 59, '_content_2_layout_0_images_5_caption', 'field_584564322e460'),
(9388, 59, 'content_2_layout_0_images', '6'),
(9389, 59, '_content_2_layout_0_images', 'field_584564172e45e'),
(9390, 59, 'content_2_layout', 'a:1:{i:0;s:13:"galeria-zdjec";}'),
(9391, 59, '_content_2_layout', 'field_584562a62e458'),
(9392, 59, 'content_3_show', '1'),
(9393, 59, '_content_3_show', 'field_5845620a97f9d'),
(9394, 59, 'content_3_layout_0_content', 'Bona FlexiSand Pro System is our innovative answer to a simpler and smoother working process – one flexible solution for all flooring challenges. Whether it’s precleaning, sanding, oiling or polishing, the all-purpose system offers a dynamic solution with exceptional quality and performance. One powerful solution is at hand in one versatile system.\r\n\r\nNow Bona introduces even more innovations into the FlexiSand Pro System, including the Bona FlexiSand 1.9. With a strong and reliable 1.9 kW motor, it delivers high performance direction-free sanding for even the toughest surfaces such as concrete.'),
(9395, 59, '_content_3_layout_0_content', 'field_584562c22e459'),
(9396, 59, 'content_3_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9397, 59, '_content_3_layout', 'field_584562a62e458'),
(9436, 59, 'content', '6'),
(9437, 59, '_content', 'field_5845620a7c5f4'),
(9528, 59, 'content_2_layout_0_content', 'Bona FlexiSand Pro System is our innovative answer to a simpler and smoother working process – one flexible solution for all flooring challenges. Whether it’s precleaning, sanding, oiling or polishing, the all-purpose system offers a dynamic solution with exceptional quality and performance. One powerful solution is at hand in one versatile system.\r\n\r\nNow Bona introduces even more innovations into the FlexiSand Pro System, including the Bona FlexiSand 1.9. With a strong and reliable 1.9 kW motor, it delivers high performance direction-free sanding for even the toughest surfaces such as concrete.'),
(9529, 59, '_content_2_layout_0_content', 'field_584562c22e459'),
(9530, 59, 'content_3_layout_0_image', '326'),
(9531, 59, '_content_3_layout_0_image', 'field_584562f32e45b'),
(9532, 59, 'content_3_layout_0_capiton', ''),
(9533, 59, '_content_3_layout_0_capiton', 'field_584563072e45c'),
(9540, 59, 'content_4_show', '1'),
(9541, 59, '_content_4_show', 'field_5845620a97f9d'),
(9542, 59, 'content_4_layout_0_image', '326'),
(9543, 59, '_content_4_layout_0_image', 'field_584562f32e45b'),
(9544, 59, 'content_4_layout_0_capiton', ''),
(9545, 59, '_content_4_layout_0_capiton', 'field_584563072e45c'),
(9546, 59, 'content_4_layout', 'a:1:{i:0;s:7:"zdjecie";}'),
(9547, 59, '_content_4_layout', 'field_584562a62e458'),
(9548, 59, 'content_5_show', '1'),
(9549, 59, '_content_5_show', 'field_5845620a97f9d'),
(9550, 59, 'content_5_layout_0_images_0_image', '327'),
(9551, 59, '_content_5_layout_0_images_0_image', 'field_584564252e45f'),
(9552, 59, 'content_5_layout_0_images_0_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9553, 59, '_content_5_layout_0_images_0_caption', 'field_584564322e460'),
(9554, 59, 'content_5_layout_0_images_1_image', '328'),
(9555, 59, '_content_5_layout_0_images_1_image', 'field_584564252e45f'),
(9556, 59, 'content_5_layout_0_images_1_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9557, 59, '_content_5_layout_0_images_1_caption', 'field_584564322e460'),
(9558, 59, 'content_5_layout_0_images_2_image', '329'),
(9559, 59, '_content_5_layout_0_images_2_image', 'field_584564252e45f'),
(9560, 59, 'content_5_layout_0_images_2_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9561, 59, '_content_5_layout_0_images_2_caption', 'field_584564322e460'),
(9562, 59, 'content_5_layout_0_images_3_image', '330'),
(9563, 59, '_content_5_layout_0_images_3_image', 'field_584564252e45f'),
(9564, 59, 'content_5_layout_0_images_3_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9565, 59, '_content_5_layout_0_images_3_caption', 'field_584564322e460'),
(9566, 59, 'content_5_layout_0_images_4_image', '331'),
(9567, 59, '_content_5_layout_0_images_4_image', 'field_584564252e45f'),
(9568, 59, 'content_5_layout_0_images_4_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9569, 59, '_content_5_layout_0_images_4_caption', 'field_584564322e460'),
(9570, 59, 'content_5_layout_0_images_5_image', '332'),
(9571, 59, '_content_5_layout_0_images_5_image', 'field_584564252e45f'),
(9572, 59, 'content_5_layout_0_images_5_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9573, 59, '_content_5_layout_0_images_5_caption', 'field_584564322e460'),
(9574, 59, 'content_5_layout_0_images', '6'),
(9575, 59, '_content_5_layout_0_images', 'field_584564172e45e'),
(9576, 59, 'content_5_layout', 'a:1:{i:0;s:13:"galeria-zdjec";}'),
(9577, 59, '_content_5_layout', 'field_584562a62e458'),
(9770, 59, 'content_1_layout_0_caption', '<h2>The Power of Flexibility</h2>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.'),
(9771, 59, '_content_1_layout_0_caption', 'field_584563072e45c'),
(9772, 59, 'content_4_layout_0_caption', ''),
(9773, 59, '_content_4_layout_0_caption', 'field_584563072e45c'),
(9874, 315, '_booking_min', ''),
(9875, 315, '_booking_max', ''),
(9876, 315, '_booking_dates', 'global') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9877, 315, '_booking_duration', 'global'),
(9878, 315, '_custom_booking_duration', ''),
(9879, 315, '_first_available_date', ''),
(9880, 315, '_booking_option', 'yes'),
(9881, 337, '_edit_last', '1'),
(9882, 337, 'discount_type', 'percent_product'),
(9883, 337, 'coupon_amount', '50'),
(9884, 337, 'individual_use', 'no'),
(9885, 337, 'product_ids', '315'),
(9886, 337, 'exclude_product_ids', ''),
(9887, 337, 'usage_limit', ''),
(9888, 337, 'usage_limit_per_user', ''),
(9889, 337, 'limit_usage_to_x_items', ''),
(9890, 337, 'expiry_date', ''),
(9891, 337, 'free_shipping', 'no'),
(9892, 337, 'exclude_sale_items', 'no'),
(9893, 337, 'product_categories', 'a:0:{}'),
(9894, 337, 'exclude_product_categories', 'a:0:{}'),
(9895, 337, 'minimum_amount', ''),
(9896, 337, 'maximum_amount', ''),
(9897, 337, 'customer_email', 'a:0:{}'),
(9898, 337, '_edit_lock', '1480957874:1'),
(9937, 337, 'usage_count', '1'),
(9938, 337, '_used_by', '1'),
(9950, 339, '_edit_last', '1'),
(9951, 339, '_wp_page_template', 'page-templates/reservation-page.php'),
(9952, 339, '_edit_lock', '1481035612:1'),
(9953, 344, '_edit_last', '1'),
(9954, 344, '_edit_lock', '1481134776:1'),
(9955, 191, '_edit_last', '1'),
(9956, 191, '_wp_page_template', 'default'),
(9957, 192, '_edit_lock', '1481194211:1'),
(9958, 192, '_edit_last', '1'),
(9959, 192, '_wp_page_template', 'default'),
(10004, 354, '_order_key', 'wc_order_58499f5930cbe'),
(10005, 354, '_order_currency', 'PLN'),
(10006, 354, '_prices_include_tax', 'yes'),
(10007, 354, '_customer_ip_address', '::1'),
(10008, 354, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'),
(10009, 354, '_customer_user', '1'),
(10010, 354, '_created_via', 'checkout'),
(10011, 354, '_cart_hash', '9668345381e010505e465d5f934fccac'),
(10012, 354, '_order_version', '2.6.8'),
(10013, 354, '_billing_first_name', 'Test'),
(10014, 354, '_billing_last_name', 'Test'),
(10015, 354, '_billing_company', ''),
(10016, 354, '_billing_email', 'patryk@visibee.pl'),
(10017, 354, '_billing_phone', '608187167'),
(10018, 354, '_billing_country', 'PL'),
(10019, 354, '_billing_address_1', 'Test'),
(10020, 354, '_billing_address_2', ''),
(10021, 354, '_billing_city', 'Test'),
(10022, 354, '_billing_state', ''),
(10023, 354, '_billing_postcode', '81-377'),
(10024, 354, '_shipping_first_name', 'Test'),
(10025, 354, '_shipping_last_name', 'Test'),
(10026, 354, '_shipping_company', ''),
(10027, 354, '_shipping_country', 'PL'),
(10028, 354, '_shipping_address_1', 'Test'),
(10029, 354, '_shipping_address_2', ''),
(10030, 354, '_shipping_city', 'Test'),
(10031, 354, '_shipping_state', ''),
(10032, 354, '_shipping_postcode', '81-377'),
(10033, 354, '_payment_method', 'bacs'),
(10034, 354, '_payment_method_title', 'Direct Bank Transfer'),
(10035, 354, '_order_shipping', ''),
(10036, 354, '_cart_discount', '0'),
(10037, 354, '_cart_discount_tax', '0'),
(10038, 354, '_order_tax', '37.3984'),
(10039, 354, '_order_shipping_tax', '0'),
(10040, 354, '_order_total', '200'),
(10041, 354, '_recorded_sales', 'yes'),
(10042, 354, '_order_stock_reduced', '1'),
(10043, 315, '_booking_days', 'a:3:{i:0;a:3:{i:0;s:10:"2016-12-08";i:1;s:10:"2016-12-27";i:2;i:1;}i:1;a:2:{i:0;s:10:"2016-12-28";i:2;i:2;}i:2;a:2:{i:0;s:10:"2016-12-29";i:2;i:1;}}'),
(10044, 355, '_order_key', 'wc_order_58499fe15da16'),
(10045, 355, '_order_currency', 'PLN'),
(10046, 355, '_prices_include_tax', 'yes'),
(10047, 355, '_customer_ip_address', '::1'),
(10048, 355, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'),
(10049, 355, '_customer_user', '1'),
(10050, 355, '_created_via', 'checkout'),
(10051, 355, '_cart_hash', '9c3f85f69e55643c8e7b81dc9082bf9a'),
(10052, 355, '_order_version', '2.6.8'),
(10053, 355, '_billing_first_name', 'Test'),
(10054, 355, '_billing_last_name', 'Test'),
(10055, 355, '_billing_company', ''),
(10056, 355, '_billing_email', 'patryk@visibee.pl'),
(10057, 355, '_billing_phone', '608187167'),
(10058, 355, '_billing_country', 'PL'),
(10059, 355, '_billing_address_1', 'Test'),
(10060, 355, '_billing_address_2', ''),
(10061, 355, '_billing_city', 'Test'),
(10062, 355, '_billing_state', ''),
(10063, 355, '_billing_postcode', '81-377'),
(10064, 355, '_shipping_first_name', 'Test'),
(10065, 355, '_shipping_last_name', 'Test'),
(10066, 355, '_shipping_company', ''),
(10067, 355, '_shipping_country', 'PL'),
(10068, 355, '_shipping_address_1', 'Test'),
(10069, 355, '_shipping_address_2', '') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(10070, 355, '_shipping_city', 'Test'),
(10071, 355, '_shipping_state', ''),
(10072, 355, '_shipping_postcode', '81-377'),
(10073, 355, '_payment_method', 'bacs'),
(10074, 355, '_payment_method_title', 'Direct Bank Transfer'),
(10075, 355, '_order_shipping', ''),
(10076, 355, '_cart_discount', '0'),
(10077, 355, '_cart_discount_tax', '0'),
(10078, 355, '_order_tax', '392.6829'),
(10079, 355, '_order_shipping_tax', '0'),
(10080, 355, '_order_total', '2100'),
(10081, 355, '_recorded_sales', 'yes'),
(10082, 355, '_order_stock_reduced', '1') ;

#
# End of data contents of table `wpwyp2810161505_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_posts`
#

DROP TABLE IF EXISTS `wpwyp2810161505_posts`;


#
# Table structure of table `wpwyp2810161505_posts`
#

CREATE TABLE `wpwyp2810161505_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_posts`
#
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', '', 0, 'http://wypozyczalniacykliniarek.com/?p=1', 0, 'post', '', 1),
(5, 1, '2016-11-03 16:13:12', '2016-11-03 16:13:12', '', 'Podstrona opisowa', '', 'publish', 'closed', 'closed', '', 'strona-opisowa', '', '', '2016-12-05 12:59:39', '2016-12-05 12:59:39', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=5', 0, 'page', '', 0),
(35, 1, '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 'cykliniarki', '', 'inherit', 'open', 'closed', '', 'cykliniarki', '', '', '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 5, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 'Strona o ciasteczkach', '', 'publish', 'closed', 'closed', '', 'strona-o-ciasteczkach', '', '', '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=45', 0, 'page', '', 0),
(48, 1, '2016-11-07 17:22:44', '2016-11-07 17:22:44', '', 'Kalkulator', '', 'publish', 'closed', 'closed', '', 'kalkulator', '', '', '2016-11-07 17:24:08', '2016-11-07 17:24:08', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=48', 0, 'page', '', 0),
(52, 1, '2016-11-23 10:00:37', '2016-11-23 10:00:37', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2016-12-01 13:41:20', '2016-12-01 13:41:20', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=52', 0, 'page', '', 0),
(54, 1, '2016-11-23 12:30:01', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:01', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2016-11-23 12:30:03', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:03', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=55', 1, 'nav_menu_item', '', 0),
(56, 1, '2016-11-23 12:30:05', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:05', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 'O nas', '', 'publish', 'closed', 'closed', '', 'o-nas', '', '', '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=57', 0, 'page', '', 0),
(59, 1, '2016-11-23 12:30:45', '2016-11-23 12:30:45', '', 'Nasze cykliniarki', '', 'publish', 'closed', 'closed', '', 'nasze-cykliniarki', '', '', '2016-12-05 16:08:13', '2016-12-05 16:08:13', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=59', 0, 'page', '', 0),
(60, 1, '2016-11-23 12:30:51', '2016-11-23 12:30:51', '', 'Nasze produkty', '', 'publish', 'closed', 'closed', '', 'produkty', '', '', '2016-11-30 13:01:42', '2016-11-30 13:01:42', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=60', 0, 'page', '', 0),
(61, 1, '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 'Kontakt', '', 'publish', 'closed', 'closed', '', 'kontakt', '', '', '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=61', 0, 'page', '', 0),
(62, 1, '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 'Zarezerwuj', '', 'publish', 'closed', 'closed', '', 'zarezerwuj', '', '', '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=62', 0, 'page', '', 0),
(67, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=67', 6, 'nav_menu_item', '', 0),
(68, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=68', 5, 'nav_menu_item', '', 0),
(69, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=69', 4, 'nav_menu_item', '', 0),
(70, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=70', 3, 'nav_menu_item', '', 0),
(71, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=71', 2, 'nav_menu_item', '', 0),
(72, 1, '2016-11-23 12:31:52', '2016-11-23 12:31:52', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2016-11-23 12:31:52', '2016-11-23 12:31:52', '', 0, 'http://wypozyczalniacykliniarek.com/?p=72', 1, 'nav_menu_item', '', 0),
(73, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:7:{s:8:"location";a:2:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:28:"page-templates/home-page.php";}}i:1;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:7:"product";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:6:{i:0;s:11:"the_content";i:1;s:7:"excerpt";i:2;s:8:"comments";i:3;s:6:"format";i:4;s:15:"page_attributes";i:5;s:14:"featured_image";}s:11:"description";s:0:"";}', 'Main', 'main', 'publish', 'closed', 'closed', '', 'group_5835d5033efe3', '', '', '2016-12-05 12:47:14', '2016-12-05 12:47:14', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=73', 0, 'acf-field-group', '', 0),
(74, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:19:"field_5836c16c062d1";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5835d51f24055', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 73, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=74', 0, 'acf-field', '', 0),
(75, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_5835d59224057', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=75', 3, 'acf-field', '', 0),
(76, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:10:{i:0;a:6:{s:3:"key";s:13:"5835d5a00eafe";s:5:"label";s:5:"Media";s:4:"name";s:5:"media";s:7:"display";s:5:"table";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"583707ef9372e";s:5:"label";s:4:"None";s:4:"name";s:4:"none";s:7:"display";s:5:"table";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:2;a:6:{s:3:"key";s:13:"5836c41b2cf6f";s:5:"label";s:15:"Jak to działa?";s:4:"name";s:13:"jak-to-dziala";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:3;a:6:{s:3:"key";s:13:"5836ca79b915f";s:5:"label";s:12:"Tani wynajem";s:4:"name";s:12:"tani-wynajem";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:4;a:6:{s:3:"key";s:13:"583837b5a4e28";s:5:"label";s:22:"Sprawdź dostępność";s:4:"name";s:18:"sprawdz-dostepnosc";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:5;a:6:{s:3:"key";s:13:"58385655f0111";s:5:"label";s:6:"Opinie";s:4:"name";s:6:"opinie";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:6;a:6:{s:3:"key";s:13:"583863ae42722";s:5:"label";s:3:"FAQ";s:4:"name";s:3:"faq";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:7;a:6:{s:3:"key";s:13:"583c06360a8f2";s:5:"label";s:24:"Szczegółowe informacje";s:4:"name";s:22:"szczegolowe-informacje";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:8;a:6:{s:3:"key";s:13:"583c06c70a8f5";s:5:"label";s:11:"Do pobrania";s:4:"name";s:11:"do-pobrania";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:9;a:6:{s:3:"key";s:13:"58400ca2fde30";s:5:"label";s:8:"Produkty";s:4:"name";s:8:"produkty";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_5835d59b24058', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=76', 4, 'acf-field', '', 0),
(77, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:16:{s:4:"type";s:5:"image";s:12:"instructions";s:17:"Dodaj zdjęcie...";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5835d5b924059', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=77', 0, 'acf-field', '', 0),
(78, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:34:"... lub kod iframe do pliku wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Iframe', 'iframe', 'publish', 'closed', 'closed', '', 'field_5835d5ea2405b', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=78', 1, 'acf-field', '', 0),
(79, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:27:"Podpis pod zdjęciem/wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_5835d5ca2405a', '', '', '2016-11-24 10:46:23', '2016-11-24 10:46:23', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=79', 2, 'acf-field', '', 0),
(80, 1, '2016-11-23 17:49:26', '2016-11-23 17:49:26', '', 'fig', '', 'inherit', 'open', 'closed', '', 'fig', '', '', '2016-12-01 11:36:44', '2016-12-01 11:36:44', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/fig.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_5836c16c062d1', '', '', '2016-11-28 10:42:11', '2016-11-28 10:42:11', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=89', 1, 'acf-field', '', 0),
(90, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_5836c16c062d1";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_5836c16c062d1";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_5836c1c4062d2', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=90', 2, 'acf-field', '', 0),
(94, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836c41b2cf6f";s:9:"collapsed";s:0:"";s:3:"min";i:4;s:3:"max";i:4;s:6:"layout";s:3:"row";s:12:"button_label";s:8:"Add Step";}', 'Step', 'step', 'publish', 'closed', 'closed', '', 'field_5836c4452cf70', '', '', '2016-11-25 16:11:12', '2016-11-25 16:11:12', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=94', 0, 'acf-field', '', 0),
(95, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:16:{s:11:"znak-cytatu";s:11:"znak-cytatu";s:12:"strzalka-rog";s:12:"strzalka-rog";s:9:"logo-znak";s:9:"logo-znak";s:13:"strzalka-lewo";s:13:"strzalka-lewo";s:14:"strzalka-prawo";s:14:"strzalka-prawo";s:11:"cykliniarka";s:11:"cykliniarka";s:5:"email";s:5:"email";s:6:"myszka";s:6:"myszka";s:6:"olowek";s:6:"olowek";s:7:"telefon";s:7:"telefon";s:7:"pinezka";s:7:"pinezka";s:4:"sejf";s:4:"sejf";s:8:"gwiazdka";s:8:"gwiazdka";s:5:"wozek";s:5:"wozek";s:10:"ciezarowka";s:10:"ciezarowka";s:16:"ciezarowka-pusta";s:16:"ciezarowka-pusta";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_5836c4722cf71', '', '', '2016-11-25 16:11:12', '2016-11-25 16:11:12', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=95', 0, 'acf-field', '', 0),
(96, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";i:2;s:9:"new_lines";s:2:"br";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_5836c4db2cf72', '', '', '2016-11-24 10:54:45', '2016-11-24 10:54:45', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=96', 1, 'acf-field', '', 0),
(101, 1, '2016-11-24 11:10:28', '2016-11-24 11:10:28', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836ca79b915f";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5836ca8eb9160', '', '', '2016-11-24 11:10:28', '2016-11-24 11:10:28', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=101', 0, 'acf-field', '', 0),
(107, 1, '2016-11-24 15:29:03', '2016-11-24 15:29:03', '', 'mapa', '', 'inherit', 'open', 'closed', '', 'mapa', '', '', '2016-11-24 15:29:03', '2016-11-24 15:29:03', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa.png', 0, 'attachment', 'image/png', 0),
(112, 1, '2016-11-24 15:46:02', '2016-11-24 15:46:02', '', 'main', '', 'inherit', 'open', 'closed', '', 'main', '', '', '2016-11-24 15:46:08', '2016-11-24 15:46:08', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/main.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2016-11-24 16:16:10', '2016-11-24 16:16:10', '', 'logo-domalux', '', 'inherit', 'open', 'closed', '', 'logo-domalux', '', '', '2016-12-01 15:31:49', '2016-12-01 15:31:49', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-domalux.png', 0, 'attachment', 'image/png', 0),
(139, 1, '2016-11-24 16:16:12', '2016-11-24 16:16:12', '', 'logo-dotpay', '', 'inherit', 'open', 'closed', '', 'logo-dotpay', '', '', '2016-12-01 15:31:59', '2016-12-01 15:31:59', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-dotpay.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2016-11-24 16:16:14', '2016-11-24 16:16:14', '', 'logo-tba', '', 'inherit', 'open', 'closed', '', 'logo-tba', '', '', '2016-11-24 16:16:14', '2016-11-24 16:16:14', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-tba.png', 0, 'attachment', 'image/png', 0),
(142, 1, '2016-11-24 16:22:48', '2016-11-24 16:22:48', '', 'Regulamin', '', 'publish', 'closed', 'closed', '', 'regulamin', '', '', '2016-11-24 16:22:48', '2016-11-24 16:22:48', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=142', 0, 'page', '', 0),
(143, 1, '2016-11-24 16:22:53', '2016-11-24 16:22:53', '', 'FAQ', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2016-11-24 16:22:53', '2016-11-24 16:22:53', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=143', 0, 'page', '', 0),
(144, 1, '2016-11-24 16:22:58', '2016-11-24 16:22:58', '', 'Cennik', '', 'publish', 'closed', 'closed', '', 'cennik', '', '', '2016-11-24 16:22:58', '2016-11-24 16:22:58', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=144', 0, 'page', '', 0),
(145, 1, '2016-11-24 16:23:18', '2016-11-24 16:23:18', ' ', '', '', 'publish', 'closed', 'closed', '', '145', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=145', 1, 'nav_menu_item', '', 0),
(149, 1, '2016-11-24 16:23:12', '2016-11-24 16:23:12', '', 'Materiały szkoleniowe', '', 'publish', 'closed', 'closed', '', 'materialy-szkoleniowe', '', '', '2016-11-24 16:23:12', '2016-11-24 16:23:12', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=149', 0, 'page', '', 0),
(151, 1, '2016-11-24 16:23:40', '2016-11-24 16:23:40', ' ', '', '', 'publish', 'closed', 'closed', '', '151', '', '', '2016-11-24 16:23:40', '2016-11-24 16:23:40', '', 0, 'http://wypozyczalniacykliniarek.com/?p=151', 5, 'nav_menu_item', '', 0),
(152, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '152', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=152', 4, 'nav_menu_item', '', 0),
(153, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '153', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=153', 3, 'nav_menu_item', '', 0),
(154, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '154', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=154', 2, 'nav_menu_item', '', 0),
(162, 1, '2016-11-25 13:08:44', '2016-11-25 13:08:44', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583837b5a4e28";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Input label', 'input_label', 'publish', 'closed', 'closed', '', 'field_583837c3a4e29', '', '', '2016-11-25 13:08:44', '2016-11-25 13:08:44', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=162', 0, 'acf-field', '', 0),
(163, 1, '2016-11-25 13:08:44', '2016-11-25 13:08:44', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583837b5a4e28";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Search label', 'search_label', 'publish', 'closed', 'closed', '', 'field_583837cfa4e2a', '', '', '2016-11-25 13:08:44', '2016-11-25 13:08:44', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=163', 1, 'acf-field', '', 0),
(165, 1, '2016-11-25 14:56:19', '2016-11-25 14:56:19', '', 'opinie', '', 'inherit', 'open', 'closed', '', 'opinie', '', '', '2016-11-25 14:56:25', '2016-11-25 14:56:25', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/opinie.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2016-11-25 14:58:07', '2016-11-25 14:58:07', '', 'ocena', '', 'inherit', 'open', 'closed', '', 'ocena', '', '', '2016-11-25 14:58:07', '2016-11-25 14:58:07', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/ocena.png', 0, 'attachment', 'image/png', 0),
(170, 1, '2016-11-25 15:20:20', '2016-11-25 15:20:20', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";s:0:"";s:17:"conditional_logic";s:0:"";s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"58385655f0111";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:0:"";s:12:"button_label";s:10:"Add Review";}', 'Reviews', 'reviews', 'publish', 'closed', 'closed', '', 'field_5838565af0112', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=170', 0, 'acf-field', '', 0),
(171, 1, '2016-11-25 15:20:20', '2016-11-25 15:20:20', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_58385679f0113', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=171', 0, 'acf-field', '', 0),
(172, 1, '2016-11-25 15:20:21', '2016-11-25 15:20:21', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:7:"wpautop";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_58385681f0114', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=172', 1, 'acf-field', '', 0),
(173, 1, '2016-11-25 15:20:21', '2016-11-25 15:20:21', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Name', 'name', 'publish', 'closed', 'closed', '', 'field_58385693f0115', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=173', 2, 'acf-field', '', 0),
(176, 1, '2016-11-25 15:26:31', '2016-11-25 15:26:31', '', 'photo', '', 'inherit', 'open', 'closed', '', 'photo', '', '', '2016-11-25 15:26:42', '2016-11-25 15:26:42', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/photo.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583863ae42722";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:12:"Add Question";}', 'FAQs', 'faqs', 'publish', 'closed', 'closed', '', 'field_583863ae42723', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=180', 0, 'acf-field', '', 0),
(181, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Question', 'question', 'publish', 'closed', 'closed', '', 'field_583863f942729', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 180, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=181', 0, 'acf-field', '', 0),
(182, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:2:"br";}', 'Answear', 'answear', 'publish', 'closed', 'closed', '', 'field_583863ff4272a', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 180, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=182', 1, 'acf-field', '', 0),
(188, 1, '2016-11-26 11:46:47', '2016-11-26 11:46:47', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_58397608ccf61', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=188', 0, 'acf-field', '', 0),
(190, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 0, 'http://wypozyczalniacykliniarek.com/shop/', 0, 'page', '', 0),
(191, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '[woocommerce_cart]', 'Koszyk', '', 'publish', 'closed', 'closed', '', 'koszyk', '', '', '2016-12-08 10:28:39', '2016-12-08 10:28:39', '', 0, 'http://wypozyczalniacykliniarek.com/cart/', 0, 'page', '', 0),
(192, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '[woocommerce_checkout]', 'Dostawa', '', 'publish', 'closed', 'closed', '', 'dostawa', '', '', '2016-12-08 10:33:41', '2016-12-08 10:33:41', '', 0, 'http://wypozyczalniacykliniarek.com/checkout/', 0, 'page', '', 0),
(193, 1, '2016-11-26 12:48:06', '2016-11-26 12:48:06', '[woocommerce_my_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2016-11-26 12:48:06', '2016-11-26 12:48:06', '', 0, 'http://wypozyczalniacykliniarek.com/my-account/', 0, 'page', '', 0),
(194, 1, '2016-11-26 12:57:20', '2016-11-26 12:57:20', 'Podkład odcinający AL jest poliwinylowym lakierem podkładowym na bazie alkoholu. Idealnie nadaje się do lakierowania drewna wewnątrz pomieszczeń, a w szczególności podłóg drewnianych: parkietów, mozaiki parkietowej, podłóg przemysłowych i desek z drewna europejskiego i egzotycznego. Minimalizuje wypłukiwanie olejków i garbników oraz stabilizuje luźne cząsteczki drewna. Dzięki właściwościom odcinającym (zamyka pory drewna) polecany na żywicznych i oleistych gatunkach drewna oraz do renowacji starych podłóg drewnianych, w tym wcześniej olejowanych. Zapewnia bardzo dobrą przyczepność lakierów nawierzchniowych oraz zwiększa ich wydajność.', 'Podkład odcinający AL. Domalux', 'To jest Product Short Description', 'publish', 'open', 'closed', '', 'podklad-odcinajacy-al-domalux', '', '', '2016-11-30 18:03:38', '2016-11-30 18:03:38', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&#038;p=194', 0, 'product', '', 0),
(195, 1, '2016-11-26 12:59:00', '2016-11-26 12:59:00', '', 'Atest', '', 'inherit', 'open', 'closed', '', 'produkt-podklad', '', '', '2016-11-28 13:22:03', '2016-11-28 13:22:03', '', 194, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/produkt-podklad.jpg', 0, 'attachment', 'image/jpeg', 0),
(196, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06360a8f2";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Left Column', 'col-left', 'publish', 'closed', 'closed', '', 'field_583c065c0a8f3', '', '', '2016-11-28 10:29:25', '2016-11-28 10:29:25', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=196', 0, 'acf-field', '', 0),
(197, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06360a8f2";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Right Column', 'col-right', 'publish', 'closed', 'closed', '', 'field_583c06a10a8f4', '', '', '2016-11-28 10:29:25', '2016-11-28 10:29:25', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=197', 1, 'acf-field', '', 0),
(198, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06c70a8f5";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add File";}', 'Files', 'files', 'publish', 'closed', 'closed', '', 'field_583c06ce0a8f6', '', '', '2016-11-28 14:11:30', '2016-11-28 14:11:30', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=198', 0, 'acf-field', '', 0),
(199, 1, '2016-11-28 10:29:26', '2016-11-28 10:29:26', 'a:10:{s:4:"type";s:4:"file";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:7:"library";s:3:"all";s:8:"min_size";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'File', 'file', 'publish', 'closed', 'closed', '', 'field_583c06e00a8f7', '', '', '2016-11-28 14:11:30', '2016-11-28 14:11:30', '', 198, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=199', 0, 'acf-field', '', 0),
(200, 1, '2016-11-28 10:37:04', '2016-11-28 10:37:04', '', 'Karta producenta', '', 'inherit', 'open', 'closed', '', 'kt-dx-capon-extra-02-2010', '', '', '2016-11-28 13:21:50', '2016-11-28 13:21:50', '', 194, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/KT-DX-CAPON-EXTRA-02-2010.pdf', 0, 'attachment', 'application/pdf', 0),
(201, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Options / Footer', 'options-footer', 'publish', 'closed', 'closed', '', 'group_583c0ba522a74', '', '', '2016-12-07 18:22:32', '2016-12-07 18:22:32', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=201', 0, 'acf-field-group', '', 0),
(202, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content--footer', 'publish', 'closed', 'closed', '', 'field_583c0ba55100f', '', '', '2016-12-01 15:29:59', '2016-12-01 15:29:59', '', 201, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=202', 0, 'acf-field', '', 0),
(203, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_583c0ba566b90', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=203', 0, 'acf-field', '', 0),
(204, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_583c0ba566f83', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=204', 1, 'acf-field', '', 0),
(205, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_583c0ba566f83";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_583c0ba566f83";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_583c0ba56736b', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=205', 2, 'acf-field', '', 0),
(206, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_583c0ba56774c', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=206', 3, 'acf-field', '', 0),
(207, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:1:{i:0;a:6:{s:3:"key";s:13:"583710010e6cb";s:5:"label";s:6:"Footer";s:4:"name";s:6:"footer";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_583c0ba567b49', '', '', '2016-11-28 10:49:51', '2016-11-28 10:49:51', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=207', 4, 'acf-field', '', 0),
(215, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";i:3;s:6:"layout";s:5:"table";s:12:"button_label";s:11:"Add Contact";}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed167', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=215', 0, 'acf-field', '', 0),
(216, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:16:{s:11:"znak-cytatu";s:11:"znak-cytatu";s:12:"strzalka-rog";s:12:"strzalka-rog";s:9:"logo-znak";s:9:"logo-znak";s:13:"strzalka-lewo";s:13:"strzalka-lewo";s:14:"strzalka-prawo";s:14:"strzalka-prawo";s:11:"cykliniarka";s:11:"cykliniarka";s:5:"email";s:5:"email";s:6:"myszka";s:6:"myszka";s:6:"olowek";s:6:"olowek";s:7:"telefon";s:7:"telefon";s:7:"pinezka";s:7:"pinezka";s:4:"sejf";s:4:"sejf";s:8:"gwiazdka";s:8:"gwiazdka";s:5:"wozek";s:5:"wozek";s:10:"ciezarowka";s:10:"ciezarowka";s:16:"ciezarowka-pusta";s:16:"ciezarowka-pusta";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_583c0ba6c5ecd', '', '', '2016-11-28 10:49:10', '2016-11-28 10:49:10', '', 215, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=216', 0, 'acf-field', '', 0),
(217, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:5:"basic";s:12:"media_upload";i:0;}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_583c0ba6c629f', '', '', '2016-11-28 10:49:10', '2016-11-28 10:49:10', '', 215, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=217', 1, 'acf-field', '', 0),
(218, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:13:"Nasz partner:";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Partners Label', 'partners_label', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed552', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=218', 1, 'acf-field', '', 0),
(219, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add Logo";}', 'Partners', 'partners', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed955', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=219', 2, 'acf-field', '', 0),
(220, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_583c0ba7362fa', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 219, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=220', 0, 'acf-field', '', 0),
(221, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:19:"Współpracujemy z:";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Cooperators Label', 'cooperators_label', 'publish', 'closed', 'closed', '', 'field_583c0ba5edd30', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=221', 3, 'acf-field', '', 0),
(222, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add Logo";}', 'Cooperators', 'cooperators', 'publish', 'closed', 'closed', '', 'field_583c0ba5ee119', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=222', 4, 'acf-field', '', 0),
(223, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_583c0ba7849e0', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 222, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=223', 0, 'acf-field', '', 0),
(244, 1, '2016-11-30 13:15:27', '2016-11-30 13:15:27', 'Podkład odcinający AL jest poliwinylowym lakierem podkładowym na bazie alkoholu. Idealnie nadaje się do lakierowania drewna wewnątrz pomieszczeń, a w szczególności podłóg drewnianych: parkietów, mozaiki parkietowej, podłóg przemysłowych i desek z drewna europejskiego i egzotycznego. Minimalizuje wypłukiwanie olejków i garbników oraz stabilizuje luźne cząsteczki drewna. Dzięki właściwościom odcinającym (zamyka pory drewna) polecany na żywicznych i oleistych gatunkach drewna oraz do renowacji starych podłóg drewnianych, w tym wcześniej olejowanych. Zapewnia bardzo dobrą przyczepność lakierów nawierzchniowych oraz zwiększa ich wydajność.', 'Podkład odcinający AL. Domalux (Kopia)', 'To jest Product Short Description', 'publish', 'open', 'closed', '', 'podklad-odcinajacy-al-domalux-kopia', '', '', '2016-11-30 18:04:16', '2016-11-30 18:04:16', '', 0, 'http://wypozyczalniacykliniarek.com/produkt/podklad-odcinajacy-al-domalux-kopia/', 0, 'product', '', 0),
(246, 1, '2016-12-01 11:44:12', '2016-12-01 11:44:12', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"58400ca2fde30";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:11:"Add Product";}', 'Products', 'products', 'publish', 'closed', 'closed', '', 'field_58400ca2fde31', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=246', 0, 'acf-field', '', 0),
(247, 1, '2016-12-01 11:44:12', '2016-12-01 11:44:12', 'a:11:{s:4:"type";s:11:"post_object";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:1:{i:0;s:7:"product";}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:13:"return_format";s:6:"object";s:2:"ui";i:1;}', 'Product', 'product', 'publish', 'closed', 'closed', '', 'field_58400cc8fde33', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 246, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=247', 0, 'acf-field', '', 0),
(248, 1, '2016-12-01 11:45:11', '2016-12-01 11:45:11', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_58400d1d4d1e5', '', '', '2016-12-01 11:45:11', '2016-12-01 11:45:11', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=248', 5, 'acf-field', '', 0),
(250, 1, '2016-12-01 12:04:09', '2016-12-01 12:04:09', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_584011ac7b85c', '', '', '2016-12-01 12:04:09', '2016-12-01 12:04:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=250', 5, 'acf-field', '', 0),
(253, 1, '2016-12-01 13:31:11', '2016-12-01 13:31:11', '', 'mapa-warszawa', '', 'inherit', 'open', 'closed', '', 'mapa-warszawa', '', '', '2016-12-01 13:31:11', '2016-12-01 13:31:11', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa-warszawa.png', 0, 'attachment', 'image/png', 0),
(257, 1, '2016-12-01 15:34:01', '2016-12-01 15:34:01', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Options / Header', 'options-header', 'publish', 'closed', 'closed', '', 'group_584042e9c9a0d', '', '', '2016-12-07 18:22:38', '2016-12-07 18:22:38', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=257', 0, 'acf-field-group', '', 0),
(258, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content--header', 'publish', 'closed', 'closed', '', 'field_584042ea01135', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 257, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=258', 0, 'acf-field', '', 0),
(259, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_584042ea1aba6', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=259', 0, 'acf-field', '', 0) ;
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(260, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_584042ea1afed', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=260', 1, 'acf-field', '', 0),
(261, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_584042ea1afed";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_584042ea1afed";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_584042ea1b3a7', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=261', 2, 'acf-field', '', 0),
(262, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_584042ea1b7c5', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=262', 3, 'acf-field', '', 0) ;
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(263, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:2:{i:0;a:6:{s:3:"key";s:13:"583710010e6cb";s:5:"label";s:12:"Notification";s:4:"name";s:12:"notification";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"58404491994e7";s:5:"label";s:4:"None";s:4:"name";s:4:"none";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_584042ea1bb70', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=263', 4, 'acf-field', '', 0),
(273, 1, '2016-12-01 15:34:03', '2016-12-01 15:34:03', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_584042ea1bf50', '', '', '2016-12-01 15:34:03', '2016-12-01 15:34:03', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=273', 5, 'acf-field', '', 0),
(274, 1, '2016-12-01 15:41:55', '2016-12-01 15:41:55', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";s:0:"";s:17:"conditional_logic";s:0:"";s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";s:0:"";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_58404456994e6', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 263, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=274', 0, 'acf-field', '', 0),
(275, 1, '2016-12-01 15:41:55', '2016-12-01 15:41:55', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'field_584044ab994e9', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 257, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=275', 1, 'acf-field', '', 0),
(276, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:31:"page-templates/content-page.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:6:{i:0;s:11:"the_content";i:1;s:7:"excerpt";i:2;s:8:"comments";i:3;s:6:"format";i:4;s:15:"page_attributes";i:5;s:14:"featured_image";}s:11:"description";s:0:"";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'group_5845620a498bb', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=276', 0, 'acf-field-group', '', 0),
(277, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";i:1;s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5845620a7c5f4', '', '', '2016-12-05 13:00:51', '2016-12-05 13:00:51', '', 276, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=277', 0, 'acf-field', '', 0),
(278, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_5845620a97f9d', '', '', '2016-12-05 12:48:10', '2016-12-05 12:48:10', '', 277, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=278', 0, 'acf-field', '', 0),
(306, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";i:1;s:3:"max";i:1;s:7:"layouts";a:3:{i:0;a:6:{s:3:"key";s:13:"584562b7e8352";s:5:"label";s:5:"Tekst";s:4:"name";s:5:"tekst";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"584562d02e45a";s:5:"label";s:8:"Zdjęcie";s:4:"name";s:7:"zdjecie";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:2;a:6:{s:3:"key";s:13:"584563412e45d";s:5:"label";s:15:"Galeria zdjęć";s:4:"name";s:13:"galeria-zdjec";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_584562a62e458', '', '', '2016-12-05 15:23:12', '2016-12-05 15:23:12', '', 277, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=306', 1, 'acf-field', '', 0),
(307, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562b7e8352";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_584562c22e459', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=307', 0, 'acf-field', '', 0),
(308, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:16:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562d02e45a";s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_584562f32e45b', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=308', 0, 'acf-field', '', 0),
(309, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562d02e45a";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_584563072e45c', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=309', 1, 'acf-field', '', 0),
(310, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584563412e45d";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:12:"Dodaj wiersz";}', 'Images', 'images', 'publish', 'closed', 'closed', '', 'field_584564172e45e', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=310', 0, 'acf-field', '', 0),
(311, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_584564252e45f', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 310, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=311', 0, 'acf-field', '', 0),
(312, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_584564322e460', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 310, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=312', 1, 'acf-field', '', 0),
(314, 1, '2016-12-05 14:36:48', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-12-05 14:36:48', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&p=314', 0, 'product', '', 0),
(315, 1, '2016-12-05 14:41:00', '2016-12-05 14:41:00', '', 'Duża i mała cykliniarka (zestaw)', '', 'publish', 'open', 'closed', '', 'duza-i-mala-cykliniarka-zestaw', '', '', '2016-12-05 16:41:45', '2016-12-05 16:41:45', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&#038;p=315', 0, 'product', '', 0),
(317, 1, '2016-12-05 14:51:52', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-12-05 14:51:52', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=317', 0, 'page', '', 0),
(318, 1, '2016-12-05 14:54:06', '2016-12-05 14:54:06', '', 'cykliniarki', '', 'inherit', 'open', 'closed', '', 'cykliniarki-2', '', '', '2016-12-05 14:54:12', '2016-12-05 14:54:12', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(320, 1, '2016-12-05 15:01:06', '2016-12-05 15:01:06', '', 'bona-1', '', 'inherit', 'open', 'closed', '', 'bona-1', '', '', '2016-12-05 15:01:27', '2016-12-05 15:01:27', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(321, 1, '2016-12-05 15:01:09', '2016-12-05 15:01:09', '', 'bona-2', '', 'inherit', 'open', 'closed', '', 'bona-2', '', '', '2016-12-05 15:01:09', '2016-12-05 15:01:09', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(322, 1, '2016-12-05 15:01:12', '2016-12-05 15:01:12', '', 'bona-3', '', 'inherit', 'open', 'closed', '', 'bona-3', '', '', '2016-12-05 15:01:12', '2016-12-05 15:01:12', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(323, 1, '2016-12-05 15:01:15', '2016-12-05 15:01:15', '', 'bona-4', '', 'inherit', 'open', 'closed', '', 'bona-4', '', '', '2016-12-05 15:01:15', '2016-12-05 15:01:15', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(324, 1, '2016-12-05 15:01:19', '2016-12-05 15:01:19', '', 'bona-5', '', 'inherit', 'open', 'closed', '', 'bona-5', '', '', '2016-12-05 15:01:19', '2016-12-05 15:01:19', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(325, 1, '2016-12-05 15:01:22', '2016-12-05 15:01:22', '', 'bona-6', '', 'inherit', 'open', 'closed', '', 'bona-6', '', '', '2016-12-05 15:25:47', '2016-12-05 15:25:47', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(326, 1, '2016-12-05 15:02:51', '2016-12-05 15:02:51', '', 'bona', '', 'inherit', 'open', 'closed', '', 'bona', '', '', '2016-12-05 15:02:55', '2016-12-05 15:02:55', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona.jpg', 0, 'attachment', 'image/jpeg', 0),
(327, 1, '2016-12-05 15:07:37', '2016-12-05 15:07:37', '', 'cyk-1', '', 'inherit', 'open', 'closed', '', 'cyk-1', '', '', '2016-12-05 15:07:59', '2016-12-05 15:07:59', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(328, 1, '2016-12-05 15:07:40', '2016-12-05 15:07:40', '', 'cyk-2', '', 'inherit', 'open', 'closed', '', 'cyk-2', '', '', '2016-12-05 15:07:40', '2016-12-05 15:07:40', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(329, 1, '2016-12-05 15:07:43', '2016-12-05 15:07:43', '', 'cyk-3', '', 'inherit', 'open', 'closed', '', 'cyk-3', '', '', '2016-12-05 15:07:43', '2016-12-05 15:07:43', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(330, 1, '2016-12-05 15:07:46', '2016-12-05 15:07:46', '', 'cyk-4', '', 'inherit', 'open', 'closed', '', 'cyk-4', '', '', '2016-12-05 15:07:46', '2016-12-05 15:07:46', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(331, 1, '2016-12-05 15:07:49', '2016-12-05 15:07:49', '', 'cyk-5', '', 'inherit', 'open', 'closed', '', 'cyk-5', '', '', '2016-12-05 15:07:49', '2016-12-05 15:07:49', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(332, 1, '2016-12-05 15:07:52', '2016-12-05 15:07:52', '', 'cyk-6', '', 'inherit', 'open', 'closed', '', 'cyk-6', '', '', '2016-12-05 15:26:49', '2016-12-05 15:26:49', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(337, 1, '2016-12-05 17:13:25', '2016-12-05 17:13:25', '', 'cyk50', '', 'publish', 'closed', 'closed', '', 'cyk50', '', '', '2016-12-05 17:13:25', '2016-12-05 17:13:25', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=shop_coupon&#038;p=337', 0, 'shop_coupon', '', 0),
(339, 1, '2016-12-06 12:36:00', '2016-12-06 12:36:00', '', 'Wycena', '', 'publish', 'closed', 'closed', '', 'wycena', '', '', '2016-12-06 13:59:18', '2016-12-06 13:59:18', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=339', 0, 'page', '', 0),
(343, 1, '2016-12-07 16:50:04', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-12-07 16:50:04', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=343', 0, 'post', '', 0),
(344, 1, '2016-12-07 18:17:56', '2016-12-07 18:17:56', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Options / Breadcrumbs', 'options-breadcrumbs', 'publish', 'closed', 'closed', '', 'group_58485202d9287', '', '', '2016-12-07 18:19:16', '2016-12-07 18:19:16', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=344', 0, 'acf-field-group', '', 0),
(345, 1, '2016-12-07 18:17:57', '2016-12-07 18:17:57', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add Page";}', 'Breadcrumbs', 'breadcrumbs', 'publish', 'closed', 'closed', '', 'field_5848520aef953', '', '', '2016-12-07 18:18:41', '2016-12-07 18:18:41', '', 344, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=345', 0, 'acf-field', '', 0),
(346, 1, '2016-12-07 18:17:57', '2016-12-07 18:17:57', 'a:11:{s:4:"type";s:11:"post_object";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:0:{}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:13:"return_format";s:6:"object";s:2:"ui";i:1;}', 'Page', 'page', 'publish', 'closed', 'closed', '', 'field_5848521aef954', '', '', '2016-12-07 18:18:41', '2016-12-07 18:18:41', '', 345, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=346', 0, 'acf-field', '', 0),
(347, 1, '2016-12-07 18:18:41', '2016-12-07 18:18:41', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_58485267548ae', '', '', '2016-12-07 18:18:41', '2016-12-07 18:18:41', '', 345, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=347', 1, 'acf-field', '', 0),
(354, 1, '2016-12-08 17:58:49', '2016-12-08 17:58:49', '', 'Order &ndash; Grudzień 8, 2016 @ 05:58 PM', '', 'wc-on-hold', 'open', 'closed', 'order_58499f592831b', 'zamowienie-dec-08-2016-o-0558-pm', '', '', '2016-12-08 17:58:49', '2016-12-08 17:58:49', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=shop_order&#038;p=354', 0, 'shop_order', '', 2),
(355, 1, '2016-12-08 18:01:05', '2016-12-08 18:01:05', '', 'Order &ndash; Grudzień 8, 2016 @ 06:01 PM', '', 'wc-on-hold', 'open', 'closed', 'order_58499fe151e42', 'zamowienie-dec-08-2016-o-0601-pm', '', '', '2016-12-08 18:01:05', '2016-12-08 18:01:05', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=shop_order&#038;p=355', 0, 'shop_order', '', 2) ;

#
# End of data contents of table `wpwyp2810161505_posts`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_relationships`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_relationships`;


#
# Table structure of table `wpwyp2810161505_term_relationships`
#

CREATE TABLE `wpwyp2810161505_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_relationships`
#
INSERT INTO `wpwyp2810161505_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(67, 2, 0),
(68, 2, 0),
(69, 2, 0),
(70, 2, 0),
(71, 2, 0),
(72, 2, 0),
(145, 3, 0),
(151, 3, 0),
(152, 3, 0),
(153, 3, 0),
(154, 3, 0),
(194, 4, 0),
(194, 8, 0),
(194, 10, 0),
(244, 4, 0),
(244, 9, 0),
(315, 4, 0),
(315, 10, 0),
(315, 11, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_taxonomy`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_taxonomy`;


#
# Table structure of table `wpwyp2810161505_term_taxonomy`
#

CREATE TABLE `wpwyp2810161505_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_taxonomy`
#
INSERT INTO `wpwyp2810161505_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'nav_menu', '', 0, 5),
(4, 4, 'product_type', '', 0, 3),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(7, 7, 'product_type', '', 0, 0),
(8, 8, 'pa_jednostka-miary', '', 0, 1),
(9, 9, 'pa_jednostka-miary', '', 0, 1),
(10, 10, 'pa_pre-price', '', 0, 2),
(11, 11, 'pa_jednostka-miary', '', 0, 1),
(12, 12, 'product_type', '', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_termmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_termmeta`;


#
# Table structure of table `wpwyp2810161505_termmeta`
#

CREATE TABLE `wpwyp2810161505_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_termmeta`
#
INSERT INTO `wpwyp2810161505_termmeta` ( `meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 8, 'order_pa_jednostka-miary', '0'),
(2, 9, 'order_pa_jednostka-miary', '0'),
(3, 10, 'order_pa_pre-price', '0'),
(4, 11, 'order_pa_jednostka-miary', '0') ;

#
# End of data contents of table `wpwyp2810161505_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_terms`
#

DROP TABLE IF EXISTS `wpwyp2810161505_terms`;


#
# Table structure of table `wpwyp2810161505_terms`
#

CREATE TABLE `wpwyp2810161505_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_terms`
#
INSERT INTO `wpwyp2810161505_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Header', 'header', 0),
(3, 'Footer', 'footer', 0),
(4, 'simple', 'simple', 0),
(5, 'grouped', 'grouped', 0),
(6, 'variable', 'variable', 0),
(7, 'external', 'external', 0),
(8, 'szt.', 'szt', 0),
(9, 'l', 'l', 0),
(10, 'od', 'od', 0),
(11, 'doba', 'doba', 0),
(12, 'redq_rental', 'redq_rental', 0) ;

#
# End of data contents of table `wpwyp2810161505_terms`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_usermeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_usermeta`;


#
# Table structure of table `wpwyp2810161505_usermeta`
#

CREATE TABLE `wpwyp2810161505_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_usermeta`
#
INSERT INTO `wpwyp2810161505_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin_wypozycczalnia'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wpwyp2810161505_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wpwyp2810161505_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'black_studio_tinymce_widget'),
(13, 1, 'show_welcome_panel', '1'),
(15, 1, 'wpwyp2810161505_dashboard_quick_press_last_post_id', '343'),
(16, 1, 'wpwyp2810161505_user-settings', 'editor=html&libraryContent=browse&imgsize=full'),
(17, 1, 'wpwyp2810161505_user-settings-time', '1481031647'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'nav_menu_recently_edited', '3'),
(21, 1, 'session_tokens', 'a:2:{s:64:"aa57f493d92a48619a50e126985e37ac1fbd9880a383fbb94f04246e3b187ecc";a:4:{s:10:"expiration";i:1481131709;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";s:5:"login";i:1479922109;}s:64:"6f07e4d64d4fe40c733ae3cb0f33af1147b3c91285cfda820924763d837d17a8";a:4:{s:10:"expiration";i:1482150877;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36";s:5:"login";i:1480941277;}}'),
(22, 1, 'acf_user_settings', 'a:2:{s:29:"collapsed_field_5835d51f24055";s:17:"0,1,2,3,4,6,7,8,9";s:29:"collapsed_field_584042ea1bb70";s:1:"0";}'),
(23, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(25, 1, 'closedpostboxes_product', 'a:1:{i:0;s:11:"postexcerpt";}'),
(26, 1, 'metaboxhidden_product', 'a:7:{i:0;s:12:"postimagediv";i:1;s:23:"acf-group_5845620a498bb";i:2;s:23:"acf-group_583c0ba522a74";i:3;s:23:"acf-group_584042e9c9a0d";i:4;s:10:"postcustom";i:5;s:7:"slugdiv";i:6;s:11:"postexcerpt";}'),
(27, 1, 'closedpostboxes_toplevel_page_acf-options', 'a:2:{i:0;s:23:"acf-group_583c0ba522a74";i:1;s:23:"acf-group_584042e9c9a0d";}'),
(28, 1, 'metaboxhidden_toplevel_page_acf-options', 'a:0:{}'),
(29, 1, 'DOPBSP_backend_language', 'en'),
(30, 1, 'billing_first_name', 'Test'),
(31, 1, 'billing_last_name', 'Test'),
(32, 1, 'billing_company', ''),
(33, 1, 'billing_email', 'patryk@visibee.pl'),
(34, 1, 'billing_phone', '608187167'),
(35, 1, 'billing_country', 'PL'),
(36, 1, 'billing_address_1', 'Test'),
(37, 1, 'billing_address_2', ''),
(38, 1, 'billing_city', 'Test'),
(39, 1, 'billing_state', ''),
(40, 1, 'billing_postcode', '81-377'),
(41, 1, 'meta-box-order_product', 'a:4:{s:15:"acf_after_title";s:0:"";s:4:"side";s:84:"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images";s:6:"normal";s:163:"woocommerce-product-data,acf-group_5845620a498bb,acf-group_583c0ba522a74,acf-group_584042e9c9a0d,acf-group_5835d5033efe3,postcustom,slugdiv,postexcerpt,commentsdiv";s:8:"advanced";s:0:"";}'),
(42, 1, 'screen_layout_product', '2') ;

#
# End of data contents of table `wpwyp2810161505_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_users`
#

DROP TABLE IF EXISTS `wpwyp2810161505_users`;


#
# Table structure of table `wpwyp2810161505_users`
#

CREATE TABLE `wpwyp2810161505_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_users`
#
INSERT INTO `wpwyp2810161505_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin_wypozyczalnia', '$P$BqCt3f5NgafK1P8Ci3qrqDTmBA1bAP0', 'admin_wypozyczalnia', 'patryk@visibee.pl', '', '2016-10-28 13:10:21', '', 0, 'admin_wypozycczalnia') ;

#
# End of data contents of table `wpwyp2810161505_users`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_api_keys`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_api_keys`;


#
# Table structure of table `wpwyp2810161505_woocommerce_api_keys`
#

CREATE TABLE `wpwyp2810161505_woocommerce_api_keys` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `permissions` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8_unicode_ci,
  `truncated_key` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  PRIMARY KEY (`key_id`),
  KEY `consumer_key` (`consumer_key`),
  KEY `consumer_secret` (`consumer_secret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_api_keys`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_api_keys`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_attribute_taxonomies`;


#
# Table structure of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#

CREATE TABLE `wpwyp2810161505_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_label` longtext COLLATE utf8_unicode_ci,
  `attribute_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_orderby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#
INSERT INTO `wpwyp2810161505_woocommerce_attribute_taxonomies` ( `attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'jednostka-miary', 'Jednostka miary', 'select', 'menu_order', 0),
(2, 'pre-price', 'Przed ceną', 'select', 'menu_order', 0) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_downloadable_product_permissions`;


#
# Table structure of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

CREATE TABLE `wpwyp2810161505_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `download_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_order_itemmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_order_itemmeta`;


#
# Table structure of table `wpwyp2810161505_woocommerce_order_itemmeta`
#

CREATE TABLE `wpwyp2810161505_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_order_itemmeta`
#
INSERT INTO `wpwyp2810161505_woocommerce_order_itemmeta` ( `meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(15, 3, '_qty', '1'),
(16, 3, '_tax_class', ''),
(17, 3, '_product_id', '315'),
(18, 3, '_variation_id', '0'),
(19, 3, '_line_subtotal', '162.6016'),
(20, 3, '_line_total', '162.6016'),
(21, 3, '_line_subtotal_tax', '37.3984'),
(22, 3, '_line_tax', '37.3984'),
(23, 3, '_line_tax_data', 'a:2:{s:5:"total";a:1:{i:1;s:7:"37.3984";}s:8:"subtotal";a:1:{i:1;s:7:"37.3984";}}'),
(24, 3, '_ebs_start_display', '28 grudzień 2016'),
(25, 3, '_ebs_end_display', '29 grudzień 2016'),
(26, 3, '_ebs_start_format', '2016-12-28'),
(27, 3, '_ebs_end_format', '2016-12-29'),
(28, 4, 'rate_id', '1'),
(29, 4, 'label', 'VAT'),
(30, 4, 'compound', '0'),
(31, 4, 'tax_amount', '37.3984'),
(32, 4, 'shipping_tax_amount', '0'),
(33, 5, '_qty', '1'),
(34, 5, '_tax_class', ''),
(35, 5, '_product_id', '315'),
(36, 5, '_variation_id', '0'),
(37, 5, '_line_subtotal', '1707.3171'),
(38, 5, '_line_total', '1707.3171'),
(39, 5, '_line_subtotal_tax', '392.6829'),
(40, 5, '_line_tax', '392.6829'),
(41, 5, '_line_tax_data', 'a:2:{s:5:"total";a:1:{i:1;s:8:"392.6829";}s:8:"subtotal";a:1:{i:1;s:8:"392.6829";}}'),
(42, 5, '_ebs_start_display', '8 grudzień 2016'),
(43, 5, '_ebs_end_display', '28 grudzień 2016'),
(44, 5, '_ebs_start_format', '2016-12-08'),
(45, 5, '_ebs_end_format', '2016-12-28'),
(46, 6, 'rate_id', '1'),
(47, 6, 'label', 'VAT'),
(48, 6, 'compound', '0'),
(49, 6, 'tax_amount', '392.6829'),
(50, 6, 'shipping_tax_amount', '0') ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_order_itemmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_order_items`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_order_items`;


#
# Table structure of table `wpwyp2810161505_woocommerce_order_items`
#

CREATE TABLE `wpwyp2810161505_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_order_items`
#
INSERT INTO `wpwyp2810161505_woocommerce_order_items` ( `order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(3, 'Duża i mała cykliniarka (zestaw)', 'line_item', 354),
(4, 'PL-VAT-1', 'tax', 354),
(5, 'Duża i mała cykliniarka (zestaw)', 'line_item', 355),
(6, 'PL-VAT-1', 'tax', 355) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_order_items`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_payment_tokenmeta`;


#
# Table structure of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

CREATE TABLE `wpwyp2810161505_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `payment_token_id` (`payment_token_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_payment_tokens`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_payment_tokens`;


#
# Table structure of table `wpwyp2810161505_woocommerce_payment_tokens`
#

CREATE TABLE `wpwyp2810161505_woocommerce_payment_tokens` (
  `token_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_payment_tokens`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_payment_tokens`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_sessions`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_sessions`;


#
# Table structure of table `wpwyp2810161505_woocommerce_sessions`
#

CREATE TABLE `wpwyp2810161505_woocommerce_sessions` (
  `session_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `session_expiry` bigint(20) NOT NULL,
  PRIMARY KEY (`session_key`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_sessions`
#
INSERT INTO `wpwyp2810161505_woocommerce_sessions` ( `session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(122, '1', 'a:6:{s:21:"removed_cart_contents";s:1267:"a:2:{s:32:"745be145c17c1d3e1bde2bf4a6ac954b";a:15:{s:14:"_booking_price";s:4:"1700";s:17:"_booking_duration";i:17;s:11:"_start_date";s:16:"6 grudzień 2016";s:9:"_end_date";s:17:"22 grudzień 2016";s:10:"_ebs_start";s:10:"2016-12-06";s:8:"_ebs_end";s:10:"2016-12-22";s:10:"product_id";i:315;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:1382.1138000000001;s:8:"line_tax";d:317.88619999999997;s:13:"line_subtotal";d:1382.1138000000001;s:17:"line_subtotal_tax";d:317.88619999999997;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:317.88619999999997;}s:8:"subtotal";a:1:{i:1;d:317.88619999999997;}}}s:32:"051fcd864e119b297c37ca97f2ad1737";a:15:{s:14:"_booking_price";s:3:"850";s:17:"_booking_duration";i:17;s:11:"_start_date";s:16:"6 grudzień 2016";s:9:"_end_date";s:17:"22 grudzień 2016";s:10:"_ebs_start";s:10:"2016-12-06";s:8:"_ebs_end";s:10:"2016-12-22";s:10:"product_id";i:315;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:8;s:10:"line_total";d:2764.2276000000002;s:8:"line_tax";d:635.77239999999995;s:13:"line_subtotal";d:5528.4552999999996;s:17:"line_subtotal_tax";d:1271.5446999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:635.77239999999995;}s:8:"subtotal";a:1:{i:1;d:1271.5446999999999;}}}}";s:14:"shipping_total";N;s:10:"wc_notices";N;s:21:"chosen_payment_method";s:4:"bacs";s:8:"customer";s:407:"a:14:{s:8:"postcode";s:6:"81-377";s:4:"city";s:4:"Test";s:9:"address_1";s:4:"Test";s:9:"address_2";s:0:"";s:5:"state";s:0:"";s:7:"country";s:2:"PL";s:17:"shipping_postcode";s:6:"81-377";s:13:"shipping_city";s:4:"Test";s:18:"shipping_address_1";s:4:"Test";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:0:"";s:16:"shipping_country";s:2:"PL";s:13:"is_vat_exempt";b:0;s:19:"calculated_shipping";b:1;}";s:7:"booking";s:6:"a:0:{}";}', 1481365751) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_sessions`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zone_locations`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`),
  KEY `location_type` (`location_type`),
  KEY `location_type_code` (`location_type`,`location_code`(90))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zone_methods`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `method_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method_order` bigint(20) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zones`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zones`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zones`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zones` (
  `zone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zone_order` bigint(20) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zones`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zones`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_tax_rate_locations`;


#
# Table structure of table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

CREATE TABLE `wpwyp2810161505_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type` (`location_type`),
  KEY `location_type_code` (`location_type`,`location_code`(90))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_tax_rate_locations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_tax_rates`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_tax_rates`;


#
# Table structure of table `wpwyp2810161505_woocommerce_tax_rates`
#

CREATE TABLE `wpwyp2810161505_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`(191)),
  KEY `tax_rate_state` (`tax_rate_state`(191)),
  KEY `tax_rate_class` (`tax_rate_class`(191)),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_tax_rates`
#
INSERT INTO `wpwyp2810161505_woocommerce_tax_rates` ( `tax_rate_id`, `tax_rate_country`, `tax_rate_state`, `tax_rate`, `tax_rate_name`, `tax_rate_priority`, `tax_rate_compound`, `tax_rate_shipping`, `tax_rate_order`, `tax_rate_class`) VALUES
(1, 'PL', '', '23.0000', 'VAT', 1, 0, 1, 0, '') ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_tax_rates`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

