# WordPress MySQL database migration
#
# Generated: Monday 5. December 2016 17:16 UTC
# Hostname: localhost
# Database: `wypozyczalnia`
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wpwyp2810161505_commentmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_commentmeta`;


#
# Table structure of table `wpwyp2810161505_commentmeta`
#

CREATE TABLE `wpwyp2810161505_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_commentmeta`
#

#
# End of data contents of table `wpwyp2810161505_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_comments`
#

DROP TABLE IF EXISTS `wpwyp2810161505_comments`;


#
# Table structure of table `wpwyp2810161505_comments`
#

CREATE TABLE `wpwyp2810161505_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_comments`
#
INSERT INTO `wpwyp2810161505_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 316, 'WooCommerce', '', '', '', '2016-12-05 14:45:40', '2016-12-05 14:45:40', 'Czekanie na przelew Status zamówienia zmieniony z Oczekujące na płatność na Wstrzymane (oczekujące na płatność).', 0, '1', 'WooCommerce', 'order_note', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_comments`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_api_keys`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_api_keys`;


#
# Table structure of table `wpwyp2810161505_dopbsp_api_keys`
#

CREATE TABLE `wpwyp2810161505_dopbsp_api_keys` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `api_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_api_keys`
#
INSERT INTO `wpwyp2810161505_dopbsp_api_keys` ( `id`, `user_id`, `api_key`) VALUES
(1, 1, 'dlXf0Re0tJ7A7bw6R8lELOkWPaLNkCOv') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_api_keys`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_calendars`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_calendars`;


#
# Table structure of table `wpwyp2810161505_dopbsp_calendars`
#

CREATE TABLE `wpwyp2810161505_dopbsp_calendars` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `max_year` int(10) unsigned NOT NULL DEFAULT '0',
  `hours_enabled` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `hours_interval_enabled` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `price_min` float NOT NULL DEFAULT '0',
  `price_max` float NOT NULL DEFAULT '0',
  `rating` float NOT NULL DEFAULT '0',
  `address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_en` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_alt` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_alt_en` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coordinates` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_calendars`
#
INSERT INTO `wpwyp2810161505_dopbsp_calendars` ( `id`, `user_id`, `post_id`, `name`, `max_year`, `hours_enabled`, `hours_interval_enabled`, `price_min`, `price_max`, `rating`, `address`, `address_en`, `address_alt`, `address_alt_en`, `coordinates`) VALUES
(1, 1, 0, 'New calendar', 0, 'false', 'false', '0', '0', '0', '', '', '', '', '') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_calendars`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_coupons`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_coupons`;


#
# Table structure of table `wpwyp2810161505_dopbsp_coupons`
#

CREATE TABLE `wpwyp2810161505_dopbsp_coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `start_date` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_date` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `start_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no_coupons` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `operation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '+',
  `price` float NOT NULL DEFAULT '0',
  `price_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fixed',
  `price_by` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'once',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_coupons`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_coupons`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_days`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_days`;


#
# Table structure of table `wpwyp2810161505_dopbsp_days`
#

CREATE TABLE `wpwyp2810161505_dopbsp_days` (
  `unique_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `day` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `year` smallint(5) unsigned NOT NULL DEFAULT '2016',
  `data` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `price_min` float NOT NULL DEFAULT '0',
  `price_max` float NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`unique_key`),
  KEY `calendar_id` (`calendar_id`),
  KEY `day` (`day`),
  KEY `year` (`year`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_days`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_days`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_days_available`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_days_available`;


#
# Table structure of table `wpwyp2810161505_dopbsp_days_available`
#

CREATE TABLE `wpwyp2810161505_dopbsp_days_available` (
  `unique_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `day` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hour` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`unique_key`),
  KEY `day` (`day`),
  KEY `hour` (`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_days_available`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_days_available`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_days_unavailable`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_days_unavailable`;


#
# Table structure of table `wpwyp2810161505_dopbsp_days_unavailable`
#

CREATE TABLE `wpwyp2810161505_dopbsp_days_unavailable` (
  `unique_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `day` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hour` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`unique_key`),
  KEY `day` (`day`),
  KEY `hour` (`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_days_unavailable`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_days_unavailable`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_discounts`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_discounts`;


#
# Table structure of table `wpwyp2810161505_dopbsp_discounts`
#

CREATE TABLE `wpwyp2810161505_dopbsp_discounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extras` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_discounts`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_discounts`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_discounts_items`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_discounts_items`;


#
# Table structure of table `wpwyp2810161505_dopbsp_discounts_items`
#

CREATE TABLE `wpwyp2810161505_dopbsp_discounts_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `discount_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `start_time_lapse` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_time_lapse` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `operation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `price` float NOT NULL DEFAULT '0',
  `price_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'percent',
  `price_by` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'once',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `discount_id` (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_discounts_items`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_discounts_items`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_discounts_items_rules`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_discounts_items_rules`;


#
# Table structure of table `wpwyp2810161505_dopbsp_discounts_items_rules`
#

CREATE TABLE `wpwyp2810161505_dopbsp_discounts_items_rules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `discount_item_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_date` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `start_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `operation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `price` float NOT NULL DEFAULT '0',
  `price_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'percent',
  `price_by` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'once',
  UNIQUE KEY `id` (`id`),
  KEY `discount_item_id` (`discount_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_discounts_items_rules`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_discounts_items_rules`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_emails`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_emails`;


#
# Table structure of table `wpwyp2810161505_dopbsp_emails`
#

CREATE TABLE `wpwyp2810161505_dopbsp_emails` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_emails`
#
INSERT INTO `wpwyp2810161505_dopbsp_emails` ( `id`, `user_id`, `name`) VALUES
(1, 0, 'Default email templates') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_emails`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_emails_translation`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_emails_translation`;


#
# Table structure of table `wpwyp2810161505_dopbsp_emails_translation`
#

CREATE TABLE `wpwyp2810161505_dopbsp_emails_translation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `template` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `email_id` (`email_id`),
  KEY `template` (`template`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_emails_translation`
#
INSERT INTO `wpwyp2810161505_dopbsp_emails_translation` ( `id`, `email_id`, `template`, `subject`, `message`) VALUES
(1, 1, 'book_admin', '{"en":"You received a booking request."}', '{"en":"Below are the details. Go to admin to approve or reject the request.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(2, 1, 'book_user', '{"en":"Your booking request has been sent."}', '{"en":"Please wait for approval. Below are the details.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(3, 1, 'book_with_approval_admin', '{"en":"You received a booking request."}', '{"en":"Below are the details. Go to admin to cancel the request.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(4, 1, 'book_with_approval_user', '{"en":"Your booking request has been sent."}', '{"en":"Below are the details.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(5, 1, 'approved', '{"en":"Your booking request has been approved."}', '{"en":"Congratulations! Your booking request has been approved. Details about your request are below.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(6, 1, 'canceled', '{"en":"Your booking request has been canceled."}', '{"en":"I<<single-quote>>m sorry but your booking request has been canceled. Details about your request are below.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}'),
(7, 1, 'rejected', '{"en":"Your booking request has been rejected."}', '{"en":"I<<single-quote>>m sorry but your booking request has been rejected. Details about your request are below.<<new-line>><br \\/><br \\/><<new-line>>|DETAILS|<<new-line>><br \\/><br \\/><<new-line>>|EXTRAS|<<new-line>><br \\/><br \\/><<new-line>>|DISCOUNT|<<new-line>><br \\/><br \\/><<new-line>>|COUPON|<<new-line>><br \\/><br \\/><<new-line>>|FEES|<<new-line>><br \\/><br \\/><<new-line>>|FORM|<<new-line>><br \\/><br \\/><<new-line>>|BILLING ADDRESS|<<new-line>><br \\/><br \\/><<new-line>>|SHIPPING ADDRESS|"}') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_emails_translation`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_extras`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_extras`;


#
# Table structure of table `wpwyp2810161505_dopbsp_extras`
#

CREATE TABLE `wpwyp2810161505_dopbsp_extras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_extras`
#
INSERT INTO `wpwyp2810161505_dopbsp_extras` ( `id`, `user_id`, `name`) VALUES
(1, 1, 'People') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_extras`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_extras_groups`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_extras_groups`;


#
# Table structure of table `wpwyp2810161505_dopbsp_extras_groups`
#

CREATE TABLE `wpwyp2810161505_dopbsp_extras_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extra_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `multiple_select` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `required` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `no_items_multiply` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `extra_id` (`extra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_extras_groups`
#
INSERT INTO `wpwyp2810161505_dopbsp_extras_groups` ( `id`, `extra_id`, `position`, `multiple_select`, `required`, `no_items_multiply`, `translation`) VALUES
(1, 1, 1, 'false', 'true', 'true', '{"en":"Adults"}'),
(2, 1, 2, 'false', 'true', 'true', '{"en":"Children"}') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_extras_groups`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_extras_groups_items`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_extras_groups_items`;


#
# Table structure of table `wpwyp2810161505_dopbsp_extras_groups_items`
#

CREATE TABLE `wpwyp2810161505_dopbsp_extras_groups_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `operation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '+',
  `price` float NOT NULL DEFAULT '0',
  `price_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fixed',
  `price_by` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'once',
  `default_value` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_extras_groups_items`
#
INSERT INTO `wpwyp2810161505_dopbsp_extras_groups_items` ( `id`, `group_id`, `position`, `operation`, `price`, `price_type`, `price_by`, `default_value`, `translation`) VALUES
(1, 1, 1, '+', '0', 'fixed', 'once', 'no', '{"en":"1"}'),
(2, 1, 2, '+', '0', 'fixed', 'once', 'no', '{"en":"2"}'),
(3, 1, 3, '+', '0', 'fixed', 'once', 'no', '{"en":"3"}'),
(4, 1, 4, '+', '0', 'fixed', 'once', 'no', '{"en":"4"}'),
(5, 1, 5, '+', '0', 'fixed', 'once', 'no', '{"en":"5"}'),
(6, 2, 1, '+', '0', 'fixed', 'once', 'no', '{"en":"0"}'),
(7, 2, 2, '+', '0', 'fixed', 'once', 'no', '{"en":"1"}'),
(8, 2, 3, '+', '0', 'fixed', 'once', 'no', '{"en":"2"}'),
(9, 2, 4, '+', '0', 'fixed', 'once', 'no', '{"en":"3"}') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_extras_groups_items`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_fees`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_fees`;


#
# Table structure of table `wpwyp2810161505_dopbsp_fees`
#

CREATE TABLE `wpwyp2810161505_dopbsp_fees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `operation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '+',
  `price` float NOT NULL DEFAULT '0',
  `price_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fixed',
  `price_by` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'once',
  `included` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `extras` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `cart` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_fees`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_fees`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_forms`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_forms`;


#
# Table structure of table `wpwyp2810161505_dopbsp_forms`
#

CREATE TABLE `wpwyp2810161505_dopbsp_forms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_forms`
#
INSERT INTO `wpwyp2810161505_dopbsp_forms` ( `id`, `user_id`, `name`) VALUES
(1, 1, 'Contact information') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_forms`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_forms_fields`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_forms_fields`;


#
# Table structure of table `wpwyp2810161505_dopbsp_forms_fields`
#

CREATE TABLE `wpwyp2810161505_dopbsp_forms_fields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `multiple_select` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `allowed_characters` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_email` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `is_phone` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `required` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `add_to_day_hour_info` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `add_to_day_hour_body` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_forms_fields`
#
INSERT INTO `wpwyp2810161505_dopbsp_forms_fields` ( `id`, `form_id`, `type`, `position`, `multiple_select`, `allowed_characters`, `size`, `is_email`, `is_phone`, `required`, `add_to_day_hour_info`, `add_to_day_hour_body`, `translation`) VALUES
(1, 1, 'text', 1, 'false', '', 0, 'false', 'false', 'true', 'false', 'false', '{"en":"First name"}'),
(2, 1, 'text', 2, 'false', '', 0, 'false', 'false', 'true', 'false', 'false', '{"en":"Last name"}'),
(3, 1, 'text', 3, 'false', '', 0, 'true', 'false', 'true', 'false', 'false', '{"en":"Email"}'),
(4, 1, 'text', 4, 'false', '0123456789+-().', 0, 'false', 'true', 'true', 'false', 'false', '{"en":"Phone"}'),
(5, 1, 'textarea', 5, 'false', '', 0, 'false', 'false', 'true', 'false', 'false', '{"en":"Message"}') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_forms_fields`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_forms_select_options`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_forms_select_options`;


#
# Table structure of table `wpwyp2810161505_dopbsp_forms_select_options`
#

CREATE TABLE `wpwyp2810161505_dopbsp_forms_select_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `field_id` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_forms_select_options`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_forms_select_options`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_languages`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_languages`;


#
# Table structure of table `wpwyp2810161505_dopbsp_languages`
#

CREATE TABLE `wpwyp2810161505_dopbsp_languages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `enabled` varchar(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`),
  KEY `code` (`code`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_languages`
#
INSERT INTO `wpwyp2810161505_dopbsp_languages` ( `id`, `name`, `code`, `enabled`) VALUES
(1, 'Afrikaans (Afrikaans)', 'af', 'false'),
(2, 'Albanian (Shqiptar)', 'sq', 'false'),
(3, 'Arabic (>العربية)', 'ar', 'false'),
(4, 'Azerbaijani (Azərbaycan)', 'az', 'false'),
(5, 'Basque (Euskal)', 'eu', 'false'),
(6, 'Belarusian (Беларускай)', 'be', 'false'),
(7, 'Bulgarian (Български)', 'bg', 'false'),
(8, 'Catalan (Català)', 'ca', 'false'),
(9, 'Chinese (中国的)', 'zh', 'false'),
(10, 'Croatian (Hrvatski)', 'hr', 'false'),
(11, 'Czech (Český)', 'cs', 'false'),
(12, 'Danish (Dansk)', 'da', 'false'),
(13, 'Dutch (Nederlands)', 'nl', 'false'),
(14, 'English', 'en', 'true'),
(15, 'Esperanto (Esperanto)', 'eo', 'false'),
(16, 'Estonian (Eesti)', 'et', 'false'),
(17, 'Filipino (na Filipino)', 'fl', 'false'),
(18, 'Finnish (Suomi)', 'fi', 'false'),
(19, 'French (Français)', 'fr', 'false'),
(20, 'Galician (Galego)', 'gl', 'false'),
(21, 'German (Deutsch)', 'de', 'false'),
(22, 'Greek (Ɛλληνικά)', 'el', 'false'),
(23, 'Haitian Creole (Kreyòl Ayisyen)', 'ht', 'false'),
(24, 'Hebrew (עברית)', 'he', 'false'),
(25, 'Hindi (हिंदी)', 'hi', 'false'),
(26, 'Hungarian (Magyar)', 'hu', 'false'),
(27, 'Icelandic (Íslenska)', 'is', 'false'),
(28, 'Indonesian (Indonesia)', 'id', 'false'),
(29, 'Irish (Gaeilge)', 'ga', 'false'),
(30, 'Italian (Italiano)', 'it', 'false'),
(31, 'Japanese (日本の)', 'ja', 'false'),
(32, 'Korean (한국의)', 'ko', 'false'),
(33, 'Latvian (Latvijas)', 'lv', 'false'),
(34, 'Lithuanian (Lietuvos)', 'lt', 'false'),
(35, 'Macedonian (македонски)', 'mk', 'false'),
(36, 'Malay (Melayu)', 'ms', 'false'),
(37, 'Maltese (Maltija)', 'mt', 'false'),
(38, 'Norwegian (Norske)', 'no', 'false'),
(39, 'Persian (فارسی)', 'fa', 'false'),
(40, 'Polish (Polski)', 'pl', 'false'),
(41, 'Portuguese (Português)', 'pt', 'false'),
(42, 'Romanian (Română)', 'ro', 'false'),
(43, 'Russian (Pусский)', 'ru', 'false'),
(44, 'Serbian (Cрпски)', 'sr', 'false'),
(45, 'Slovak (Slovenských)', 'sk', 'false'),
(46, 'Slovenian (Slovenski)', 'sl', 'false'),
(47, 'Spanish (Español)', 'es', 'false'),
(48, 'Swahili (Kiswahili)', 'sw', 'false'),
(49, 'Swedish (Svenskt)', 'sv', 'false'),
(50, 'Thai (ภาษาไทย)', 'th', 'false'),
(51, 'Turkish (Türk)', 'tr', 'false'),
(52, 'Ukrainian (Український)', 'uk', 'false'),
(53, 'Urdu (اردو)', 'ur', 'false'),
(54, 'Vietnamese (Việt)', 'vi', 'false'),
(55, 'Welsh (Cymraeg)', 'cy', 'false'),
(56, 'Yiddish (ייִדיש)', 'yi', 'false') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_languages`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_locations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_locations`;


#
# Table structure of table `wpwyp2810161505_dopbsp_locations`
#

CREATE TABLE `wpwyp2810161505_dopbsp_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_en` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_alt` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_alt_en` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coordinates` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `calendars` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `businesses` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `businesses_other` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `languages` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_locations`
#
INSERT INTO `wpwyp2810161505_dopbsp_locations` ( `id`, `user_id`, `name`, `address`, `address_en`, `address_alt`, `address_alt_en`, `coordinates`, `calendars`, `link`, `image`, `businesses`, `businesses_other`, `languages`, `email`) VALUES
(1, 1, 'New location', '', '', '', '', '', '', '', '', '', '', '', '') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_locations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_models`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_models`;


#
# Table structure of table `wpwyp2810161505_dopbsp_models`
#

CREATE TABLE `wpwyp2810161505_dopbsp_models` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `enabled` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `multiple_calendars` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `translation_calendar` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_models`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_models`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_reservations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_reservations`;


#
# Table structure of table `wpwyp2810161505_dopbsp_reservations`
#

CREATE TABLE `wpwyp2810161505_dopbsp_reservations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `language` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `currency` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '$',
  `currency_code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `check_in` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `check_out` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `start_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `end_hour` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no_items` int(10) unsigned NOT NULL DEFAULT '1',
  `price` float NOT NULL DEFAULT '0',
  `price_total` float NOT NULL DEFAULT '0',
  `refund` float NOT NULL DEFAULT '0',
  `extras` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `extras_price` float NOT NULL DEFAULT '0',
  `discount` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `discount_price` float NOT NULL DEFAULT '0',
  `coupon` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `coupon_price` float NOT NULL DEFAULT '0',
  `fees` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `fees_price` float NOT NULL DEFAULT '0',
  `deposit` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `deposit_price` float NOT NULL DEFAULT '0',
  `days_hours_history` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `form` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `address_billing` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `address_shipping` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `payment_method` varchar(32) NOT NULL DEFAULT 'default',
  `payment_status` varchar(32) NOT NULL DEFAULT 'pending',
  `transaction_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(128) NOT NULL DEFAULT '',
  `ip` varchar(32) NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `check_in` (`check_in`),
  KEY `check_out` (`check_out`),
  KEY `start_hour` (`end_hour`),
  KEY `status` (`status`),
  KEY `payment_method` (`payment_method`),
  KEY `transaction_id` (`transaction_id`),
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_reservations`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_reservations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_rules`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_rules`;


#
# Table structure of table `wpwyp2810161505_dopbsp_rules`
#

CREATE TABLE `wpwyp2810161505_dopbsp_rules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_lapse_min` float NOT NULL DEFAULT '0',
  `time_lapse_max` float NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_rules`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_rules`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_settings`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_settings`;


#
# Table structure of table `wpwyp2810161505_dopbsp_settings`
#

CREATE TABLE `wpwyp2810161505_dopbsp_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unique_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  KEY `unique_key` (`unique_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_settings`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_settings`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_settings_calendar`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_settings_calendar`;


#
# Table structure of table `wpwyp2810161505_dopbsp_settings_calendar`
#

CREATE TABLE `wpwyp2810161505_dopbsp_settings_calendar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `unique_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `unique_key` (`unique_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_settings_calendar`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_settings_calendar`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_settings_notifications`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_settings_notifications`;


#
# Table structure of table `wpwyp2810161505_dopbsp_settings_notifications`
#

CREATE TABLE `wpwyp2810161505_dopbsp_settings_notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `unique_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `unique_key` (`unique_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_settings_notifications`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_settings_notifications`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_settings_payment`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_settings_payment`;


#
# Table structure of table `wpwyp2810161505_dopbsp_settings_payment`
#

CREATE TABLE `wpwyp2810161505_dopbsp_settings_payment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `unique_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `unique_key` (`unique_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_settings_payment`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_settings_payment`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_translation_en`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_translation_en`;


#
# Table structure of table `wpwyp2810161505_dopbsp_translation_en`
#

CREATE TABLE `wpwyp2810161505_dopbsp_translation_en` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key_data` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text_data` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `key_data` (`key_data`)
) ENGINE=InnoDB AUTO_INCREMENT=1558 DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_translation_en`
#
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1, 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', '', 'Settings - PayPal', 'Settings - PayPal'),
(2, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal', 'PayPal'),
(3, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Enable PayPal payment', 'Enable PayPal payment'),
(4, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_CREDIT_CARD', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Enable PayPal credit card payment', 'Enable PayPal credit card payment'),
(5, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_USERNAME', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API user name', 'PayPal API user name'),
(6, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_PASSWORD', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API password', 'PayPal API password'),
(7, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SIGNATURE', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API signature', 'PayPal API signature'),
(8, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Enable PayPal sandbox', 'Enable PayPal sandbox'),
(9, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_USERNAME', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API sandbox user name', 'PayPal API sandbox user name'),
(10, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_PASSWORD', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API sandbox password', 'PayPal API sandbox password'),
(11, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_SIGNATURE', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal API sandbox signature', 'PayPal API sandbox signature'),
(12, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Enable refunds', 'Enable refunds'),
(13, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_VALUE', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Refund value', 'Refund value'),
(14, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_TYPE', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Refund type', 'Refund type'),
(15, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_TYPE_FIXED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Fixed', 'Fixed'),
(16, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_TYPE_PERCENT', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Percent', 'Percent'),
(17, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REDIRECT', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'Redirect after payment', 'Redirect after payment'),
(18, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SEND_ADMIN', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal - Notify admin', 'PayPal - Notify admin'),
(19, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SEND_USER', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL', 'PayPal - Notify user', 'PayPal - Notify user'),
(20, 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', '', 'Settings - PayPal - Help', 'Settings - PayPal - Help'),
(21, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: Disabled. Allow users to pay with PayPal. The period is instantly booked.', 'Default value: Disabled. Allow users to pay with PayPal. The period is instantly booked.'),
(22, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_CREDIT_CARD_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: Disabled. Enable so that users can pay directly with their credit card.', 'Default value: Disabled. Enable so that users can pay directly with their credit card.'),
(23, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_USERNAME_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API credentials user name. View documentation to see from were you can get them.', 'Enter PayPal API credentials user name. View documentation to see from were you can get them.'),
(24, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_PASSWORD_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API credentials password. View documentation to see from were you can get them.', 'Enter PayPal API credentials password. View documentation to see from were you can get them.'),
(25, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SIGNATURE_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API credentials signature. View documentation to see from were you can get them.', 'Enter PayPal API credentials signature. View documentation to see from were you can get them.'),
(26, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: Disabled. Enable to use PayPal sandbox features.', 'Default value: Disabled. Enable to use PayPal sandbox features.'),
(27, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_USERNAME_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API sandbox credentials user name.', 'Enter PayPal API sandbox credentials user name.'),
(28, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_PASSWORD_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API sandbox credentials password.', 'Enter PayPal API sandbox credentials password.'),
(29, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SANDBOX_SIGNATURE_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter PayPal API sandbox credentials signature.', 'Enter PayPal API sandbox credentials signature.'),
(30, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: Disabled. Users that paid with PayPal will be refunded automatically if a reservation is canceled.', 'Default value: Disabled. Users that paid with PayPal will be refunded automatically if a reservation is canceled.'),
(31, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_VALUE_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: 100. Enter the refund value from reservation total price.', 'Default value: 100. Enter the refund value from reservation total price.'),
(32, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REFUND_TYPE_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Default value: Percent. Select refund value type. It can be a fixed value or a percent from reservation price.', 'Default value: Percent. Select refund value type. It can be a fixed value or a percent from reservation price.'),
(33, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_REDIRECT_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enter the URL where to redirect after the payment has been completed. Leave it blank to redirect back to the calendar.', 'Enter the URL where to redirect after the payment has been completed. Leave it blank to redirect back to the calendar.'),
(34, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SEND_ADMIN_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enable to send an email notification to admin on book request payed with PayPal.', 'Enable to send an email notification to admin on book request payed with PayPal.'),
(35, 'SETTINGS_PAYMENT_GATEWAYS_PAYPAL_SEND_USER_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_PAYPAL_HELP', 'Enable to send an email notification to user on book request payed with PayPal.', 'Enable to send an email notification to user on book request payed with PayPal.'),
(36, 'PARENT_ORDER_PAYMENT_GATEWAYS_PAYPAL', '', 'Order - PayPal', 'Order - PayPal'),
(37, 'ORDER_PAYMENT_METHOD_PAYPAL', 'PARENT_RESERVATIONS_RESERVATION', 'PayPal', 'PayPal'),
(38, 'ORDER_PAYMENT_GATEWAYS_PAYPAL', 'PARENT_ORDER_PAYMENT_GATEWAYS_PAYPAL', 'Pay on PayPal (instant booking)', 'Pay on PayPal (instant booking)'),
(39, 'ORDER_PAYMENT_GATEWAYS_PAYPAL_SUCCESS', 'PARENT_ORDER_PAYMENT_GATEWAYS_PAYPAL', 'Your payment was approved and services are booked.', 'Your payment was approved and services are booked.'),
(40, 'ORDER_PAYMENT_GATEWAYS_PAYPAL_CANCEL', 'PARENT_ORDER_PAYMENT_GATEWAYS_PAYPAL', 'You canceled your payment with PayPal. Please try again.', 'You canceled your payment with PayPal. Please try again.'),
(41, 'ORDER_PAYMENT_GATEWAYS_PAYPAL_ERROR', 'PARENT_ORDER_PAYMENT_GATEWAYS_PAYPAL', 'There was an error while processing PayPal payment. Please try again.', 'There was an error while processing PayPal payment. Please try again.'),
(42, 'PARENT_EMAILS_DEFAULT_PAYPAL', '', 'Email templates - PayPal default messages', 'Email templates - PayPal default messages'),
(43, 'EMAILS_EMAIL_TEMPLATE_SELECT_PAYPAL_ADMIN', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'PayPal admin notification', 'PayPal admin notification'),
(44, 'EMAILS_DEFAULT_PAYPAL_ADMIN_SUBJECT', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'You received a booking request.', 'You received a booking request.'),
(45, 'EMAILS_DEFAULT_PAYPAL_ADMIN', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'Below are the details. Payment has been done via PayPal and the period has been booked.', 'Below are the details. Payment has been done via PayPal and the period has been booked.'),
(46, 'EMAILS_EMAIL_TEMPLATE_SELECT_PAYPAL_USER', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'PayPal user notification', 'PayPal user notification'),
(47, 'EMAILS_DEFAULT_PAYPAL_USER_SUBJECT', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'Your booking request has been sent.', 'Your booking request has been sent.'),
(48, 'EMAILS_DEFAULT_PAYPAL_USER', 'PARENT_EMAILS_DEFAULT_PAYPAL', 'The period has been book. Below are the details.', 'The period has been book. Below are the details.'),
(49, 'PARENT_WOOCOMMERCE', '', 'WooCommerce', 'WooCommerce'),
(50, 'WOOCOMMERCE_TAB', 'PARENT_WOOCOMMERCE', 'Pinpoint Booking System', 'Pinpoint Booking System'),
(51, 'WOOCOMMERCE_TAB_CALENDAR', 'PARENT_WOOCOMMERCE', 'Calendar', 'Calendar'),
(52, 'WOOCOMMERCE_TAB_CALENDAR_NO_CALENDARS', 'PARENT_WOOCOMMERCE', 'No calendars.', 'No calendars.'),
(53, 'WOOCOMMERCE_TAB_CALENDAR_SELECT', 'PARENT_WOOCOMMERCE', 'Select calendar', 'Select calendar'),
(54, 'WOOCOMMERCE_TAB_LANGUAGE', 'PARENT_WOOCOMMERCE', 'Language', 'Language'),
(55, 'WOOCOMMERCE_TAB_POSITION', 'PARENT_WOOCOMMERCE', 'Position', 'Position'),
(56, 'WOOCOMMERCE_TAB_POSITION_SUMMARY', 'PARENT_WOOCOMMERCE', 'Summary', 'Summary'),
(57, 'WOOCOMMERCE_TAB_POSITION_TABS', 'PARENT_WOOCOMMERCE', 'Tabs', 'Tabs'),
(58, 'WOOCOMMERCE_TAB_POSITION_SUMMARY_AND_TABS', 'PARENT_WOOCOMMERCE', 'Summary & Tabs', 'Summary & Tabs'),
(59, 'WOOCOMMERCE_TAB_ADD_TO_CART', 'PARENT_WOOCOMMERCE', 'Use the "Add To Cart" button from', 'Use the "Add To Cart" button from'),
(60, 'WOOCOMMERCE_VIEW_AVAILABILITY', 'PARENT_WOOCOMMERCE', 'View availability', 'View availability'),
(61, 'WOOCOMMERCE_STARTING_FROM', 'PARENT_WOOCOMMERCE', 'Starting from', 'Starting from'),
(62, 'WOOCOMMERCE_ADD_TO_CART', 'PARENT_WOOCOMMERCE', 'Add to cart', 'Add to cart'),
(63, 'WOOCOMMERCE_TABS', 'PARENT_WOOCOMMERCE', 'Book', 'Book'),
(64, 'WOOCOMMERCE_VIEW_CART', 'PARENT_CART', 'View cart', 'View cart'),
(65, 'WOOCOMMERCE_SUCCESS', 'PARENT_CART', 'The reservation has been added to cart.', 'The reservation has been added to cart.'),
(66, 'WOOCOMMERCE_UNAVAILABLE', 'PARENT_CART', 'The period you selected is not available anymore.', 'The period you selected is not available anymore.'),
(67, 'WOOCOMMERCE_OVERLAP', 'PARENT_CART', 'The period you selected will overlap with the ones you already added to cart. Please select another one.', 'The period you selected will overlap with the ones you already added to cart. Please select another one.'),
(68, 'WOOCOMMERCE_DELETED', 'PARENT_CART', 'The reservation(s) has(have) been deleted from cart.', 'The reservation(s) has(have) been deleted from cart.'),
(69, 'PARENT_WOOCOMMERCE_HELP', '', 'WooCommerce - Help', 'WooCommerce - Help'),
(70, 'WOOCOMMERCE_TAB_CALENDAR_HELP', 'PARENT_WOOCOMMERCE_HELP', 'Select the calendar that you want asociated with this product.', 'Select the calendar that you want asociated with this product.'),
(71, 'WOOCOMMERCE_TAB_LANGUAGE_HELP', 'PARENT_WOOCOMMERCE_HELP', 'Select the language for the calendar.', 'Select the language for the calendar.'),
(72, 'WOOCOMMERCE_TAB_POSITION_HELP', 'PARENT_WOOCOMMERCE_HELP', 'Select the calendar position. Add it in "product summary", "product tabs" or add the form in "summary" and the calendar in "product tabs".', 'Select the calendar position. Add it in "product summary", "product tabs" or add the form in "summary" and the calendar in "product tabs".'),
(73, 'WOOCOMMERCE_TAB_ADD_TO_CART_HELP', 'PARENT_WOOCOMMERCE_HELP', 'Select to choose to use Pinpoint Booking System<<single-quote>>s "Add to cart" button, or WooCommerce default button.', 'Select to choose to use Pinpoint Booking System<<single-quote>>s "Add to cart" button, or WooCommerce default button.'),
(74, 'PARENT_ADDONS', '', 'Add-ons', 'Add-ons'),
(75, 'ADDONS_TITLE', 'PARENT_ADDONS', 'Add-ons', 'Add-ons'),
(76, 'ADDONS_HELP', 'PARENT_ADDONS', 'Increase and improve booking system functionalities with one of the following addons.', 'Increase and improve booking system functionalities with one of the following addons.'),
(77, 'ADDONS_LOAD_SUCCESS', 'PARENT_ADDONS', 'Add-ons list loaded.', 'Add-ons list loaded.'),
(78, 'ADDONS_LOAD_ERROR', 'PARENT_ADDONS', 'Add-ons list failed to load. Please refresh the page to try again.', 'Add-ons list failed to load. Please refresh the page to try again.'),
(79, 'ADDONS_FILTERS_SEARCH', 'PARENT_ADDONS', 'Search', 'Search'),
(80, 'ADDONS_FILTERS_SEARCH_TERMS', 'PARENT_ADDONS', 'Enter search terms', 'Enter search terms'),
(81, 'ADDONS_FILTERS_CATEGORIES', 'PARENT_ADDONS', 'Categories', 'Categories'),
(82, 'ADDONS_FILTERS_CATEGORIES_ALL', 'PARENT_ADDONS', 'All', 'All'),
(83, 'ADDONS_ADDON_PRICE', 'PARENT_ADDONS', 'Price:', 'Price:'),
(84, 'ADDONS_ADDON_GET_IT_NOW', 'PARENT_ADDONS', 'Get it now', 'Get it now'),
(85, 'PARENT_AMENITIES', '', 'Amenities', 'Amenities'),
(86, 'AMENITIES_TITLE', 'PARENT_AMENITIES', 'Amenities', 'Amenities'),
(87, 'PARENT_CALENDARS', '', 'Calendars', 'Calendars'),
(88, 'CALENDARS_TITLE', 'PARENT_CALENDARS', 'Calendars', 'Calendars'),
(89, 'CALENDARS_CREATED_BY', 'PARENT_CALENDARS', 'Created by', 'Created by'),
(90, 'CALENDARS_NO_PENDING_RESERVATIONS', 'PARENT_CALENDARS', 'pending reservations', 'pending reservations'),
(91, 'CALENDARS_NO_APPROVED_RESERVATIONS', 'PARENT_CALENDARS', 'approved reservations', 'approved reservations'),
(92, 'CALENDARS_NO_REJECTED_RESERVATIONS', 'PARENT_CALENDARS', 'rejected reservations', 'rejected reservations'),
(93, 'CALENDARS_NO_CANCELED_RESERVATIONS', 'PARENT_CALENDARS', 'canceled reservations', 'canceled reservations'),
(94, 'CALENDARS_LOAD_SUCCESS', 'PARENT_CALENDARS', 'Calendars list loaded.', 'Calendars list loaded.'),
(95, 'CALENDARS_NO_CALENDARS', 'PARENT_CALENDARS', 'No calendars. Click the above "plus" icon to add a new one.', 'No calendars. Click the above "plus" icon to add a new one.'),
(96, 'PARENT_CALENDARS_CALENDAR', '', 'Calendars - Calendar', 'Calendars - Calendar'),
(97, 'CALENDARS_CALENDAR_LOAD_SUCCESS', 'PARENT_CALENDARS_CALENDAR', 'Calendar loaded.', 'Calendar loaded.'),
(98, 'CALENDARS_CALENDAR_SAVING_SUCCESS', 'PARENT_CALENDARS_CALENDAR', 'Schedule saved.', 'Schedule saved.'),
(99, 'CALENDARS_CALENDAR_ADD_MONTH_VIEW', 'PARENT_CALENDARS_CALENDAR', 'Add month view', 'Add month view'),
(100, 'CALENDARS_CALENDAR_REMOVE_MONTH_VIEW', 'PARENT_CALENDARS_CALENDAR', 'Remove month view', 'Remove month view') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(101, 'CALENDARS_CALENDAR_PREVIOUS_MONTH', 'PARENT_CALENDARS_CALENDAR', 'Previous month', 'Previous month'),
(102, 'CALENDARS_CALENDAR_NEXT_MONTH', 'PARENT_CALENDARS_CALENDAR', 'Next month', 'Next month'),
(103, 'CALENDARS_CALENDAR_AVAILABLE_ONE_TEXT', 'PARENT_CALENDARS_CALENDAR', 'available', 'available'),
(104, 'CALENDARS_CALENDAR_AVAILABLE_TEXT', 'PARENT_CALENDARS_CALENDAR', 'available', 'available'),
(105, 'CALENDARS_CALENDAR_BOOKED_TEXT', 'PARENT_CALENDARS_CALENDAR', 'booked', 'booked'),
(106, 'CALENDARS_CALENDAR_UNAVAILABLE_TEXT', 'PARENT_CALENDARS_CALENDAR', 'unavailable', 'unavailable'),
(107, 'PARENT_CALENDARS_CALENDAR_FORM', '', 'Calendars - Calendar form', 'Calendars - Calendar form'),
(108, 'CALENDARS_CALENDAR_FORM_DATE_START_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Start date', 'Start date'),
(109, 'CALENDARS_CALENDAR_FORM_DATE_END_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'End date', 'End date'),
(110, 'CALENDARS_CALENDAR_FORM_HOURS_START_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Start hour', 'Start hour'),
(111, 'CALENDARS_CALENDAR_FORM_HOURS_END_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'End hour', 'End hour'),
(112, 'CALENDARS_CALENDAR_FORM_SET_DAYS_AVAILABILITY_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Set days availability', 'Set days availability'),
(113, 'CALENDARS_CALENDAR_FORM_SET_HOURS_DEFINITIONS_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Set hours definitions', 'Set hours definitions'),
(114, 'CALENDARS_CALENDAR_FORM_SET_HOURS_AVAILABILITY_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Set hours availability', 'Set hours availability'),
(115, 'CALENDARS_CALENDAR_FORM_STATUS_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Status', 'Status'),
(116, 'CALENDARS_CALENDAR_FORM_STATUS_AVAILABLE_TEXT', 'PARENT_CALENDARS_CALENDAR_FORM', 'Available', 'Available'),
(117, 'CALENDARS_CALENDAR_FORM_STATUS_BOOKED_TEXT', 'PARENT_CALENDARS_CALENDAR_FORM', 'Booked', 'Booked'),
(118, 'CALENDARS_CALENDAR_FORM_STATUS_SPECIAL_TEXT', 'PARENT_CALENDARS_CALENDAR_FORM', 'Special', 'Special'),
(119, 'CALENDARS_CALENDAR_FORM_STATUS_UNAVAILABLE_TEXT', 'PARENT_CALENDARS_CALENDAR_FORM', 'Unavailable', 'Unavailable'),
(120, 'CALENDARS_CALENDAR_FORM_PRICE_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Price', 'Price'),
(121, 'CALENDARS_CALENDAR_FORM_PROMO_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Promo price', 'Promo price'),
(122, 'CALENDARS_CALENDAR_FORM_AVAILABLE_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Number available', 'Number available'),
(123, 'CALENDARS_CALENDAR_FORM_HOURS_DEFINITIONS_CHANGE_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Change hours definitions (changing the definitions will overwrite any previous hours data)', 'Change hours definitions (changing the definitions will overwrite any previous hours data)'),
(124, 'CALENDARS_CALENDAR_FORM_HOURS_DEFINITIONS_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Hours definitions (hh:mm add one per line). Use only 24 hours format.', 'Hours definitions (hh:mm add one per line). Use only 24 hours format.'),
(125, 'CALENDARS_CALENDAR_FORM_HOURS_SET_DEFAULT_DATA_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Set default hours values for this day(s). This will overwrite any existing data.', 'Set default hours values for this day(s). This will overwrite any existing data.'),
(126, 'CALENDARS_CALENDAR_FORM_HOURS_INFO_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Information (users will see this message)', 'Information (users will see this message)'),
(127, 'CALENDARS_CALENDAR_FORM_HOURS_NOTES_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Notes (only administrators will see this message)', 'Notes (only administrators will see this message)'),
(128, 'CALENDARS_CALENDAR_FORM_GROUP_DAYS_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Group days', 'Group days'),
(129, 'CALENDARS_CALENDAR_FORM_GROUP_HOURS_LABEL', 'PARENT_CALENDARS_CALENDAR_FORM', 'Group hours', 'Group hours'),
(130, 'CALENDARS_CALENDAR_FORM_RESET_CONFIRMATION', 'PARENT_CALENDARS_CALENDAR_FORM', 'Are you sure you want to reset the data? If you reset the days, hours data from those days will reset to.', 'Are you sure you want to reset the data? If you reset the days, hours data from those days will reset to.'),
(131, 'PARENT_CALENDARS_ADD_CALENDAR', '', 'Calendars - Add calendar', 'Calendars - Add calendar'),
(132, 'CALENDARS_ADD_CALENDAR_NAME', 'PARENT_CALENDARS_ADD_CALENDAR', 'New calendar', 'New calendar'),
(133, 'CALENDARS_ADD_CALENDAR_SUBMIT', 'PARENT_CALENDARS_ADD_CALENDAR', 'Add calendar', 'Add calendar'),
(134, 'CALENDARS_DUPLICATE_CALENDAR_SUBMIT', 'PARENT_CALENDARS_ADD_CALENDAR', 'Duplicate calendar', 'Duplicate calendar'),
(135, 'CALENDARS_ADD_CALENDAR_ADDING', 'PARENT_CALENDARS_ADD_CALENDAR', 'Adding a new calendar ...', 'Adding a new calendar ...'),
(136, 'CALENDARS_ADD_CALENDAR_SUCCESS', 'PARENT_CALENDARS_ADD_CALENDAR', 'You have succesfully added a new calendar.', 'You have succesfully added a new calendar.'),
(137, 'PARENT_CALENDARS_EDIT_CALENDAR', '', 'Calendars - Edit calendar', 'Calendars - Edit calendar'),
(138, 'CALENDARS_EDIT_CALENDAR', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Edit calendar availability', 'Edit calendar availability'),
(139, 'CALENDARS_EDIT_CALENDAR_SETTINGS', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Edit calendar settings', 'Edit calendar settings'),
(140, 'CALENDARS_EDIT_CALENDAR_NOTIFICATIONS', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Edit calendar notifications', 'Edit calendar notifications'),
(141, 'CALENDARS_EDIT_CALENDAR_PAYMENT_GATEWAYS', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Edit calendar payment gateways', 'Edit calendar payment gateways'),
(142, 'CALENDARS_EDIT_CALENDAR_USERS_PERMISSIONS', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Edit users permissions', 'Edit users permissions'),
(143, 'CALENDARS_EDIT_CALENDAR_NEW_RESERVATIONS', 'PARENT_CALENDARS_EDIT_CALENDAR', 'new reservations', 'new reservations'),
(144, 'CALENDARS_EDIT_CALENDAR_DELETE', 'PARENT_CALENDARS_EDIT_CALENDAR', 'Delete calendar', 'Delete calendar'),
(145, 'PARENT_CALENDARS_DELETE_CALENDAR', '', 'Calendars - Delete calendar', 'Calendars - Delete calendar'),
(146, 'CALENDARS_DELETE_CALENDAR_CONFIRMATION', 'PARENT_CALENDARS_DELETE_CALENDAR', 'Are you sure you want to delete this calendar?', 'Are you sure you want to delete this calendar?'),
(147, 'CALENDARS_DELETE_CALENDAR_DELETING', 'PARENT_CALENDARS_DELETE_CALENDAR', 'Deleting calendar ...', 'Deleting calendar ...'),
(148, 'CALENDARS_DELETE_CALENDAR_SUCCESS', 'PARENT_CALENDARS_DELETE_CALENDAR', 'You have succesfully deleted the calendar.', 'You have succesfully deleted the calendar.'),
(149, 'PARENT_CALENDARS_HELP', '', 'Calendars - Help', 'Calendars - Help'),
(150, 'CALENDARS_HELP', 'PARENT_CALENDARS_HELP', 'Click on a calendar item to open the editing area.', 'Click on a calendar item to open the editing area.'),
(151, 'CALENDARS_ADD_CALENDAR_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "plus" icon to add a calendar.', 'Click on the "plus" icon to add a calendar.'),
(152, 'CALENDARS_EDIT_CALENDAR_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "calendar" icon to edit calendar availability. Select the days and hours to edit them.', 'Click on the "calendar" icon to edit calendar availability. Select the days and hours to edit them.'),
(153, 'CALENDARS_EDIT_CALENDAR_SETTINGS_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "gear" icon to edit calendar settings.', 'Click on the "gear" icon to edit calendar settings.'),
(154, 'CALENDARS_EDIT_CALENDAR_EMAILS_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "email" icon to set emails/notifications options.', 'Click on the "email" icon to set emails/notifications options.'),
(155, 'CALENDARS_EDIT_CALENDAR_PAYMENT_GATEWAYS_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "wallet" icon to set payment options.', 'Click on the "wallet" icon to set payment options.'),
(156, 'CALENDARS_EDIT_CALENDAR_USERS_HELP', 'PARENT_CALENDARS_HELP', 'Click on the "users" icon to set users permissions.', 'Click on the "users" icon to set users permissions.'),
(157, 'CALENDARS_CALENDAR_NOTIFICATIONS_HELP', 'PARENT_CALENDARS_HELP', 'The "bulb" icon notifies you if you have new reserservations.', 'The "bulb" icon notifies you if you have new reserservations.'),
(158, 'PARENT_CART', '', 'Cart', 'Cart'),
(159, 'CART_TITLE', 'PARENT_CART', 'Cart', 'Cart'),
(160, 'CART_IS_EMPTY', 'PARENT_CART', 'Cart is empty.', 'Cart is empty.'),
(161, 'CART_ERROR', 'PARENT_CART', 'Error', 'Error'),
(162, 'CART_UNAVAILABLE', 'PARENT_CART', 'The period you selected is not available anymore. Please review your reservations.', 'The period you selected is not available anymore. Please review your reservations.'),
(163, 'CART_OVERLAP', 'PARENT_CART', 'The period you selected will overlap with the ones you already added to cart. Please select another one.', 'The period you selected will overlap with the ones you already added to cart. Please select another one.'),
(164, 'PARENT_COUPONS', '', 'Coupons', 'Coupons'),
(165, 'COUPONS_TITLE', 'PARENT_COUPONS', 'Coupons', 'Coupons'),
(166, 'COUPONS_CREATED_BY', 'PARENT_COUPONS', 'Created by', 'Created by'),
(167, 'COUPONS_LOAD_SUCCESS', 'PARENT_COUPONS', 'Coupons list loaded.', 'Coupons list loaded.'),
(168, 'COUPONS_NO_COUPONS', 'PARENT_COUPONS', 'No coupons. Click the above "plus" icon to add a new one.', 'No coupons. Click the above "plus" icon to add a new one.'),
(169, 'PARENT_COUPONS_COUPON', '', 'Coupons - Coupon', 'Coupons - Coupon'),
(170, 'COUPONS_COUPON_NAME', 'PARENT_COUPONS_COUPON', 'Name', 'Name'),
(171, 'COUPONS_COUPON_LANGUAGE', 'PARENT_COUPONS_COUPON', 'Language', 'Language'),
(172, 'COUPONS_COUPON_LABEL', 'PARENT_COUPONS_COUPON', 'Label', 'Label'),
(173, 'COUPONS_COUPON_CODE', 'PARENT_COUPONS_COUPON', 'Code', 'Code'),
(174, 'COUPONS_COUPON_CODE_GENERATE', 'PARENT_COUPONS_COUPON', 'Generate a random code', 'Generate a random code'),
(175, 'COUPONS_COUPON_START_DATE', 'PARENT_COUPONS_COUPON', 'Start date', 'Start date'),
(176, 'COUPONS_COUPON_END_DATE', 'PARENT_COUPONS_COUPON', 'End date', 'End date'),
(177, 'COUPONS_COUPON_START_HOUR', 'PARENT_COUPONS_COUPON', 'Start hour', 'Start hour'),
(178, 'COUPONS_COUPON_END_HOUR', 'PARENT_COUPONS_COUPON', 'End hour', 'End hour'),
(179, 'COUPONS_COUPON_NO_COUPONS', 'PARENT_COUPONS_COUPON', 'Number of coupons', 'Number of coupons'),
(180, 'COUPONS_COUPON_OPERATION', 'PARENT_COUPONS_COUPON', 'Operation', 'Operation'),
(181, 'COUPONS_COUPON_PRICE', 'PARENT_COUPONS_COUPON', 'Price/Percent', 'Price/Percent'),
(182, 'COUPONS_COUPON_PRICE_TYPE', 'PARENT_COUPONS_COUPON', 'Price type', 'Price type'),
(183, 'COUPONS_COUPON_PRICE_TYPE_FIXED', 'PARENT_COUPONS_COUPON', 'Fixed', 'Fixed'),
(184, 'COUPONS_COUPON_PRICE_TYPE_PERCENT', 'PARENT_COUPONS_COUPON', 'Percent', 'Percent'),
(185, 'COUPONS_COUPON_PRICE_BY', 'PARENT_COUPONS_COUPON', 'Price by', 'Price by'),
(186, 'COUPONS_COUPON_PRICE_BY_ONCE', 'PARENT_COUPONS_COUPON', 'Once', 'Once'),
(187, 'COUPONS_COUPON_PRICE_BY_PERIOD', 'PARENT_COUPONS_COUPON', 'day/hour', 'day/hour'),
(188, 'COUPONS_COUPON_LOADED', 'PARENT_COUPONS_COUPON', 'Coupon loaded.', 'Coupon loaded.'),
(189, 'PARENT_COUPONS_ADD_COUPON', '', 'Coupons - Add coupon', 'Coupons - Add coupon'),
(190, 'COUPONS_ADD_COUPON_NAME', 'PARENT_COUPONS_ADD_COUPON', 'New coupon', 'New coupon'),
(191, 'COUPONS_ADD_COUPON_LABEL', 'PARENT_COUPONS_ADD_COUPON', 'Coupon', 'Coupon'),
(192, 'COUPONS_ADD_COUPON_SUBMIT', 'PARENT_COUPONS_ADD_COUPON', 'Add coupon', 'Add coupon'),
(193, 'COUPONS_ADD_COUPON_ADDING', 'PARENT_COUPONS_ADD_COUPON', 'Adding a new coupon ...', 'Adding a new coupon ...'),
(194, 'COUPONS_ADD_COUPON_SUCCESS', 'PARENT_COUPONS_ADD_COUPON', 'You have succesfully added a new coupon.', 'You have succesfully added a new coupon.'),
(195, 'PARENT_COUPONS_DELETE_COUPON', '', 'Coupons - Delete coupon', 'Coupons - Delete coupon'),
(196, 'COUPONS_DELETE_COUPON_CONFIRMATION', 'PARENT_COUPONS_DELETE_COUPON', 'Are you sure you want to delete this coupon?', 'Are you sure you want to delete this coupon?'),
(197, 'COUPONS_DELETE_COUPON_SUBMIT', 'PARENT_COUPONS_DELETE_COUPON', 'Delete coupon', 'Delete coupon'),
(198, 'COUPONS_DELETE_COUPON_DELETING', 'PARENT_COUPONS_DELETE_COUPON', 'Deleting coupon ...', 'Deleting coupon ...'),
(199, 'COUPONS_DELETE_COUPON_SUCCESS', 'PARENT_COUPONS_DELETE_COUPON', 'You have succesfully deleted the coupon.', 'You have succesfully deleted the coupon.'),
(200, 'PARENT_COUPONS_HELP', '', 'Coupons - Help', 'Coupons - Help') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(201, 'COUPONS_HELP', 'PARENT_COUPONS_HELP', 'Click on a coupon item to open the editing area.', 'Click on a coupon item to open the editing area.'),
(202, 'COUPONS_ADD_COUPON_HELP', 'PARENT_COUPONS_HELP', 'Click on the "plus" icon to add a coupon.', 'Click on the "plus" icon to add a coupon.'),
(203, 'COUPONS_COUPON_HELP', 'PARENT_COUPONS_HELP', 'Click the group "trash" icon to delete the coupon.', 'Click the group "trash" icon to delete the coupon.'),
(204, 'COUPONS_COUPON_NAME_HELP', 'PARENT_COUPONS_HELP', 'Change coupon name.', 'Change coupon name.'),
(205, 'COUPONS_COUPON_LANGUAGE_HELP', 'PARENT_COUPONS_HELP', 'Change to the language you want to edit the coupon.', 'Change to the language you want to edit the coupon.'),
(206, 'COUPONS_COUPON_LABEL_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon label.', 'Enter coupon label.'),
(207, 'COUPONS_COUPON_CODE_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon code.', 'Enter coupon code.'),
(208, 'COUPONS_COUPON_START_DATE_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon start date, when the coupon will start being used.. Leave it blank to start today.', 'Enter coupon start date, when the coupon will start being used.. Leave it blank to start today.'),
(209, 'COUPONS_COUPON_END_DATE_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon end date, when the coupon will stop being used. Leave it blank for the coupons to have an unlimited time lapse.', 'Enter coupon end date, when the coupon will stop being used. Leave it blank for the coupons to have an unlimited time lapse.'),
(210, 'COUPONS_COUPON_START_HOUR_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon start hour, when the coupon will start being used. Leave it blank so you can use the coupons from the start of the day.', 'Enter coupon start hour, when the coupon will start being used. Leave it blank so you can use the coupons from the start of the day.'),
(211, 'COUPONS_COUPON_END_HOUR_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon end hour, when the coupon will end being used. Leave it blank so you can use the coupons until the end of the day.', 'Enter coupon end hour, when the coupon will end being used. Leave it blank so you can use the coupons until the end of the day.'),
(212, 'COUPONS_COUPON_NO_COUPONS_HELP', 'PARENT_COUPONS_HELP', 'Enter the number of coupons available. Leave it blank for unlimited number of coupons.', 'Enter the number of coupons available. Leave it blank for unlimited number of coupons.'),
(213, 'COUPONS_COUPON_OPERATION_HELP', 'PARENT_COUPONS_HELP', 'Select coupon price operation. You can add or subtract a value.', 'Select coupon price operation. You can add or subtract a value.'),
(214, 'COUPONS_COUPON_PRICE_HELP', 'PARENT_COUPONS_HELP', 'Enter coupon price.', 'Enter coupon price.'),
(215, 'COUPONS_COUPON_PRICE_TYPE_HELP', 'PARENT_COUPONS_HELP', 'Select coupon price type. It can be a fixed value or a percent from price.', 'Select coupon price type. It can be a fixed value or a percent from price.'),
(216, 'COUPONS_COUPON_PRICE_BY_HELP', 'PARENT_COUPONS_HELP', 'Select coupon price by. The price can be calculated once or by day/hour.', 'Select coupon price by. The price can be calculated once or by day/hour.'),
(217, 'PARENT_COUPONS_FRONT_END', '', 'Coupons - Front end', 'Coupons - Front end'),
(218, 'COUPONS_FRONT_END_TITLE', 'PARENT_COUPONS_FRONT_END', 'Coupons', 'Coupons'),
(219, 'COUPONS_FRONT_END_CODE', 'PARENT_COUPONS_FRONT_END', 'Enter code', 'Enter code'),
(220, 'COUPONS_FRONT_END_VERIFY', 'PARENT_COUPONS_FRONT_END', 'Verify code', 'Verify code'),
(221, 'COUPONS_FRONT_END_VERIFY_SUCCESS', 'PARENT_COUPONS_FRONT_END', 'The coupon code is valid.', 'The coupon code is valid.'),
(222, 'COUPONS_FRONT_END_VERIFY_ERROR', 'PARENT_COUPONS_FRONT_END', 'The coupon code is invalid. Please enter another one.', 'The coupon code is invalid. Please enter another one.'),
(223, 'COUPONS_FRONT_END_USE', 'PARENT_COUPONS_FRONT_END', 'Use coupon', 'Use coupon'),
(224, 'COUPONS_FRONT_END_BY_DAY', 'PARENT_COUPONS_FRONT_END', 'day', 'day'),
(225, 'COUPONS_FRONT_END_BY_HOUR', 'PARENT_COUPONS_FRONT_END', 'hour', 'hour'),
(226, 'PARENT_DASHBOARD', '', 'Dashboard', 'Dashboard'),
(227, 'DASHBOARD_TITLE', 'PARENT_DASHBOARD', 'Dashboard', 'Dashboard'),
(228, 'DASHBOARD_SUBTITLE', 'PARENT_DASHBOARD', 'Welcome to Pinpoint Booking System!', 'Welcome to Pinpoint Booking System!'),
(229, 'DASHBOARD_TEXT', 'PARENT_DASHBOARD', 'This plugin will help you to easily create a booking/reservation system on your WordPress website or blog. This is intended to book, anything, anywhere, anytime ... so if you have any suggestions please tell us.', 'This plugin will help you to easily create a booking/reservation system on your WordPress website or blog. This is intended to book, anything, anywhere, anytime ... so if you have any suggestions please tell us.'),
(230, 'DASHBOARD_GET_STARTED', 'PARENT_DASHBOARD', 'Get started', 'Get started'),
(231, 'DASHBOARD_GET_STARTED_CALENDARS', 'PARENT_DASHBOARD', 'Add a new calendar', 'Add a new calendar'),
(232, 'DASHBOARD_GET_STARTED_CALENDARS_VIEW', 'PARENT_DASHBOARD', 'View calendars', 'View calendars'),
(233, 'DASHBOARD_GET_STARTED_EVENTS', 'PARENT_DASHBOARD', 'Add a new event', 'Add a new event'),
(234, 'DASHBOARD_GET_STARTED_STAFF', 'PARENT_DASHBOARD', 'Add a staff member', 'Add a staff member'),
(235, 'DASHBOARD_GET_STARTED_LOCATIONS', 'PARENT_DASHBOARD', 'Add a new location', 'Add a new location'),
(236, 'DASHBOARD_GET_STARTED_RESERVATIONS', 'PARENT_DASHBOARD', 'View reservations', 'View reservations'),
(237, 'DASHBOARD_GET_STARTED_REVIEWS', 'PARENT_DASHBOARD', 'View reviews', 'View reviews'),
(238, 'DASHBOARD_MORE_ACTIONS', 'PARENT_DASHBOARD', 'More actions', 'More actions'),
(239, 'DASHBOARD_MORE_ACTIONS_ADDONS', 'PARENT_DASHBOARD', 'Get add-ons!', 'Get add-ons!'),
(240, 'DASHBOARD_MORE_ACTIONS_COUPONS', 'PARENT_DASHBOARD', 'Add coupons', 'Add coupons'),
(241, 'DASHBOARD_MORE_ACTIONS_DISCOUNTS', 'PARENT_DASHBOARD', 'Add discounts', 'Add discounts'),
(242, 'DASHBOARD_MORE_ACTIONS_EMAILS', 'PARENT_DASHBOARD', 'Add email templates', 'Add email templates'),
(243, 'DASHBOARD_MORE_ACTIONS_EXTRAS', 'PARENT_DASHBOARD', 'Add extras', 'Add extras'),
(244, 'DASHBOARD_MORE_ACTIONS_FEES', 'PARENT_DASHBOARD', 'Add taxes & fees', 'Add taxes & fees'),
(245, 'DASHBOARD_MORE_ACTIONS_FORMS', 'PARENT_DASHBOARD', 'Add forms', 'Add forms'),
(246, 'DASHBOARD_MORE_ACTIONS_RULES', 'PARENT_DASHBOARD', 'Add rules', 'Add rules'),
(247, 'DASHBOARD_MORE_ACTIONS_SEARCH', 'PARENT_DASHBOARD', 'Add search', 'Add search'),
(248, 'DASHBOARD_MORE_ACTIONS_SETTINGS', 'PARENT_DASHBOARD', 'Change settings', 'Change settings'),
(249, 'DASHBOARD_MORE_ACTIONS_TEMPLATES', 'PARENT_DASHBOARD', 'Add templates', 'Add templates'),
(250, 'DASHBOARD_MORE_ACTIONS_THEMES', 'PARENT_DASHBOARD', 'Get themes!', 'Get themes!'),
(251, 'DASHBOARD_MORE_ACTIONS_TOOLS', 'PARENT_DASHBOARD', 'Tools', 'Tools'),
(252, 'DASHBOARD_MORE_ACTIONS_TRANSLATION', 'PARENT_DASHBOARD', 'Change translation', 'Change translation'),
(253, 'DASHBOARD_API_TITLE', 'PARENT_DASHBOARD', 'API key', 'API key'),
(254, 'DASHBOARD_API_RESET', 'PARENT_DASHBOARD', 'Reset API key', 'Reset API key'),
(255, 'DASHBOARD_API_RESET_SUCCESS', 'PARENT_DASHBOARD', 'API key was reset successfully!', 'API key was reset successfully!'),
(256, 'DASHBOARD_API_HELP', 'PARENT_DASHBOARD', 'This is the key you need to use the API to access data from Pinpoint booking system. Each user has their own unique key, which can be reset using the adjacent button.', 'This is the key you need to use the API to access data from Pinpoint booking system. Each user has their own unique key, which can be reset using the adjacent button.'),
(257, 'DASHBOARD_SERVER_TITLE', 'PARENT_DASHBOARD', 'Server environment', 'Server environment'),
(258, 'DASHBOARD_SERVER_REQUIRED', 'PARENT_DASHBOARD', 'Required', 'Required'),
(259, 'DASHBOARD_SERVER_AVAILABLE', 'PARENT_DASHBOARD', 'Available', 'Available'),
(260, 'DASHBOARD_SERVER_STATUS', 'PARENT_DASHBOARD', 'Status', 'Status'),
(261, 'DASHBOARD_SERVER_NO', 'PARENT_DASHBOARD', 'No', 'No'),
(262, 'DASHBOARD_SERVER_YES', 'PARENT_DASHBOARD', 'Yes', 'Yes'),
(263, 'DASHBOARD_SERVER_VERSION', 'PARENT_DASHBOARD', 'Pinpoint Booking System version', 'Pinpoint Booking System version'),
(264, 'DASHBOARD_SERVER_WORDPRESS_VERSION', 'PARENT_DASHBOARD', 'WordPress version', 'WordPress version'),
(265, 'DASHBOARD_SERVER_WORDPRESS_MULTISITE', 'PARENT_DASHBOARD', 'WordPress multisite', 'WordPress multisite'),
(266, 'DASHBOARD_SERVER_WOOCOMMERCE_VERSION', 'PARENT_DASHBOARD', 'WooCommerce version', 'WooCommerce version'),
(267, 'DASHBOARD_SERVER_WOOCOMMERCE_ENABLE_CODE', 'PARENT_DASHBOARD', 'code is enabled even if WooCommerce plugin is not detected', 'code is enabled even if WooCommerce plugin is not detected'),
(268, 'DASHBOARD_SERVER_PHP_VERSION', 'PARENT_DASHBOARD', 'PHP version', 'PHP version'),
(269, 'DASHBOARD_SERVER_MYSQL_VERSION', 'PARENT_DASHBOARD', 'MySQL version', 'MySQL version'),
(270, 'DASHBOARD_SERVER_MEMORY_LIMIT', 'PARENT_DASHBOARD', 'Memory limit', 'Memory limit'),
(271, 'DASHBOARD_SERVER_MEMORY_LIMIT_WP', 'PARENT_DASHBOARD', 'WordPress memory limit', 'WordPress memory limit'),
(272, 'DASHBOARD_SERVER_MEMORY_LIMIT_WP_MAX', 'PARENT_DASHBOARD', 'WordPress maximum memory limit', 'WordPress maximum memory limit'),
(273, 'DASHBOARD_SERVER_MEMORY_LIMIT_WOOCOMMERCE', 'PARENT_DASHBOARD', 'WooCommerce memory limit', 'WooCommerce memory limit'),
(274, 'DASHBOARD_SERVER_CURL_VERSION', 'PARENT_DASHBOARD', 'cURL version - necessary for TLS security protocol', 'cURL version - necessary for TLS security protocol'),
(275, 'PARENT_DISCOUNTS', '', 'Discounts', 'Discounts'),
(276, 'DISCOUNTS_TITLE', 'PARENT_DISCOUNTS', 'Discounts', 'Discounts'),
(277, 'DISCOUNTS_CREATED_BY', 'PARENT_DISCOUNTS', 'Created by', 'Created by'),
(278, 'DISCOUNTS_LOAD_SUCCESS', 'PARENT_DISCOUNTS', 'Discounts list loaded.', 'Discounts list loaded.'),
(279, 'DISCOUNTS_NO_DISCOUNTS', 'PARENT_DISCOUNTS', 'No discounts. Click the above "plus" icon to add a new one.', 'No discounts. Click the above "plus" icon to add a new one.'),
(280, 'PARENT_DISCOUNTS_DISCOUNT', '', 'Discounts - Discount', 'Discounts - Discount'),
(281, 'DISCOUNTS_DISCOUNT_NAME', 'PARENT_DISCOUNTS_DISCOUNT', 'Name', 'Name'),
(282, 'DISCOUNTS_DISCOUNT_LANGUAGE', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Language', 'Language'),
(283, 'DISCOUNTS_DISCOUNT_EXTRAS', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Add the extra<<single-quote>>s price in the calculations', 'Add the extra<<single-quote>>s price in the calculations'),
(284, 'DISCOUNTS_DISCOUNT_LOADED', 'PARENT_DISCOUNTS_DISCOUNT', 'Discount loaded.', 'Discount loaded.'),
(285, 'PARENT_DISCOUNTS_ADD_DISCOUNT', '', 'Discounts - Add discount', 'Discounts - Add discount'),
(286, 'DISCOUNTS_ADD_DISCOUNT_NAME', 'PARENT_DISCOUNTS_ADD_DISCOUNT', 'New discount', 'New discount'),
(287, 'DISCOUNTS_ADD_DISCOUNT_SUBMIT', 'PARENT_DISCOUNTS_ADD_DISCOUNT', 'Add discount', 'Add discount'),
(288, 'DISCOUNTS_ADD_DISCOUNT_ADDING', 'PARENT_DISCOUNTS_ADD_DISCOUNT', 'Adding a new discount ...', 'Adding a new discount ...'),
(289, 'DISCOUNTS_ADD_DISCOUNT_SUCCESS', 'PARENT_DISCOUNTS_ADD_DISCOUNT', 'You have succesfully added a new discount.', 'You have succesfully added a new discount.'),
(290, 'PARENT_DISCOUNTS_DELETE_DISCOUNT', '', 'Discounts - Delete discount', 'Discounts - Delete discount'),
(291, 'DISCOUNTS_DELETE_DISCOUNT_CONFIRMATION', 'PARENT_DISCOUNTS_DELETE_DISCOUNT', 'Are you sure you want to delete this discount?', 'Are you sure you want to delete this discount?'),
(292, 'DISCOUNTS_DELETE_DISCOUNT_SUBMIT', 'PARENT_DISCOUNTS_DELETE_DISCOUNT', 'Delete discount', 'Delete discount'),
(293, 'DISCOUNTS_DELETE_DISCOUNT_DELETING', 'PARENT_DISCOUNTS_DELETE_DISCOUNT', 'Deleting discount ...', 'Deleting discount ...'),
(294, 'DISCOUNTS_DELETE_DISCOUNT_SUCCESS', 'PARENT_DISCOUNTS_DELETE_DISCOUNT', 'You have succesfully deleted the discount.', 'You have succesfully deleted the discount.'),
(295, 'PARENT_DISCOUNTS_DISCOUNT_ITEMS', '', 'Discounts - Discount items', 'Discounts - Discount items'),
(296, 'DISCOUNTS_DISCOUNT_ITEMS', 'PARENT_DISCOUNTS_DISCOUNT_ITEMS', 'Discount items', 'Discount items'),
(297, 'PARENT_DISCOUNTS_DISCOUNT_ITEM', '', 'Discounts - Discount item', 'Discounts - Discount item'),
(298, 'DISCOUNTS_DISCOUNT_ITEM_SHOW_SETTINGS', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Show settings', 'Show settings'),
(299, 'DISCOUNTS_DISCOUNT_ITEM_HIDE_SETTINGS', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Hide settings', 'Hide settings'),
(300, 'DISCOUNTS_DISCOUNT_ITEM_SORT', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Sort', 'Sort') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(301, 'DISCOUNTS_DISCOUNT_ITEM_LABEL_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Label', 'Label'),
(302, 'DISCOUNTS_DISCOUNT_ITEM_START_TIME_LAPSE_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Start time lapse (days/hours)', 'Start time lapse (days/hours)'),
(303, 'DISCOUNTS_DISCOUNT_ITEM_END_TIME_LAPSE_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'End time lapse (days/hours)', 'End time lapse (days/hours)'),
(304, 'DISCOUNTS_DISCOUNT_ITEM_OPERATION_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Operation', 'Operation'),
(305, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Price/Percent', 'Price/Percent'),
(306, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_TYPE_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Price type', 'Price type'),
(307, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_BY_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM', 'Price by', 'Price by'),
(308, 'PARENT_DISCOUNTS_DISCOUNT_ADD_ITEM', '', 'Discounts - Add discount item', 'Discounts - Add discount item'),
(309, 'DISCOUNTS_DISCOUNT_ADD_ITEM_SUBMIT', 'PARENT_DISCOUNTS_DISCOUNT_ADD_ITEM', 'Add item', 'Add item'),
(310, 'DISCOUNTS_DISCOUNT_ADD_ITEM_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ADD_ITEM', 'New item', 'New item'),
(311, 'DISCOUNTS_DISCOUNT_ADD_ITEM_ADDING', 'PARENT_DISCOUNTS_DISCOUNT_ADD_ITEM', 'Adding a new discount item ...', 'Adding a new discount item ...'),
(312, 'DISCOUNTS_DISCOUNT_ADD_ITEM_SUCCESS', 'PARENT_DISCOUNTS_DISCOUNT_ADD_ITEM', 'You have succesfully added a new discount item.', 'You have succesfully added a new discount item.'),
(313, 'PARENT_DISCOUNTS_DISCOUNT_DELETE_ITEM', '', 'Discounts - Delete discount item', 'Discounts - Delete discount item'),
(314, 'DISCOUNTS_DISCOUNT_DELETE_ITEM_CONFIRMATION', 'PARENT_DISCOUNTS_DISCOUNT_DELETE_ITEM', 'Are you sure you want to delete this discount item?', 'Are you sure you want to delete this discount item?'),
(315, 'DISCOUNTS_DISCOUNT_DELETE_ITEM_SUBMIT', 'PARENT_DISCOUNTS_DISCOUNT_DELETE_ITEM', 'Delete', 'Delete'),
(316, 'DISCOUNTS_DISCOUNT_DELETE_ITEM_DELETING', 'PARENT_DISCOUNTS_DISCOUNT_DELETE_ITEM', 'Deleting discount item ...', 'Deleting discount item ...'),
(317, 'DISCOUNTS_DISCOUNT_DELETE_ITEM_SUCCESS', 'PARENT_DISCOUNTS_DISCOUNT_DELETE_ITEM', 'You have succesfully deleted the discount item.', 'You have succesfully deleted the discount item.'),
(318, 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', '', 'Discounts - Discount item - Rules', 'Discounts - Discount item - Rules'),
(319, 'DISCOUNTS_DISCOUNT_ITEM_RULES_LABEL', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', 'Rules', 'Rules'),
(320, 'DISCOUNTS_DISCOUNT_ITEM_RULES_LABELS_OPERATION', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', 'Operation', 'Operation'),
(321, 'DISCOUNTS_DISCOUNT_ITEM_RULES_LABELS_PRICE', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', 'Price/Percent', 'Price/Percent'),
(322, 'DISCOUNTS_DISCOUNT_ITEM_RULES_LABELS_PRICE_TYPE', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', 'Price type', 'Price type'),
(323, 'DISCOUNTS_DISCOUNT_ITEM_RULES_LABELS_PRICE_BY', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULES', 'Price by', 'Price by'),
(324, 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', '', 'Discounts - Discount item - Rule', 'Discounts - Discount item - Rule'),
(325, 'DISCOUNTS_DISCOUNT_ITEM_RULES_PRICE_TYPE_FIXED', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', 'Fixed', 'Fixed'),
(326, 'DISCOUNTS_DISCOUNT_ITEM_RULES_PRICE_TYPE_PERCENT', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', 'Percent', 'Percent'),
(327, 'DISCOUNTS_DISCOUNT_ITEM_RULES_PRICE_BY_ONCE', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', 'Once', 'Once'),
(328, 'DISCOUNTS_DISCOUNT_ITEM_RULES_PRICE_BY_PERIOD', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', 'day/hour', 'day/hour'),
(329, 'DISCOUNTS_DISCOUNT_ITEM_RULE_SORT', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_RULE', 'Sort', 'Sort'),
(330, 'PARENT_DISCOUNTS_DISCOUNT_ITEM_ADD_RULE', '', 'Discounts - Discount item - Add rule', 'Discounts - Discount item - Add rule'),
(331, 'DISCOUNTS_DISCOUNT_ITEM_ADD_RULE_SUBMIT', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_ADD_RULE', 'Add rule', 'Add rule'),
(332, 'DISCOUNTS_DISCOUNT_ITEM_ADD_RULE_ADDING', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_ADD_RULE', 'Adding a new rule ...', 'Adding a new rule ...'),
(333, 'DISCOUNTS_DISCOUNT_ITEM_ADD_RULE_SUCCESS', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_ADD_RULE', 'You have succesfully added a new rule.', 'You have succesfully added a new rule.'),
(334, 'PARENT_DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE', '', 'Discounts - Discount item - Delete rule', 'Discounts - Discount item - Delete rule'),
(335, 'DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE_CONFIRMATION', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE', 'Are you sure you want to delete this  rule?', 'Are you sure you want to delete this  rule?'),
(336, 'DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE_SUBMIT', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE', 'Delete', 'Delete'),
(337, 'DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE_DELETING', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE', 'Deleting  rule ...', 'Deleting  rule ...'),
(338, 'DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE_SUCCESS', 'PARENT_DISCOUNTS_DISCOUNT_ITEM_DELETE_RULE', 'You have succesfully deleted the  rule.', 'You have succesfully deleted the  rule.'),
(339, 'PARENT_DISCOUNTS_HELP', '', 'Discounts - Help', 'Discounts - Help'),
(340, 'DISCOUNTS_HELP', 'PARENT_DISCOUNTS_HELP', 'Click on a discount rule to open the editing area.', 'Click on a discount rule to open the editing area.'),
(341, 'DISCOUNTS_ADD_DISCOUNT_HELP', 'PARENT_DISCOUNTS_HELP', 'Click on the "plus" icon to add a discount.', 'Click on the "plus" icon to add a discount.'),
(342, 'DISCOUNTS_DISCOUNT_NAME_HELP', 'PARENT_DISCOUNTS_HELP', 'Change discount name.', 'Change discount name.'),
(343, 'DISCOUNTS_DISCOUNT_LANGUAGE_HELP', 'PARENT_DISCOUNTS_HELP', 'Change to the language you want to edit the discount.', 'Change to the language you want to edit the discount.'),
(344, 'DISCOUNTS_DISCOUNT_EXTRAS_HELP', 'PARENT_DISCOUNTS_HELP', 'Calculate reservation discounts including extras price, if used.', 'Calculate reservation discounts including extras price, if used.'),
(345, 'DISCOUNTS_DISCOUNT_ADD_ITEM_HELP', 'PARENT_DISCOUNTS_HELP', 'Click on the bellow "plus" icon to add a new discount item.', 'Click on the bellow "plus" icon to add a new discount item.'),
(346, 'DISCOUNTS_DISCOUNT_EDIT_ITEM_HELP', 'PARENT_DISCOUNTS_HELP', 'Click the item "expand" icon to display/hide the settings.', 'Click the item "expand" icon to display/hide the settings.'),
(347, 'DISCOUNTS_DISCOUNT_DELETE_ITEM_HELP', 'PARENT_DISCOUNTS_HELP', 'Click the item "trash" icon to delete it.', 'Click the item "trash" icon to delete it.'),
(348, 'DISCOUNTS_DISCOUNT_SORT_ITEM_HELP', 'PARENT_DISCOUNTS_HELP', 'Drag the item "arrows" icon to sort it.', 'Drag the item "arrows" icon to sort it.'),
(349, 'DISCOUNTS_DISCOUNT_ITEM_LABEL_HELP', 'PARENT_DISCOUNTS_HELP', 'Enter item label.', 'Enter item label.'),
(350, 'DISCOUNTS_DISCOUNT_ITEM_START_TIME_LAPSE_HELP', 'PARENT_DISCOUNTS_HELP', 'Enter the number of days/hours for the begining of the time lapse. Leave it blank for it to start from 1 day/hour.', 'Enter the number of days/hours for the begining of the time lapse. Leave it blank for it to start from 1 day/hour.'),
(351, 'DISCOUNTS_DISCOUNT_ITEM_END_TIME_LAPSE_HELP', 'PARENT_DISCOUNTS_HELP', 'Enter the number of days/hours for the ending of the time lapse. Leave it blank to be unlimited.', 'Enter the number of days/hours for the ending of the time lapse. Leave it blank to be unlimited.'),
(352, 'DISCOUNTS_DISCOUNT_ITEM_OPERATION_HELP', 'PARENT_DISCOUNTS_HELP', 'Select item price operation. You can add or subtract a value.', 'Select item price operation. You can add or subtract a value.'),
(353, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_HELP', 'PARENT_DISCOUNTS_HELP', 'Enter item price.', 'Enter item price.'),
(354, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_TYPE_HELP', 'PARENT_DISCOUNTS_HELP', 'Select item price type. It can be a fixed value or a percent from price.', 'Select item price type. It can be a fixed value or a percent from price.'),
(355, 'DISCOUNTS_DISCOUNT_ITEM_PRICE_BY_HELP', 'PARENT_DISCOUNTS_HELP', 'Select item price by. The price can be calculated once or by day/hour.', 'Select item price by. The price can be calculated once or by day/hour.'),
(356, 'DISCOUNTS_DISCOUNT_ITEM_RULES_HELP', 'PARENT_DISCOUNTS_HELP', 'Click the "plus" icon to add another rule and enter the name and price conditions. Click on the "delete" icon to remove the rule. Add dates and hours intervals for which you want the rule to apply.', 'Click the "plus" icon to add another rule and enter the name and price conditions. Click on the "delete" icon to remove the rule. Add dates and hours intervals for which you want the rule to apply.'),
(357, 'PARENT_DISCOUNTS_FRONT_END', '', 'Discounts - Front end', 'Discounts - Front end'),
(358, 'DISCOUNTS_FRONT_END_TITLE', 'PARENT_DISCOUNTS_FRONT_END', 'Discount', 'Discount'),
(359, 'DISCOUNTS_FRONT_END_BY_DAY', 'PARENT_DISCOUNTS_FRONT_END', 'day', 'day'),
(360, 'DISCOUNTS_FRONT_END_BY_HOUR', 'PARENT_DISCOUNTS_FRONT_END', 'hour', 'hour'),
(361, 'PARENT_EMAILS', '', 'Email templates', 'Email templates'),
(362, 'EMAILS_TITLE', 'PARENT_EMAILS', 'Email templates', 'Email templates'),
(363, 'EMAILS_CREATED_BY', 'PARENT_EMAILS', 'Created by', 'Created by'),
(364, 'EMAILS_LOAD_SUCCESS', 'PARENT_EMAILS', 'Email templates  list loaded.', 'Email templates  list loaded.'),
(365, 'EMAILS_NO_EMAILS', 'PARENT_EMAILS', 'No email templates. Click the above "plus" icon to add new ones.', 'No email templates. Click the above "plus" icon to add new ones.'),
(366, 'PARENT_EMAILS_DEFAULT', '', 'Email templates - Default messages', 'Email templates - Default messages'),
(367, 'EMAILS_DEFAULT_NAME', 'PARENT_EMAILS_DEFAULT', 'Default email templates', 'Default email templates'),
(368, 'EMAILS_DEFAULT_BOOK_ADMIN_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'You received a booking request.', 'You received a booking request.'),
(369, 'EMAILS_DEFAULT_BOOK_ADMIN', 'PARENT_EMAILS_DEFAULT', 'Below are the details. Go to admin to approve or reject the request.', 'Below are the details. Go to admin to approve or reject the request.'),
(370, 'EMAILS_DEFAULT_BOOK_USER_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'Your booking request has been sent.', 'Your booking request has been sent.'),
(371, 'EMAILS_DEFAULT_BOOK_USER', 'PARENT_EMAILS_DEFAULT', 'Please wait for approval. Below are the details.', 'Please wait for approval. Below are the details.'),
(372, 'EMAILS_DEFAULT_BOOK_WITH_APPROVAL_ADMIN_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'You received a booking request.', 'You received a booking request.'),
(373, 'EMAILS_DEFAULT_BOOK_WITH_APPROVAL_ADMIN', 'PARENT_EMAILS_DEFAULT', 'Below are the details. Go to admin to cancel the request.', 'Below are the details. Go to admin to cancel the request.'),
(374, 'EMAILS_DEFAULT_BOOK_WITH_APPROVAL_USER_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'Your booking request has been sent.', 'Your booking request has been sent.'),
(375, 'EMAILS_DEFAULT_BOOK_WITH_APPROVAL_USER', 'PARENT_EMAILS_DEFAULT', 'Below are the details.', 'Below are the details.'),
(376, 'EMAILS_DEFAULT_APPROVED_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'Your booking request has been approved.', 'Your booking request has been approved.'),
(377, 'EMAILS_DEFAULT_APPROVED', 'PARENT_EMAILS_DEFAULT', 'Congratulations! Your booking request has been approved. Details about your request are below.', 'Congratulations! Your booking request has been approved. Details about your request are below.'),
(378, 'EMAILS_DEFAULT_CANCELED_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'Your booking request has been canceled.', 'Your booking request has been canceled.'),
(379, 'EMAILS_DEFAULT_CANCELED', 'PARENT_EMAILS_DEFAULT', 'I<<single-quote>>m sorry but your booking request has been canceled. Details about your request are below.', 'I<<single-quote>>m sorry but your booking request has been canceled. Details about your request are below.'),
(380, 'EMAILS_DEFAULT_REJECTED_SUBJECT', 'PARENT_EMAILS_DEFAULT', 'Your booking request has been rejected.', 'Your booking request has been rejected.'),
(381, 'EMAILS_DEFAULT_REJECTED', 'PARENT_EMAILS_DEFAULT', 'I<<single-quote>>m sorry but your booking request has been rejected. Details about your request are below.', 'I<<single-quote>>m sorry but your booking request has been rejected. Details about your request are below.'),
(382, 'PARENT_EMAILS_EMAIL', '', 'Email templates - Templates', 'Email templates - Templates'),
(383, 'EMAILS_EMAIL_NAME', 'PARENT_EMAILS_EMAIL', 'Name', 'Name'),
(384, 'EMAILS_EMAIL_LANGUAGE', 'PARENT_EMAILS_EMAIL', 'Language', 'Language'),
(385, 'EMAILS_EMAIL_TEMPLATE_SELECT', 'PARENT_EMAILS_EMAIL', 'Select template', 'Select template'),
(386, 'EMAILS_EMAIL_TEMPLATE_SELECT_BOOK_ADMIN', 'PARENT_EMAILS_EMAIL', 'Admin notification', 'Admin notification'),
(387, 'EMAILS_EMAIL_TEMPLATE_SELECT_BOOK_USER', 'PARENT_EMAILS_EMAIL', 'User notification', 'User notification'),
(388, 'EMAILS_EMAIL_TEMPLATE_SELECT_BOOK_WITH_APPROVAL_ADMIN', 'PARENT_EMAILS_EMAIL', 'Instant approval admin notification', 'Instant approval admin notification'),
(389, 'EMAILS_EMAIL_TEMPLATE_SELECT_BOOK_WITH_APPROVAL_USER', 'PARENT_EMAILS_EMAIL', 'Instant approval user notification', 'Instant approval user notification'),
(390, 'EMAILS_EMAIL_TEMPLATE_SELECT_APPROVED', 'PARENT_EMAILS_EMAIL', 'Approve resevation', 'Approve resevation'),
(391, 'EMAILS_EMAIL_TEMPLATE_SELECT_CANCELED', 'PARENT_EMAILS_EMAIL', 'Cancel resevation', 'Cancel resevation'),
(392, 'EMAILS_EMAIL_TEMPLATE_SELECT_REJECTED', 'PARENT_EMAILS_EMAIL', 'Reject resevation', 'Reject resevation'),
(393, 'EMAILS_EMAIL_SUBJECT', 'PARENT_EMAILS_EMAIL', 'Subject', 'Subject'),
(394, 'EMAILS_EMAIL_MESSAGE', 'PARENT_EMAILS_EMAIL', 'Message', 'Message'),
(395, 'EMAILS_EMAIL_LOADED', 'PARENT_EMAILS_EMAIL', 'Email templates loaded.', 'Email templates loaded.'),
(396, 'PARENT_EMAILS_ADD_EMAIL', '', 'Email templates - Add templates', 'Email templates - Add templates'),
(397, 'EMAILS_ADD_EMAIL_NAME', 'PARENT_EMAILS_ADD_EMAIL', 'New email templates', 'New email templates'),
(398, 'EMAILS_ADD_EMAIL_SUBMIT', 'PARENT_EMAILS_ADD_EMAIL', 'Add email templates', 'Add email templates'),
(399, 'EMAILS_ADD_EMAIL_ADDING', 'PARENT_EMAILS_ADD_EMAIL', 'Adding new email templates ...', 'Adding new email templates ...'),
(400, 'EMAILS_ADD_EMAIL_SUCCESS', 'PARENT_EMAILS_ADD_EMAIL', 'You have succesfully added new email templates.', 'You have succesfully added new email templates.') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(401, 'PARENT_EMAILS_DELETE_EMAIL', '', 'Email templates - Delete templates', 'Email templates - Delete templates'),
(402, 'EMAILS_DELETE_EMAIL_CONFIRMATION', 'PARENT_EMAILS_DELETE_EMAIL', 'Are you sure you want to delete the email templates?', 'Are you sure you want to delete the email templates?'),
(403, 'EMAILS_DELETE_EMAIL_SUBMIT', 'PARENT_EMAILS_DELETE_EMAIL', 'Delete email templates', 'Delete email templates'),
(404, 'EMAILS_DELETE_EMAIL_DELETING', 'PARENT_EMAILS_DELETE_EMAIL', 'Deleting email templates ...', 'Deleting email templates ...'),
(405, 'EMAILS_DELETE_EMAIL_SUCCESS', 'PARENT_EMAILS_DELETE_EMAIL', 'You have succesfully deleted the email templates.', 'You have succesfully deleted the email templates.'),
(406, 'PARENT_EMAILS_HELP', '', 'Email templates - Help', 'Email templates - Help'),
(407, 'EMAILS_HELP', 'PARENT_EMAILS_HELP', 'Click on a templates item to open the editing area.', 'Click on a templates item to open the editing area.'),
(408, 'EMAILS_ADD_EMAIL_HELP', 'PARENT_EMAILS_HELP', 'Click on the "plus" icon to add email templates.', 'Click on the "plus" icon to add email templates.'),
(409, 'EMAILS_EMAIL_HELP', 'PARENT_EMAILS_HELP', 'Click the "trash" icon to delete the email.', 'Click the "trash" icon to delete the email.'),
(410, 'EMAILS_EMAIL_NAME_HELP', 'PARENT_EMAILS_HELP', 'Change email templates name.', 'Change email templates name.'),
(411, 'EMAILS_EMAIL_LANGUAGE_HELP', 'PARENT_EMAILS_HELP', 'Change to the language you want to edit the email templates.', 'Change to the language you want to edit the email templates.'),
(412, 'EMAILS_EMAIL_TEMPLATE_SELECT_HELP', 'PARENT_EMAILS_HELP', 'Select the template you want to edit and modify the subject and message.', 'Select the template you want to edit and modify the subject and message.'),
(413, 'PARENT_EXTRAS', '', 'Extras', 'Extras'),
(414, 'EXTRAS_TITLE', 'PARENT_EXTRAS', 'Extras', 'Extras'),
(415, 'EXTRAS_CREATED_BY', 'PARENT_EXTRAS', 'Created by', 'Created by'),
(416, 'EXTRAS_LOAD_SUCCESS', 'PARENT_EXTRAS', 'Extras list loaded.', 'Extras list loaded.'),
(417, 'EXTRAS_NO_EXTRAS', 'PARENT_EXTRAS', 'No extras. Click the above "plus" icon to add a new one.', 'No extras. Click the above "plus" icon to add a new one.'),
(418, 'PARENT_EXTRAS_DEFAULT', '', 'Extras - Default data', 'Extras - Default data'),
(419, 'EXTRAS_DEFAULT_PEOPLE', 'PARENT_EXTRAS_DEFAULT', 'People', 'People'),
(420, 'EXTRAS_DEFAULT_ADULTS', 'PARENT_EXTRAS_DEFAULT', 'Adults', 'Adults'),
(421, 'EXTRAS_DEFAULT_CHILDREN', 'PARENT_EXTRAS_DEFAULT', 'Children', 'Children'),
(422, 'PARENT_EXTRAS_EXTRA', '', 'Extras - Extra', 'Extras - Extra'),
(423, 'EXTRAS_EXTRA_NAME', 'PARENT_EXTRAS_EXTRA', 'Name', 'Name'),
(424, 'EXTRAS_EXTRA_LANGUAGE', 'PARENT_EXTRAS_EXTRA_GROUP', 'Language', 'Language'),
(425, 'EXTRAS_EXTRA_LOADED', 'PARENT_EXTRAS_EXTRA', 'Extra loaded.', 'Extra loaded.'),
(426, 'PARENT_EXTRAS_ADD_EXTRA', '', 'Extras - Add extra', 'Extras - Add extra'),
(427, 'EXTRAS_ADD_EXTRA_NAME', 'PARENT_EXTRAS_ADD_EXTRA', 'New extra', 'New extra'),
(428, 'EXTRAS_ADD_EXTRA_SUBMIT', 'PARENT_EXTRAS_ADD_EXTRA', 'Add extra', 'Add extra'),
(429, 'EXTRAS_ADD_EXTRA_ADDING', 'PARENT_EXTRAS_ADD_EXTRA', 'Adding a new extra ...', 'Adding a new extra ...'),
(430, 'EXTRAS_ADD_EXTRA_SUCCESS', 'PARENT_EXTRAS_ADD_EXTRA', 'You have succesfully added a new extra.', 'You have succesfully added a new extra.'),
(431, 'PARENT_EXTRAS_DELETE_EXTRA', '', 'Extras - Delete extra', 'Extras - Delete extra'),
(432, 'EXTRAS_DELETE_EXTRA_CONFIRMATION', 'PARENT_EXTRAS_DELETE_EXTRA', 'Are you sure you want to delete this extra?', 'Are you sure you want to delete this extra?'),
(433, 'EXTRAS_DELETE_EXTRA_SUBMIT', 'PARENT_EXTRAS_DELETE_EXTRA', 'Delete extra', 'Delete extra'),
(434, 'EXTRAS_DELETE_EXTRA_DELETING', 'PARENT_EXTRAS_DELETE_EXTRA', 'Deleting extra ...', 'Deleting extra ...'),
(435, 'EXTRAS_DELETE_EXTRA_SUCCESS', 'PARENT_EXTRAS_DELETE_EXTRA', 'You have succesfully deleted the extra.', 'You have succesfully deleted the extra.'),
(436, 'PARENT_EXTRAS_EXTRA_GROUPS', '', 'Extras - Extra groups', 'Extras - Extra groups'),
(437, 'EXTRAS_EXTRA_GROUPS', 'PARENT_EXTRAS_EXTRA_GROUPS', 'Extra fields', 'Extra fields'),
(438, 'PARENT_EXTRAS_EXTRA_GROUP', '', 'Extras - Extra group', 'Extras - Extra group'),
(439, 'EXTRAS_EXTRA_GROUP_SHOW_SETTINGS', 'PARENT_EXTRAS_EXTRA_GROUP', 'Show settings', 'Show settings'),
(440, 'EXTRAS_EXTRA_GROUP_HIDE_SETTINGS', 'PARENT_EXTRAS_EXTRA_GROUP', 'Hide settings', 'Hide settings'),
(441, 'EXTRAS_EXTRA_GROUP_SORT', 'PARENT_EXTRAS_EXTRA_GROUP', 'Sort', 'Sort'),
(442, 'EXTRAS_EXTRA_GROUP_LABEL_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP', 'Label', 'Label'),
(443, 'EXTRAS_EXTRA_GROUP_REQUIRED_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP', 'Required', 'Required'),
(444, 'EXTRAS_EXTRA_GROUP_NO_ITEMS_MULTIPLY_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP', 'Multiply with No. Items', 'Multiply with No. Items'),
(445, 'EXTRAS_EXTRA_GROUP_MULTIPLE_SELECT_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP', 'Multiple select', 'Multiple select'),
(446, 'PARENT_EXTRAS_EXTRA_ADD_GROUP', '', 'Extras - Add extra group', 'Extras - Add extra group'),
(447, 'EXTRAS_EXTRA_ADD_GROUP_SUBMIT', 'PARENT_EXTRAS_EXTRA_ADD_GROUP', 'Add group', 'Add group'),
(448, 'EXTRAS_EXTRA_ADD_GROUP_LABEL', 'PARENT_EXTRAS_EXTRA_ADD_GROUP', 'New group', 'New group'),
(449, 'EXTRAS_EXTRA_ADD_GROUP_ADDING', 'PARENT_EXTRAS_EXTRA_ADD_GROUP', 'Adding a new extra group ...', 'Adding a new extra group ...'),
(450, 'EXTRAS_EXTRA_ADD_GROUP_SUCCESS', 'PARENT_EXTRAS_EXTRA_ADD_GROUP', 'You have succesfully added a new extra group.', 'You have succesfully added a new extra group.'),
(451, 'PARENT_EXTRAS_EXTRA_DELETE_GROUP', '', 'Extras - Delete extra group', 'Extras - Delete extra group'),
(452, 'EXTRAS_EXTRA_DELETE_GROUP_CONFIRMATION', 'PARENT_EXTRAS_EXTRA_DELETE_GROUP', 'Are you sure you want to delete this extra group?', 'Are you sure you want to delete this extra group?'),
(453, 'EXTRAS_EXTRA_DELETE_GROUP_SUBMIT', 'PARENT_EXTRAS_EXTRA_DELETE_GROUP', 'Delete', 'Delete'),
(454, 'EXTRAS_EXTRA_DELETE_GROUP_DELETING', 'PARENT_EXTRAS_EXTRA_DELETE_GROUP', 'Deleting extra group ...', 'Deleting extra group ...'),
(455, 'EXTRAS_EXTRA_DELETE_GROUP_SUCCESS', 'PARENT_EXTRAS_EXTRA_DELETE_GROUP', 'You have succesfully deleted the extra group.', 'You have succesfully deleted the extra group.'),
(456, 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', '', 'Extras - Extra group - Items', 'Extras - Extra group - Items'),
(457, 'EXTRAS_EXTRA_GROUP_ITEMS_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Items', 'Items'),
(458, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Label', 'Label'),
(459, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_OPERATION', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Operation', 'Operation'),
(460, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_PRICE', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Price/Percent', 'Price/Percent'),
(461, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_PRICE_TYPE', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Price type', 'Price type'),
(462, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_PRICE_BY', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Price by', 'Price by'),
(463, 'EXTRAS_EXTRA_GROUP_ITEMS_LABELS_DEFAULT', 'PARENT_EXTRAS_EXTRA_GROUP_ITEMS', 'Set as default', 'Set as default'),
(464, 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', '', 'Extras - Extra group - Item', 'Extras - Extra group - Item'),
(465, 'EXTRAS_EXTRA_GROUP_ITEMS_PRICE_TYPE_FIXED', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'Fixed', 'Fixed'),
(466, 'EXTRAS_EXTRA_GROUP_ITEMS_PRICE_TYPE_PERCENT', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'Percent', 'Percent'),
(467, 'EXTRAS_EXTRA_GROUP_ITEMS_PRICE_BY_ONCE', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'Once', 'Once'),
(468, 'EXTRAS_EXTRA_GROUP_ITEMS_PRICE_BY_PERIOD', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'day/hour', 'day/hour'),
(469, 'EXTRAS_EXTRA_GROUP_ITEM_SORT', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'Sort', 'Sort'),
(470, 'EXTRAS_EXTRA_GROUP_ITEMS_DEFAULT_NO', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'No', 'No'),
(471, 'EXTRAS_EXTRA_GROUP_ITEMS_DEFAULT_YES', 'PARENT_EXTRAS_EXTRA_GROUP_ITEM', 'Yes', 'Yes'),
(472, 'PARENT_EXTRAS_EXTRA_GROUP_ADD_ITEM', '', 'Extras - Extra group - Add item', 'Extras - Extra group - Add item'),
(473, 'EXTRAS_EXTRA_GROUP_ADD_ITEM_LABEL', 'PARENT_EXTRAS_EXTRA_GROUP_ADD_ITEM', 'New item', 'New item'),
(474, 'EXTRAS_EXTRA_GROUP_ADD_ITEM_SUBMIT', 'PARENT_EXTRAS_EXTRA_GROUP_ADD_ITEM', 'Add item', 'Add item'),
(475, 'EXTRAS_EXTRA_GROUP_ADD_ITEM_ADDING', 'PARENT_EXTRAS_EXTRA_GROUP_ADD_ITEM', 'Adding a new item ...', 'Adding a new item ...'),
(476, 'EXTRAS_EXTRA_GROUP_ADD_ITEM_SUCCESS', 'PARENT_EXTRAS_EXTRA_GROUP_ADD_ITEM', 'You have succesfully added a new item.', 'You have succesfully added a new item.'),
(477, 'PARENT_EXTRAS_EXTRA_GROUP_DELETE_ITEM', '', 'Extras - Extra group - Delete item', 'Extras - Extra group - Delete item'),
(478, 'EXTRAS_EXTRA_GROUP_DELETE_ITEM_CONFIRMATION', 'PARENT_EXTRAS_EXTRA_GROUP_DELETE_ITEM', 'Are you sure you want to delete this  item?', 'Are you sure you want to delete this  item?'),
(479, 'EXTRAS_EXTRA_GROUP_DELETE_ITEM_SUBMIT', 'PARENT_EXTRAS_EXTRA_GROUP_DELETE_ITEM', 'Delete', 'Delete'),
(480, 'EXTRAS_EXTRA_GROUP_DELETE_ITEM_DELETING', 'PARENT_EXTRAS_EXTRA_GROUP_DELETE_ITEM', 'Deleting  item ...', 'Deleting  item ...'),
(481, 'EXTRAS_EXTRA_GROUP_DELETE_ITEM_SUCCESS', 'PARENT_EXTRAS_EXTRA_GROUP_DELETE_ITEM', 'You have succesfully deleted the  item.', 'You have succesfully deleted the  item.'),
(482, 'PARENT_EXTRAS_HELP', '', 'Extras - Help', 'Extras - Help'),
(483, 'EXTRAS_HELP', 'PARENT_EXTRAS_HELP', 'Click on a extra item to open the editing area.', 'Click on a extra item to open the editing area.'),
(484, 'EXTRAS_ADD_EXTRA_HELP', 'PARENT_EXTRAS_HELP', 'Click on the "plus" icon to add a extra.', 'Click on the "plus" icon to add a extra.'),
(485, 'EXTRAS_EXTRA_NAME_HELP', 'PARENT_EXTRAS_HELP', 'Change extra name.', 'Change extra name.'),
(486, 'EXTRAS_EXTRA_LANGUAGE_HELP', 'PARENT_EXTRAS_HELP', 'Change to the language you want to edit the extra.', 'Change to the language you want to edit the extra.'),
(487, 'EXTRAS_EXTRA_ADD_GROUP_HELP', 'PARENT_EXTRAS_HELP', 'Click on the bellow "plus" icon to add a new extra group.', 'Click on the bellow "plus" icon to add a new extra group.'),
(488, 'EXTRAS_EXTRA_EDIT_GROUP_HELP', 'PARENT_EXTRAS_HELP', 'Click the group "expand" icon to display/hide the settings.', 'Click the group "expand" icon to display/hide the settings.'),
(489, 'EXTRAS_EXTRA_DELETE_GROUP_HELP', 'PARENT_EXTRAS_HELP', 'Click the group "trash" icon to delete it.', 'Click the group "trash" icon to delete it.'),
(490, 'EXTRAS_EXTRA_SORT_GROUP_HELP', 'PARENT_EXTRAS_HELP', 'Drag the group "arrows" icon to sort it.', 'Drag the group "arrows" icon to sort it.'),
(491, 'EXTRAS_EXTRA_GROUP_LABEL_HELP', 'PARENT_EXTRAS_HELP', 'Enter group label.', 'Enter group label.'),
(492, 'EXTRAS_EXTRA_GROUP_REQUIRED_HELP', 'PARENT_EXTRAS_HELP', 'Enable it if you want the group to be mandatory.', 'Enable it if you want the group to be mandatory.'),
(493, 'EXTRAS_EXTRA_GROUP_MULTIPLE_SELECT_HELP', 'PARENT_EXTRAS_HELP', 'Enable it if you want multiple selection.', 'Enable it if you want multiple selection.'),
(494, 'EXTRAS_EXTRA_GROUP_NO_ITEMS_MULTIPLY_HELP', 'PARENT_EXTRAS_HELP', 'Enable it if you want the extras price to be multiplied with the number of items.', 'Enable it if you want the extras price to be multiplied with the number of items.'),
(495, 'EXTRAS_EXTRA_GROUP_ITEMS_HELP', 'PARENT_EXTRAS_HELP', 'Click the "plus" icon to add another item and enter the name and price conditions. Click on the "delete" icon to remove the item.', 'Click the "plus" icon to add another item and enter the name and price conditions. Click on the "delete" icon to remove the item.'),
(496, 'PARENT_EXTRAS_FRONT_END', '', 'Extras - Front end', 'Extras - Front end'),
(497, 'EXTRAS_FRONT_END_TITLE', 'PARENT_EXTRAS_FRONT_END', 'Extras', 'Extras'),
(498, 'EXTRAS_FRONT_END_BY_DAY', 'PARENT_EXTRAS_FRONT_END', 'day', 'day'),
(499, 'EXTRAS_FRONT_END_BY_HOUR', 'PARENT_EXTRAS_FRONT_END', 'hour', 'hour'),
(500, 'EXTRAS_FRONT_END_INVALID', 'PARENT_EXTRAS_FRONT_END', 'Select an option from', 'Select an option from') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(501, 'PARENT_FEES', '', 'Taxes & fees', 'Taxes & fees'),
(502, 'FEES_TITLE', 'PARENT_FEES', 'Taxes & fees', 'Taxes & fees'),
(503, 'FEES_CREATED_BY', 'PARENT_FEES', 'Created by', 'Created by'),
(504, 'FEES_LOAD_SUCCESS', 'PARENT_FEES', 'Taxes & fees list loaded.', 'Taxes & fees list loaded.'),
(505, 'FEES_NO_FEES', 'PARENT_FEES', 'No taxes or fees. Click the above "plus" icon to add a new one.', 'No taxes or fees. Click the above "plus" icon to add a new one.'),
(506, 'PARENT_FEES_FEE', '', 'Taxes & fees - Tax or fee', 'Taxes & fees - Tax or fee'),
(507, 'FEES_FEE_NAME', 'PARENT_FEES_FEE', 'Name', 'Name'),
(508, 'FEES_FEE_LANGUAGE', 'PARENT_FEES_FEE', 'Language', 'Language'),
(509, 'FEES_FEE_LABEL', 'PARENT_FEES_FEE', 'Label', 'Label'),
(510, 'FEES_FEE_OPERATION', 'PARENT_FEES_FEE', 'Operation', 'Operation'),
(511, 'FEES_FEE_PRICE', 'PARENT_FEES_FEE', 'Price/Percent', 'Price/Percent'),
(512, 'FEES_FEE_PRICE_TYPE', 'PARENT_FEES_FEE', 'Price type', 'Price type'),
(513, 'FEES_FEE_PRICE_TYPE_FIXED', 'PARENT_FEES_FEE', 'Fixed', 'Fixed'),
(514, 'FEES_FEE_PRICE_TYPE_PERCENT', 'PARENT_FEES_FEE', 'Percent', 'Percent'),
(515, 'FEES_FEE_PRICE_BY', 'PARENT_FEES_FEE', 'Price by', 'Price by'),
(516, 'FEES_FEE_PRICE_BY_ONCE', 'PARENT_FEES_FEE', 'Once', 'Once'),
(517, 'FEES_FEE_PRICE_BY_PERIOD', 'PARENT_FEES_FEE', 'day/hour', 'day/hour'),
(518, 'FEES_FEE_INCLUDED', 'PARENT_FEES_FEE', 'Included', 'Included'),
(519, 'FEES_FEE_EXTRAS', 'PARENT_FEES_FEE', 'Add the extra<<single-quote>>s price in the calculations', 'Add the extra<<single-quote>>s price in the calculations'),
(520, 'FEES_FEE_CART', 'PARENT_FEES_FEE', 'Display fees in cart<<single-quote>>s total', 'Display fees in cart<<single-quote>>s total'),
(521, 'FEES_FEE_LOADED', 'PARENT_FEES_FEE', 'Tax or fee loaded.', 'Tax or fee loaded.'),
(522, 'PARENT_FEES_ADD_FEE', '', 'Taxes & fees - Add tax or fee', 'Taxes & fees - Add tax or fee'),
(523, 'FEES_ADD_FEE_NAME', 'PARENT_FEES_ADD_FEE', 'New tax / fee', 'New tax / fee'),
(524, 'FEES_ADD_FEE_LABEL', 'PARENT_FEES_ADD_FEE', 'New tax / fee label', 'New tax / fee label'),
(525, 'FEES_ADD_FEE_SUBMIT', 'PARENT_FEES_ADD_FEE', 'Add tax or fee', 'Add tax or fee'),
(526, 'FEES_ADD_FEE_ADDING', 'PARENT_FEES_ADD_FEE', 'Adding a new tax or fee ...', 'Adding a new tax or fee ...'),
(527, 'FEES_ADD_FEE_SUCCESS', 'PARENT_FEES_ADD_FEE', 'You have succesfully added a new tax or fee.', 'You have succesfully added a new tax or fee.'),
(528, 'PARENT_FEES_DELETE_FEE', '', 'Taxes & fees - Delete tax or fee', 'Taxes & fees - Delete tax or fee'),
(529, 'FEES_DELETE_FEE_CONFIRMATION', 'PARENT_FEES_DELETE_FEE', 'Are you sure you want to delete this tax / fee?', 'Are you sure you want to delete this tax / fee?'),
(530, 'FEES_DELETE_FEE_SUBMIT', 'PARENT_FEES_DELETE_FEE', 'Delete tax / fee', 'Delete tax / fee'),
(531, 'FEES_DELETE_FEE_DELETING', 'PARENT_FEES_DELETE_FEE', 'Deleting tax / fee ...', 'Deleting tax / fee ...'),
(532, 'FEES_DELETE_FEE_SUCCESS', 'PARENT_FEES_DELETE_FEE', 'You have succesfully deleted the tax / fee.', 'You have succesfully deleted the tax / fee.'),
(533, 'PARENT_FEES_HELP', '', 'Taxes & fees - Help', 'Taxes & fees - Help'),
(534, 'FEES_HELP', 'PARENT_FEES_HELP', 'Click on a tax / fee item to open the editing area.', 'Click on a tax / fee item to open the editing area.'),
(535, 'FEES_ADD_FEE_HELP', 'PARENT_FEES_HELP', 'Click on the "plus" icon to add a tax or fee.', 'Click on the "plus" icon to add a tax or fee.'),
(536, 'FEES_FEE_HELP', 'PARENT_FEES_HELP', 'Click the group "trash" icon to delete the tax / fee.', 'Click the group "trash" icon to delete the tax / fee.'),
(537, 'FEES_FEE_NAME_HELP', 'PARENT_FEES_HELP', 'Change tax / fee name.', 'Change tax / fee name.'),
(538, 'FEES_FEE_LANGUAGE_HELP', 'PARENT_FEES_HELP', 'Change to the language you want to edit the tax / fee.', 'Change to the language you want to edit the tax / fee.'),
(539, 'FEES_FEE_LABEL_HELP', 'PARENT_FEES_HELP', 'Enter tax / fee label.', 'Enter tax / fee label.'),
(540, 'FEES_FEE_OPERATION_HELP', 'PARENT_FEES_FEE', 'Select tax / fee price operation.', 'Select tax / fee price operation.'),
(541, 'FEES_FEE_PRICE_HELP', 'PARENT_FEES_FEE', 'Enter tax / fee price.', 'Enter tax / fee price.'),
(542, 'FEES_FEE_PRICE_TYPE_HELP', 'PARENT_FEES_FEE', 'Select tax / fee price type.', 'Select tax / fee price type.'),
(543, 'FEES_FEE_PRICE_BY_HELP', 'PARENT_FEES_FEE', 'Select tax / fee price by.', 'Select tax / fee price by.'),
(544, 'FEES_FEE_INCLUDED_HELP', 'PARENT_FEES_FEE', 'Tax / fee is included in reservation prices.', 'Tax / fee is included in reservation prices.'),
(545, 'FEES_FEE_EXTRAS_HELP', 'PARENT_FEES_FEE', 'Calculate reservation tax / fee including extras price, if used.', 'Calculate reservation tax / fee including extras price, if used.'),
(546, 'FEES_FEE_CART_HELP', 'PARENT_FEES_FEE', 'If you use the cart option, you can choose to display the tax to total price or to each reservation.', 'If you use the cart option, you can choose to display the tax to total price or to each reservation.'),
(547, 'PARENT_FEES_FRONT_END', '', 'Fees - Front end', 'Fees - Front end'),
(548, 'FEES_FRONT_END_TITLE', 'PARENT_FEES_FRONT_END', 'Taxes & fees', 'Taxes & fees'),
(549, 'FEES_FRONT_END_BY_DAY', 'PARENT_FEES_FRONT_END', 'day', 'day'),
(550, 'FEES_FRONT_END_BY_HOUR', 'PARENT_FEES_FRONT_END', 'hour', 'hour'),
(551, 'FEES_FRONT_END_INCLUDED', 'PARENT_FEES_FRONT_END', 'Included in price', 'Included in price'),
(552, 'PARENT_FORMS', '', 'Forms', 'Forms'),
(553, 'FORMS_TITLE', 'PARENT_FORMS', 'Forms', 'Forms'),
(554, 'FORMS_CREATED_BY', 'PARENT_FORMS', 'Created by', 'Created by'),
(555, 'FORMS_LOAD_SUCCESS', 'PARENT_FORMS', 'Forms list loaded.', 'Forms list loaded.'),
(556, 'FORMS_NO_FORMS', 'PARENT_FORMS', 'No forms. Click the above "plus" icon to add a new one.', 'No forms. Click the above "plus" icon to add a new one.'),
(557, 'PARENT_FORMS_DEFAULT', '', 'Forms - Default data', 'Forms - Default data'),
(558, 'FORMS_DEFAULT_NAME', 'PARENT_FORMS_DEFAULT', 'Contact information', 'Contact information'),
(559, 'FORMS_DEFAULT_FIRST_NAME', 'PARENT_FORMS_DEFAULT', 'First name', 'First name'),
(560, 'FORMS_DEFAULT_LAST_NAME', 'PARENT_FORMS_DEFAULT', 'Last name', 'Last name'),
(561, 'FORMS_DEFAULT_EMAIL', 'PARENT_FORMS_DEFAULT', 'Email', 'Email'),
(562, 'FORMS_DEFAULT_PHONE', 'PARENT_FORMS_DEFAULT', 'Phone', 'Phone'),
(563, 'FORMS_DEFAULT_MESSAGE', 'PARENT_FORMS_DEFAULT', 'Message', 'Message'),
(564, 'PARENT_FORMS_FORM', '', 'Forms - Form', 'Forms - Form'),
(565, 'FORMS_FORM_NAME', 'PARENT_FORMS_FORM', 'Name', 'Name'),
(566, 'FORMS_FORM_LANGUAGE', 'PARENT_FORMS_FORM_FIELD', 'Language', 'Language'),
(567, 'FORMS_FORM_LOADED', 'PARENT_FORMS_FORM', 'Form loaded.', 'Form loaded.'),
(568, 'PARENT_FORMS_ADD_FORM', '', 'Forms - Add form', 'Forms - Add form'),
(569, 'FORMS_ADD_FORM_NAME', 'PARENT_FORMS_ADD_FORM', 'New form', 'New form'),
(570, 'FORMS_ADD_FORM_SUBMIT', 'PARENT_FORMS_ADD_FORM', 'Add form', 'Add form'),
(571, 'FORMS_ADD_FORM_ADDING', 'PARENT_FORMS_ADD_FORM', 'Adding a new form ...', 'Adding a new form ...'),
(572, 'FORMS_ADD_FORM_SUCCESS', 'PARENT_FORMS_ADD_FORM', 'You have succesfully added a new form.', 'You have succesfully added a new form.'),
(573, 'PARENT_FORMS_DELETE_FORM', '', 'Forms - Delete form', 'Forms - Delete form'),
(574, 'FORMS_DELETE_FORM_CONFIRMATION', 'PARENT_FORMS_DELETE_FORM', 'Are you sure you want to delete this form?', 'Are you sure you want to delete this form?'),
(575, 'FORMS_DELETE_FORM_SUBMIT', 'PARENT_FORMS_DELETE_FORM', 'Delete form', 'Delete form'),
(576, 'FORMS_DELETE_FORM_DELETING', 'PARENT_FORMS_DELETE_FORM', 'Deleting form ...', 'Deleting form ...'),
(577, 'FORMS_DELETE_FORM_SUCCESS', 'PARENT_FORMS_DELETE_FORM', 'You have succesfully deleted the form.', 'You have succesfully deleted the form.'),
(578, 'PARENT_FORMS_FORM_FIELDS', '', 'Forms - Form fields', 'Forms - Form fields'),
(579, 'FORMS_FORM_FIELDS', 'PARENT_FORMS_FORM_FIELDS', 'Form fiels', 'Form fiels'),
(580, 'PARENT_FORMS_FORM_FIELD', '', 'Forms - Form field', 'Forms - Form field'),
(581, 'FORMS_FORM_FIELD_SHOW_SETTINGS', 'PARENT_FORMS_FORM_FIELD', 'Show settings', 'Show settings'),
(582, 'FORMS_FORM_FIELD_HIDE_SETTINGS', 'PARENT_FORMS_FORM_FIELD', 'Hide settings', 'Hide settings'),
(583, 'FORMS_FORM_FIELD_SORT', 'PARENT_FORMS_FORM_FIELD', 'Sort', 'Sort'),
(584, 'FORMS_FORM_FIELD_TYPE_TEXT_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Text', 'Text'),
(585, 'FORMS_FORM_FIELD_TYPE_TEXTAREA_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Textarea', 'Textarea'),
(586, 'FORMS_FORM_FIELD_TYPE_CHECKBOX_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Checkbox', 'Checkbox'),
(587, 'FORMS_FORM_FIELD_TYPE_CHECKBOX_CHECKED_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Checked', 'Checked'),
(588, 'FORMS_FORM_FIELD_TYPE_CHECKBOX_UNCHECKED_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Unchecked', 'Unchecked'),
(589, 'FORMS_FORM_FIELD_TYPE_SELECT_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Drop down', 'Drop down'),
(590, 'FORMS_FORM_FIELD_LABEL_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Label', 'Label'),
(591, 'FORMS_FORM_FIELD_ALLOWED_CHARACTERS_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Allowed Characters', 'Allowed Characters'),
(592, 'FORMS_FORM_FIELD_SIZE_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Size', 'Size'),
(593, 'FORMS_FORM_FIELD_EMAIL_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Is email', 'Is email'),
(594, 'FORMS_FORM_FIELD_REQUIRED_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Required', 'Required'),
(595, 'FORMS_FORM_FIELD_MULTIPLE_SELECT_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Multiple select', 'Multiple select'),
(596, 'FORMS_FORM_FIELD_ADD_TO_DAY_HOUR_INFO_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Add field to day/hour info', 'Add field to day/hour info'),
(597, 'FORMS_FORM_FIELD_ADD_TO_DAY_HOUR_BODY_LABEL', 'PARENT_FORMS_FORM_FIELD', 'Add field to day/hour body', 'Add field to day/hour body'),
(598, 'PARENT_FORMS_FORM_ADD_FIELD', '', 'Forms - Add form field', 'Forms - Add form field'),
(599, 'FORMS_FORM_ADD_FIELD_TEXT_LABEL', 'PARENT_FORMS_FORM_ADD_FIELD', 'New text field', 'New text field'),
(600, 'FORMS_FORM_ADD_FIELD_TEXTAREA_LABEL', 'PARENT_FORMS_FORM_ADD_FIELD', 'New textarea field', 'New textarea field') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(601, 'FORMS_FORM_ADD_FIELD_CHECKBOX_LABEL', 'PARENT_FORMS_FORM_ADD_FIELD', 'New checkbox field', 'New checkbox field'),
(602, 'FORMS_FORM_ADD_FIELD_SELECT_LABEL', 'PARENT_FORMS_FORM_ADD_FIELD', 'New drop down field', 'New drop down field'),
(603, 'FORMS_FORM_ADD_FIELD_ADDING', 'PARENT_FORMS_FORM_ADD_FIELD', 'Adding a new form field ...', 'Adding a new form field ...'),
(604, 'FORMS_FORM_ADD_FIELD_SUCCESS', 'PARENT_FORMS_FORM_ADD_FIELD', 'You have succesfully added a new form field.', 'You have succesfully added a new form field.'),
(605, 'PARENT_FORMS_FORM_DELETE_FIELD', '', 'Forms - Delete form field', 'Forms - Delete form field'),
(606, 'FORMS_FORM_DELETE_FIELD_CONFIRMATION', 'PARENT_FORMS_FORM_DELETE_FIELD', 'Are you sure you want to delete this form field?', 'Are you sure you want to delete this form field?'),
(607, 'FORMS_FORM_DELETE_FIELD_SUBMIT', 'PARENT_FORMS_FORM_DELETE_FIELD', 'Delete', 'Delete'),
(608, 'FORMS_FORM_DELETE_FIELD_DELETING', 'PARENT_FORMS_FORM_DELETE_FIELD', 'Deleting form field ...', 'Deleting form field ...'),
(609, 'FORMS_FORM_DELETE_FIELD_SUCCESS', 'PARENT_FORMS_FORM_DELETE_FIELD', 'You have succesfully deleted the form field.', 'You have succesfully deleted the form field.'),
(610, 'PARENT_FORMS_FORM_FIELD_SELECT_OPTIONS', '', 'Forms - Form field - Select options', 'Forms - Form field - Select options'),
(611, 'FORMS_FORM_FIELD_SELECT_OPTIONS_LABEL', 'PARENT_FORMS_FORM_FIELD_SELECT_OPTIONS', 'Options', 'Options'),
(612, 'PARENT_FORMS_FORM_FIELD_SELECT_OPTION', '', 'Forms - Form field - Select option', 'Forms - Form field - Select option'),
(613, 'FORMS_FORM_FIELD_SELECT_OPTION_SORT', 'PARENT_FORMS_FORM_FIELD_SELECT_OPTION', 'Sort', 'Sort'),
(614, 'PARENT_FORMS_FORM_FIELD_SELECT_ADD_OPTION', '', 'Forms - Form field - Add select option', 'Forms - Form field - Add select option'),
(615, 'FORMS_FORM_FIELD_SELECT_ADD_OPTION_LABEL', 'PARENT_FORMS_FORM_FIELD_SELECT_ADD_OPTION', 'New option', 'New option'),
(616, 'FORMS_FORM_FIELD_SELECT_ADD_OPTION_SUBMIT', 'PARENT_FORMS_FORM_FIELD_SELECT_ADD_OPTION', 'Add select option', 'Add select option'),
(617, 'FORMS_FORM_FIELD_SELECT_ADD_OPTION_ADDING', 'PARENT_FORMS_FORM_FIELD_SELECT_ADD_OPTION', 'Adding a new select option ...', 'Adding a new select option ...'),
(618, 'FORMS_FORM_FIELD_SELECT_ADD_OPTION_SUCCESS', 'PARENT_FORMS_FORM_FIELD_SELECT_ADD_OPTION', 'You have succesfully added a new select option.', 'You have succesfully added a new select option.'),
(619, 'PARENT_FORMS_FORM_FIELD_SELECT_DELETE_OPTION', '', 'Forms - Form field - Delete select option', 'Forms - Form field - Delete select option'),
(620, 'FORMS_FORM_FIELD_SELECT_DELETE_OPTION_CONFIRMATION', 'PARENT_FORMS_FORM_FIELD_SELECT_DELETE_OPTION', 'Are you sure you want to delete this select option?', 'Are you sure you want to delete this select option?'),
(621, 'FORMS_FORM_FIELD_SELECT_DELETE_OPTION_SUBMIT', 'PARENT_FORMS_FORM_FIELD_SELECT_DELETE_OPTION', 'Delete', 'Delete'),
(622, 'FORMS_FORM_FIELD_SELECT_DELETE_OPTION_DELETING', 'PARENT_FORMS_FORM_FIELD_SELECT_DELETE_OPTION', 'Deleting select option ...', 'Deleting select option ...'),
(623, 'FORMS_FORM_FIELD_SELECT_DELETE_OPTION_SUCCESS', 'PARENT_FORMS_FORM_FIELD_SELECT_DELETE_OPTION', 'You have succesfully deleted the select option.', 'You have succesfully deleted the select option.'),
(624, 'PARENT_FORMS_HELP', '', 'Forms - Help', 'Forms - Help'),
(625, 'FORMS_HELP', 'PARENT_FORMS_HELP', 'Click on a form item to open the editing area.', 'Click on a form item to open the editing area.'),
(626, 'FORMS_ADD_FORM_HELP', 'PARENT_FORMS_HELP', 'Click on the "plus" icon to add a form.', 'Click on the "plus" icon to add a form.'),
(627, 'FORMS_FORM_NAME_HELP', 'PARENT_FORMS_HELP', 'Change form name.', 'Change form name.'),
(628, 'FORMS_FORM_LANGUAGE_HELP', 'PARENT_FORMS_HELP', 'Change to the language you want to edit the form.', 'Change to the language you want to edit the form.'),
(629, 'FORMS_FORM_ADD_FIELD_HELP', 'PARENT_FORMS_HELP', 'Click on the bellow "plus" icon to add a form field.', 'Click on the bellow "plus" icon to add a form field.'),
(630, 'FORMS_FORM_EDIT_FIELD_HELP', 'PARENT_FORMS_HELP', 'Click the field "expand" icon to display/hide the settings.', 'Click the field "expand" icon to display/hide the settings.'),
(631, 'FORMS_FORM_DELETE_FIELD_HELP', 'PARENT_FORMS_HELP', 'Click the field "trash" icon to delete it.', 'Click the field "trash" icon to delete it.'),
(632, 'FORMS_FORM_SORT_FIELD_HELP', 'PARENT_FORMS_HELP', 'Drag the field "arrows" icon to sort it.', 'Drag the field "arrows" icon to sort it.'),
(633, 'FORMS_FORM_FIELD_LABEL_HELP', 'PARENT_FORMS_HELP', 'Enter field label.', 'Enter field label.'),
(634, 'FORMS_FORM_FIELD_ALLOWED_CHARACTERS_HELP', 'PARENT_FORMS_HELP', 'Enter the caracters allowed in this field. Leave it blank if all characters are allowed.', 'Enter the caracters allowed in this field. Leave it blank if all characters are allowed.'),
(635, 'FORMS_FORM_FIELD_SIZE_HELP', 'PARENT_FORMS_HELP', 'Enter the maximum number of characters allowed. Leave it blank for unlimited.', 'Enter the maximum number of characters allowed. Leave it blank for unlimited.'),
(636, 'FORMS_FORM_FIELD_EMAIL_HELP', 'PARENT_FORMS_HELP', 'Enable it if you want this field to be verified if an email has been added or not.', 'Enable it if you want this field to be verified if an email has been added or not.'),
(637, 'FORMS_FORM_FIELD_REQUIRED_HELP', 'PARENT_FORMS_HELP', 'Enable it if you want the field to be mandatory.', 'Enable it if you want the field to be mandatory.'),
(638, 'FORMS_FORM_FIELD_MULTIPLE_SELECT_HELP', 'PARENT_FORMS_HELP', 'Enable it if you want a multiple select drop down.', 'Enable it if you want a multiple select drop down.'),
(639, 'FORMS_FORM_FIELD_ADD_TO_DAY_HOUR_INFO_HELP', 'PARENT_FORMS_FORM_FIELD', 'Enable it if you want to display the field in a reservations list, in the info tooltip, in calendars days/hours.', 'Enable it if you want to display the field in a reservations list, in the info tooltip, in calendars days/hours.'),
(640, 'FORMS_FORM_FIELD_ADD_TO_DAY_HOUR_BODY_HELP', 'PARENT_FORMS_FORM_FIELD', 'Enable it if you want to display the field in a reservations list, in the days/hours boby (under availability), in calendars.', 'Enable it if you want to display the field in a reservations list, in the days/hours boby (under availability), in calendars.'),
(641, 'FORMS_FORM_FIELD_SELECT_OPTIONS_HELP', 'PARENT_FORMS_HELP', 'Click the "plus" icon to add another option and enter the name. Click on the "delete" icon to remove the option.', 'Click the "plus" icon to add another option and enter the name. Click on the "delete" icon to remove the option.'),
(642, 'PARENT_FORMS_FRONT_END', '', 'Forms - Front end', 'Forms - Front end'),
(643, 'FORMS_FRONT_END_TITLE', 'PARENT_FORMS_FRONT_END', 'Contact information', 'Contact information'),
(644, 'FORMS_FRONT_END_REQUIRED', 'PARENT_FORMS_FRONT_END', 'is required.', 'is required.'),
(645, 'FORMS_FRONT_END_INVALID_EMAIL', 'PARENT_FORMS_FRONT_END', 'is invalid. Please enter a valid email.', 'is invalid. Please enter a valid email.'),
(646, 'TITLE', 'none', 'Pinpoint Booking System', 'Pinpoint Booking System'),
(647, 'PARENT_MESSAGES', '', 'Messages', 'Messages'),
(648, 'MESSAGES_LOADING', 'PARENT_MESSAGES', 'Loading data ...', 'Loading data ...'),
(649, 'MESSAGES_LOADING_SUCCESS', 'PARENT_MESSAGES', 'Data has been loaded.', 'Data has been loaded.'),
(650, 'MESSAGES_SAVING', 'PARENT_MESSAGES', 'Saving data ...', 'Saving data ...'),
(651, 'MESSAGES_SAVING_SUCCESS', 'PARENT_MESSAGES', 'Data has been saved.', 'Data has been saved.'),
(652, 'MESSAGES_CONFIRMATION_YES', 'PARENT_MESSAGES', 'Yes', 'Yes'),
(653, 'MESSAGES_CONFIRMATION_NO', 'PARENT_MESSAGES', 'No', 'No'),
(654, 'MESSAGES_PRO_TITLE', 'PARENT_MESSAGES', 'PRO', 'PRO'),
(655, 'MESSAGES_PRO_INFO', 'PARENT_PRO_VERSION', 'only in PRO', 'only in PRO'),
(656, 'MESSAGES_PRO_TEXT', 'PARENT_PRO_VERSION', 'This feature is only available in PRO version.', 'This feature is only available in PRO version.'),
(657, 'MESSAGES_PRO_REMOVE_TEXT1', 'PARENT_PRO_VERSION', 'Permanently remove any reference to Pinpoint Booking System PRO version by clicking the close button. This action cannot be undone.', 'Permanently remove any reference to Pinpoint Booking System PRO version by clicking the close button. This action cannot be undone.'),
(658, 'MESSAGES_PRO_REMOVE_TEXT2', 'PARENT_PRO_VERSION', 'You can also remove any information about PRO version if you set the constant %s value to %s in file %s.', 'You can also remove any information about PRO version if you set the constant %s value to %s in file %s.'),
(659, 'PARENT_MONTHS_WEEK_DAYS', '', 'Months & Week Days', 'Months & Week Days'),
(660, 'MONTH_JANUARY', 'PARENT_MONTHS_WEEK_DAYS', 'January', 'January'),
(661, 'MONTH_FEBRUARY', 'PARENT_MONTHS_WEEK_DAYS', 'February', 'February'),
(662, 'MONTH_MARCH', 'PARENT_MONTHS_WEEK_DAYS', 'March', 'March'),
(663, 'MONTH_APRIL', 'PARENT_MONTHS_WEEK_DAYS', 'April', 'April'),
(664, 'MONTH_MAY', 'PARENT_MONTHS_WEEK_DAYS', 'May', 'May'),
(665, 'MONTH_JUNE', 'PARENT_MONTHS_WEEK_DAYS', 'June', 'June'),
(666, 'MONTH_JULY', 'PARENT_MONTHS_WEEK_DAYS', 'July', 'July'),
(667, 'MONTH_AUGUST', 'PARENT_MONTHS_WEEK_DAYS', 'August', 'August'),
(668, 'MONTH_SEPTEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'September', 'September'),
(669, 'MONTH_OCTOBER', 'PARENT_MONTHS_WEEK_DAYS', 'October', 'October'),
(670, 'MONTH_NOVEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'November', 'November'),
(671, 'MONTH_DECEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'December', 'December'),
(672, 'SHORT_MONTH_JANUARY', 'PARENT_MONTHS_WEEK_DAYS', 'Jan', 'Jan'),
(673, 'SHORT_MONTH_FEBRUARY', 'PARENT_MONTHS_WEEK_DAYS', 'Feb', 'Feb'),
(674, 'SHORT_MONTH_MARCH', 'PARENT_MONTHS_WEEK_DAYS', 'Mar', 'Mar'),
(675, 'SHORT_MONTH_APRIL', 'PARENT_MONTHS_WEEK_DAYS', 'Apr', 'Apr'),
(676, 'SHORT_MONTH_MAY', 'PARENT_MONTHS_WEEK_DAYS', 'May', 'May'),
(677, 'SHORT_MONTH_JUNE', 'PARENT_MONTHS_WEEK_DAYS', 'Jun', 'Jun'),
(678, 'SHORT_MONTH_JULY', 'PARENT_MONTHS_WEEK_DAYS', 'Jul', 'Jul'),
(679, 'SHORT_MONTH_AUGUST', 'PARENT_MONTHS_WEEK_DAYS', 'Aug', 'Aug'),
(680, 'SHORT_MONTH_SEPTEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'Sep', 'Sep'),
(681, 'SHORT_MONTH_OCTOBER', 'PARENT_MONTHS_WEEK_DAYS', 'Oct', 'Oct'),
(682, 'SHORT_MONTH_NOVEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'Nov', 'Nov'),
(683, 'SHORT_MONTH_DECEMBER', 'PARENT_MONTHS_WEEK_DAYS', 'Dec', 'Dec'),
(684, 'DAY_MONDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Monday', 'Monday'),
(685, 'DAY_TUESDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Tuesday', 'Tuesday'),
(686, 'DAY_WEDNESDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Wednesday', 'Wednesday'),
(687, 'DAY_THURSDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Thursday', 'Thursday'),
(688, 'DAY_FRIDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Friday', 'Friday'),
(689, 'DAY_SATURDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Saturday', 'Saturday'),
(690, 'DAY_SUNDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Sunday', 'Sunday'),
(691, 'SHORT_DAY_MONDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Mo', 'Mo'),
(692, 'SHORT_DAY_TUESDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Tu', 'Tu'),
(693, 'SHORT_DAY_WEDNESDAY', 'PARENT_MONTHS_WEEK_DAYS', 'We', 'We'),
(694, 'SHORT_DAY_THURSDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Th', 'Th'),
(695, 'SHORT_DAY_FRIDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Fr', 'Fr'),
(696, 'SHORT_DAY_SATURDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Sa', 'Sa'),
(697, 'SHORT_DAY_SUNDAY', 'PARENT_MONTHS_WEEK_DAYS', 'Su', 'Su'),
(698, 'PARENT_TINYMCE', '', 'TinyMCE', 'TinyMCE'),
(699, 'TINYMCE_ADD', 'PARENT_TINYMCE', 'Add Calendar', 'Add Calendar'),
(700, 'PARENT_DOCUMENTATION', '', 'Documentation', 'Documentation') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(701, 'HELP_DOCUMENTATION', 'PARENT_DOCUMENTATION', 'Documentation', 'Documentation'),
(702, 'HELP_VIEW_DOCUMENTATION', 'PARENT_DOCUMENTATION', 'Click this to view the documentation for more informations.', 'Click this to view the documentation for more informations.'),
(703, 'BETA', 'none', 'beta', 'beta'),
(704, 'BETA_TITLE', 'none', 'Beta', 'Beta'),
(705, 'SOON', 'none', 'soon', 'soon'),
(706, 'SOON_TITLE', 'none', 'Comming soon', 'Comming soon'),
(707, 'PARENT_LANGUAGES', '', 'Languages', 'Languages'),
(708, 'LANGUAGES_MANAGE', 'PARENT_LANGUAGES', 'Manage languages', 'Manage languages'),
(709, 'LANGUAGES_LOADED', 'PARENT_LANGUAGES', 'Languages have been loaded.', 'Languages have been loaded.'),
(710, 'LANGUAGES_SETTING', 'PARENT_LANGUAGES', 'The language is being configured ...', 'The language is being configured ...'),
(711, 'LANGUAGES_SET_SUCCESS', 'PARENT_LANGUAGES', 'The language has been added. The page will refresh shortly.', 'The language has been added. The page will refresh shortly.'),
(712, 'LANGUAGES_REMOVE_CONFIGURATION', 'PARENT_LANGUAGES', 'Are you sure you want to remove this language? Data will be deleted only when you reset the translation!', 'Are you sure you want to remove this language? Data will be deleted only when you reset the translation!'),
(713, 'LANGUAGES_REMOVING', 'PARENT_LANGUAGES', 'The language is being removed ...', 'The language is being removed ...'),
(714, 'LANGUAGES_REMOVE_SUCCESS', 'PARENT_LANGUAGES', 'The language has been removed. The page will refresh shortly.', 'The language has been removed. The page will refresh shortly.'),
(715, 'PARENT_LANGUAGES_HELP', '', 'Languages - Help', 'Languages - Help'),
(716, 'LANGUAGES_HELP', 'PARENT_LANGUAGES_HELP', 'If you need to use more language with the plugin go to "Manage langauges" section and enable them.', 'If you need to use more language with the plugin go to "Manage langauges" section and enable them.'),
(717, 'PARENT_LOCATIONS', '', 'Locations', 'Locations'),
(718, 'LOCATIONS_TITLE', 'PARENT_LOCATIONS', 'Locations', 'Locations'),
(719, 'LOCATIONS_CREATED_BY', 'PARENT_LOCATIONS', 'Created by', 'Created by'),
(720, 'LOCATIONS_LOAD_SUCCESS', 'PARENT_LOCATIONS', 'Locations list loaded.', 'Locations list loaded.'),
(721, 'LOCATIONS_NO_LOCATIONS', 'PARENT_LOCATIONS', 'No locations. Click the above "plus" icon to add a new one.', 'No locations. Click the above "plus" icon to add a new one.'),
(722, 'PARENT_LOCATIONS_LOCATION', '', 'Locations - Location', 'Locations - Location'),
(723, 'LOCATIONS_LOCATION_NAME', 'PARENT_LOCATIONS_LOCATION', 'Name', 'Name'),
(724, 'LOCATIONS_LOCATION_MAP', 'PARENT_LOCATIONS_LOCATION', 'Enter the address', 'Enter the address'),
(725, 'LOCATIONS_LOCATION_ADDRESS', 'PARENT_LOCATIONS_LOCATION', 'Address', 'Address'),
(726, 'LOCATIONS_LOCATION_ALT_ADDRESS', 'PARENT_LOCATIONS_LOCATION', 'Alternative address', 'Alternative address'),
(727, 'LOCATIONS_LOCATION_CALENDARS', 'PARENT_LOCATIONS_LOCATION', 'Add calendars to location', 'Add calendars to location'),
(728, 'LOCATIONS_LOCATION_NO_CALENDARS', 'PARENT_LOCATIONS_LOCATION', 'There are no calendars created. Go to <a href="%s">calendars</a> page to create one.', 'There are no calendars created. Go to <a href="%s">calendars</a> page to create one.'),
(729, 'LOCATIONS_LOCATION_SHARE', 'PARENT_LOCATIONS_LOCATION', 'Share your location with ', 'Share your location with '),
(730, 'LOCATIONS_LOCATION_LINK', 'PARENT_LOCATIONS_LOCATION', 'Enter the link of your site', 'Enter the link of your site'),
(731, 'LOCATIONS_LOCATION_IMAGE', 'PARENT_LOCATIONS_LOCATION', 'Enter a link with an image', 'Enter a link with an image'),
(732, 'LOCATIONS_LOCATION_BUSINESSES', 'PARENT_LOCATIONS_LOCATION', 'Select what kind of businesses you have at this location', 'Select what kind of businesses you have at this location'),
(733, 'LOCATIONS_LOCATION_BUSINESSES_OTHER', 'PARENT_LOCATIONS_LOCATION', 'Enter businesses that are not in the list', 'Enter businesses that are not in the list'),
(734, 'LOCATIONS_LOCATION_LANGUAGES', 'PARENT_LOCATIONS_LOCATION', 'Enter the languages that are spoken in your business', 'Enter the languages that are spoken in your business'),
(735, 'LOCATIONS_LOCATION_EMAIL', 'PARENT_LOCATIONS_LOCATION', 'Your email', 'Your email'),
(736, 'LOCATIONS_LOCATION_SHARE_SUBMIT', 'PARENT_LOCATIONS_LOCATION', 'Share to PINPOINT.WORLD', 'Share to PINPOINT.WORLD'),
(737, 'LOCATIONS_LOCATION_SHARE_SUBMIT_SUCCESS', 'PARENT_LOCATIONS_LOCATION', 'Your location has been sent to PINPOINT.WORLD', 'Your location has been sent to PINPOINT.WORLD'),
(738, 'LOCATIONS_LOCATION_SHARE_SUBMIT_ERROR', 'PARENT_LOCATIONS_LOCATION', 'Please complete all location data. Only alternative address is mandatory and you need to select a businness or enter another business.', 'Please complete all location data. Only alternative address is mandatory and you need to select a businness or enter another business.'),
(739, 'LOCATIONS_LOCATION_SHARE_SUBMIT_ERROR_DUPLICATE', 'PARENT_LOCATIONS_LOCATION', 'The location has already been submitted to PINPOINT.WORLD', 'The location has already been submitted to PINPOINT.WORLD'),
(740, 'LOCATIONS_LOCATION_BUSINESS_APARTMENT', 'PARENT_LOCATIONS_LOCATION', 'Appartment', 'Appartment'),
(741, 'LOCATIONS_LOCATION_BUSINESS_BABY_SITTER', 'PARENT_LOCATIONS_LOCATION', 'Baby sitter', 'Baby sitter'),
(742, 'LOCATIONS_LOCATION_BUSINESS_BAR', 'PARENT_LOCATIONS_LOCATION', 'Bar', 'Bar'),
(743, 'LOCATIONS_LOCATION_BUSINESS_BASKETBALL_COURT', 'PARENT_LOCATIONS_LOCATION', 'Basketball court(s)', 'Basketball court(s)'),
(744, 'LOCATIONS_LOCATION_BUSINESS_BEAUTY_SALON', 'PARENT_LOCATIONS_LOCATION', 'Beauty salon', 'Beauty salon'),
(745, 'LOCATIONS_LOCATION_BUSINESS_BIKES', 'PARENT_LOCATIONS_LOCATION', 'Bikes', 'Bikes'),
(746, 'LOCATIONS_LOCATION_BUSINESS_BOAT', 'PARENT_LOCATIONS_LOCATION', 'Boat', 'Boat'),
(747, 'LOCATIONS_LOCATION_BUSINESS_BUSINESS', 'PARENT_LOCATIONS_LOCATION', 'Business', 'Business'),
(748, 'LOCATIONS_LOCATION_BUSINESS_CAMPING', 'PARENT_LOCATIONS_LOCATION', 'Camping', 'Camping'),
(749, 'LOCATIONS_LOCATION_BUSINESS_CAMPING_GEAR', 'PARENT_LOCATIONS_LOCATION', 'Camping gear', 'Camping gear'),
(750, 'LOCATIONS_LOCATION_BUSINESS_CARS', 'PARENT_LOCATIONS_LOCATION', 'Cars', 'Cars'),
(751, 'LOCATIONS_LOCATION_BUSINESS_CHEF', 'PARENT_LOCATIONS_LOCATION', 'Chef', 'Chef'),
(752, 'LOCATIONS_LOCATION_BUSINESS_CINEMA', 'PARENT_LOCATIONS_LOCATION', 'Cinema', 'Cinema'),
(753, 'LOCATIONS_LOCATION_BUSINESS_CLOTHES', 'PARENT_LOCATIONS_LOCATION', 'Clothes', 'Clothes'),
(754, 'LOCATIONS_LOCATION_BUSINESS_COSTUMES', 'PARENT_LOCATIONS_LOCATION', 'Costumes', 'Costumes'),
(755, 'LOCATIONS_LOCATION_BUSINESS_CLUB', 'PARENT_LOCATIONS_LOCATION', 'Club', 'Club'),
(756, 'LOCATIONS_LOCATION_BUSINESS_DANCE_INSTRUCTOR', 'PARENT_LOCATIONS_LOCATION', 'Dance instructor', 'Dance instructor'),
(757, 'LOCATIONS_LOCATION_BUSINESS_DENTIST', 'PARENT_LOCATIONS_LOCATION', 'Dentist', 'Dentist'),
(758, 'LOCATIONS_LOCATION_BUSINESS_DESIGNER_HANDBAGS', 'PARENT_LOCATIONS_LOCATION', 'Designer handbags', 'Designer handbags'),
(759, 'LOCATIONS_LOCATION_BUSINESS_DOCTOR', 'PARENT_LOCATIONS_LOCATION', 'Doctor', 'Doctor'),
(760, 'LOCATIONS_LOCATION_BUSINESS_ESTHETICIAN', 'PARENT_LOCATIONS_LOCATION', 'Esthetician', 'Esthetician'),
(761, 'LOCATIONS_LOCATION_BUSINESS_FOOTBALL_COURT', 'PARENT_LOCATIONS_LOCATION', 'Football court(s)', 'Football court(s)'),
(762, 'LOCATIONS_LOCATION_BUSINESS_FISHING', 'PARENT_LOCATIONS_LOCATION', 'Fishing', 'Fishing'),
(763, 'LOCATIONS_LOCATION_BUSINESS_GADGETS', 'PARENT_LOCATIONS_LOCATION', 'Gadgets', 'Gadgets'),
(764, 'LOCATIONS_LOCATION_BUSINESS_GAMES', 'PARENT_LOCATIONS_LOCATION', 'Games', 'Games'),
(765, 'LOCATIONS_LOCATION_BUSINESS_GOLF', 'PARENT_LOCATIONS_LOCATION', 'Golf', 'Golf'),
(766, 'LOCATIONS_LOCATION_BUSINESS_HAIRDRESSER', 'PARENT_LOCATIONS_LOCATION', 'Hairdresser', 'Hairdresser'),
(767, 'LOCATIONS_LOCATION_BUSINESS_HEALTH_CLUB', 'PARENT_LOCATIONS_LOCATION', 'Health club', 'Health club'),
(768, 'LOCATIONS_LOCATION_BUSINESS_HOSPITAL', 'PARENT_LOCATIONS_LOCATION', 'Hospital', 'Hospital'),
(769, 'LOCATIONS_LOCATION_BUSINESS_HOTEL', 'PARENT_LOCATIONS_LOCATION', 'Hotel', 'Hotel'),
(770, 'LOCATIONS_LOCATION_BUSINESS_HUNTING', 'PARENT_LOCATIONS_LOCATION', 'Hunting', 'Hunting'),
(771, 'LOCATIONS_LOCATION_BUSINESS_LAWYER', 'PARENT_LOCATIONS_LOCATION', 'Lawyer', 'Lawyer'),
(772, 'LOCATIONS_LOCATION_BUSINESS_LIBRARY', 'PARENT_LOCATIONS_LOCATION', 'Library', 'Library'),
(773, 'LOCATIONS_LOCATION_BUSINESS_MASSAGE', 'PARENT_LOCATIONS_LOCATION', 'Massage', 'Massage'),
(774, 'LOCATIONS_LOCATION_BUSINESS_MUSIC_BAND', 'PARENT_LOCATIONS_LOCATION', 'Music band', 'Music band'),
(775, 'LOCATIONS_LOCATION_BUSINESS_NAILS_SALON', 'PARENT_LOCATIONS_LOCATION', 'Nails salon', 'Nails salon'),
(776, 'LOCATIONS_LOCATION_BUSINESS_PARTY_SUPPLIES', 'PARENT_LOCATIONS_LOCATION', 'Party supplies', 'Party supplies'),
(777, 'LOCATIONS_LOCATION_BUSINESS_PERSONAL_TRAINER', 'PARENT_LOCATIONS_LOCATION', 'Personal trainer', 'Personal trainer'),
(778, 'LOCATIONS_LOCATION_BUSINESS_PET_CARE', 'PARENT_LOCATIONS_LOCATION', 'Pet care', 'Pet care'),
(779, 'LOCATIONS_LOCATION_BUSINESS_PHOTO_EQUIPMENT', 'PARENT_LOCATIONS_LOCATION', 'Photo equipment', 'Photo equipment'),
(780, 'LOCATIONS_LOCATION_BUSINESS_PHOTOGRAPHER', 'PARENT_LOCATIONS_LOCATION', 'Photographer', 'Photographer'),
(781, 'LOCATIONS_LOCATION_BUSINESS_PILLATES_INSTRUCTOR', 'PARENT_LOCATIONS_LOCATION', 'Pillates instructor', 'Pillates instructor'),
(782, 'LOCATIONS_LOCATION_BUSINESS_PLANE_TICKETS', 'PARENT_LOCATIONS_LOCATION', 'Plane tickets', 'Plane tickets'),
(783, 'LOCATIONS_LOCATION_BUSINESS_PLANES', 'PARENT_LOCATIONS_LOCATION', 'Plane(s)', 'Plane(s)'),
(784, 'LOCATIONS_LOCATION_BUSINESS_RESTAURANT', 'PARENT_LOCATIONS_LOCATION', 'Restaurant', 'Restaurant'),
(785, 'LOCATIONS_LOCATION_BUSINESS_SHOES', 'PARENT_LOCATIONS_LOCATION', 'Shoes', 'Shoes'),
(786, 'LOCATIONS_LOCATION_BUSINESS_SNOW_EQUIPMENT', 'PARENT_LOCATIONS_LOCATION', 'Snow equipment', 'Snow equipment'),
(787, 'LOCATIONS_LOCATION_BUSINESS_SPA', 'PARENT_LOCATIONS_LOCATION', 'Spa', 'Spa'),
(788, 'LOCATIONS_LOCATION_BUSINESS_SPORTS_COACH', 'PARENT_LOCATIONS_LOCATION', 'Sports coach', 'Sports coach'),
(789, 'LOCATIONS_LOCATION_BUSINESS_TAXIES', 'PARENT_LOCATIONS_LOCATION', 'Taxies', 'Taxies'),
(790, 'LOCATIONS_LOCATION_BUSINESS_TENIS_COURT', 'PARENT_LOCATIONS_LOCATION', 'Tenis court(s)', 'Tenis court(s)'),
(791, 'LOCATIONS_LOCATION_BUSINESS_THEATRE', 'PARENT_LOCATIONS_LOCATION', 'Theatre', 'Theatre'),
(792, 'LOCATIONS_LOCATION_BUSINESS_VILLA', 'PARENT_LOCATIONS_LOCATION', 'Villa', 'Villa'),
(793, 'LOCATIONS_LOCATION_BUSINESS_WEAPONS', 'PARENT_LOCATIONS_LOCATION', 'Weapons', 'Weapons'),
(794, 'LOCATIONS_LOCATION_BUSINESS_WORKING_TOOLS', 'PARENT_LOCATIONS_LOCATION', 'Working tools', 'Working tools'),
(795, 'LOCATIONS_LOCATION_LOADED', 'PARENT_LOCATIONS_LOCATION', 'Location loaded.', 'Location loaded.'),
(796, 'LOCATIONS_LOCATION_NO_GOOGLE_MAPS', 'PARENT_LOCATIONS_LOCATION', 'Google maps did not load. Please refresh the page to try again.', 'Google maps did not load. Please refresh the page to try again.'),
(797, 'PARENT_LOCATIONS_ADD_LOCATION', '', 'Locations - Add location', 'Locations - Add location'),
(798, 'LOCATIONS_ADD_LOCATION_NAME', 'PARENT_LOCATIONS_ADD_LOCATION', 'New location', 'New location'),
(799, 'LOCATIONS_ADD_LOCATION_SUBMIT', 'PARENT_LOCATIONS_ADD_LOCATION', 'Add location', 'Add location'),
(800, 'LOCATIONS_ADD_LOCATION_ADDING', 'PARENT_LOCATIONS_ADD_LOCATION', 'Adding a new location ...', 'Adding a new location ...') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(801, 'LOCATIONS_ADD_LOCATION_SUCCESS', 'PARENT_LOCATIONS_ADD_LOCATION', 'You have succesfully added a new location.', 'You have succesfully added a new location.'),
(802, 'PARENT_LOCATIONS_DELETE_LOCATION', '', 'Locations - Delete location', 'Locations - Delete location'),
(803, 'LOCATIONS_DELETE_LOCATION_CONFIRMATION', 'PARENT_LOCATIONS_DELETE_LOCATION', 'Are you sure you want to delete this location?', 'Are you sure you want to delete this location?'),
(804, 'LOCATIONS_DELETE_LOCATION_SUBMIT', 'PARENT_LOCATIONS_DELETE_LOCATION', 'Delete location', 'Delete location'),
(805, 'LOCATIONS_DELETE_LOCATION_DELETING', 'PARENT_LOCATIONS_DELETE_LOCATION', 'Deleting location ...', 'Deleting location ...'),
(806, 'LOCATIONS_DELETE_LOCATION_SUCCESS', 'PARENT_LOCATIONS_DELETE_LOCATION', 'You have succesfully deleted the location.', 'You have succesfully deleted the location.'),
(807, 'PARENT_LOCATIONS_HELP', '', 'Locations - Help', 'Locations - Help'),
(808, 'LOCATIONS_HELP', 'PARENT_LOCATIONS_HELP', 'Click on a location item to open the editing area.', 'Click on a location item to open the editing area.'),
(809, 'LOCATIONS_ADD_LOCATION_HELP', 'PARENT_LOCATIONS_HELP', 'Click on the "plus" icon to add a location.', 'Click on the "plus" icon to add a location.'),
(810, 'LOCATIONS_LOCATION_HELP', 'PARENT_LOCATIONS_HELP', 'Click the "trash" icon to delete the location.', 'Click the "trash" icon to delete the location.'),
(811, 'LOCATIONS_LOCATION_NAME_HELP', 'PARENT_LOCATIONS_HELP', 'Change location name.', 'Change location name.'),
(812, 'LOCATIONS_LOCATION_ADDRESS_HELP', 'PARENT_LOCATIONS_HELP', 'Enter location address or drag the marker on the map to select it.', 'Enter location address or drag the marker on the map to select it.'),
(813, 'LOCATIONS_LOCATION_ALT_ADDRESS_HELP', 'PARENT_LOCATIONS_HELP', 'Enter an alternative address if the marker is in the correct position but the address is not right.', 'Enter an alternative address if the marker is in the correct position but the address is not right.'),
(814, 'LOCATIONS_LOCATION_LINK_HELP', 'PARENT_LOCATIONS_HELP', 'Enter the link of your site. Make sure it redirects to a page where people can make a booking or can view relevant content.', 'Enter the link of your site. Make sure it redirects to a page where people can make a booking or can view relevant content.'),
(815, 'LOCATIONS_LOCATION_IMAGE_HELP', 'PARENT_LOCATIONS_HELP', 'Make sure the image is relevant to your business.', 'Make sure the image is relevant to your business.'),
(816, 'LOCATIONS_LOCATION_BUSINESSES_HELP', 'PARENT_LOCATIONS_HELP', 'Select what kind of businesses you have at this location. You can select multiple businesses.', 'Select what kind of businesses you have at this location. You can select multiple businesses.'),
(817, 'LOCATIONS_LOCATION_BUSINESSES_OTHER_HELP', 'PARENT_LOCATIONS_HELP', 'We will add them in the list as soon as possible.', 'We will add them in the list as soon as possible.'),
(818, 'LOCATIONS_LOCATION_LANGUAGES_HELP', 'PARENT_LOCATIONS_HELP', 'Enter the languages that are spoken in your business. You can select multiple languages.', 'Enter the languages that are spoken in your business. You can select multiple languages.'),
(819, 'LOCATIONS_LOCATION_EMAIL_HELP', 'PARENT_LOCATIONS_HELP', 'Enter the email where we can contact you if there are problems with your submission', 'Enter the email where we can contact you if there are problems with your submission'),
(820, 'PARENT_MODELS', '', 'Business models', 'Business models'),
(821, 'MODELS_TITLE', 'PARENT_MODELS', 'Business models', 'Business models'),
(822, 'MODELS_CREATED_BY', 'PARENT_MODELS', 'Created by', 'Created by'),
(823, 'MODELS_LOAD_SUCCESS', 'PARENT_MODELS', 'Business models list loaded.', 'Business models list loaded.'),
(824, 'MODELS_NO_MODELS', 'PARENT_MODELS', 'No business models. Click the above "plus" icon to add a new one.', 'No business models. Click the above "plus" icon to add a new one.'),
(825, 'PARENT_MODELS_MODEL', '', 'Business models - Model', 'Business models - Model'),
(826, 'MODELS_MODEL_NAME', 'PARENT_MODELS_MODEL', 'Name', 'Name'),
(827, 'MODELS_MODEL_LANGUAGE', 'PARENT_MODELS_MODEL', 'Language', 'Language'),
(828, 'MODELS_MODEL_ENABLED', 'PARENT_MODELS_MODEL', 'Use this business model', 'Use this business model'),
(829, 'MODELS_MODEL_LABEL', 'PARENT_MODELS_MODEL', 'Label', 'Label'),
(830, 'MODELS_MODEL_MULTIPLE_CALENDARS', 'PARENT_MODELS_MODEL', 'Use multiple calendars', 'Use multiple calendars'),
(831, 'MODELS_MODEL_CALENDAR_LABEL', 'PARENT_MODELS_MODEL', 'Calendar label', 'Calendar label'),
(832, 'MODELS_MODEL_LOADED', 'PARENT_MODELS_MODEL', 'Business model loaded.', 'Business model loaded.'),
(833, 'PARENT_MODELS_ADD_MODEL', '', 'Business models - Add business model', 'Business models - Add business model'),
(834, 'MODELS_ADD_MODEL_NAME', 'PARENT_MODELS_ADD_MODEL', 'New business model', 'New business model'),
(835, 'MODELS_ADD_MODEL_LABEL', 'PARENT_MODELS_ADD_MODEL', 'New business model label', 'New business model label'),
(836, 'MODELS_ADD_MODEL_LABEL_CALENDAR', 'PARENT_MODELS_ADD_MODEL', 'Calendar label', 'Calendar label'),
(837, 'MODELS_ADD_MODEL_SUBMIT', 'PARENT_MODELS_ADD_MODEL', 'Add business model', 'Add business model'),
(838, 'MODELS_ADD_MODEL_ADDING', 'PARENT_MODELS_ADD_MODEL', 'Adding a business model ...', 'Adding a business model ...'),
(839, 'MODELS_ADD_MODEL_SUCCESS', 'PARENT_MODELS_ADD_MODEL', 'You have succesfully added a new business model.', 'You have succesfully added a new business model.'),
(840, 'PARENT_MODELS_DELETE_MODEL', '', 'Business models - Delete business model', 'Business models - Delete business model'),
(841, 'MODELS_DELETE_MODEL_CONFIRMATION', 'PARENT_MODELS_DELETE_MODEL', 'Are you sure you want to delete this business model?', 'Are you sure you want to delete this business model?'),
(842, 'MODELS_DELETE_MODEL_SUBMIT', 'PARENT_MODELS_DELETE_MODEL', 'Delete business model', 'Delete business model'),
(843, 'MODELS_DELETE_MODEL_DELETING', 'PARENT_MODELS_DELETE_MODEL', 'Deleting business model ...', 'Deleting business model ...'),
(844, 'MODELS_DELETE_MODEL_SUCCESS', 'PARENT_MODELS_DELETE_MODEL', 'You have succesfully deleted the business model.', 'You have succesfully deleted the business model.'),
(845, 'PARENT_MODELS_HELP', '', 'Business models - Help', 'Business models - Help'),
(846, 'MODELS_HELP', 'PARENT_MODELS_HELP', 'Click on a business model item to open the editing area.', 'Click on a business model item to open the editing area.'),
(847, 'MODELS_ADD_MODEL_HELP', 'PARENT_MODELS_HELP', 'Click on the "plus" icon to add a business model.', 'Click on the "plus" icon to add a business model.'),
(848, 'MODELS_MODEL_HELP', 'PARENT_MODELS_HELP', 'Click the "trash" icon to delete the business model.', 'Click the "trash" icon to delete the business model.'),
(849, 'MODELS_MODEL_NAME_HELP', 'PARENT_MODELS_HELP', 'Change business model name.', 'Change business model name.'),
(850, 'MODELS_MODEL_LANGUAGE_HELP', 'PARENT_MODELS_HELP', 'Change to the language you want to edit the business models.', 'Change to the language you want to edit the business models.'),
(851, 'MODELS_MODEL_ENABLED_HELP', 'PARENT_MODELS_HELP', 'Enable this to use the business model.', 'Enable this to use the business model.'),
(852, 'MODELS_MODEL_LABEL_HELP', 'PARENT_MODELS_HELP', 'Enter business model label.', 'Enter business model label.'),
(853, 'MODELS_MODEL_MULTIPLE_CALENDARS_HELP', 'PARENT_MODELS_HELP', 'Enable this option to add more than one calendar to your business model.', 'Enable this option to add more than one calendar to your business model.'),
(854, 'MODELS_MODEL_CALENDAR_LABEL_HELP', 'PARENT_MODELS_HELP', 'Set how the calendars should be called. Examples: Room, Staff, ...', 'Set how the calendars should be called. Examples: Room, Staff, ...'),
(855, 'PARENT_ORDER', '', 'Order', 'Order'),
(856, 'ORDER_TITLE', 'PARENT_ORDER', 'Order', 'Order'),
(857, 'ORDER_UNAVAILABLE', 'PARENT_ORDER', 'The period you selected is not available anymore. The calendar will refresh to update the schedule.', 'The period you selected is not available anymore. The calendar will refresh to update the schedule.'),
(858, 'ORDER_UNAVAILABLE_COUPON', 'PARENT_ORDER', 'The coupon you entered is not available anymore.', 'The coupon you entered is not available anymore.'),
(859, 'ORDER_PAYMENT_METHOD', 'PARENT_RESERVATIONS_RESERVATION', 'Payment method', 'Payment method'),
(860, 'ORDER_PAYMENT_METHOD_NONE', 'PARENT_RESERVATIONS_RESERVATION', 'None', 'None'),
(861, 'ORDER_PAYMENT_METHOD_ARRIVAL', 'PARENT_RESERVATIONS_RESERVATION', 'On arrival', 'On arrival'),
(862, 'ORDER_PAYMENT_METHOD_WOOCOMMERCE', 'PARENT_RESERVATIONS_RESERVATION', 'WooCommerce', 'WooCommerce'),
(863, 'ORDER_PAYMENT_METHOD_WOOCOMMERCE_ORDER_ID', 'PARENT_RESERVATIONS_RESERVATION', 'Order ID', 'Order ID'),
(864, 'ORDER_PAYMENT_METHOD_TRANSACTION_ID', 'PARENT_RESERVATIONS_RESERVATION', 'Transaction ID', 'Transaction ID'),
(865, 'ORDER_PAYMENT_ARRIVAL', 'PARENT_ORDER', 'Pay on arrival (need to be approved)', 'Pay on arrival (need to be approved)'),
(866, 'ORDER_PAYMENT_ARRIVAL_WITH_APPROVAL', 'PARENT_ORDER', 'Pay on arrival (instant booking)', 'Pay on arrival (instant booking)'),
(867, 'ORDER_PAYMENT_ARRIVAL_SUCCESS', 'PARENT_ORDER', 'Your request has been successfully sent. Please wait for approval.', 'Your request has been successfully sent. Please wait for approval.'),
(868, 'ORDER_PAYMENT_ARRIVAL_WITH_APPROVAL_SUCCESS', 'PARENT_ORDER', 'Your request has been successfully received. We are waiting you!', 'Your request has been successfully received. We are waiting you!'),
(869, 'ORDER_TERMS_AND_CONDITIONS', 'PARENT_ORDER', 'I accept to agree to the Terms & Conditions.', 'I accept to agree to the Terms & Conditions.'),
(870, 'ORDER_TERMS_AND_CONDITIONS_INVALID', 'PARENT_ORDER', 'You must agree with our Terms & Conditions to continue.', 'You must agree with our Terms & Conditions to continue.'),
(871, 'ORDER_BOOK', 'PARENT_ORDER', 'Book now', 'Book now'),
(872, 'PARENT_ORDER_ADDRESS', '', 'Order - Billing/shipping address', 'Order - Billing/shipping address'),
(873, 'ORDER_ADDRESS_SELECT_PAYMENT_METHOD', 'PARENT_ORDER_ADDRESS', 'Select payment method.', 'Select payment method.'),
(874, 'ORDER_ADDRESS_BILLING', 'PARENT_ORDER_ADDRESS', 'Billing address', 'Billing address'),
(875, 'ORDER_ADDRESS_BILLING_DISABLED', 'PARENT_ORDER_ADDRESS', 'Billing address is not enabled.', 'Billing address is not enabled.'),
(876, 'ORDER_ADDRESS_SHIPPING', 'PARENT_ORDER_ADDRESS', 'Shipping address', 'Shipping address'),
(877, 'ORDER_ADDRESS_SHIPPING_DISABLED', 'PARENT_ORDER_ADDRESS', 'Shipping address is not enabled.', 'Shipping address is not enabled.'),
(878, 'ORDER_ADDRESS_SHIPPING_COPY', 'PARENT_ORDER_ADDRESS', 'Use billing address', 'Use billing address'),
(879, 'ORDER_ADDRESS_FIRST_NAME', 'PARENT_ORDER_ADDRESS', 'First name', 'First name'),
(880, 'ORDER_ADDRESS_LAST_NAME', 'PARENT_ORDER_ADDRESS', 'Last name', 'Last name'),
(881, 'ORDER_ADDRESS_COMPANY', 'PARENT_ORDER_ADDRESS', 'Company', 'Company'),
(882, 'ORDER_ADDRESS_EMAIL', 'PARENT_ORDER_ADDRESS', 'Email', 'Email'),
(883, 'ORDER_ADDRESS_PHONE', 'PARENT_ORDER_ADDRESS', 'Phone number', 'Phone number'),
(884, 'ORDER_ADDRESS_COUNTRY', 'PARENT_ORDER_ADDRESS', 'Country', 'Country'),
(885, 'ORDER_ADDRESS_ADDRESS_FIRST', 'PARENT_ORDER_ADDRESS', 'Address line 1', 'Address line 1'),
(886, 'ORDER_ADDRESS_ADDRESS_SECOND', 'PARENT_ORDER_ADDRESS', 'Address line 2', 'Address line 2'),
(887, 'ORDER_ADDRESS_CITY', 'PARENT_ORDER_ADDRESS', 'City', 'City'),
(888, 'ORDER_ADDRESS_STATE', 'PARENT_ORDER_ADDRESS', 'State/Province', 'State/Province'),
(889, 'ORDER_ADDRESS_ZIP_CODE', 'PARENT_ORDER_ADDRESS', 'Zip code', 'Zip code'),
(890, 'PARENT_RESERVATIONS', '', 'Reservations', 'Reservations'),
(891, 'RESERVATIONS_TITLE', 'PARENT_RESERVATIONS', 'Reservations', 'Reservations'),
(892, 'RESERVATIONS_DISPLAY_NEW_RESERVATIONS', 'PARENT_RESERVATIONS', 'Display new reservations', 'Display new reservations'),
(893, 'RESERVATIONS_NO_RESERVATIONS', 'PARENT_RESERVATIONS', 'There are no reservations.', 'There are no reservations.'),
(894, 'PARENT_RESERVATIONS_FILTERS', '', 'Reservations - Filters', 'Reservations - Filters'),
(895, 'RESERVATIONS_FILTERS_CALENDAR', 'PARENT_RESERVATIONS_FILTERS', 'Calendar', 'Calendar'),
(896, 'RESERVATIONS_FILTERS_CALENDAR_ALL', 'PARENT_RESERVATIONS_FILTERS', 'All', 'All'),
(897, 'RESERVATIONS_FILTERS_VIEW_CALENDAR', 'PARENT_RESERVATIONS_FILTERS', 'View calendar', 'View calendar'),
(898, 'RESERVATIONS_FILTERS_VIEW_LIST', 'PARENT_RESERVATIONS_FILTERS', 'View list', 'View list'),
(899, 'RESERVATIONS_FILTERS_PERIOD', 'PARENT_RESERVATIONS_FILTERS', 'Period', 'Period'),
(900, 'RESERVATIONS_FILTERS_START_DAY', 'PARENT_RESERVATIONS_FILTERS', 'Start day', 'Start day') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(901, 'RESERVATIONS_FILTERS_END_DAY', 'PARENT_RESERVATIONS_FILTERS', 'End day', 'End day'),
(902, 'RESERVATIONS_FILTERS_START_HOUR', 'PARENT_RESERVATIONS_FILTERS', 'Start hour', 'Start hour'),
(903, 'RESERVATIONS_FILTERS_END_HOUR', 'PARENT_RESERVATIONS_FILTERS', 'End hour', 'End hour'),
(904, 'RESERVATIONS_FILTERS_STATUS', 'PARENT_RESERVATIONS_FILTERS', 'Status', 'Status'),
(905, 'RESERVATIONS_FILTERS_STATUS_LABEL', 'PARENT_RESERVATIONS_FILTERS', 'Select statuses', 'Select statuses'),
(906, 'RESERVATIONS_FILTERS_STATUS_PENDING', 'PARENT_RESERVATIONS_FILTERS', 'Pending', 'Pending'),
(907, 'RESERVATIONS_FILTERS_STATUS_APPROVED', 'PARENT_RESERVATIONS_FILTERS', 'Approved', 'Approved'),
(908, 'RESERVATIONS_FILTERS_STATUS_REJECTED', 'PARENT_RESERVATIONS_FILTERS', 'Rejected', 'Rejected'),
(909, 'RESERVATIONS_FILTERS_STATUS_CANCELED', 'PARENT_RESERVATIONS_FILTERS', 'Canceled', 'Canceled'),
(910, 'RESERVATIONS_FILTERS_STATUS_EXPIRED', 'PARENT_RESERVATIONS_FILTERS', 'Expired', 'Expired'),
(911, 'RESERVATIONS_FILTERS_PAYMENT_LABEL', 'PARENT_RESERVATIONS_FILTERS', 'Select payment methods', 'Select payment methods'),
(912, 'RESERVATIONS_FILTERS_SEARCH', 'PARENT_RESERVATIONS_FILTERS', 'Search', 'Search'),
(913, 'RESERVATIONS_FILTERS_PAGE', 'PARENT_RESERVATIONS_FILTERS', 'Page', 'Page'),
(914, 'RESERVATIONS_FILTERS_PER_PAGE', 'PARENT_RESERVATIONS_FILTERS', 'Reservations per page', 'Reservations per page'),
(915, 'RESERVATIONS_FILTERS_ORDER', 'PARENT_RESERVATIONS_FILTERS', 'Order', 'Order'),
(916, 'RESERVATIONS_FILTERS_ORDER_ASCENDING', 'PARENT_RESERVATIONS_FILTERS', 'Ascending', 'Ascending'),
(917, 'RESERVATIONS_FILTERS_ORDER_DESCENDING', 'PARENT_RESERVATIONS_FILTERS', 'Descending', 'Descending'),
(918, 'RESERVATIONS_FILTERS_ORDER_BY', 'PARENT_RESERVATIONS_FILTERS', 'Order by', 'Order by'),
(919, 'PARENT_RESERVATIONS_RESERVATION', '', 'Reservations - Reservation', 'Reservations - Reservation'),
(920, 'RESERVATIONS_RESERVATION_ADD', 'PARENT_RESERVATIONS_RESERVATION', 'Add reservation', 'Add reservation'),
(921, 'RESERVATIONS_RESERVATION_ADD_SUCCESS', 'PARENT_RESERVATIONS_RESERVATION', 'Add reservation', 'Add reservation'),
(922, 'RESERVATIONS_RESERVATION_DETAILS_TITLE', 'PARENT_RESERVATIONS_RESERVATION', 'Details', 'Details'),
(923, 'RESERVATIONS_RESERVATION_ID', 'PARENT_RESERVATIONS_RESERVATION', 'Reservation ID', 'Reservation ID'),
(924, 'RESERVATIONS_RESERVATION_DATE_CREATED', 'PARENT_RESERVATIONS_RESERVATION', 'Date created', 'Date created'),
(925, 'RESERVATIONS_RESERVATION_IP_ADDRESS', 'PARENT_RESERVATIONS_RESERVATION', 'IP address', 'IP address'),
(926, 'RESERVATIONS_RESERVATION_CALENDAR_ID', 'PARENT_RESERVATIONS_RESERVATION', 'Calendar ID', 'Calendar ID'),
(927, 'RESERVATIONS_RESERVATION_CALENDAR_NAME', 'PARENT_RESERVATIONS_RESERVATION', 'Calendar name', 'Calendar name'),
(928, 'RESERVATIONS_RESERVATION_LANGUAGE', 'PARENT_RESERVATIONS_RESERVATION', 'Selected language', 'Selected language'),
(929, 'RESERVATIONS_RESERVATION_STATUS', 'PARENT_RESERVATIONS_RESERVATION', 'Status', 'Status'),
(930, 'RESERVATIONS_RESERVATION_STATUS_PENDING', 'PARENT_RESERVATIONS_RESERVATION', 'Pending', 'Pending'),
(931, 'RESERVATIONS_RESERVATION_STATUS_APPROVED', 'PARENT_RESERVATIONS_RESERVATION', 'Approved', 'Approved'),
(932, 'RESERVATIONS_RESERVATION_STATUS_REJECTED', 'PARENT_RESERVATIONS_RESERVATION', 'Rejected', 'Rejected'),
(933, 'RESERVATIONS_RESERVATION_STATUS_CANCELED', 'PARENT_RESERVATIONS_RESERVATION', 'Canceled', 'Canceled'),
(934, 'RESERVATIONS_RESERVATION_STATUS_EXPIRED', 'PARENT_RESERVATIONS_RESERVATION', 'Expired', 'Expired'),
(935, 'RESERVATIONS_RESERVATION_PAYMENT_PRICE_CHANGE', 'PARENT_RESERVATIONS_RESERVATION', 'Price change', 'Price change'),
(936, 'RESERVATIONS_RESERVATION_NO_EXTRAS', 'PARENT_RESERVATIONS_RESERVATION', 'No extras.', 'No extras.'),
(937, 'RESERVATIONS_RESERVATION_NO_DISCOUNT', 'PARENT_RESERVATIONS_RESERVATION', 'No discount.', 'No discount.'),
(938, 'RESERVATIONS_RESERVATION_NO_COUPON', 'PARENT_RESERVATIONS_RESERVATION', 'No coupon.', 'No coupon.'),
(939, 'RESERVATIONS_RESERVATION_NO_FEES', 'PARENT_RESERVATIONS_RESERVATION', 'No taxes or fees.', 'No taxes or fees.'),
(940, 'RESERVATIONS_RESERVATION_NO_ADDRESS_BILLING', 'PARENT_RESERVATIONS_RESERVATION', 'No billing address.', 'No billing address.'),
(941, 'RESERVATIONS_RESERVATION_NO_ADDRESS_SHIPPING', 'PARENT_RESERVATIONS_RESERVATION', 'No shipping address.', 'No shipping address.'),
(942, 'RESERVATIONS_RESERVATION_NO_FORM', 'PARENT_RESERVATIONS_RESERVATION', 'Form was not completed.', 'Form was not completed.'),
(943, 'RESERVATIONS_RESERVATION_NO_FORM_FIELD', 'PARENT_RESERVATIONS_RESERVATION', 'Form field was not completed.', 'Form field was not completed.'),
(944, 'RESERVATIONS_RESERVATION_ADDRESS_SHIPPING_COPY', 'PARENT_RESERVATIONS_RESERVATION', 'Same as billing address.', 'Same as billing address.'),
(945, 'RESERVATIONS_RESERVATION_INSTRUCTIONS', 'PARENT_RESERVATIONS_RESERVATION', 'Click to edit the reservation.', 'Click to edit the reservation.'),
(946, 'RESERVATIONS_RESERVATION_APPROVE', 'PARENT_RESERVATIONS_RESERVATION', 'Approve', 'Approve'),
(947, 'RESERVATIONS_RESERVATION_APPROVE_CONFIRMATION', 'PARENT_RESERVATIONS_RESERVATION', 'Are you sure you want to approve this reservation?', 'Are you sure you want to approve this reservation?'),
(948, 'RESERVATIONS_RESERVATION_APPROVE_SUCCESS', 'PARENT_RESERVATIONS_RESERVATION', 'The reservation has been approved.', 'The reservation has been approved.'),
(949, 'RESERVATIONS_RESERVATION_CANCEL', 'PARENT_RESERVATIONS_RESERVATION', 'Cancel', 'Cancel'),
(950, 'RESERVATIONS_RESERVATION_CANCEL_CONFIRMATION', 'PARENT_RESERVATIONS_RESERVATION', 'Are you sure you want to cancel this reservation?', 'Are you sure you want to cancel this reservation?'),
(951, 'RESERVATIONS_RESERVATION_CANCEL_CONFIRMATION_REFUND', 'PARENT_RESERVATIONS_RESERVATION', 'Are you sure you want to cancel this reservation? A refund will be issued automatically.', 'Are you sure you want to cancel this reservation? A refund will be issued automatically.'),
(952, 'RESERVATIONS_RESERVATION_CANCEL_SUCCESS', 'PARENT_RESERVATIONS_RESERVATION', 'The reservation has been canceled.', 'The reservation has been canceled.'),
(953, 'RESERVATIONS_RESERVATION_CANCEL_SUCCESS_REFUND', 'PARENT_RESERVATIONS_RESERVATION', 'A refund of %s has been made.', 'A refund of %s has been made.'),
(954, 'RESERVATIONS_RESERVATION_CANCEL_SUCCESS_REFUND_WARNING', 'PARENT_RESERVATIONS_RESERVATION', 'A refund or a partial refund has already been made.', 'A refund or a partial refund has already been made.'),
(955, 'RESERVATIONS_RESERVATION_DELETE', 'PARENT_RESERVATIONS_RESERVATION', 'Delete', 'Delete'),
(956, 'RESERVATIONS_RESERVATION_DELETE_CONFIRMATION', 'PARENT_RESERVATIONS_RESERVATION', 'Are you sure you want to delete this reservation?', 'Are you sure you want to delete this reservation?'),
(957, 'RESERVATIONS_RESERVATION_DELETE_SUCCESS', 'PARENT_RESERVATIONS_RESERVATION', 'The reservation has been deleted.', 'The reservation has been deleted.'),
(958, 'RESERVATIONS_RESERVATION_REJECT', 'PARENT_RESERVATIONS_RESERVATION', 'Reject', 'Reject'),
(959, 'RESERVATIONS_RESERVATION_REJECT_CONFIRMATION', 'PARENT_RESERVATIONS_RESERVATION', 'Are you sure you want to reject this reservation?', 'Are you sure you want to reject this reservation?'),
(960, 'RESERVATIONS_RESERVATION_REJECT_SUCCESS', 'PARENT_RESERVATIONS_RESERVATION', 'The reservation has been rejected.', 'The reservation has been rejected.'),
(961, 'RESERVATIONS_RESERVATION_CLOSE', 'PARENT_RESERVATIONS_RESERVATION', 'Close', 'Close'),
(962, 'PARENT_RESERVATIONS_HELP', '', 'Reservations - Help', 'Reservations - Help'),
(963, 'RESERVATIONS_FILTERS_CALENDAR_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the calendar for which you want to see reservations, or display all reservations.', 'Select the calendar for which you want to see reservations, or display all reservations.'),
(964, 'RESERVATIONS_FILTERS_VIEW_CALENDAR_HELP', 'PARENT_RESERVATIONS_HELP', 'Selecting "Calendar view" will display the reservations in a calendar. Only possible when you select one calendar.', 'Selecting "Calendar view" will display the reservations in a calendar. Only possible when you select one calendar.'),
(965, 'RESERVATIONS_FILTERS_VIEW_LIST_HELP', 'PARENT_RESERVATIONS_HELP', 'Selecting "List view" will display the reservations in a list.', 'Selecting "List view" will display the reservations in a list.'),
(966, 'RESERVATIONS_FILTERS_START_DAY_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the day from where displayed reservations start.', 'Select the day from where displayed reservations start.'),
(967, 'RESERVATIONS_FILTERS_END_DAY_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the day where displayed reservations end.', 'Select the day where displayed reservations end.'),
(968, 'RESERVATIONS_FILTERS_START_HOUR_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the hour from where displayed reservations start.', 'Select the hour from where displayed reservations start.'),
(969, 'RESERVATIONS_FILTERS_END_HOUR_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the hour where displayed reservations end.', 'Select the hour where displayed reservations end.'),
(970, 'RESERVATIONS_FILTERS_STATUS_HELP', 'PARENT_RESERVATIONS_HELP', 'Display reservations with selected status.', 'Display reservations with selected status.'),
(971, 'RESERVATIONS_FILTERS_PAYMENT_HELP', 'PARENT_RESERVATIONS_HELP', 'Display reservations with selected payment methods.', 'Display reservations with selected payment methods.'),
(972, 'RESERVATIONS_FILTERS_SEARCH_HELP', 'PARENT_RESERVATIONS_HELP', 'Enter the search value.', 'Enter the search value.'),
(973, 'RESERVATIONS_FILTERS_PAGE_HELP', 'PARENT_RESERVATIONS_HELP', 'Select page.', 'Select page.'),
(974, 'RESERVATIONS_FILTERS_PER_PAGE_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the number of reservations which will be displayed on page.', 'Select the number of reservations which will be displayed on page.'),
(975, 'RESERVATIONS_FILTERS_ORDER_HELP', 'PARENT_RESERVATIONS_HELP', 'Order the reservations ascending or descending.', 'Order the reservations ascending or descending.'),
(976, 'RESERVATIONS_FILTERS_ORDER_BY_HELP', 'PARENT_RESERVATIONS_HELP', 'Select the field after which the reservations will be sorted.', 'Select the field after which the reservations will be sorted.'),
(977, 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', '', 'Reservations - Reservation front end', 'Reservations - Reservation front end'),
(978, 'RESERVATIONS_RESERVATION_FRONT_END_TITLE', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Reservation', 'Reservation'),
(979, 'RESERVATIONS_RESERVATION_FRONT_END_SELECT_DAYS', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Please select the days from calendar.', 'Please select the days from calendar.'),
(980, 'RESERVATIONS_RESERVATION_FRONT_END_SELECT_HOURS', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Please select the days and hours from calendar.', 'Please select the days and hours from calendar.'),
(981, 'RESERVATIONS_RESERVATION_FRONT_END_PRICE', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Price', 'Price'),
(982, 'RESERVATIONS_RESERVATION_FRONT_END_TOTAL_PRICE', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Total', 'Total'),
(983, 'RESERVATIONS_RESERVATION_FRONT_END_DEPOSIT', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Deposit', 'Deposit'),
(984, 'RESERVATIONS_RESERVATION_FRONT_END_DEPOSIT_LEFT', 'PARENT_RESERVATIONS_RESERVATION_FRONT_END', 'Left to pay', 'Left to pay'),
(985, 'PARENT_REVIEWS', '', 'Reviews', 'Reviews'),
(986, 'REVIEWS_TITLE', 'PARENT_REVIEWS', 'Reviews', 'Reviews'),
(987, 'PARENT_RULES', '', 'Rules', 'Rules'),
(988, 'RULES_TITLE', 'PARENT_RULES', 'Rules', 'Rules'),
(989, 'RULES_DEFAULT_NAME', 'PARENT_RULES', 'New rule', 'New rule'),
(990, 'RULES_CREATED_BY', 'PARENT_RULES', 'Created by', 'Created by'),
(991, 'RULES_LOAD_SUCCESS', 'PARENT_RULES', 'Rules list loaded.', 'Rules list loaded.'),
(992, 'RULES_NO_RULES', 'PARENT_RULES', 'No rules. Click the above "plus" icon to add a new one.', 'No rules. Click the above "plus" icon to add a new one.'),
(993, 'PARENT_RULES_RULE', '', 'Rules - Rule', 'Rules - Rule'),
(994, 'RULES_RULE_NAME', 'PARENT_RULES_RULE', 'Name', 'Name'),
(995, 'RULES_RULE_TIME_LAPSE_MIN', 'PARENT_RULES_RULE', 'Minimum time lapse', 'Minimum time lapse'),
(996, 'RULES_RULE_TIME_LAPSE_MAX', 'PARENT_RULES_RULE', 'Maximum time lapse', 'Maximum time lapse'),
(997, 'RULES_RULE_LOADED', 'PARENT_RULES_RULE', 'Rule loaded.', 'Rule loaded.'),
(998, 'PARENT_RULES_ADD_RULE', '', 'Rules - Add rule', 'Rules - Add rule'),
(999, 'RULES_ADD_RULE_NAME', 'PARENT_RULES_ADD_RULE', 'New rule', 'New rule'),
(1000, 'RULES_ADD_RULE_SUBMIT', 'PARENT_RULES_ADD_RULE', 'Add rule', 'Add rule') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1001, 'RULES_ADD_RULE_ADDING', 'PARENT_RULES_ADD_RULE', 'Adding a new rule ...', 'Adding a new rule ...'),
(1002, 'RULES_ADD_RULE_SUCCESS', 'PARENT_RULES_ADD_RULE', 'You have succesfully added a new rule.', 'You have succesfully added a new rule.'),
(1003, 'PARENT_RULES_DELETE_RULE', '', 'Rules - Delete rule', 'Rules - Delete rule'),
(1004, 'RULES_DELETE_RULE_CONFIRMATION', 'PARENT_RULES_DELETE_RULE', 'Are you sure you want to delete this rule?', 'Are you sure you want to delete this rule?'),
(1005, 'RULES_DELETE_RULE_SUBMIT', 'PARENT_RULES_DELETE_RULE', 'Delete rule', 'Delete rule'),
(1006, 'RULES_DELETE_RULE_DELETING', 'PARENT_RULES_DELETE_RULE', 'Deleting rule ...', 'Deleting rule ...'),
(1007, 'RULES_DELETE_RULE_SUCCESS', 'PARENT_RULES_DELETE_RULE', 'You have succesfully deleted the rule.', 'You have succesfully deleted the rule.'),
(1008, 'PARENT_RULES_HELP', '', 'Rules - Help', 'Rules - Help'),
(1009, 'RULES_HELP', 'PARENT_RULES_HELP', 'Click on a rule item to open the editing area.', 'Click on a rule item to open the editing area.'),
(1010, 'RULES_ADD_RULE_HELP', 'PARENT_RULES_HELP', 'Click on the "plus" icon to add a rule.', 'Click on the "plus" icon to add a rule.'),
(1011, 'RULES_RULE_TIME_LAPSE_MIN_HELP', 'PARENT_RULES_RULE', 'Enter minimum booking time lapse. Default value is 1.', 'Enter minimum booking time lapse. Default value is 1.'),
(1012, 'RULES_RULE_TIME_LAPSE_MAX_HELP', 'PARENT_RULES_RULE', 'Enter maximum booking time lapse. Add 0 for unlimited period.', 'Enter maximum booking time lapse. Add 0 for unlimited period.'),
(1013, 'PARENT_RULES_FRONT_END', '', 'Rules - Front end', 'Rules - Front end'),
(1014, 'RULES_FRONT_END_MIN_TIME_LAPSE_DAYS_WARNING', 'PARENT_RULES_FRONT_END', 'You need to book a minimum number of %d days.', 'You need to book a minimum number of %d days.'),
(1015, 'RULES_FRONT_END_MAX_TIME_LAPSE_DAYS_WARNING', 'PARENT_RULES_FRONT_END', 'You can book only a maximum number of %d days.', 'You can book only a maximum number of %d days.'),
(1016, 'RULES_FRONT_END_MIN_TIME_LAPSE_HOURS_WARNING', 'PARENT_RULES_FRONT_END', 'You need to book a minimum number of %d hours.', 'You need to book a minimum number of %d hours.'),
(1017, 'RULES_FRONT_END_MAX_TIME_LAPSE_HOURS_WARNING', 'PARENT_RULES_FRONT_END', 'You can book only a maximum number of %d hours.', 'You can book only a maximum number of %d hours.'),
(1018, 'PARENT_SEARCHES', '', 'Search', 'Search'),
(1019, 'SEARCHES_TITLE', 'PARENT_SEARCHES', 'Search', 'Search'),
(1020, 'SEARCHES_CREATED_BY', 'PARENT_SEARCHES', 'Created by', 'Created by'),
(1021, 'SEARCHES_LOAD_SUCCESS', 'PARENT_SEARCHES', 'Search list loaded.', 'Search list loaded.'),
(1022, 'SEARCHES_NO_SEARCHES', 'PARENT_SEARCHES', 'No searches. Click the above "plus" icon to add a new one.', 'No searches. Click the above "plus" icon to add a new one.'),
(1023, 'PARENT_SEARCHES_SEARCH', '', 'Search', 'Search'),
(1024, 'SEARCHES_SEARCH_NAME', 'PARENT_SEARCHES_SEARCH', 'Name', 'Name'),
(1025, 'SEARCHES_SEARCH_LOADED', 'PARENT_SEARCHES_SEARCH', 'Search loaded.', 'Search loaded.'),
(1026, 'PARENT_SEARCHES_ADD_SEARCH', '', 'Search - Add search', 'Search - Add search'),
(1027, 'SEARCHES_ADD_SEARCH_NAME', 'PARENT_SEARCHES_ADD_SEARCH', 'New search', 'New search'),
(1028, 'SEARCHES_ADD_SEARCH_SUBMIT', 'PARENT_SEARCHES_ADD_SEARCH', 'Add search', 'Add search'),
(1029, 'SEARCHES_ADD_SEARCH_ADDING', 'PARENT_SEARCHES_ADD_SEARCH', 'Adding a new search ...', 'Adding a new search ...'),
(1030, 'SEARCHES_ADD_SEARCH_SUCCESS', 'PARENT_SEARCHES_ADD_SEARCH', 'You have succesfully added a new search.', 'You have succesfully added a new search.'),
(1031, 'PARENT_SEARCHES_EDIT_SEARCH', '', 'Search - Edit search', 'Search - Edit search'),
(1032, 'SEARCHES_EDIT_SEARCH', 'PARENT_SEARCHES_EDIT_SEARCH', 'Edit search details', 'Edit search details'),
(1033, 'SEARCHES_EDIT_SEARCH_SETTINGS', 'PARENT_SEARCHES_EDIT_SEARCH', 'Edit search settings', 'Edit search settings'),
(1034, 'SEARCHES_EDIT_SEARCH_EXCLUDED_CALENDARS_DAYS', 'PARENT_SEARCHES_EDIT_SEARCH', 'Exclude calendars from search [hours filters disabled]', 'Exclude calendars from search [hours filters disabled]'),
(1035, 'SEARCHES_EDIT_SEARCH_EXCLUDED_CALENDARS_HOURS', 'PARENT_SEARCHES_EDIT_SEARCH', 'Exclude calendars from search [hours filters enabled]', 'Exclude calendars from search [hours filters enabled]'),
(1036, 'SEARCHES_EDIT_SEARCH_NO_CALENDARS', 'PARENT_SEARCHES_EDIT_SEARCH', 'There are no calendars created. Go to <a href="%s">calendars</a> page to create one.', 'There are no calendars created. Go to <a href="%s">calendars</a> page to create one.'),
(1037, 'PARENT_SEARCHES_DELETE_SEARCH', '', 'Search - Delete search', 'Search - Delete search'),
(1038, 'SEARCHES_DELETE_SEARCH_CONFIRMATION', 'PARENT_SEARCHES_DELETE_SEARCH', 'Are you sure you want to delete this search?', 'Are you sure you want to delete this search?'),
(1039, 'SEARCHES_DELETE_SEARCH_SUBMIT', 'PARENT_SEARCHES_DELETE_SEARCH', 'Delete search', 'Delete search'),
(1040, 'SEARCHES_DELETE_SEARCH_DELETING', 'PARENT_SEARCHES_DELETE_SEARCH', 'Deleting search ...', 'Deleting search ...'),
(1041, 'SEARCHES_DELETE_SEARCH_SUCCESS', 'PARENT_SEARCHES_DELETE_SEARCH', 'You have succesfully deleted the search.', 'You have succesfully deleted the search.'),
(1042, 'PARENT_SEARCHES_HELP', '', 'Search - Help', 'Search - Help'),
(1043, 'SEARCHES_HELP', 'PARENT_SEARCHES_HELP', 'Click on a search item to open the editing area.', 'Click on a search item to open the editing area.'),
(1044, 'SEARCHES_ADD_SEARCH_HELP', 'PARENT_SEARCHES_HELP', 'Click on the "plus" icon to add a search.', 'Click on the "plus" icon to add a search.'),
(1045, 'SEARCHES_EDIT_SEARCH_HELP', 'PARENT_SEARCHES_HELP', 'Click on the "search" icon to edit search details.', 'Click on the "search" icon to edit search details.'),
(1046, 'SEARCHES_EDIT_SEARCH_SETTINGS_HELP', 'PARENT_SEARCHES_HELP', 'Click on the "gear" icon to edit search settings.', 'Click on the "gear" icon to edit search settings.'),
(1047, 'SEARCHES_EDIT_SEARCH_DELETE_HELP', 'PARENT_SEARCHES_HELP', 'Click the "trash" icon to delete the search.', 'Click the "trash" icon to delete the search.'),
(1048, 'SEARCHES_EDIT_SEARCH_EXCLUDED_CALENDARS_HELP', 'PARENT_SEARCHES_HELP', 'If hours filters are enabled only calendars that have availability set for hours are included in search, else only calendar that have availability set for days are included.', 'If hours filters are enabled only calendars that have availability set for hours are included in search, else only calendar that have availability set for days are included.'),
(1049, 'SEARCHES_SEARCH_NAME_HELP', 'PARENT_SEARCHES_HELP', 'Change search name.', 'Change search name.'),
(1050, 'PARENT_SEARCH_FRONT_END', '', 'Search - Front end', 'Search - Front end'),
(1051, 'SEARCH_FRONT_END_TITLE', 'PARENT_SEARCH_FRONT_END', 'Search', 'Search'),
(1052, 'SEARCH_FRONT_END_CHECK_IN', 'PARENT_SEARCH_FRONT_END', 'Check in', 'Check in'),
(1053, 'SEARCH_FRONT_END_CHECK_OUT', 'PARENT_SEARCH_FRONT_END', 'Check out', 'Check out'),
(1054, 'SEARCH_FRONT_END_START_HOUR', 'PARENT_SEARCH_FRONT_END', 'Start at', 'Start at'),
(1055, 'SEARCH_FRONT_END_END_HOUR', 'PARENT_SEARCH_FRONT_END', 'Finish at', 'Finish at'),
(1056, 'SEARCH_FRONT_END_NO_ITEMS', 'PARENT_SEARCH_FRONT_END', 'No book items', 'No book items'),
(1057, 'SEARCH_FRONT_END_NO_AVAILABILITY', 'PARENT_SEARCH_FRONT_END', 'Nothing available.', 'Nothing available.'),
(1058, 'SEARCH_FRONT_END_NO_SERVICES_AVAILABLE', 'PARENT_SEARCH_FRONT_END', 'There are no services available for the period you selected.', 'There are no services available for the period you selected.'),
(1059, 'SEARCH_FRONT_END_NO_SERVICES_AVAILABLE_SPLIT_GROUP', 'PARENT_SEARCH_FRONT_END', 'You cannot add divided groups to a reservation.', 'You cannot add divided groups to a reservation.'),
(1060, 'SEARCH_FRONT_END_SORT_TITLE', 'PARENT_SEARCH_FRONT_END', 'Sort by', 'Sort by'),
(1061, 'SEARCH_FRONT_END_SORT_NAME', 'PARENT_SEARCH_FRONT_END', 'Name', 'Name'),
(1062, 'SEARCH_FRONT_END_SORT_PRICE', 'PARENT_SEARCH_FRONT_END', 'Price', 'Price'),
(1063, 'SEARCH_FRONT_END_SORT_ASC', 'PARENT_SEARCH_FRONT_END', 'Ascending', 'Ascending'),
(1064, 'SEARCH_FRONT_END_SORT_DESC', 'PARENT_SEARCH_FRONT_END', 'Descending', 'Descending'),
(1065, 'SEARCH_FRONT_END_VIEW_GRID', 'PARENT_SEARCH_FRONT_END', 'Grid view', 'Grid view'),
(1066, 'SEARCH_FRONT_END_VIEW_LIST', 'PARENT_SEARCH_FRONT_END', 'List view', 'List view'),
(1067, 'SEARCH_FRONT_END_VIEW_MAP', 'PARENT_SEARCH_FRONT_END', 'Map view', 'Map view'),
(1068, 'SEARCH_FRONT_END_RESULTS_PRICE', 'PARENT_SEARCH_FRONT_END', 'Start at %s', 'Start at %s'),
(1069, 'SEARCH_FRONT_END_RESULTS_VIEW', 'PARENT_SEARCH_FRONT_END', 'View', 'View'),
(1070, 'PARENT_SETTINGS', '', 'Settings', 'Settings'),
(1071, 'SETTINGS_TITLE', 'PARENT_SETTINGS', 'Settings', 'Settings'),
(1072, 'SETTINGS_ENABLED', 'PARENT_SETTINGS', 'Enabled', 'Enabled'),
(1073, 'SETTINGS_DISABLED', 'PARENT_SETTINGS', 'Disabled', 'Disabled'),
(1074, 'SETTINGS_GENERAL_TITLE', 'PARENT_SETTINGS', 'General settings', 'General settings'),
(1075, 'PARENT_SETTINGS_HELP', '', 'Settings - Help', 'Settings - Help'),
(1076, 'SETTINGS_HELP', 'PARENT_SETTINGS_HELP', 'Edit booking system settings.', 'Edit booking system settings.'),
(1077, 'PARENT_SETTINGS_CALENDAR', '', 'Settings - Calendar', 'Settings - Calendar'),
(1078, 'SETTINGS_CALENDAR_TITLE', 'PARENT_SETTINGS_CALENDAR', 'Calendar settings', 'Calendar settings'),
(1079, 'SETTINGS_CALENDAR_NAME', 'PARENT_SETTINGS_CALENDAR', 'Name', 'Name'),
(1080, 'SETTINGS_CALENDAR_GENERAL_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'General settings', 'General settings'),
(1081, 'SETTINGS_CALENDAR_GENERAL_DATE_TYPE', 'PARENT_SETTINGS_CALENDAR', 'Date type', 'Date type'),
(1082, 'SETTINGS_CALENDAR_GENERAL_DATE_TYPE_AMERICAN', 'PARENT_SETTINGS_CALENDAR', 'American (mm dd, yyyy)', 'American (mm dd, yyyy)'),
(1083, 'SETTINGS_CALENDAR_GENERAL_DATE_TYPE_EUROPEAN', 'PARENT_SETTINGS_CALENDAR', 'European (dd mm yyyy)', 'European (dd mm yyyy)'),
(1084, 'SETTINGS_CALENDAR_GENERAL_TEMPLATE', 'PARENT_SETTINGS_CALENDAR', 'Style template', 'Style template'),
(1085, 'SETTINGS_CALENDAR_GENERAL_BOOKING_STOP', 'PARENT_SETTINGS_CALENDAR', 'Stop booking x minutes in advance', 'Stop booking x minutes in advance'),
(1086, 'SETTINGS_CALENDAR_GENERAL_MONTHS_NO', 'PARENT_SETTINGS_CALENDAR', 'Number of months displayed', 'Number of months displayed'),
(1087, 'SETTINGS_CALENDAR_GENERAL_VIEW_ONLY', 'PARENT_SETTINGS_CALENDAR', 'View only info', 'View only info'),
(1088, 'SETTINGS_CALENDAR_GENERAL_SERVER_TIME', 'PARENT_SETTINGS_CALENDAR', 'Enable server time', 'Enable server time'),
(1089, 'SETTINGS_CALENDAR_GENERAL_POST_ID', 'PARENT_SETTINGS_CALENDAR', 'Post ID', 'Post ID'),
(1090, 'SETTINGS_CALENDAR_CURRENCY_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Currency settings', 'Currency settings'),
(1091, 'SETTINGS_CALENDAR_CURRENCY', 'PARENT_SETTINGS_CALENDAR', 'Currency', 'Currency'),
(1092, 'SETTINGS_CALENDAR_CURRENCY_POSITION', 'PARENT_SETTINGS_CALENDAR', 'Currency position', 'Currency position'),
(1093, 'SETTINGS_CALENDAR_CURRENCY_POSITION_BEFORE', 'PARENT_SETTINGS_CALENDAR', 'Before', 'Before'),
(1094, 'SETTINGS_CALENDAR_CURRENCY_POSITION_BEFORE_WITH_SPACE', 'PARENT_SETTINGS_CALENDAR', 'Before with space', 'Before with space'),
(1095, 'SETTINGS_CALENDAR_CURRENCY_POSITION_AFTER', 'PARENT_SETTINGS_CALENDAR', 'After', 'After'),
(1096, 'SETTINGS_CALENDAR_CURRENCY_POSITION_AFTER_WITH_SPACE', 'PARENT_SETTINGS_CALENDAR', 'After with space', 'After with space'),
(1097, 'SETTINGS_CALENDAR_DAYS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Days settings', 'Days settings'),
(1098, 'SETTINGS_CALENDAR_DAYS_AVAILABLE', 'PARENT_SETTINGS_CALENDAR', 'Available days', 'Available days'),
(1099, 'SETTINGS_CALENDAR_DAYS_FIRST', 'PARENT_SETTINGS_CALENDAR', 'First weekday', 'First weekday'),
(1100, 'SETTINGS_CALENDAR_DAYS_FIRST_DISPLAYED', 'PARENT_SETTINGS_CALENDAR', 'First day displayed', 'First day displayed') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1101, 'SETTINGS_CALENDAR_DAYS_MULTIPLE_SELECT', 'PARENT_SETTINGS_CALENDAR', 'Use Check in/Check out', 'Use Check in/Check out'),
(1102, 'SETTINGS_CALENDAR_DAYS_MORNING_CHECK_OUT', 'PARENT_SETTINGS_CALENDAR', 'Morning check out', 'Morning check out'),
(1103, 'SETTINGS_CALENDAR_DAYS_DETAILS_FROM_HOURS', 'PARENT_SETTINGS_CALENDAR', 'Use hours details to set day details', 'Use hours details to set day details'),
(1104, 'SETTINGS_CALENDAR_HOURS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Hours settings', 'Hours settings'),
(1105, 'SETTINGS_CALENDAR_HOURS_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Use hours', 'Use hours'),
(1106, 'SETTINGS_CALENDAR_HOURS_INFO_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Enable hours info', 'Enable hours info'),
(1107, 'SETTINGS_CALENDAR_HOURS_DEFINITIONS', 'PARENT_SETTINGS_CALENDAR', 'Define hours', 'Define hours'),
(1108, 'SETTINGS_CALENDAR_HOURS_MULTIPLE_SELECT', 'PARENT_SETTINGS_CALENDAR', 'Use start/finish hours', 'Use start/finish hours'),
(1109, 'SETTINGS_CALENDAR_HOURS_AMPM', 'PARENT_SETTINGS_CALENDAR', 'Enable AM/PM format', 'Enable AM/PM format'),
(1110, 'SETTINGS_CALENDAR_HOURS_ADD_LAST_HOUR_TO_TOTAL_PRICE', 'PARENT_SETTINGS_CALENDAR', 'Add last selected hour price to total price', 'Add last selected hour price to total price'),
(1111, 'SETTINGS_CALENDAR_HOURS_INTERVAL_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Enable hours interval', 'Enable hours interval'),
(1112, 'SETTINGS_CALENDAR_SIDEBAR_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Sidebar settings', 'Sidebar settings'),
(1113, 'SETTINGS_CALENDAR_SIDEBAR_STYLE', 'PARENT_SETTINGS_CALENDAR', 'Sidebar style', 'Sidebar style'),
(1114, 'SETTINGS_CALENDAR_SIDEBAR_NO_ITEMS_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Enable number of items select', 'Enable number of items select'),
(1115, 'SETTINGS_CALENDAR_RULES_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Rules settings', 'Rules settings'),
(1116, 'SETTINGS_CALENDAR_RULES', 'PARENT_SETTINGS_CALENDAR', 'Select rule', 'Select rule'),
(1117, 'SETTINGS_CALENDAR_RULES_NONE', 'PARENT_SETTINGS_CALENDAR', 'None', 'None'),
(1118, 'SETTINGS_CALENDAR_EXTRAS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Extras settings', 'Extras settings'),
(1119, 'SETTINGS_CALENDAR_EXTRAS', 'PARENT_SETTINGS_CALENDAR', 'Select extra', 'Select extra'),
(1120, 'SETTINGS_CALENDAR_EXTRAS_NONE', 'PARENT_SETTINGS_CALENDAR', 'None', 'None'),
(1121, 'SETTINGS_CALENDAR_CART_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Cart settings', 'Cart settings'),
(1122, 'SETTINGS_CALENDAR_CART_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Enable cart', 'Enable cart'),
(1123, 'SETTINGS_CALENDAR_DISCOUNTS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Discounts settings', 'Discounts settings'),
(1124, 'SETTINGS_CALENDAR_DISCOUNTS', 'PARENT_SETTINGS_CALENDAR', 'Select discount', 'Select discount'),
(1125, 'SETTINGS_CALENDAR_DISCOUNTS_NONE', 'PARENT_SETTINGS_CALENDAR', 'None', 'None'),
(1126, 'SETTINGS_CALENDAR_FEES_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Taxes & fees settings', 'Taxes & fees settings'),
(1127, 'SETTINGS_CALENDAR_FEES', 'PARENT_SETTINGS_CALENDAR', 'Select taxes and/or fees', 'Select taxes and/or fees'),
(1128, 'SETTINGS_CALENDAR_COUPONS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Coupons settings', 'Coupons settings'),
(1129, 'SETTINGS_CALENDAR_COUPONS', 'PARENT_SETTINGS_CALENDAR', 'Select coupons', 'Select coupons'),
(1130, 'SETTINGS_CALENDAR_COUPONS_NONE', 'PARENT_SETTINGS_CALENDAR', 'None', 'None'),
(1131, 'SETTINGS_CALENDAR_DEPOSIT_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Deposit settings', 'Deposit settings'),
(1132, 'SETTINGS_CALENDAR_DEPOSIT', 'PARENT_SETTINGS_CALENDAR', 'Deposit value', 'Deposit value'),
(1133, 'SETTINGS_CALENDAR_DEPOSIT_TYPE', 'PARENT_SETTINGS_CALENDAR', 'Deposit type', 'Deposit type'),
(1134, 'SETTINGS_CALENDAR_DEPOSIT_TYPE_FIXED', 'PARENT_SETTINGS_CALENDAR', 'Fixed', 'Fixed'),
(1135, 'SETTINGS_CALENDAR_DEPOSIT_TYPE_PERCENT', 'PARENT_SETTINGS_CALENDAR', 'Percent', 'Percent'),
(1136, 'SETTINGS_CALENDAR_FORMS_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Forms settings', 'Forms settings'),
(1137, 'SETTINGS_CALENDAR_FORMS', 'PARENT_SETTINGS_CALENDAR', 'Select form', 'Select form'),
(1138, 'SETTINGS_CALENDAR_ORDER_SETTINGS', 'PARENT_SETTINGS_CALENDAR', 'Order settings', 'Order settings'),
(1139, 'SETTINGS_CALENDAR_ORDER_TERMS_AND_CONDITIONS_ENABLED', 'PARENT_SETTINGS_CALENDAR', 'Enable Terms & Conditions', 'Enable Terms & Conditions'),
(1140, 'SETTINGS_CALENDAR_ORDER_TERMS_AND_CONDITIONS_LINK', 'PARENT_SETTINGS_CALENDAR', 'Terms & Conditions link', 'Terms & Conditions link'),
(1141, 'PARENT_SETTINGS_CALENDAR_HELP', '', 'Settings - Calendar - Help', 'Settings - Calendar - Help'),
(1142, 'SETTINGS_CALENDAR_NAME_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Change calendar name.', 'Change calendar name.'),
(1143, 'SETTINGS_CALENDAR_GENERAL_DATE_TYPE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: American. Select date format: American (mm dd, yyyy) or European (dd mm yyyy).', 'Default value: American. Select date format: American (mm dd, yyyy) or European (dd mm yyyy).'),
(1144, 'SETTINGS_CALENDAR_GENERAL_TEMPLATE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: default. Select styles template.', 'Default value: default. Select styles template.'),
(1145, 'SETTINGS_CALENDAR_GENERAL_BOOKING_STOP_HELP', 'PARENT_SETTINGS_CALENDAR', 'Default value: 0. Set the number of minutes before the booking is stopped in advance. For 1 hour you have 60 minutes, for 1 day you have 1440 minutes.', 'Default value: 0. Set the number of minutes before the booking is stopped in advance. For 1 hour you have 60 minutes, for 1 day you have 1440 minutes.'),
(1146, 'SETTINGS_CALENDAR_GENERAL_MONTHS_NO_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: 1. Set the number of months initialy displayed. Maximum number allowed is 6.', 'Default value: 1. Set the number of months initialy displayed. Maximum number allowed is 6.'),
(1147, 'SETTINGS_CALENDAR_GENERAL_VIEW_ONLY_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Set to display only booking information in front end.', 'Default value: Enabled. Set to display only booking information in front end.'),
(1148, 'SETTINGS_CALENDAR_GENERAL_SERVER_TIME_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Use server time.', 'Default value: Disabled. Use server time.'),
(1149, 'SETTINGS_CALENDAR_GENERAL_POST_ID_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Set post ID were the calendar will be added. It is mandatory if you create a searching system through some calendars.', 'Set post ID were the calendar will be added. It is mandatory if you create a searching system through some calendars.'),
(1150, 'SETTINGS_CALENDAR_CURRENCY_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: United States Dollar ($, USD). Select calendar currency.', 'Default value: United States Dollar ($, USD). Select calendar currency.'),
(1151, 'SETTINGS_CALENDAR_CURRENCY_POSITION_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Before. Select currency position.', 'Default value: Before. Select currency position.'),
(1152, 'SETTINGS_CALENDAR_DAYS_MULTIPLE_SELECT_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Use Check in/Check out or select only one day.', 'Default value: Enabled. Use Check in/Check out or select only one day.'),
(1153, 'SETTINGS_CALENDAR_DAYS_AVAILABLE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: all available. Select available weekdays.', 'Default value: all available. Select available weekdays.'),
(1154, 'SETTINGS_CALENDAR_DAYS_FIRST_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Monday. Select calendar first weekday.', 'Default value: Monday. Select calendar first weekday.'),
(1155, 'SETTINGS_CALENDAR_DAYS_FIRST_DISPLAYED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Format: YYYY-MM-DD. Default value: today. Select the day to be first displayed when the calendar calendar is loaded.', 'Format: YYYY-MM-DD. Default value: today. Select the day to be first displayed when the calendar calendar is loaded.'),
(1156, 'SETTINGS_CALENDAR_DAYS_MORNING_CHECK_OUT_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. This option enables "Check in" in the afternoon of first day and "Check out" in the morning of the day after last day.', 'Default value: Disabled. This option enables "Check in" in the afternoon of first day and "Check out" in the morning of the day after last day.'),
(1157, 'SETTINGS_CALENDAR_DAYS_DETAILS_FROM_HOURS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Check this option, when hours are enabled, if you want for days details to be updated (calculated) from hours details or disable it if you want to have complete control of day derails.', 'Default value: Enabled. Check this option, when hours are enabled, if you want for days details to be updated (calculated) from hours details or disable it if you want to have complete control of day derails.'),
(1158, 'SETTINGS_CALENDAR_HOURS_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Enable hours for the calendar.', 'Default value: Disabled. Enable hours for the calendar.'),
(1159, 'SETTINGS_CALENDAR_HOURS_INFO_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Display hours info when you hover a day in calendar.', 'Default value: Enabled. Display hours info when you hover a day in calendar.'),
(1160, 'SETTINGS_CALENDAR_HOURS_DEFINITIONS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Enter hh:mm ... add one per line. Changing the definitions will overwrite any previous hours data. Use only 24 hours format.', 'Enter hh:mm ... add one per line. Changing the definitions will overwrite any previous hours data. Use only 24 hours format.'),
(1161, 'SETTINGS_CALENDAR_HOURS_MULTIPLE_SELECT_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Use Start/Finish Hours or select only one hour.', 'Default value: Enabled. Use Start/Finish Hours or select only one hour.'),
(1162, 'SETTINGS_CALENDAR_HOURS_AMPM_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Display hours in AM/PM format. NOTE: Hours definitions still need to be in 24 hours format.', 'Default value: Disabled. Display hours in AM/PM format. NOTE: Hours definitions still need to be in 24 hours format.'),
(1163, 'SETTINGS_CALENDAR_HOURS_ADD_LAST_HOUR_TO_TOTAL_PRICE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. It calculates the total price before the last hours selected if Disabled. It calculates the total price including the last hour selected if Enabled. <br /><br /><strong>Warning: </strong> In administration area the last hours from your definitions list will not be displayed.', 'Default value: Enabled. It calculates the total price before the last hours selected if Disabled. It calculates the total price including the last hour selected if Enabled. <br /><br /><strong>Warning: </strong> In administration area the last hours from your definitions list will not be displayed.'),
(1164, 'SETTINGS_CALENDAR_HOURS_INTERVAL_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Show hours interval from the current hour to the next one.', 'Default value: Disabled. Show hours interval from the current hour to the next one.'),
(1165, 'SETTINGS_CALENDAR_SIDEBAR_STYLE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Set sidebar position and number of columns.', 'Set sidebar position and number of columns.'),
(1166, 'SETTINGS_CALENDAR_SIDEBAR_NO_ITEMS_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Enabled. Set to display number of items you want to book in front end.', 'Default value: Enabled. Set to display number of items you want to book in front end.'),
(1167, 'SETTINGS_CALENDAR_RULES_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar rules.', 'Select calendar rules.'),
(1168, 'SETTINGS_CALENDAR_EXTRAS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar extras.', 'Select calendar extras.'),
(1169, 'SETTINGS_CALENDAR_CART_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Use a shopping cart in calendar.', 'Default value: Disabled. Use a shopping cart in calendar.'),
(1170, 'SETTINGS_CALENDAR_DISCOUNTS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar discount.', 'Select calendar discount.'),
(1171, 'SETTINGS_CALENDAR_FEES_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar taxes and/or fees.', 'Select calendar taxes and/or fees.'),
(1172, 'SETTINGS_CALENDAR_COUPONS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar coupons.', 'Select calendar coupons.'),
(1173, 'SETTINGS_CALENDAR_DEPOSIT_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: 0. Set calendar deposit value.', 'Default value: 0. Set calendar deposit value.'),
(1174, 'SETTINGS_CALENDAR_DEPOSIT_TYPE_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Percent. Set deposit value type.', 'Default value: Percent. Set deposit value type.'),
(1175, 'SETTINGS_CALENDAR_FORMS_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Select calendar form.', 'Select calendar form.'),
(1176, 'SETTINGS_CALENDAR_ORDER_TERMS_AND_CONDITIONS_ENABLED_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Default value: Disabled. Enable Terms & Conditions check box.', 'Default value: Disabled. Enable Terms & Conditions check box.'),
(1177, 'SETTINGS_CALENDAR_ORDER_TERMS_AND_CONDITIONS_LINK_HELP', 'PARENT_SETTINGS_CALENDAR_HELP', 'Enter the link to Terms & Conditions page.', 'Enter the link to Terms & Conditions page.'),
(1178, 'PARENT_SETTINGS_NOTIFICATIONS', '', 'Settings - Notifications', 'Settings - Notifications'),
(1179, 'SETTINGS_NOTIFICATIONS_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications', 'Notifications'),
(1180, 'SETTINGS_NOTIFICATIONS_TEMPLATES', 'PARENT_SETTINGS_NOTIFICATIONS', 'Email templates', 'Email templates'),
(1181, 'SETTINGS_NOTIFICATIONS_METHOD_ADMIN', 'PARENT_SETTINGS_NOTIFICATIONS', 'Admin notifications method', 'Admin notifications method'),
(1182, 'SETTINGS_NOTIFICATIONS_METHOD_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'User notifications method', 'User notifications method'),
(1183, 'SETTINGS_NOTIFICATIONS_EMAIL', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications email', 'Notifications email'),
(1184, 'SETTINGS_NOTIFICATIONS_EMAIL_REPLY', 'PARENT_SETTINGS_NOTIFICATIONS', 'Reply email', 'Reply email'),
(1185, 'SETTINGS_NOTIFICATIONS_EMAIL_NAME', 'PARENT_SETTINGS_NOTIFICATIONS', 'Email name', 'Email name'),
(1186, 'SETTINGS_NOTIFICATIONS_EMAIL_CC', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications Cc email(s)', 'Notifications Cc email(s)'),
(1187, 'SETTINGS_NOTIFICATIONS_EMAIL_CC_NAME', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications Cc name(s)', 'Notifications Cc name(s)'),
(1188, 'SETTINGS_NOTIFICATIONS_EMAIL_BCC', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications Bcc email(s)', 'Notifications Bcc email(s)'),
(1189, 'SETTINGS_NOTIFICATIONS_EMAIL_BCC_NAME', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications Bcc name(s)', 'Notifications Bcc name(s)'),
(1190, 'SETTINGS_NOTIFICATIONS_SEND_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enable notifications', 'Enable notifications'),
(1191, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_ADMIN', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify admin on book request', 'Notify admin on book request'),
(1192, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify user on book request', 'Notify user on book request'),
(1193, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_WITH_APPROVAL_ADMIN', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify admin on approved book request', 'Notify admin on approved book request'),
(1194, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_WITH_APPROVAL_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify user on approved book request', 'Notify user on approved book request'),
(1195, 'SETTINGS_NOTIFICATIONS_SEND_APPROVED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify user when reservation is approved', 'Notify user when reservation is approved'),
(1196, 'SETTINGS_NOTIFICATIONS_SEND_CANCELED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify user when reservation is canceled', 'Notify user when reservation is canceled'),
(1197, 'SETTINGS_NOTIFICATIONS_SEND_REJECTED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notify user when reservation is rejected', 'Notify user when reservation is rejected'),
(1198, 'SETTINGS_NOTIFICATIONS_SMTP_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP settings', 'SMTP settings'),
(1199, 'SETTINGS_NOTIFICATIONS_SMTP_SECOND_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Second SMTP settings', 'Second SMTP settings'),
(1200, 'SETTINGS_NOTIFICATIONS_SMTP_HOST_NAME', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP host name', 'SMTP host name') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1201, 'SETTINGS_NOTIFICATIONS_SMTP_HOST_PORT', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP host port', 'SMTP host port'),
(1202, 'SETTINGS_NOTIFICATIONS_SMTP_SSL', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP SSL conection', 'SMTP SSL conection'),
(1203, 'SETTINGS_NOTIFICATIONS_SMTP_TLS', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP TLS conection', 'SMTP TLS conection'),
(1204, 'SETTINGS_NOTIFICATIONS_SMTP_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP host user', 'SMTP host user'),
(1205, 'SETTINGS_NOTIFICATIONS_SMTP_PASSWORD', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMTP host password', 'SMTP host password'),
(1206, 'SETTINGS_NOTIFICATIONS_TEST_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Test notification methods', 'Test notification methods'),
(1207, 'SETTINGS_NOTIFICATIONS_TEST_METHOD', 'PARENT_SETTINGS_NOTIFICATIONS', 'Select notifications method', 'Select notifications method'),
(1208, 'SETTINGS_NOTIFICATIONS_TEST_EMAIL', 'PARENT_SETTINGS_NOTIFICATIONS', 'Test email', 'Test email'),
(1209, 'SETTINGS_NOTIFICATIONS_TEST_SUBMIT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Send test', 'Send test'),
(1210, 'SETTINGS_NOTIFICATIONS_TEST_SENDING', 'PARENT_SETTINGS_NOTIFICATIONS', 'Sending notification test email ...', 'Sending notification test email ...'),
(1211, 'SETTINGS_NOTIFICATIONS_TEST_SUCCESS', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notification test email has been sent.', 'Notification test email has been sent.'),
(1212, 'SETTINGS_NOTIFICATIONS_TEST_ERROR', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notification test email could not be sent.', 'Notification test email could not be sent.'),
(1213, 'SETTINGS_NOTIFICATIONS_TEST_MAIL_SUBJECT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System - PHP mail notification test', 'Pinpoint Booking System - PHP mail notification test'),
(1214, 'SETTINGS_NOTIFICATIONS_TEST_MAIL_MESSAGE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System notification test sent with PHP mail function.', 'Pinpoint Booking System notification test sent with PHP mail function.'),
(1215, 'SETTINGS_NOTIFICATIONS_TEST_MAILER_SUBJECT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System - PHPMailer notification test', 'Pinpoint Booking System - PHPMailer notification test'),
(1216, 'SETTINGS_NOTIFICATIONS_TEST_MAILER_MESSAGE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System notification test sent with PHPMailer class.', 'Pinpoint Booking System notification test sent with PHPMailer class.'),
(1217, 'SETTINGS_NOTIFICATIONS_TEST_SMTP_SUBJECT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System - SMTP notification test', 'Pinpoint Booking System - SMTP notification test'),
(1218, 'SETTINGS_NOTIFICATIONS_TEST_SMTP_MESSAGE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System notification test sent with PHPMailer SMTP.', 'Pinpoint Booking System notification test sent with PHPMailer SMTP.'),
(1219, 'SETTINGS_NOTIFICATIONS_TEST_SMTP2_SUBJECT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System - Second SMTP notification test', 'Pinpoint Booking System - Second SMTP notification test'),
(1220, 'SETTINGS_NOTIFICATIONS_TEST_SMTP2_MESSAGE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System notification test sent with second SMTP.', 'Pinpoint Booking System notification test sent with second SMTP.'),
(1221, 'SETTINGS_NOTIFICATIONS_TEST_WP_SUBJECT', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System - WordPress mail notification test', 'Pinpoint Booking System - WordPress mail notification test'),
(1222, 'SETTINGS_NOTIFICATIONS_TEST_WP_MESSAGE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Pinpoint Booking System notification test sent with WordPress mail function.', 'Pinpoint Booking System notification test sent with WordPress mail function.'),
(1223, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_TITLE', 'PARENT_SETTINGS_NOTIFICATIONS', 'SMS notifications - Clickatell.com', 'SMS notifications - Clickatell.com'),
(1224, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_USERNAME', 'PARENT_SETTINGS_NOTIFICATIONS', 'Username', 'Username'),
(1225, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_PASSWORD', 'PARENT_SETTINGS_NOTIFICATIONS', 'Password', 'Password'),
(1226, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_API_ID', 'PARENT_SETTINGS_NOTIFICATIONS', 'API ID', 'API ID'),
(1227, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_FROM', 'PARENT_SETTINGS_NOTIFICATIONS', 'From', 'From'),
(1228, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_ADMIN_PHONE', 'PARENT_SETTINGS_NOTIFICATIONS', 'Notifications phone', 'Notifications phone'),
(1229, 'PARENT_SETTINGS_NOTIFICATIONS_HELP', '', 'Settings - Notifications - Help', 'Settings - Notifications - Help'),
(1230, 'SETTINGS_NOTIFICATIONS_TEMPLATES_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Select email templates.', 'Select email templates.'),
(1231, 'SETTINGS_NOTIFICATIONS_METHOD_ADMIN_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Select notifications method used to send emails to admins. You can use PHP mail function, PHPMailer class, SMTP, second SMTP or WordPress wp_mail function.', 'Select notifications method used to send emails to admins. You can use PHP mail function, PHPMailer class, SMTP, second SMTP or WordPress wp_mail function.'),
(1232, 'SETTINGS_NOTIFICATIONS_METHOD_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Select notifications method used to send emails to users. You can use PHP mail function, PHPMailer class, SMTP, second SMTP or WordPress wp_mail function.', 'Select notifications method used to send emails to users. You can use PHP mail function, PHPMailer class, SMTP, second SMTP or WordPress wp_mail function.'),
(1233, 'SETTINGS_NOTIFICATIONS_EMAIL_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the email were you will be notified about booking requests or you will use to notify users. Enter other emails that will be notified in Cc & Bcc fields.', 'Enter the email were you will be notified about booking requests or you will use to notify users. Enter other emails that will be notified in Cc & Bcc fields.'),
(1234, 'SETTINGS_NOTIFICATIONS_EMAIL_REPLY_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the reply email that will appear in the email the user will receive.', 'Enter the reply email that will appear in the email the user will receive.'),
(1235, 'SETTINGS_NOTIFICATIONS_EMAIL_NAME_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the name that will appear in the email the user will receive.', 'Enter the name that will appear in the email the user will receive.'),
(1236, 'SETTINGS_NOTIFICATIONS_EMAIL_CC_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the email(s) for Cc field, were others be notified about booking requests or they will use to notify users. Add an email per line.', 'Enter the email(s) for Cc field, were others be notified about booking requests or they will use to notify users. Add an email per line.'),
(1237, 'SETTINGS_NOTIFICATIONS_EMAIL_CC_NAME_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the name(s) for Cc field, equivalent to Cc email(s). Add a name per line, like emails.', 'Enter the name(s) for Cc field, equivalent to Cc email(s). Add a name per line, like emails.'),
(1238, 'SETTINGS_NOTIFICATIONS_EMAIL_BCC_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the email(s) for Bcc field, were others be notified about booking requests or they will use to notify users. Add an email per line.', 'Enter the email(s) for Bcc field, were others be notified about booking requests or they will use to notify users. Add an email per line.'),
(1239, 'SETTINGS_NOTIFICATIONS_EMAIL_BCC_NAME_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter the name(s) for Bcc field, equivalent to Bcc email(s). Add a name per line, like emails.', 'Enter the name(s) for Bcc field, equivalent to Bcc email(s). Add a name per line, like emails.'),
(1240, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_ADMIN_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to admin on book request.', 'Enable to send an email notification to admin on book request.'),
(1241, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to user on book request.', 'Enable to send an email notification to user on book request.'),
(1242, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_WITH_APPROVAL_ADMIN_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to admin on book request and reservation is approved.', 'Enable to send an email notification to admin on book request and reservation is approved.'),
(1243, 'SETTINGS_NOTIFICATIONS_SEND_BOOK_WITH_APPROVAL_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to user on book request and reservation is approved.', 'Enable to send an email notification to user on book request and reservation is approved.'),
(1244, 'SETTINGS_NOTIFICATIONS_SEND_APPROVED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to user when reservation is approved.', 'Enable to send an email notification to user when reservation is approved.'),
(1245, 'SETTINGS_NOTIFICATIONS_SEND_CANCELED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to user when reservation is canceled.', 'Enable to send an email notification to user when reservation is canceled.'),
(1246, 'SETTINGS_NOTIFICATIONS_SEND_REJECTED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an email notification to user when reservation is rejected.', 'Enable to send an email notification to user when reservation is rejected.'),
(1247, 'SETTINGS_NOTIFICATIONS_SMTP_HOST_NAME_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter SMTP host name.', 'Enter SMTP host name.'),
(1248, 'SETTINGS_NOTIFICATIONS_SMTP_HOST_PORT_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter SMTP host port.', 'Enter SMTP host port.'),
(1249, 'SETTINGS_NOTIFICATIONS_SMTP_SSL_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Use a  SSL conection.', 'Use a  SSL conection.'),
(1250, 'SETTINGS_NOTIFICATIONS_SMTP_TLS_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Use a TLS conection.', 'Use a TLS conection.'),
(1251, 'SETTINGS_NOTIFICATIONS_SMTP_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter SMTP host username.', 'Enter SMTP host username.'),
(1252, 'SETTINGS_NOTIFICATIONS_SMTP_PASSWORD_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enter SMTP host password.', 'Enter SMTP host password.'),
(1253, 'SETTINGS_NOTIFICATIONS_TEST_METHOD_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Select the notifications method for which the test will be performed.', 'Select the notifications method for which the test will be performed.'),
(1254, 'SETTINGS_NOTIFICATIONS_TEST_EMAIL_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the email to which the test notification will be sent.', 'Enter the email to which the test notification will be sent.'),
(1255, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_USERNAME_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the username from clickatell.com.', 'Enter the username from clickatell.com.'),
(1256, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_PASSWORD_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the password from clickatell.com.', 'Enter the password from clickatell.com.'),
(1257, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_API_ID_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the API ID from clickatell.com.', 'Enter the API ID from clickatell.com.'),
(1258, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_FROM_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the text from message header.', 'Enter the text from message header.'),
(1259, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_ADMIN_PHONE_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Enter the admin phone.', 'Enter the admin phone.'),
(1260, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_ADMIN_PHONE_ADD_HELP', 'PARENT_SETTINGS_NOTIFICATIONS', 'Add phone number.', 'Add phone number.'),
(1261, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_BOOK_ADMIN', 'PARENT_SETTINGS_NOTIFICATIONS', 'You received a booking request.|DETAILS|', 'You received a booking request.|DETAILS|'),
(1262, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_BOOK_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'Your booking request has been sent.|DETAILS|', 'Your booking request has been sent.|DETAILS|'),
(1263, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_BOOK_WITH_APPROVAL_ADMIN', 'PARENT_SETTINGS_NOTIFICATIONS', 'You received a booking request.|DETAILS|', 'You received a booking request.|DETAILS|'),
(1264, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_BOOK_WITH_APPROVAL_USER', 'PARENT_SETTINGS_NOTIFICATIONS', 'Your booking request has been sent.|DETAILS|', 'Your booking request has been sent.|DETAILS|'),
(1265, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_APPROVED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Your booking request has been approved.|DETAILS|', 'Your booking request has been approved.|DETAILS|'),
(1266, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_CANCELED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Your booking request has been canceled.|DETAILS|', 'Your booking request has been canceled.|DETAILS|'),
(1267, 'SETTINGS_NOTIFICATIONS_SMS_CLICKATELL_REJECTED', 'PARENT_SETTINGS_NOTIFICATIONS', 'Your booking request has been rejected.|DETAILS|', 'Your booking request has been rejected.|DETAILS|'),
(1268, 'SETTINGS_NOTIFICATIONS_SMS_SEND_BOOK_ADMIN_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to admin on book request.', 'Enable to send an SMS notification to admin on book request.'),
(1269, 'SETTINGS_NOTIFICATIONS_SMS_SEND_BOOK_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to user on book request.', 'Enable to send an SMS notification to user on book request.'),
(1270, 'SETTINGS_NOTIFICATIONS_SMS_SEND_BOOK_WITH_APPROVAL_ADMIN_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to admin on book request and reservation is approved.', 'Enable to send an SMS notification to admin on book request and reservation is approved.'),
(1271, 'SETTINGS_NOTIFICATIONS_SMS_SEND_BOOK_WITH_APPROVAL_USER_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to user on book request and reservation is approved.', 'Enable to send an SMS notification to user on book request and reservation is approved.'),
(1272, 'SETTINGS_NOTIFICATIONS_SMS_SEND_APPROVED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to user when reservation is approved.', 'Enable to send an SMS notification to user when reservation is approved.'),
(1273, 'SETTINGS_NOTIFICATIONS_SMS_SEND_CANCELED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to user when reservation is canceled.', 'Enable to send an SMS notification to user when reservation is canceled.'),
(1274, 'SETTINGS_NOTIFICATIONS_SMS_SEND_REJECTED_HELP', 'PARENT_SETTINGS_NOTIFICATIONS_HELP', 'Enable to send an SMS notification to user when reservation is rejected.', 'Enable to send an SMS notification to user when reservation is rejected.'),
(1275, 'PARENT_SETTINGS_PAYMENT_GATEWAYS', '', 'Settings - Payment gateways', 'Settings - Payment gateways'),
(1276, 'SETTINGS_PAYMENT_GATEWAYS_TITLE', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Payment gateways', 'Payment gateways'),
(1277, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_ARRIVAL_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Enable payment on arrival', 'Enable payment on arrival'),
(1278, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_ARRIVAL_WITH_APPROVAL_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Enable instant approval', 'Enable instant approval'),
(1279, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_REDIRECT', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Redirect after book', 'Redirect after book'),
(1280, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Billing address', 'Billing address'),
(1281, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Enable billing address', 'Enable billing address'),
(1282, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_FIRST_NAME_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'First name (enable)', 'First name (enable)'),
(1283, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_FIRST_NAME_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'First name (required)', 'First name (required)'),
(1284, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_LAST_NAME_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Last name (enable)', 'Last name (enable)'),
(1285, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_LAST_NAME_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Last name (required)', 'Last name (required)'),
(1286, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COMPANY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Company (enable)', 'Company (enable)'),
(1287, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COMPANY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Company (required)', 'Company (required)'),
(1288, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_EMAIL_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Email (enable)', 'Email (enable)'),
(1289, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_EMAIL_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Email (required)', 'Email (required)'),
(1290, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_PHONE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Phone number (enable)', 'Phone number (enable)'),
(1291, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_PHONE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Phone number (required)', 'Phone number (required)'),
(1292, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COUNTRY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Country (enable)', 'Country (enable)'),
(1293, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COUNTRY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Country (required)', 'Country (required)'),
(1294, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_FIRST_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 1 (enable)', 'Address line 1 (enable)'),
(1295, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_FIRST_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 1 (required)', 'Address line 1 (required)'),
(1296, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_SECOND_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 2 (enable)', 'Address line 2 (enable)'),
(1297, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_SECOND_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 2 (required)', 'Address line 2 (required)'),
(1298, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_CITY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'City (enable)', 'City (enable)'),
(1299, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_CITY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'City (required)', 'City (required)'),
(1300, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_STATE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'State (enable)', 'State (enable)') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1301, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_STATE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'State (required)', 'State (required)'),
(1302, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ZIP_CODE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Zip code (enable)', 'Zip code (enable)'),
(1303, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ZIP_CODE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Zip code (required)', 'Zip code (required)'),
(1304, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Shipping address', 'Shipping address'),
(1305, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Enable shipping address', 'Enable shipping address'),
(1306, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_FIRST_NAME_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'First name (enable)', 'First name (enable)'),
(1307, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_FIRST_NAME_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'First name (required)', 'First name (required)'),
(1308, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_LAST_NAME_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Last name (enable)', 'Last name (enable)'),
(1309, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_LAST_NAME_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Last name (required)', 'Last name (required)'),
(1310, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COMPANY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Company (enable)', 'Company (enable)'),
(1311, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COMPANY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Company (required)', 'Company (required)'),
(1312, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_EMAIL_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Email (enable)', 'Email (enable)'),
(1313, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_EMAIL_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Email (required)', 'Email (required)'),
(1314, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_PHONE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Phone number (enable)', 'Phone number (enable)'),
(1315, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_PHONE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Phone number (required)', 'Phone number (required)'),
(1316, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COUNTRY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Country (enable)', 'Country (enable)'),
(1317, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COUNTRY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Country (required)', 'Country (required)'),
(1318, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_FIRST_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 1 (enable)', 'Address line 1 (enable)'),
(1319, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_FIRST_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 1 (required)', 'Address line 1 (required)'),
(1320, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_SECOND_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 2 (enable)', 'Address line 2 (enable)'),
(1321, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_SECOND_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Address line 2 (required)', 'Address line 2 (required)'),
(1322, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_CITY_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'City (enable)', 'City (enable)'),
(1323, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_CITY_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'City (required)', 'City (required)'),
(1324, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_STATE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'State (enable)', 'State (enable)'),
(1325, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_STATE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'State (required)', 'State (required)'),
(1326, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ZIP_CODE_ENABLED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Zip code (enable)', 'Zip code (enable)'),
(1327, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ZIP_CODE_REQUIRED', 'PARENT_SETTINGS_PAYMENT_GATEWAYS', 'Zip code (required)', 'Zip code (required)'),
(1328, 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', '', 'Settings - Payment gateways - Help', 'Settings - Payment gateways - Help'),
(1329, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_ARRIVAL_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Allow user to pay on arrival. Need approval.', 'Default value: Enabled. Allow user to pay on arrival. Need approval.'),
(1330, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_ARRIVAL_WITH_APPROVAL_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Instantly approve the reservation once the request to pay on arrival has been submitted.', 'Default value: Disabled. Instantly approve the reservation once the request to pay on arrival has been submitted.'),
(1331, 'SETTINGS_PAYMENT_GATEWAYS_PAYMENT_REDIRECT_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Enter the URL where to redirect after the booking request has been sent. Leave it blank to not redirect.', 'Enter the URL where to redirect after the booking request has been sent. Leave it blank to not redirect.'),
(1332, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the billing address form to be visible.', 'Default value: Disabled. Enable it if you want the billing address form to be visible.'),
(1333, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_FIRST_NAME_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "First name" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "First name" field to be visible in billing address form.'),
(1334, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_FIRST_NAME_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "First name" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "First name" field to be mandatory in billing address form.'),
(1335, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_LAST_NAME_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Last name" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Last name" field to be visible in billing address form.'),
(1336, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_LAST_NAME_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Last name" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Last name" field to be mandatory in billing address form.'),
(1337, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COMPANY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Company" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Company" field to be visible in billing address form.'),
(1338, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COMPANY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the "Company" field to be mandatory in billing address form.', 'Default value: Disabled. Enable it if you want the "Company" field to be mandatory in billing address form.'),
(1339, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_EMAIL_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Email" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Email" field to be visible in billing address form.'),
(1340, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_EMAIL_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Email" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Email" field to be mandatory in billing address form.'),
(1341, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_PHONE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Phone number" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Phone number" field to be visible in billing address form.'),
(1342, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_PHONE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Phone number" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Phone number" field to be mandatory in billing address form.'),
(1343, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COUNTRY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Country" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Country" field to be visible in billing address form.'),
(1344, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_COUNTRY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Country" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Country" field to be mandatory in billing address form.'),
(1345, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_FIRST_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be visible in billing address form.'),
(1346, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_FIRST_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be mandatory in billing address form.'),
(1347, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_SECOND_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 2" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Address line 2" field to be visible in billing address form.'),
(1348, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ADDRESS_SECOND_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the "Address line 2" field to be mandatory in billing address form.', 'Default value: Disabled. Enable it if you want the "Address line 2" field to be mandatory in billing address form.'),
(1349, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_CITY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "City" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "City" field to be visible in billing address form.'),
(1350, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_CITY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "City" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "City" field to be mandatory in billing address form.'),
(1351, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_STATE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "State" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "State" field to be visible in billing address form.'),
(1352, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_STATE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "State" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "State" field to be mandatory in billing address form.'),
(1353, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ZIP_CODE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Zip code" field to be visible in billing address form.', 'Default value: Enabled. Enable it if you want the "Zip code" field to be visible in billing address form.'),
(1354, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_BILLING_ZIP_CODE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Zip code" field to be mandatory in billing address form.', 'Default value: Enabled. Enable it if you want the "Zip code" field to be mandatory in billing address form.'),
(1355, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the shipping address form to be visible.', 'Default value: Disabled. Enable it if you want the shipping address form to be visible.'),
(1356, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_FIRST_NAME_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "First name" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "First name" field to be visible in shipping address form.'),
(1357, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_FIRST_NAME_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "First name" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "First name" field to be mandatory in shipping address form.'),
(1358, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_LAST_NAME_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Last name" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Last name" field to be visible in shipping address form.'),
(1359, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_LAST_NAME_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Last name" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Last name" field to be mandatory in shipping address form.'),
(1360, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COMPANY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Company" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Company" field to be visible in shipping address form.'),
(1361, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COMPANY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the "Company" field to be mandatory in shipping address form.', 'Default value: Disabled. Enable it if you want the "Company" field to be mandatory in shipping address form.'),
(1362, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_EMAIL_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Email" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Email" field to be visible in shipping address form.'),
(1363, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_EMAIL_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Email" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Email" field to be mandatory in shipping address form.'),
(1364, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_PHONE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Phone number" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Phone number" field to be visible in shipping address form.'),
(1365, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_PHONE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Phone number" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Phone number" field to be mandatory in shipping address form.'),
(1366, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COUNTRY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Country" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Country" field to be visible in shipping address form.'),
(1367, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_COUNTRY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Country" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Country" field to be mandatory in shipping address form.'),
(1368, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_FIRST_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be visible in shipping address form.'),
(1369, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_FIRST_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Address line 1" field to be mandatory in shipping address form.'),
(1370, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_SECOND_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Address line 2" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Address line 2" field to be visible in shipping address form.'),
(1371, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ADDRESS_SECOND_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Disabled. Enable it if you want the "Address line 2" field to be mandatory in shipping address form.', 'Default value: Disabled. Enable it if you want the "Address line 2" field to be mandatory in shipping address form.'),
(1372, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_CITY_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "City" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "City" field to be visible in shipping address form.'),
(1373, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_CITY_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "City" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "City" field to be mandatory in shipping address form.'),
(1374, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_STATE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "State" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "State" field to be visible in shipping address form.'),
(1375, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_STATE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "State" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "State" field to be mandatory in shipping address form.'),
(1376, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ZIP_CODE_ENABLED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Zip code" field to be visible in shipping address form.', 'Default value: Enabled. Enable it if you want the "Zip code" field to be visible in shipping address form.'),
(1377, 'SETTINGS_PAYMENT_GATEWAYS_ADDRESS_SHIPPING_ZIP_CODE_REQUIRED_HELP', 'PARENT_SETTINGS_PAYMENT_GATEWAYS_HELP', 'Default value: Enabled. Enable it if you want the "Zip code" field to be mandatory in shipping address form.', 'Default value: Enabled. Enable it if you want the "Zip code" field to be mandatory in shipping address form.'),
(1378, 'PARENT_SETTINGS_SEARCH', '', 'Settings - Search', 'Settings - Search'),
(1379, 'SETTINGS_SEARCH_TITLE', 'PARENT_SETTINGS_SEARCH', 'Search settings', 'Search settings'),
(1380, 'SETTINGS_SEARCH_GENERAL_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'General settings', 'General settings'),
(1381, 'SETTINGS_SEARCH_GENERAL_DATE_TYPE', 'PARENT_SETTINGS_SEARCH', 'Date type', 'Date type'),
(1382, 'SETTINGS_SEARCH_GENERAL_DATE_TYPE_AMERICAN', 'PARENT_SETTINGS_SEARCH', 'American (mm dd, yyyy)', 'American (mm dd, yyyy)'),
(1383, 'SETTINGS_SEARCH_GENERAL_DATE_TYPE_EUROPEAN', 'PARENT_SETTINGS_SEARCH', 'European (dd mm yyyy)', 'European (dd mm yyyy)'),
(1384, 'SETTINGS_SEARCH_GENERAL_TEMPLATE', 'PARENT_SETTINGS_SEARCH', 'Style template', 'Style template'),
(1385, 'SETTINGS_SEARCH_GENERAL_SEARCH_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Enable search input', 'Enable search input'),
(1386, 'SETTINGS_SEARCH_GENERAL_PRICE_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Filter results by price', 'Filter results by price'),
(1387, 'SETTINGS_SEARCH_VIEW_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'View settings', 'View settings'),
(1388, 'SETTINGS_SEARCH_VIEW_DEFAULT', 'PARENT_SETTINGS_SEARCH', 'Defaul view', 'Defaul view'),
(1389, 'SETTINGS_SEARCH_VIEW_DEFAULT_LIST', 'PARENT_SETTINGS_SEARCH', 'List', 'List'),
(1390, 'SETTINGS_SEARCH_VIEW_DEFAULT_GRID', 'PARENT_SETTINGS_SEARCH', 'Grid', 'Grid'),
(1391, 'SETTINGS_SEARCH_VIEW_DEFAULT_MAP', 'PARENT_SETTINGS_SEARCH', 'Map', 'Map'),
(1392, 'SETTINGS_SEARCH_VIEW_LIST_VIEW_ENABLED', 'PARENT_SETTINGS_SEARCH', 'List view', 'List view'),
(1393, 'SETTINGS_SEARCH_VIEW_GRID_VIEW_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Grid view', 'Grid view'),
(1394, 'SETTINGS_SEARCH_VIEW_MAP_VIEW_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Map view', 'Map view'),
(1395, 'SETTINGS_SEARCH_VIEW_RESULTS_PAGE', 'PARENT_SETTINGS_SEARCH', 'Results per page', 'Results per page'),
(1396, 'SETTINGS_SEARCH_VIEW_SIDEBAR_POSITION', 'PARENT_SETTINGS_SEARCH', 'Sidebar position', 'Sidebar position'),
(1397, 'SETTINGS_SEARCH_VIEW_SIDEBAR_POSITION_LEFT', 'PARENT_SETTINGS_SEARCH', 'Left', 'Left'),
(1398, 'SETTINGS_SEARCH_VIEW_SIDEBAR_POSITION_RIGHT', 'PARENT_SETTINGS_SEARCH', 'Right', 'Right'),
(1399, 'SETTINGS_SEARCH_VIEW_SIDEBAR_POSITION_TOP', 'PARENT_SETTINGS_SEARCH', 'Top', 'Top'),
(1400, 'SETTINGS_SEARCH_CURRENCY_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'Currency settings', 'Currency settings') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1401, 'SETTINGS_SEARCH_CURRENCY', 'PARENT_SETTINGS_SEARCH', 'Currency', 'Currency'),
(1402, 'SETTINGS_SEARCH_CURRENCY_POSITION', 'PARENT_SETTINGS_SEARCH', 'Currency position', 'Currency position'),
(1403, 'SETTINGS_SEARCH_CURRENCY_POSITION_BEFORE', 'PARENT_SETTINGS_SEARCH', 'Before', 'Before'),
(1404, 'SETTINGS_SEARCH_CURRENCY_POSITION_BEFORE_WITH_SPACE', 'PARENT_SETTINGS_SEARCH', 'Before with space', 'Before with space'),
(1405, 'SETTINGS_SEARCH_CURRENCY_POSITION_AFTER', 'PARENT_SETTINGS_SEARCH', 'After', 'After'),
(1406, 'SETTINGS_SEARCH_CURRENCY_POSITION_AFTER_WITH_SPACE', 'PARENT_SETTINGS_SEARCH', 'After with space', 'After with space'),
(1407, 'SETTINGS_SEARCH_DAYS_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'Days settings', 'Days settings'),
(1408, 'SETTINGS_SEARCH_DAYS_FIRST', 'PARENT_SETTINGS_SEARCH', 'First weekday', 'First weekday'),
(1409, 'SETTINGS_SEARCH_DAYS_MULTIPLE_SELECT', 'PARENT_SETTINGS_SEARCH', 'Search start/end days', 'Search start/end days'),
(1410, 'SETTINGS_SEARCH_HOURS_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'Hours settings', 'Hours settings'),
(1411, 'SETTINGS_SEARCH_HOURS_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Search hours', 'Search hours'),
(1412, 'SETTINGS_SEARCH_HOURS_DEFINITIONS', 'PARENT_SETTINGS_SEARCH', 'Define hours', 'Define hours'),
(1413, 'SETTINGS_SEARCH_HOURS_MULTIPLE_SELECT', 'PARENT_SETTINGS_SEARCH', 'Search start/end hours', 'Search start/end hours'),
(1414, 'SETTINGS_SEARCH_HOURS_AMPM', 'PARENT_SETTINGS_SEARCH', 'Enable AM/PM format', 'Enable AM/PM format'),
(1415, 'SETTINGS_SEARCH_AVAILABILITY_SETTINGS', 'PARENT_SETTINGS_SEARCH', 'Availability settings', 'Availability settings'),
(1416, 'SETTINGS_SEARCH_AVAILABILITY_ENABLED', 'PARENT_SETTINGS_SEARCH', 'Filter results by no of items available', 'Filter results by no of items available'),
(1417, 'SETTINGS_SEARCH_AVAILABILITY_MIN', 'PARENT_SETTINGS_SEARCH', 'Minimum availability value', 'Minimum availability value'),
(1418, 'SETTINGS_SEARCH_AVAILABILITY_MAX', 'PARENT_SETTINGS_SEARCH', 'Maximum availability value', 'Maximum availability value'),
(1419, 'PARENT_SETTINGS_SEARCH_HELP', '', 'Settings - Search - Help', 'Settings - Search - Help'),
(1420, 'SETTINGS_SEARCH_GENERAL_DATE_TYPE_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: American. Select date format: American (mm dd, yyyy) or European (dd mm yyyy).', 'Default value: American. Select date format: American (mm dd, yyyy) or European (dd mm yyyy).'),
(1421, 'SETTINGS_SEARCH_GENERAL_TEMPLATE_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: default. Select styles template.', 'Default value: default. Select styles template.'),
(1422, 'SETTINGS_SEARCH_GENERAL_SEARCH_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable the option to search by name or location (a location needs to be created).', 'Default value: Disabled. Enable the option to search by name or location (a location needs to be created).'),
(1423, 'SETTINGS_SEARCH_GENERAL_PRICE_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable the option to filter results by price.', 'Default value: Disabled. Enable the option to filter results by price.'),
(1424, 'SETTINGS_SEARCH_VIEW_DEFAULT_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: List. Select the default view that the search results will first display.', 'Default value: List. Select the default view that the search results will first display.'),
(1425, 'SETTINGS_SEARCH_VIEW_LIST_VIEW_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Enabled. Enable to display results in list view.', 'Default value: Enabled. Enable to display results in list view.'),
(1426, 'SETTINGS_SEARCH_VIEW_GRID_VIEW_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable to display results in grid view.', 'Default value: Disabled. Enable to display results in grid view.'),
(1427, 'SETTINGS_SEARCH_VIEW_MAP_VIEW_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable to display results on a google map.', 'Default value: Disabled. Enable to display results on a google map.'),
(1428, 'SETTINGS_SEARCH_VIEW_RESULTS_PAGE_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: 10. Set the number of results to display on a page.', 'Default value: 10. Set the number of results to display on a page.'),
(1429, 'SETTINGS_SEARCH_VIEW_SIDEBAR_POSITION_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Left. Set filters sidebar position: Left, Right & Top.', 'Default value: Left. Set filters sidebar position: Left, Right & Top.'),
(1430, 'SETTINGS_SEARCH_CURRENCY_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: United States Dollar ($, USD). Select search default currency.', 'Default value: United States Dollar ($, USD). Select search default currency.'),
(1431, 'SETTINGS_SEARCH_CURRENCY_POSITION_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Before. Select currency position.', 'Default value: Before. Select currency position.'),
(1432, 'SETTINGS_SEARCH_DAYS_MULTIPLE_SELECT_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Enabled. Use start/end days or select only one day to filter results.', 'Default value: Enabled. Use start/end days or select only one day to filter results.'),
(1433, 'SETTINGS_SEARCH_DAYS_FIRST_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Monday. Select search first weekday.', 'Default value: Monday. Select search first weekday.'),
(1434, 'SETTINGS_SEARCH_HOURS_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable hours to use them to filter results.', 'Default value: Disabled. Enable hours to use them to filter results.'),
(1435, 'SETTINGS_SEARCH_HOURS_DEFINITIONS_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Enter hh:mm ... add one per line.', 'Enter hh:mm ... add one per line.'),
(1436, 'SETTINGS_SEARCH_HOURS_MULTIPLE_SELECT_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Enabled. Use start/end hours or select only one hour to filter results.', 'Default value: Enabled. Use start/end hours or select only one hour to filter results.'),
(1437, 'SETTINGS_SEARCH_HOURS_AMPM_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Display hours in AM/PM format. NOTE: Hours definitions still need to be in 24 hours format.', 'Default value: Disabled. Display hours in AM/PM format. NOTE: Hours definitions still need to be in 24 hours format.'),
(1438, 'SETTINGS_SEARCH_AVAILABILITY_ENABLED_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: Disabled. Enable the option to filter results by the number of items available to book.', 'Default value: Disabled. Enable the option to filter results by the number of items available to book.'),
(1439, 'SETTINGS_SEARCH_AVAILABILITY_MIN_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: 1. Set minimum availability value to filter results.', 'Default value: 1. Set minimum availability value to filter results.'),
(1440, 'SETTINGS_SEARCH_AVAILABILITY_MAX_HELP', 'PARENT_SETTINGS_SEARCH_HELP', 'Default value: 10. Set maximum availability value to filter results.', 'Default value: 10. Set maximum availability value to filter results.'),
(1441, 'PARENT_SETTINGS_USERS', '', 'Settings - Users permissions', 'Settings - Users permissions'),
(1442, 'SETTINGS_USERS_TITLE', 'PARENT_SETTINGS_USERS', 'Users permissions', 'Users permissions'),
(1443, 'SETTINGS_USERS_PERMISSIONS', 'PARENT_SETTINGS_USERS', 'Set users permissions to use the booking system.', 'Set users permissions to use the booking system.'),
(1444, 'SETTINGS_USERS_PERMISSIONS_ADMINISTRATORS_LABEL', 'PARENT_SETTINGS_USERS', 'Allow %s users to view all the calendars from all the users and/or individually add/edit them.', 'Allow %s users to view all the calendars from all the users and/or individually add/edit them.'),
(1445, 'SETTINGS_USERS_PERMISSIONS_LABEL', 'PARENT_SETTINGS_USERS', 'Allow %s users to view the plugin and individually edit only their own calendars.', 'Allow %s users to view the plugin and individually edit only their own calendars.'),
(1446, 'SETTINGS_USERS_PERMISSIONS_CUSTOM_POSTS', 'PARENT_SETTINGS_USERS', 'Set users permissions to use custom posts', 'Set users permissions to use custom posts'),
(1447, 'SETTINGS_USERS_PERMISSIONS_CUSTOM_POSTS_LABEL', 'PARENT_SETTINGS_USERS', 'Allow %s users to use custom posts.', 'Allow %s users to use custom posts.'),
(1448, 'SETTINGS_USERS_PERMISSIONS_INDIVIDUAL', 'PARENT_SETTINGS_USERS', 'Set permissions on individual users', 'Set permissions on individual users'),
(1449, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ROLE', 'PARENT_SETTINGS_USERS', 'Change role to', 'Change role to'),
(1450, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ROLE_ALL', 'PARENT_SETTINGS_USERS', 'All', 'All'),
(1451, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_BY', 'PARENT_SETTINGS_USERS', 'Order by', 'Order by'),
(1452, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_BY_EMAIL', 'PARENT_SETTINGS_USERS', 'Email', 'Email'),
(1453, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_BY_ID', 'PARENT_SETTINGS_USERS', 'ID', 'ID'),
(1454, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_BY_USERNAME', 'PARENT_SETTINGS_USERS', 'Username', 'Username'),
(1455, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER', 'PARENT_SETTINGS_USERS', 'Order', 'Order'),
(1456, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_ASCENDING', 'PARENT_SETTINGS_USERS', 'Ascending', 'Ascending'),
(1457, 'SETTINGS_USERS_PERMISSIONS_FILTERS_ORDER_DESCENDING', 'PARENT_SETTINGS_USERS', 'Descending', 'Descending'),
(1458, 'SETTINGS_USERS_PERMISSIONS_FILTERS_SEARCH', 'PARENT_SETTINGS_USERS', 'Search', 'Search'),
(1459, 'SETTINGS_USERS_PERMISSIONS_LIST_ID', 'PARENT_SETTINGS_USERS', 'ID', 'ID'),
(1460, 'SETTINGS_USERS_PERMISSIONS_USERNAME', 'PARENT_SETTINGS_USERS', 'Username', 'Username'),
(1461, 'SETTINGS_USERS_PERMISSIONS_EMAIL', 'PARENT_SETTINGS_USERS', 'Email', 'Email'),
(1462, 'SETTINGS_USERS_PERMISSIONS_ROLE', 'PARENT_SETTINGS_USERS', 'Role', 'Role'),
(1463, 'SETTINGS_USERS_PERMISSIONS_VIEW', 'PARENT_SETTINGS_USERS', 'View all calendars', 'View all calendars'),
(1464, 'SETTINGS_USERS_PERMISSIONS_USE', 'PARENT_SETTINGS_USERS', 'Use booking system', 'Use booking system'),
(1465, 'SETTINGS_USERS_PERMISSIONS_USE_CUSTOM_POSTS', 'PARENT_SETTINGS_USERS', 'Use custom posts', 'Use custom posts'),
(1466, 'SETTINGS_USERS_PERMISSIONS_USE_CALENDAR', 'PARENT_SETTINGS_USERS', 'Use calendar', 'Use calendar'),
(1467, 'PARENT_SETTINGS_USERS_HELP', '', 'Settings - Users Permissions - Help', 'Settings - Users Permissions - Help'),
(1468, 'SETTINGS_USERS_PERMISSIONS_HELP', 'PARENT_SETTINGS_USERS_HELP', 'Allow administrators to edit/view all calendars and other users to use the plugin.', 'Allow administrators to edit/view all calendars and other users to use the plugin.'),
(1469, 'SETTINGS_USERS_CUSTOM_POSTS_PERMISSIONS_HELP', 'PARENT_SETTINGS_USERS_HELP', 'Allow users to use custom posts.', 'Allow users to use custom posts.'),
(1470, 'PARENT_SETTINGS_LICENCES', '', 'Settings - Licences', 'Settings - Licences'),
(1471, 'SETTINGS_LICENCES_TITLE', 'PARENT_SETTINGS_LICENCES', 'Licences', 'Licences'),
(1472, 'SETTINGS_LICENCES_TITLE_PRO', 'PARENT_SETTINGS_LICENCES', 'Pinpoint Booking System PRO licence', 'Pinpoint Booking System PRO licence'),
(1473, 'SETTINGS_LICENCES_STATUS', 'PARENT_SETTINGS_LICENCES', 'Status', 'Status'),
(1474, 'SETTINGS_LICENCES_STATUS_ACTIVATE', 'PARENT_SETTINGS_LICENCES', 'Activate', 'Activate'),
(1475, 'SETTINGS_LICENCES_STATUS_DEACTIVATE', 'PARENT_SETTINGS_LICENCES', 'Deactivate', 'Deactivate'),
(1476, 'SETTINGS_LICENCES_STATUS_ACTIVATED', 'PARENT_SETTINGS_LICENCES', 'Activated', 'Activated'),
(1477, 'SETTINGS_LICENCES_STATUS_ACTIVATED_SUCCESS', 'PARENT_SETTINGS_LICENCES', 'The item was successfully activated.', 'The item was successfully activated.'),
(1478, 'SETTINGS_LICENCES_STATUS_ACTIVATED_ERROR', 'PARENT_SETTINGS_LICENCES', 'There is an error when trying to activate the item. Please try again.', 'There is an error when trying to activate the item. Please try again.'),
(1479, 'SETTINGS_LICENCES_STATUS_DEACTIVATED', 'PARENT_SETTINGS_LICENCES', 'Deactivated', 'Deactivated'),
(1480, 'SETTINGS_LICENCES_STATUS_DEACTIVATED_SUCCESS', 'PARENT_SETTINGS_LICENCES', 'The item was successfully deactivated.', 'The item was successfully deactivated.'),
(1481, 'SETTINGS_LICENCES_STATUS_DEACTIVATED_ERROR', 'PARENT_SETTINGS_LICENCES', 'There is an error when trying to deactivate the item. Please try again.', 'There is an error when trying to deactivate the item. Please try again.'),
(1482, 'SETTINGS_LICENCES_STATUS_TIMEOUT_ERROR', 'PARENT_SETTINGS_LICENCES', 'The connection to the server timed out. Please try again later.', 'The connection to the server timed out. Please try again later.'),
(1483, 'SETTINGS_LICENCES_KEY', 'PARENT_SETTINGS_LICENCES', 'Licence key', 'Licence key'),
(1484, 'SETTINGS_LICENCES_EMAIL', 'PARENT_SETTINGS_LICENCES', 'Licence email', 'Licence email'),
(1485, 'PARENT_SETTINGS_LICENCES_HELP', '', 'Settings - Licences - Help', 'Settings - Licences - Help'),
(1486, 'SETTINGS_LICENCES_HELP', 'PARENT_SETTINGS_LICENCES_HELP', 'Activate the plugin and add-ons to check and receive automatic updates. Activation is not required to use the items.', 'Activate the plugin and add-ons to check and receive automatic updates. Activation is not required to use the items.'),
(1487, 'SETTINGS_LICENCES_KEY_HELP', 'PARENT_SETTINGS_LICENCES_HELP', 'Enter the licence key which you received with your order confirmation email. You can also find it in %s', 'Enter the licence key which you received with your order confirmation email. You can also find it in %s'),
(1488, 'SETTINGS_LICENCES_EMAIL_HELP', 'PARENT_SETTINGS_LICENCES_HELP', 'Enter the email you are using on %s', 'Enter the email you are using on %s'),
(1489, 'PARENT_TEMPLATES', '', 'Templates', 'Templates'),
(1490, 'TEMPLATES_TITLE', 'PARENT_TEMPLATES', 'Templates', 'Templates'),
(1491, 'PARENT_THEMES', '', 'Themes', 'Themes'),
(1492, 'THEMES_TITLE', 'PARENT_THEMES', 'Themes', 'Themes'),
(1493, 'THEMES_HELP', 'PARENT_THEMES', 'A collection of themes specially created to be used with the Pinpoint Booking System. PRO version is included with each.', 'A collection of themes specially created to be used with the Pinpoint Booking System. PRO version is included with each.'),
(1494, 'THEMES_LOAD_SUCCESS', 'PARENT_THEMES', 'Themes list loaded.', 'Themes list loaded.'),
(1495, 'THEMES_LOAD_ERROR', 'PARENT_THEMES', 'Themes list failed to load. Please refresh the page to try again.', 'Themes list failed to load. Please refresh the page to try again.'),
(1496, 'THEMES_FILTERS_SEARCH', 'PARENT_THEMES', 'Search', 'Search'),
(1497, 'THEMES_FILTERS_SEARCH_TERMS', 'PARENT_THEMES', 'Enter search terms', 'Enter search terms'),
(1498, 'THEMES_FILTERS_TAGS', 'PARENT_THEMES', 'Tags', 'Tags'),
(1499, 'THEMES_FILTERS_TAGS_ALL', 'PARENT_THEMES', 'All', 'All'),
(1500, 'THEMES_THEME_PRICE', 'PARENT_THEMES', 'Price:', 'Price:') ;
INSERT INTO `wpwyp2810161505_dopbsp_translation_en` ( `id`, `key_data`, `parent_key`, `text_data`, `translation`) VALUES
(1501, 'THEMES_THEME_GET_IT_NOW', 'PARENT_THEMES', 'Get it now', 'Get it now'),
(1502, 'THEMES_THEME_VIEW_DEMO', 'PARENT_THEMES', 'View demo', 'View demo'),
(1503, 'PARENT_TOOLS', '', 'Tools', 'Tools'),
(1504, 'TOOLS_TITLE', 'PARENT_TOOLS', 'Tools', 'Tools'),
(1505, 'PARENT_TOOLS_HELP', '', 'Tools - Help', 'Tools - Help'),
(1506, 'TOOLS_HELP', 'PARENT_TOOLS_HELP', 'Tools to help you with some of the booking system needs.', 'Tools to help you with some of the booking system needs.'),
(1507, 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', '', 'Tools - Repair calendars settings', 'Tools - Repair calendars settings'),
(1508, 'TOOLS_REPAIR_CALENDARS_SETTINGS_TITLE', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Repair calendars settings', 'Repair calendars settings'),
(1509, 'TOOLS_REPAIR_CALENDARS_SETTINGS_CONFIRMATION', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Are you sure you want to start calendars settings repairs?', 'Are you sure you want to start calendars settings repairs?'),
(1510, 'TOOLS_REPAIR_CALENDARS_SETTINGS_REPAIRING', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Repairing calendars settings ...', 'Repairing calendars settings ...'),
(1511, 'TOOLS_REPAIR_CALENDARS_SETTINGS_SUCCESS', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'The settings have been repaired.', 'The settings have been repaired.'),
(1512, 'TOOLS_REPAIR_CALENDARS_SETTINGS_CALENDARS', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Calendars', 'Calendars'),
(1513, 'TOOLS_REPAIR_CALENDARS_SETTINGS_SETTINGS_DATABASE', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Settings database', 'Settings database'),
(1514, 'TOOLS_REPAIR_CALENDARS_SETTINGS_NOTIFICATIONS_DATABASE', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Notifications database', 'Notifications database'),
(1515, 'TOOLS_REPAIR_CALENDARS_SETTINGS_PAYMENT_DATABASE', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Payment database', 'Payment database'),
(1516, 'TOOLS_REPAIR_CALENDARS_SETTINGS_UNCHANGED', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Unchanged', 'Unchanged'),
(1517, 'TOOLS_REPAIR_CALENDARS_SETTINGS_REPAIRED', 'PARENT_TOOLS_REPAIR_CALENDARS_SETTINGS', 'Repaired', 'Repaired'),
(1518, 'PARENT_TOOLS_REPAIR_DATABASE_TEXT', '', 'Tools - Repair database & text', 'Tools - Repair database & text'),
(1519, 'TOOLS_REPAIR_DATABASE_TEXT_TITLE', 'PARENT_TOOLS_REPAIR_DATABASE_TEXT', 'Repair database & text', 'Repair database & text'),
(1520, 'TOOLS_REPAIR_DATABASE_TEXT_CONFIRMATION', 'PARENT_TOOLS_REPAIR_DATABASE_TEXT', 'Are you sure you want to verify the database & the text and repair them if needed?', 'Are you sure you want to verify the database & the text and repair them if needed?'),
(1521, 'TOOLS_REPAIR_DATABASE_TEXT_REPAIRING', 'PARENT_TOOLS_REPAIR_DATABASE_TEXT', 'Verifying and repairing the database & the text ...', 'Verifying and repairing the database & the text ...'),
(1522, 'TOOLS_REPAIR_DATABASE_TEXT_SUCCESS', 'PARENT_TOOLS_REPAIR_DATABASE_TEXT', 'The database & the text have been verified and repaired. The page will redirect shortly to Dashboard.', 'The database & the text have been verified and repaired. The page will redirect shortly to Dashboard.'),
(1523, 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', '', 'Tools - Repair search settings', 'Tools - Repair search settings'),
(1524, 'TOOLS_REPAIR_SEARCH_SETTINGS_TITLE', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Repair search settings', 'Repair search settings'),
(1525, 'TOOLS_REPAIR_SEARCH_SETTINGS_CONFIRMATION', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Are you sure you want to start search settings repairs?', 'Are you sure you want to start search settings repairs?'),
(1526, 'TOOLS_REPAIR_SEARCH_SETTINGS_REPAIRING', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Repairing search settings ...', 'Repairing search settings ...'),
(1527, 'TOOLS_REPAIR_SEARCH_SETTINGS_SUCCESS', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'The settings have been repaired.', 'The settings have been repaired.'),
(1528, 'TOOLS_REPAIR_SEARCH_SETTINGS_SEARCHES', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Searches', 'Searches'),
(1529, 'TOOLS_REPAIR_SEARCH_SETTINGS_SETTINGS_DATABASE', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Settings database', 'Settings database'),
(1530, 'TOOLS_REPAIR_SEARCH_SETTINGS_UNCHANGED', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Unchanged', 'Unchanged'),
(1531, 'TOOLS_REPAIR_SEARCH_SETTINGS_REPAIRED', 'PARENT_TOOLS_REPAIR_SEARCH_SETTINGS', 'Repaired', 'Repaired'),
(1532, 'PARENT_TRANSLATION', '', 'Translation', 'Translation'),
(1533, 'TRANSLATION_TITLE', 'PARENT_TRANSLATION', 'Translation', 'Translation'),
(1534, 'TRANSLATION_SUBMIT', 'PARENT_TRANSLATION', 'Manage translation', 'Manage translation'),
(1535, 'TRANSLATION_LOADED', 'PARENT_TRANSLATION', 'Translation has been loaded.', 'Translation has been loaded.'),
(1536, 'TRANSLATION_LANGUAGE', 'PARENT_TRANSLATION', 'Select language', 'Select language'),
(1537, 'TRANSLATION_TEXT_GROUP', 'PARENT_TRANSLATION', 'Select text group', 'Select text group'),
(1538, 'TRANSLATION_TEXT_GROUP_ALL', 'PARENT_TRANSLATION', 'All', 'All'),
(1539, 'TRANSLATION_SEARCH', 'PARENT_TRANSLATION', 'Search', 'Search'),
(1540, 'TRANSLATION_RESET', 'PARENT_TRANSLATION', 'Reset translation', 'Reset translation'),
(1541, 'TRANSLATION_RESET_CONFIRMATION', 'PARENT_TRANSLATION', 'Are you sure you want to reset all translation data? All your modifications are going to be overwritten.', 'Are you sure you want to reset all translation data? All your modifications are going to be overwritten.'),
(1542, 'TRANSLATION_RESETING', 'PARENT_TRANSLATION', 'Translation is resetting ...', 'Translation is resetting ...'),
(1543, 'TRANSLATION_RESET_SUCCESS', 'PARENT_TRANSLATION', 'The translation has reset. The page will refresh shortly.', 'The translation has reset. The page will refresh shortly.'),
(1544, 'PARENT_TRANSLATION_HELP', '', 'Translation - Help', 'Translation - Help'),
(1545, 'TRANSLATION_HELP', 'PARENT_TRANSLATION_HELP', 'Select the language & text group you want to translate.', 'Select the language & text group you want to translate.'),
(1546, 'TRANSLATION_SEARCH_HELP', 'PARENT_TRANSLATION_HELP', 'Use the search field to look & display the text you want.', 'Use the search field to look & display the text you want.'),
(1547, 'TRANSLATION_RESET_HELP', 'PARENT_TRANSLATION_HELP', 'If you want to use the translation that came with the plugin click "Reset translation" button. Note that all your modifications will be overwritten.', 'If you want to use the translation that came with the plugin click "Reset translation" button. Note that all your modifications will be overwritten.'),
(1548, 'PARENT_WIDGET', '', 'Widget', 'Widget'),
(1549, 'WIDGET_TITLE', 'PARENT_WIDGET', 'Pinpoint Booking System', 'Pinpoint Booking System'),
(1550, 'WIDGET_DESCRIPTION', 'PARENT_WIDGET', 'Select option you want to appear in the widget and ID(s) of the calendar(s).', 'Select option you want to appear in the widget and ID(s) of the calendar(s).'),
(1551, 'WIDGET_TITLE_LABEL', 'PARENT_WIDGET', 'Title:', 'Title:'),
(1552, 'WIDGET_SELECTION_LABEL', 'PARENT_WIDGET', 'Select action:', 'Select action:'),
(1553, 'WIDGET_SELECTION_ADD_CALENDAR', 'PARENT_WIDGET', 'Add calendar', 'Add calendar'),
(1554, 'WIDGET_SELECTION_ADD_SIDEBAR', 'PARENT_WIDGET', 'Add calendar sidebar', 'Add calendar sidebar'),
(1555, 'WIDGET_ID_LABEL', 'PARENT_WIDGET', 'Select calendar ID:', 'Select calendar ID:'),
(1556, 'WIDGET_NO_CALENDARS', 'PARENT_WIDGET', 'No calendars.', 'No calendars.'),
(1557, 'WIDGET_LANGUAGE_LABEL', 'PARENT_WIDGET', 'Select language:', 'Select language:') ;

#
# End of data contents of table `wpwyp2810161505_dopbsp_translation_en`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_dopbsp_woocommerce`
#

DROP TABLE IF EXISTS `wpwyp2810161505_dopbsp_woocommerce`;


#
# Table structure of table `wpwyp2810161505_dopbsp_woocommerce`
#

CREATE TABLE `wpwyp2810161505_dopbsp_woocommerce` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cart_item_key` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_item_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `product_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `calendar_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `language` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `currency_code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text CHARACTER SET utf8mb4 COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  KEY `cart_item_key` (`cart_item_key`),
  KEY `token` (`token`),
  KEY `order_item_id` (`order_item_id`),
  KEY `product_id` (`product_id`),
  KEY `date_created` (`date_created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wpwyp2810161505_dopbsp_woocommerce`
#

#
# End of data contents of table `wpwyp2810161505_dopbsp_woocommerce`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_links`
#

DROP TABLE IF EXISTS `wpwyp2810161505_links`;


#
# Table structure of table `wpwyp2810161505_links`
#

CREATE TABLE `wpwyp2810161505_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_links`
#

#
# End of data contents of table `wpwyp2810161505_links`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_options`
#

DROP TABLE IF EXISTS `wpwyp2810161505_options`;


#
# Table structure of table `wpwyp2810161505_options`
#

CREATE TABLE `wpwyp2810161505_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1030 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_options`
#
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wypozyczalniacykliniarek.com', 'yes'),
(2, 'home', 'http://wypozyczalniacykliniarek.com', 'yes'),
(3, 'blogname', 'Wypożyczalnia Cykliniarek', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'patryk@visibee.pl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:192:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:11:"produkty/?$";s:27:"index.php?post_type=product";s:41:"produkty/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:36:"produkty/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:28:"produkty/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:57:"kategoria-produktu/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:52:"kategoria-produktu/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:33:"kategoria-produktu/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:45:"kategoria-produktu/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:27:"kategoria-produktu/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:53:"tag-produktu/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:48:"tag-produktu/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:29:"tag-produktu/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:41:"tag-produktu/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:23:"tag-produktu/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"produkt/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"produkt/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"produkt/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produkt/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produkt/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"produkt/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"produkt/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"produkt/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"produkt/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"produkt/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"produkt/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"produkt/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"produkt/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"produkt/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"produkt/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"produkt/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"produkt/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"produkt/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"produkt/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produkt/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produkt/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"produkt/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"product_variation/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"product_variation/([^/]+)/embed/?$";s:50:"index.php?product_variation=$matches[1]&embed=true";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"product_variation/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"product_variation/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"shop_order_refund/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"shop_order_refund/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"shop_order_refund/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"shop_order_refund/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"shop_order_refund/([^/]+)/embed/?$";s:50:"index.php?shop_order_refund=$matches[1]&embed=true";s:38:"shop_order_refund/([^/]+)/trackback/?$";s:44:"index.php?shop_order_refund=$matches[1]&tb=1";s:46:"shop_order_refund/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&paged=$matches[2]";s:53:"shop_order_refund/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&cpage=$matches[2]";s:43:"shop_order_refund/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?shop_order_refund=$matches[1]&wc-api=$matches[3]";s:49:"shop_order_refund/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"shop_order_refund/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"shop_order_refund/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?shop_order_refund=$matches[1]&page=$matches[2]";s:34:"shop_order_refund/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"shop_order_refund/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"shop_order_refund/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"shop_order_refund/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=52&cpage=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:31:"cookie-notice/cookie-notice.php";i:1;s:37:"tinymce-advanced/tinymce-advanced.php";i:2;s:47:"tinymce-custom-styles/tinymce-custom-styles.php";i:3;s:60:"woocommerce-easy-booking-system/woocommerce-easy-booking.php";i:4;s:27:"woocommerce/woocommerce.php";i:5;s:31:"wp-migrate-db/wp-migrate-db.php";i:6;s:23:"wp-smushit/wp-smush.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:91:"\\public_html/wp-content/plugins/black-studio-tinymce-widget/black-studio-tinymce-widget.php";i:1;s:54:"\\public_html/wp-content/themes/wypozyczalnia/style.css";i:2;s:68:"\\public_html/wp-content/themes/wypozyczalnia/editor-style-shared.css";i:3;s:61:"\\public_html/wp-content/themes/wypozyczalnia/editor-style.css";i:4;s:0:"";}', 'no'),
(40, 'template', 'wypozyczalnia', 'yes'),
(41, 'stylesheet', 'wypozyczalnia', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:47:"tinymce-custom-styles/tinymce-custom-styles.php";s:13:"tcs_uninstall";s:24:"booking-system/dopbs.php";s:15:"DOPBSPUninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '52', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'wpwyp2810161505_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:131:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'cron', 'a:7:{i:1480982400;a:2:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1480985248;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1480986623;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1481028448;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1481029845;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1481040738;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(116, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1477660736;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(136, 'current_theme', '', 'yes'),
(137, 'theme_mods_wypozyczalnia', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:6:"header";i:2;s:10:"header-nav";i:2;s:10:"footer-nav";i:3;}}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'recently_activated', 'a:2:{s:66:"booking-and-rental-system-woocommerce/redq-rental-and-bookings.php";i:1480955545;s:24:"booking-system/dopbs.php";i:1480948646;}', 'yes'),
(145, 'wdev-frash', 'a:3:{s:7:"plugins";a:1:{s:23:"wp-smushit/wp-smush.php";i:1477660862;}s:5:"queue";a:1:{s:32:"fc50097023d0d34c5a66f6cddcf77694";a:3:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:4:"rate";s:7:"show_at";i:1481027691;}}s:4:"done";a:1:{i:0;a:6:{s:6:"plugin";s:23:"wp-smushit/wp-smush.php";s:4:"type";s:5:"email";s:7:"show_at";i:1477660862;s:5:"state";s:6:"ignore";s:4:"hash";s:32:"fc50097023d0d34c5a66f6cddcf77694";s:10:"handled_at";i:1480941295;}}}', 'no'),
(146, 'wp-smush-version', '2.4.5', 'no'),
(147, 'wp-smush-skip-redirect', '1', 'no'),
(148, 'wp-smush-install-type', 'new', 'no'),
(151, 'wp-smush-hide_upgrade_notice', '1', 'no'),
(185, 'tadv_settings', 'a:6:{s:9:"toolbar_1";s:141:"formatselect,bold,italic,blockquote,subscript,superscript,bullist,numlist,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo,redo";s:9:"toolbar_2";s:110:"styleselect,removeformat,media,image,wp_more,table,wp_help,strikethrough,underline,wp_code,code,indent,outdent";s:9:"toolbar_3";s:0:"";s:9:"toolbar_4";s:0:"";s:7:"options";s:27:"menubar,advlist,contextmenu";s:7:"plugins";s:35:"table,advlist,importcss,contextmenu";}', 'yes'),
(186, 'tadv_admin_settings', 'a:2:{s:7:"options";s:9:"importcss";s:16:"disabled_editors";s:0:"";}', 'yes'),
(187, 'tadv_version', '4000', 'yes'),
(190, 'wp-smush-hide_update_info', '1', 'no'),
(193, 'tcs_addstyledrop', 'a:5:{i:0;a:7:{s:5:"title";s:5:"intro";s:5:"block";s:1:"p";s:7:"classes";s:5:"intro";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:1;a:7:{s:5:"title";s:6:"medium";s:5:"block";s:1:"p";s:7:"classes";s:6:"medium";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:2;a:7:{s:5:"title";s:5:"small";s:5:"block";s:1:"p";s:7:"classes";s:5:"small";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:3;a:7:{s:5:"title";s:2:"lg";s:5:"block";s:1:"p";s:7:"classes";s:2:"lg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}i:4;a:7:{s:5:"title";s:3:"xlg";s:5:"block";s:1:"p";s:7:"classes";s:3:"xlg";s:6:"styles";a:0:{}s:10:"attributes";a:0:{}s:5:"exact";b:0;s:7:"wrapper";b:0;}}', 'yes'),
(196, 'tcs_locstyle', 'themes_directory', 'yes'),
(201, 'widget_black-studio-tinymce', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(253, 'cookie_notice_options', 'a:18:{s:12:"message_text";s:27:"Ta strona używa ciasteczek";s:11:"accept_text";s:2:"Ok";s:8:"see_more";s:3:"yes";s:12:"see_more_opt";a:4:{s:4:"text";s:12:"Więcej info";s:9:"link_type";s:4:"page";s:2:"id";i:45;s:4:"link";s:0:"";}s:11:"link_target";s:6:"_blank";s:11:"refuse_text";s:2:"No";s:11:"refuse_code";s:0:"";s:16:"on_scroll_offset";i:100;s:4:"time";s:5:"month";s:16:"script_placement";s:6:"footer";s:8:"position";s:6:"bottom";s:11:"hide_effect";s:4:"fade";s:9:"css_style";s:4:"none";s:6:"colors";a:2:{s:4:"text";s:7:"#000000";s:3:"bar";s:7:"#ffffff";}s:10:"refuse_opt";s:2:"no";s:9:"on_scroll";s:2:"no";s:19:"deactivation_delete";s:2:"no";s:9:"translate";b:0;}', 'no'),
(254, 'cookie_notice_version', '1.2.36.1', 'no'),
(265, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(329, 'acf_version', '5.4.8', 'yes'),
(354, 'category_children', 'a:0:{}', 'yes'),
(431, 'woocommerce_default_country', 'PL', 'yes'),
(432, 'woocommerce_allowed_countries', 'specific', 'yes'),
(433, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(434, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"PL";}', 'yes'),
(435, 'woocommerce_ship_to_countries', 'specific', 'yes'),
(436, 'woocommerce_specific_ship_to_countries', 'a:1:{i:0;s:2:"PL";}', 'yes'),
(437, 'woocommerce_default_customer_address', 'base', 'yes'),
(438, 'woocommerce_calc_taxes', 'yes', 'yes'),
(439, 'woocommerce_demo_store', 'no', 'yes'),
(440, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(441, 'woocommerce_currency', 'PLN', 'yes'),
(442, 'woocommerce_currency_pos', 'right', 'yes'),
(443, 'woocommerce_price_thousand_sep', '.', 'yes'),
(444, 'woocommerce_price_decimal_sep', ',', 'yes'),
(445, 'woocommerce_price_num_decimals', '0', 'yes'),
(446, 'woocommerce_weight_unit', 'kg', 'yes'),
(447, 'woocommerce_dimension_unit', 'cm', 'yes'),
(448, 'woocommerce_enable_review_rating', 'no', 'yes'),
(449, 'woocommerce_review_rating_required', 'yes', 'no'),
(450, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(451, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(452, 'woocommerce_shop_page_id', '60', 'yes'),
(453, 'woocommerce_shop_page_display', '', 'yes'),
(454, 'woocommerce_category_archive_display', '', 'yes'),
(455, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(456, 'woocommerce_cart_redirect_after_add', 'yes', 'yes'),
(457, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(458, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(459, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:1;}', 'yes'),
(460, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:1;}', 'yes'),
(461, 'woocommerce_enable_lightbox', 'no', 'yes'),
(462, 'woocommerce_manage_stock', 'yes', 'yes'),
(463, 'woocommerce_hold_stock_minutes', '', 'no'),
(464, 'woocommerce_notify_low_stock', 'yes', 'no'),
(465, 'woocommerce_notify_no_stock', 'yes', 'no'),
(466, 'woocommerce_stock_email_recipient', 'patryk@visibee.pl', 'no'),
(467, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(468, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(469, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(470, 'woocommerce_stock_format', 'no_amount', 'yes'),
(471, 'woocommerce_file_download_method', 'force', 'no'),
(472, 'woocommerce_downloads_require_login', 'no', 'no'),
(473, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(474, 'woocommerce_prices_include_tax', 'yes', 'yes'),
(475, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(476, 'woocommerce_shipping_tax_class', '', 'yes'),
(477, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(478, 'woocommerce_tax_classes', 'Reduced Rate\r\nZero Rate', 'yes'),
(479, 'woocommerce_tax_display_shop', 'incl', 'yes'),
(480, 'woocommerce_tax_display_cart', 'incl', 'no'),
(481, 'woocommerce_price_display_suffix', '', 'yes'),
(482, 'woocommerce_tax_total_display', 'itemized', 'no'),
(483, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(484, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(485, 'woocommerce_ship_to_destination', 'billing', 'no'),
(486, 'woocommerce_enable_coupons', 'yes', 'yes'),
(487, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(488, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(489, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(490, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(491, 'woocommerce_cart_page_id', '191', 'yes'),
(492, 'woocommerce_checkout_page_id', '192', 'yes'),
(493, 'woocommerce_terms_page_id', '142', 'no'),
(494, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(495, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(496, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(497, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(498, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(499, 'woocommerce_myaccount_page_id', '193', 'yes'),
(500, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(501, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(502, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(503, 'woocommerce_registration_generate_username', 'yes', 'no'),
(504, 'woocommerce_registration_generate_password', 'no', 'no') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(505, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(506, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(507, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(508, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(509, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(510, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(511, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(512, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(513, 'woocommerce_email_from_name', 'Wypożyczalnia Cykliniarek', 'no'),
(514, 'woocommerce_email_from_address', 'patryk@visibee.pl', 'no'),
(515, 'woocommerce_email_header_image', '', 'no'),
(516, 'woocommerce_email_footer_text', 'Wypożyczalnia Cykliniarek - Powered by WooCommerce', 'no'),
(517, 'woocommerce_email_base_color', '#557da1', 'no'),
(518, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(519, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(520, 'woocommerce_email_text_color', '#505050', 'no'),
(521, 'woocommerce_api_enabled', 'yes', 'yes'),
(525, 'woocommerce_db_version', '2.6.8', 'yes'),
(526, 'woocommerce_version', '2.6.8', 'yes'),
(527, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(530, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(531, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(532, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(533, 'widget_woocommerce_price_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(534, 'widget_woocommerce_product_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(535, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(536, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(537, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(538, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(539, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(540, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(541, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(545, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(553, 'woocommerce_allow_tracking', 'no', 'yes'),
(571, 'product_cat_children', 'a:0:{}', 'yes'),
(596, 'woocommerce_bacs_settings', 'a:5:{s:7:"enabled";s:3:"yes";s:5:"title";s:20:"Direct Bank Transfer";s:11:"description";s:173:"Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.";s:12:"instructions";s:173:"Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.";s:15:"account_details";s:0:"";}', 'yes'),
(597, 'woocommerce_bacs_accounts', 'a:1:{i:0;a:6:{s:12:"account_name";s:0:"";s:14:"account_number";s:0:"";s:9:"bank_name";s:0:"";s:9:"sort_code";s:0:"";s:4:"iban";s:0:"";s:3:"bic";s:0:"";}}', 'yes'),
(599, 'woocommerce_cheque_settings', 'a:4:{s:7:"enabled";s:2:"no";s:5:"title";s:14:"Check Payments";s:11:"description";s:98:"Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.";s:12:"instructions";s:98:"Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.";}', 'yes'),
(601, 'woocommerce_cod_settings', 'a:6:{s:7:"enabled";s:3:"yes";s:5:"title";s:16:"Cash on Delivery";s:11:"description";s:28:"Pay with cash upon delivery.";s:12:"instructions";s:28:"Pay with cash upon delivery.";s:18:"enable_for_methods";s:0:"";s:18:"enable_for_virtual";s:3:"yes";}', 'yes'),
(603, 'woocommerce_paypal_settings', 'a:18:{s:7:"enabled";s:2:"no";s:5:"title";s:6:"PayPal";s:11:"description";s:85:"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.";s:5:"email";s:17:"patryk@visibee.pl";s:8:"testmode";s:2:"no";s:5:"debug";s:2:"no";s:8:"advanced";s:0:"";s:14:"receiver_email";s:17:"patryk@visibee.pl";s:14:"identity_token";s:0:"";s:14:"invoice_prefix";s:3:"WC-";s:13:"send_shipping";s:2:"no";s:16:"address_override";s:2:"no";s:13:"paymentaction";s:4:"sale";s:10:"page_style";s:0:"";s:11:"api_details";s:0:"";s:12:"api_username";s:19:"admin_wypozyczalnia";s:12:"api_password";s:18:"G$Bu1UCLw!xI0tmMjB";s:13:"api_signature";s:0:"";}', 'yes'),
(605, 'woocommerce_gateway_order', 'a:4:{s:4:"bacs";i:0;s:6:"cheque";i:1;s:3:"cod";i:2;s:6:"paypal";i:3;}', 'yes'),
(643, 'options_content_0_show', '1', 'no'),
(644, '_options_content_0_show', 'field_583c0ba566b90', 'no'),
(645, 'options_content_0_section_bg', 'red', 'no'),
(646, '_options_content_0_section_bg', 'field_583c0ba566f83', 'no'),
(647, 'options_content_0_section_intro', '<h1>Skontaktuj się z nami</h1>', 'no'),
(648, '_options_content_0_section_intro', 'field_583c0ba56774c', 'no'),
(649, 'options_content_0_layout_0_contact_0_icon', 'email', 'no'),
(650, '_options_content_0_layout_0_contact_0_icon', 'field_583c0ba6c5ecd', 'no'),
(651, 'options_content_0_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>', 'no'),
(652, '_options_content_0_layout_0_contact_0_label', 'field_583c0ba6c629f', 'no'),
(653, 'options_content_0_layout_0_contact_1_icon', 'telefon', 'no'),
(654, '_options_content_0_layout_0_contact_1_icon', 'field_583c0ba6c5ecd', 'no'),
(655, 'options_content_0_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>', 'no'),
(656, '_options_content_0_layout_0_contact_1_label', 'field_583c0ba6c629f', 'no'),
(657, 'options_content_0_layout_0_contact_2_icon', 'pinezka', 'no'),
(658, '_options_content_0_layout_0_contact_2_icon', 'field_583c0ba6c5ecd', 'no'),
(659, 'options_content_0_layout_0_contact_2_label', 'Radzymińska 98,\r\n03-574 Warszawa', 'no'),
(660, '_options_content_0_layout_0_contact_2_label', 'field_583c0ba6c629f', 'no'),
(661, 'options_content_0_layout_0_contact', '3', 'no'),
(662, '_options_content_0_layout_0_contact', 'field_583c0ba5ed167', 'no'),
(663, 'options_content_0_layout_0_partners_label', 'Nasz partner:', 'no'),
(664, '_options_content_0_layout_0_partners_label', 'field_583c0ba5ed552', 'no'),
(665, 'options_content_0_layout_0_partners_0_img', '138', 'no'),
(666, '_options_content_0_layout_0_partners_0_img', 'field_583c0ba7362fa', 'no'),
(667, 'options_content_0_layout_0_partners', '1', 'no'),
(668, '_options_content_0_layout_0_partners', 'field_583c0ba5ed955', 'no'),
(669, 'options_content_0_layout_0_cooperators_label', 'Współpracujemy z:', 'no'),
(670, '_options_content_0_layout_0_cooperators_label', 'field_583c0ba5edd30', 'no'),
(671, 'options_content_0_layout_0_cooperators_0_img', '140', 'no'),
(672, '_options_content_0_layout_0_cooperators_0_img', 'field_583c0ba7849e0', 'no'),
(673, 'options_content_0_layout_0_cooperators_1_img', '139', 'no'),
(674, '_options_content_0_layout_0_cooperators_1_img', 'field_583c0ba7849e0', 'no'),
(675, 'options_content_0_layout_0_cooperators', '2', 'no'),
(676, '_options_content_0_layout_0_cooperators', 'field_583c0ba5ee119', 'no'),
(677, 'options_content_0_layout', 'a:1:{i:0;s:6:"footer";}', 'no'),
(678, '_options_content_0_layout', 'field_583c0ba567b49', 'no'),
(679, 'options_content', '1', 'no'),
(680, '_options_content', 'field_583c0ba55100f', 'no'),
(712, 'WPLANG', 'pl_PL', 'yes'),
(718, 'woocommerce_permalinks', 'a:4:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:0:"";}', 'yes'),
(775, 'pa_pre-price_children', 'a:0:{}', 'yes'),
(813, 'options_content--footer_0_show', '1', 'no'),
(814, '_options_content--footer_0_show', 'field_583c0ba566b90', 'no'),
(815, 'options_content--footer_0_section_bg', 'red', 'no'),
(816, '_options_content--footer_0_section_bg', 'field_583c0ba566f83', 'no'),
(817, 'options_content--footer_0_section_intro', '<h1>Skontaktuj się z nami</h1>', 'no'),
(818, '_options_content--footer_0_section_intro', 'field_583c0ba56774c', 'no'),
(819, 'options_content--footer_0_layout_0_contact_0_icon', 'email', 'no'),
(820, '_options_content--footer_0_layout_0_contact_0_icon', 'field_583c0ba6c5ecd', 'no'),
(821, 'options_content--footer_0_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>', 'no'),
(822, '_options_content--footer_0_layout_0_contact_0_label', 'field_583c0ba6c629f', 'no'),
(823, 'options_content--footer_0_layout_0_contact_1_icon', 'telefon', 'no'),
(824, '_options_content--footer_0_layout_0_contact_1_icon', 'field_583c0ba6c5ecd', 'no'),
(825, 'options_content--footer_0_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>', 'no'),
(826, '_options_content--footer_0_layout_0_contact_1_label', 'field_583c0ba6c629f', 'no'),
(827, 'options_content--footer_0_layout_0_contact_2_icon', 'pinezka', 'no'),
(828, '_options_content--footer_0_layout_0_contact_2_icon', 'field_583c0ba6c5ecd', 'no'),
(829, 'options_content--footer_0_layout_0_contact_2_label', 'Radzymińska 98,\r\n03-574 Warszawa', 'no'),
(830, '_options_content--footer_0_layout_0_contact_2_label', 'field_583c0ba6c629f', 'no') ;
INSERT INTO `wpwyp2810161505_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(831, 'options_content--footer_0_layout_0_contact', '3', 'no'),
(832, '_options_content--footer_0_layout_0_contact', 'field_583c0ba5ed167', 'no'),
(833, 'options_content--footer_0_layout_0_partners_label', 'Nasz partner:', 'no'),
(834, '_options_content--footer_0_layout_0_partners_label', 'field_583c0ba5ed552', 'no'),
(835, 'options_content--footer_0_layout_0_partners_0_img', '138', 'no'),
(836, '_options_content--footer_0_layout_0_partners_0_img', 'field_583c0ba7362fa', 'no'),
(837, 'options_content--footer_0_layout_0_partners', '1', 'no'),
(838, '_options_content--footer_0_layout_0_partners', 'field_583c0ba5ed955', 'no'),
(839, 'options_content--footer_0_layout_0_cooperators_label', 'Współpracujemy z:', 'no'),
(840, '_options_content--footer_0_layout_0_cooperators_label', 'field_583c0ba5edd30', 'no'),
(841, 'options_content--footer_0_layout_0_cooperators_0_img', '140', 'no'),
(842, '_options_content--footer_0_layout_0_cooperators_0_img', 'field_583c0ba7849e0', 'no'),
(843, 'options_content--footer_0_layout_0_cooperators_1_img', '139', 'no'),
(844, '_options_content--footer_0_layout_0_cooperators_1_img', 'field_583c0ba7849e0', 'no'),
(845, 'options_content--footer_0_layout_0_cooperators', '2', 'no'),
(846, '_options_content--footer_0_layout_0_cooperators', 'field_583c0ba5ee119', 'no'),
(847, 'options_content--footer_0_layout', 'a:1:{i:0;s:6:"footer";}', 'no'),
(848, '_options_content--footer_0_layout', 'field_583c0ba567b49', 'no'),
(849, 'options_content--footer_0_content_after', '', 'no'),
(850, '_options_content--footer_0_content_after', 'field_584011ac7b85c', 'no'),
(851, 'options_content--footer', '1', 'no'),
(852, '_options_content--footer', 'field_583c0ba55100f', 'no'),
(853, 'options_content--header', '1', 'no'),
(854, '_options_content--header', 'field_584042ea01135', 'no'),
(855, 'options_contact', '<a href="tel:+48668387017">668 387 017</a>\r\n<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalniacykliniarek.com</a>', 'no'),
(856, '_options_contact', 'field_584044ab994e9', 'no'),
(857, 'options_content--header_0_show', '1', 'no'),
(858, '_options_content--header_0_show', 'field_584042ea1aba6', 'no'),
(859, 'options_content--header_0_section_bg', 'red', 'no'),
(860, '_options_content--header_0_section_bg', 'field_584042ea1afed', 'no'),
(861, 'options_content--header_0_section_intro', '', 'no'),
(862, '_options_content--header_0_section_intro', 'field_584042ea1b7c5', 'no'),
(863, 'options_content--header_0_layout_0_content', '<strong>UWAGA!</strong> Aktualnie jesteśmy w fazie testów i działamy tylko na terenie miasta Warszawy.', 'no'),
(864, '_options_content--header_0_layout_0_content', 'field_58404456994e6', 'no'),
(865, 'options_content--header_0_layout', 'a:1:{i:0;s:12:"notification";}', 'no'),
(866, '_options_content--header_0_layout', 'field_584042ea1bb70', 'no'),
(867, 'options_content--header_0_content_after', '', 'no'),
(868, '_options_content--header_0_content_after', 'field_584042ea1bf50', 'no'),
(913, 'widget_dopbspwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(914, 'DOPBSP_db_version', '2.196', 'yes'),
(915, 'DOPBSP_db_version_api_keys', '1', 'yes'),
(916, 'DOPBSP_db_version_calendars', '1', 'yes'),
(917, 'DOPBSP_db_version_coupons', '1', 'yes'),
(918, 'DOPBSP_db_version_days', '1', 'yes'),
(919, 'DOPBSP_db_version_days_available', '1', 'yes'),
(920, 'DOPBSP_db_version_days_unavailable', '1', 'yes'),
(921, 'DOPBSP_db_version_discounts', '1', 'yes'),
(922, 'DOPBSP_db_version_discounts_items', '1', 'yes'),
(923, 'DOPBSP_db_version_discounts_items_rules', '1', 'yes'),
(924, 'DOPBSP_db_version_emails', '1', 'yes'),
(925, 'DOPBSP_db_version_emails_translation', '1', 'yes'),
(926, 'DOPBSP_db_version_extras', '1', 'yes'),
(927, 'DOPBSP_db_version_extras_groups', '1.001', 'yes'),
(928, 'DOPBSP_db_version_extras_groups_items', '1.001', 'yes'),
(929, 'DOPBSP_db_version_fees', '1', 'yes'),
(930, 'DOPBSP_db_version_forms', '1', 'yes'),
(931, 'DOPBSP_db_version_forms_fields', '1.001', 'yes'),
(932, 'DOPBSP_db_version_forms_select_options', '1', 'yes'),
(933, 'DOPBSP_db_version_languages', '1', 'yes'),
(934, 'DOPBSP_db_version_locations', '1', 'yes'),
(935, 'DOPBSP_db_version_models', '1', 'yes'),
(936, 'DOPBSP_db_version_reservations', '1.004', 'yes'),
(937, 'DOPBSP_db_version_rules', '1', 'yes'),
(938, 'DOPBSP_db_version_settings', '1', 'yes'),
(939, 'DOPBSP_db_version_settings_calendar', '1', 'yes'),
(940, 'DOPBSP_db_version_settings_notifications', '1', 'yes'),
(941, 'DOPBSP_db_version_settings_payment', '1', 'yes'),
(942, 'DOPBSP_db_version_translation', '1', 'yes'),
(943, 'DOPBSP_db_version_woocommerce', '1.002', 'yes'),
(977, 'pa_jednostka-miary_children', 'a:0:{}', 'yes'),
(1014, 'easy_booking_settings', 'a:16:{s:27:"easy_booking_calendar_theme";s:7:"classic";s:29:"easy_booking_background_color";s:7:"#FFFFFF";s:23:"easy_booking_main_color";s:7:"#bf1f24";s:23:"easy_booking_text_color";s:7:"#000000";s:22:"easy_booking_calc_mode";s:4:"days";s:17:"post_all_bookable";s:1:"1";s:18:"easy_booking_dates";s:3:"two";s:21:"easy_booking_duration";s:4:"days";s:28:"easy_booking_custom_duration";i:1;s:24:"easy_booking_booking_min";s:1:"0";s:24:"easy_booking_booking_max";s:1:"0";s:33:"easy_booking_first_available_date";s:1:"0";s:21:"easy_booking_max_year";i:2021;s:22:"easy_booking_first_day";s:1:"1";s:25:"easy_booking_all_bookable";s:2:"no";s:21:"easy_booking_year_max";i:2021;}', 'yes'),
(1015, 'easy_booking_data_needs_update', '1', 'yes'),
(1027, 'easy_booking_display_notice_wceb-addons', '1', 'yes') ;

#
# End of data contents of table `wpwyp2810161505_options`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_postmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_postmeta`;


#
# Table structure of table `wpwyp2810161505_postmeta`
#

CREATE TABLE `wpwyp2810161505_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=9899 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_postmeta`
#
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1480949369:1'),
(6, 5, '_wp_page_template', 'page-templates/content-page.php'),
(13, 35, '_wp_attached_file', '2016/11/cykliniarki.jpg'),
(14, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:825;s:6:"height";i:455;s:4:"file";s:23:"2016/11/cykliniarki.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"cykliniarki-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"cykliniarki-300x165.jpg";s:5:"width";i:300;s:6:"height";i:165;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"cykliniarki-768x424.jpg";s:5:"width";i:768;s:6:"height";i:424;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(15, 35, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.8365642069187098;s:5:"bytes";i:7317;s:11:"size_before";i:93370;s:10:"size_after";i:86053;s:4:"time";d:0.20000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:4:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:9.8800000000000008;s:5:"bytes";i:349;s:11:"size_before";i:3533;s:10:"size_after";i:3184;s:4:"time";d:0.040000000000000001;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:8.9299999999999997;s:5:"bytes";i:695;s:11:"size_before";i:7784;s:10:"size_after";i:7089;s:4:"time";d:0.02;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:11.65;s:5:"bytes";i:3324;s:11:"size_before";i:28532;s:10:"size_after";i:25208;s:4:"time";d:0.059999999999999998;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:5.5099999999999998;s:5:"bytes";i:2949;s:11:"size_before";i:53521;s:10:"size_after";i:50572;s:4:"time";d:0.080000000000000002;}}}'),
(16, 45, '_edit_last', '1'),
(17, 45, '_wp_page_template', 'default'),
(18, 45, '_edit_lock', '1478346498:1'),
(28, 48, '_edit_last', '1'),
(29, 48, '_wp_page_template', 'page-templates/calc.php'),
(30, 48, '_edit_lock', '1478539322:1'),
(31, 52, '_edit_last', '1'),
(32, 52, '_edit_lock', '1480600295:1'),
(33, 52, '_wp_page_template', 'page-templates/home-page.php'),
(34, 54, '_menu_item_type', 'post_type'),
(35, 54, '_menu_item_menu_item_parent', '0'),
(36, 54, '_menu_item_object_id', '52'),
(37, 54, '_menu_item_object', 'page'),
(38, 54, '_menu_item_target', ''),
(39, 54, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(40, 54, '_menu_item_xfn', ''),
(41, 54, '_menu_item_url', ''),
(42, 54, '_menu_item_orphaned', '1479904201'),
(43, 55, '_menu_item_type', 'post_type'),
(44, 55, '_menu_item_menu_item_parent', '0'),
(45, 55, '_menu_item_object_id', '48'),
(46, 55, '_menu_item_object', 'page'),
(47, 55, '_menu_item_target', ''),
(48, 55, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(49, 55, '_menu_item_xfn', ''),
(50, 55, '_menu_item_url', ''),
(51, 55, '_menu_item_orphaned', '1479904204'),
(52, 56, '_menu_item_type', 'post_type'),
(53, 56, '_menu_item_menu_item_parent', '0'),
(54, 56, '_menu_item_object_id', '45'),
(55, 56, '_menu_item_object', 'page'),
(56, 56, '_menu_item_target', ''),
(57, 56, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(58, 56, '_menu_item_xfn', ''),
(59, 56, '_menu_item_url', ''),
(60, 56, '_menu_item_orphaned', '1479904205'),
(61, 57, '_edit_last', '1'),
(62, 57, '_wp_page_template', 'default'),
(63, 57, '_edit_lock', '1479904094:1'),
(64, 59, '_edit_last', '1'),
(65, 59, '_edit_lock', '1480955382:1'),
(66, 59, '_wp_page_template', 'page-templates/content-page.php'),
(67, 60, '_edit_last', '1'),
(68, 60, '_wp_page_template', 'default'),
(69, 60, '_edit_lock', '1480510803:1'),
(70, 61, '_edit_last', '1'),
(71, 61, '_wp_page_template', 'default'),
(72, 61, '_edit_lock', '1479904129:1'),
(73, 62, '_edit_last', '1'),
(74, 62, '_wp_page_template', 'default'),
(75, 62, '_edit_lock', '1479904125:1'),
(76, 67, '_menu_item_type', 'post_type'),
(77, 67, '_menu_item_menu_item_parent', '0'),
(78, 67, '_menu_item_object_id', '62'),
(79, 67, '_menu_item_object', 'page'),
(80, 67, '_menu_item_target', ''),
(81, 67, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(82, 67, '_menu_item_xfn', ''),
(83, 67, '_menu_item_url', ''),
(85, 68, '_menu_item_type', 'post_type'),
(86, 68, '_menu_item_menu_item_parent', '0'),
(87, 68, '_menu_item_object_id', '61'),
(88, 68, '_menu_item_object', 'page'),
(89, 68, '_menu_item_target', ''),
(90, 68, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(91, 68, '_menu_item_xfn', ''),
(92, 68, '_menu_item_url', ''),
(94, 69, '_menu_item_type', 'post_type'),
(95, 69, '_menu_item_menu_item_parent', '0'),
(96, 69, '_menu_item_object_id', '60'),
(97, 69, '_menu_item_object', 'page'),
(98, 69, '_menu_item_target', ''),
(99, 69, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(100, 69, '_menu_item_xfn', ''),
(101, 69, '_menu_item_url', ''),
(103, 70, '_menu_item_type', 'post_type'),
(104, 70, '_menu_item_menu_item_parent', '0'),
(105, 70, '_menu_item_object_id', '59'),
(106, 70, '_menu_item_object', 'page'),
(107, 70, '_menu_item_target', ''),
(108, 70, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(109, 70, '_menu_item_xfn', ''),
(110, 70, '_menu_item_url', ''),
(112, 71, '_menu_item_type', 'post_type'),
(113, 71, '_menu_item_menu_item_parent', '0'),
(114, 71, '_menu_item_object_id', '57'),
(115, 71, '_menu_item_object', 'page'),
(116, 71, '_menu_item_target', ''),
(117, 71, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(118, 71, '_menu_item_xfn', ''),
(119, 71, '_menu_item_url', ''),
(121, 72, '_menu_item_type', 'post_type'),
(122, 72, '_menu_item_menu_item_parent', '0'),
(123, 72, '_menu_item_object_id', '52') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(124, 72, '_menu_item_object', 'page'),
(125, 72, '_menu_item_target', ''),
(126, 72, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(127, 72, '_menu_item_xfn', ''),
(128, 72, '_menu_item_url', ''),
(130, 73, '_edit_last', '1'),
(131, 73, '_edit_lock', '1480941940:1'),
(132, 80, '_wp_attached_file', '2016/11/fig.png'),
(133, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1048;s:6:"height";i:562;s:4:"file";s:15:"2016/11/fig.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"fig-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:15:"fig-300x161.png";s:5:"width";i:300;s:6:"height";i:161;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:15:"fig-768x412.png";s:5:"width";i:768;s:6:"height";i:412;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:16:"fig-1024x549.png";s:5:"width";i:1024;s:6:"height";i:549;s:9:"mime-type";s:9:"image/png";}s:21:"section-background-sm";a:4:{s:4:"file";s:15:"fig-992x532.png";s:5:"width";i:992;s:6:"height";i:532;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(137, 80, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:1973097;s:10:"size_after";i:1973097;s:4:"time";d:1.9199999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:5:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:46174;s:10:"size_after";i:46174;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:87908;s:10:"size_after";i:87908;s:4:"time";d:0.16;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:447161;s:10:"size_after";i:447161;s:4:"time";d:0.34999999999999998;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:712145;s:10:"size_after";i:712145;s:4:"time";d:0.67000000000000004;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:679709;s:10:"size_after";i:679709;s:4:"time";d:0.70999999999999996;}}}'),
(138, 52, 'content_0_section_intro', ''),
(139, 52, '_content_0_section_intro', 'field_5835d59224057'),
(140, 52, 'content_0_content_0_image', ''),
(141, 52, '_content_0_content_0_image', 'field_5835d5b924059'),
(142, 52, 'content_0_content_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(143, 52, '_content_0_content_0_iframe', 'field_5835d5ea2405b'),
(144, 52, 'content_0_content_0_caption', ''),
(145, 52, '_content_0_content_0_caption', 'field_5835d5ca2405a'),
(146, 52, 'content_0_content', 'a:1:{i:0;s:5:"media";}'),
(147, 52, '_content_0_content', 'field_5835d59b24058'),
(148, 52, 'content', '10'),
(149, 52, '_content', 'field_5835d51f24055'),
(168, 52, 'content_0_layout', 'a:1:{i:0;s:18:"sprawdz-dostepnosc";}'),
(169, 52, '_content_0_layout', 'field_5835d59b24058'),
(190, 52, 'content_1_section_intro', ''),
(191, 52, '_content_1_section_intro', 'field_5835d59224057'),
(192, 52, 'content_1_layout_0_image', ''),
(193, 52, '_content_1_layout_0_image', 'field_5835d5b924059'),
(194, 52, 'content_1_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(195, 52, '_content_1_layout_0_iframe', 'field_5835d5ea2405b'),
(196, 52, 'content_1_layout_0_caption', ''),
(197, 52, '_content_1_layout_0_caption', 'field_5835d5ca2405a'),
(198, 52, 'content_1_layout', 'a:1:{i:0;s:12:"tani-wynajem";}'),
(199, 52, '_content_1_layout', 'field_5835d59b24058'),
(320, 52, 'content_0_section_bg', 'red'),
(321, 52, '_content_0_section_bg', 'field_5836c16c062d1'),
(322, 52, 'content_1_section_bg', 'img'),
(323, 52, '_content_1_section_bg', 'field_5836c16c062d1'),
(392, 52, 'content_2_section_bg', 'black'),
(393, 52, '_content_2_section_bg', 'field_5836c16c062d1'),
(394, 52, 'content_2_section_intro', '<h1>Jak to działa?</h1>\r\nLorem ipsum dolor sit amet enim. Etiam ullamcorper convallis ac, laoreet enim.\r\nSuspendisse a pellentesque dui, non felis.'),
(395, 52, '_content_2_section_intro', 'field_5835d59224057'),
(396, 52, 'content_2_layout_0_image', ''),
(397, 52, '_content_2_layout_0_image', 'field_5835d5b924059'),
(398, 52, 'content_2_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(399, 52, '_content_2_layout_0_iframe', 'field_5835d5ea2405b'),
(400, 52, 'content_2_layout_0_caption', ''),
(401, 52, '_content_2_layout_0_caption', 'field_5835d5ca2405a'),
(402, 52, 'content_2_layout', 'a:1:{i:0;s:13:"jak-to-dziala";}'),
(403, 52, '_content_2_layout', 'field_5835d59b24058'),
(636, 52, 'content_0_section_bg_img', '112'),
(637, 52, '_content_0_section_bg_img', 'field_5836c1c4062d2'),
(638, 52, 'content_1_layout_0_step_0_icon', 'mouse'),
(639, 52, '_content_1_layout_0_step_0_icon', 'field_5836c4722cf71'),
(640, 52, 'content_1_layout_0_step_0_label', 'Zarezerwuj online'),
(641, 52, '_content_1_layout_0_step_0_label', 'field_5836c4db2cf72'),
(642, 52, 'content_1_layout_0_step_1_icon', 'trolley'),
(643, 52, '_content_1_layout_0_step_1_icon', 'field_5836c4722cf71'),
(644, 52, 'content_1_layout_0_step_1_label', 'Kurier dostarczy cykliniarkę pod Twój dom'),
(645, 52, '_content_1_layout_0_step_1_label', 'field_5836c4db2cf72'),
(646, 52, 'content_1_layout_0_step_2_icon', 'cykliniarka'),
(647, 52, '_content_1_layout_0_step_2_icon', 'field_5836c4722cf71'),
(648, 52, 'content_1_layout_0_step_2_label', 'Wycyklinuj podłogę'),
(649, 52, '_content_1_layout_0_step_2_label', 'field_5836c4db2cf72'),
(650, 52, 'content_1_layout_0_step_3_icon', 'truck'),
(651, 52, '_content_1_layout_0_step_3_icon', 'field_5836c4722cf71'),
(652, 52, 'content_1_layout_0_step_3_label', 'Kurier odbierze cykliniarkę od Ciebie'),
(653, 52, '_content_1_layout_0_step_3_label', 'field_5836c4db2cf72'),
(654, 52, 'content_1_layout_0_step', '4'),
(655, 52, '_content_1_layout_0_step', 'field_5836c4452cf70'),
(656, 52, 'content_3_section_bg', 'white'),
(657, 52, '_content_3_section_bg', 'field_5836c16c062d1'),
(658, 52, 'content_3_section_intro', '<h1>Każdy może\r\ncyklinować</h1>\r\nNasze maszyny są tak proste w obsłudze, że nie\r\npotrzebujesz nawet specjalistycznego szkolenia.\r\nWystarczy Ci krótki instruktarz, który\r\nprzygotowaliśmy w formie wideo na YouTube.\r\n\r\n<span class="btn--fill btn btn--primary">Dowiedz się więcej</span>'),
(659, 52, '_content_3_section_intro', 'field_5835d59224057'),
(666, 52, 'content_3_layout', 'a:1:{i:0;s:5:"media";}'),
(667, 52, '_content_3_layout', 'field_5835d59b24058'),
(740, 52, 'content_0_layout_0_content', '<h1>Tani\r\nwynajem od\r\n<strong>50zł\r\n</strong>za dobę</h1>\r\n<span class="btn btn--primary btn--fill">Sprawdź ceny</span>'),
(741, 52, '_content_0_layout_0_content', 'field_5836ca8eb9160'),
(964, 52, 'content_4_section_bg', 'img-dark'),
(965, 52, '_content_4_section_bg', 'field_5836c16c062d1'),
(966, 52, 'content_4_section_intro', '<h1>Opinie o wypożyczalni</h1>\r\n&nbsp;\r\n\r\n<img class="alignnone size-medium wp-image-166" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/ocena-300x72.png" alt="ocena" width="300" height="72" />\r\n<p class="lg">Ocena 4,7 na 5 gwiazdek</p>'),
(967, 52, '_content_4_section_intro', 'field_5835d59224057'),
(968, 52, 'content_4_layout_0_image', '80'),
(969, 52, '_content_4_layout_0_image', 'field_5835d5b924059'),
(970, 52, 'content_4_layout_0_iframe', ''),
(971, 52, '_content_4_layout_0_iframe', 'field_5835d5ea2405b'),
(972, 52, 'content_4_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(973, 52, '_content_4_layout_0_caption', 'field_5835d5ca2405a'),
(974, 52, 'content_4_layout', 'a:1:{i:0;s:6:"opinie";}'),
(975, 52, '_content_4_layout', 'field_5835d59b24058'),
(1056, 107, '_wp_attached_file', '2016/11/mapa.png'),
(1057, 107, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:622;s:6:"height";i:602;s:4:"file";s:16:"2016/11/mapa.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"mapa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"mapa-300x290.png";s:5:"width";i:300;s:6:"height";i:290;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1058, 107, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:23.692348901767886;s:5:"bytes";i:19459;s:11:"size_before";i:82132;s:10:"size_after";i:62673;s:4:"time";d:0.52000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:30.75;s:5:"bytes";i:3534;s:11:"size_before";i:11491;s:10:"size_after";i:7957;s:4:"time";d:0.089999999999999997;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:31.289999999999999;s:5:"bytes";i:11245;s:11:"size_before";i:35941;s:10:"size_after";i:24696;s:4:"time";d:0.14999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:13.49;s:5:"bytes";i:4680;s:11:"size_before";i:34700;s:10:"size_after";i:30020;s:4:"time";d:0.28000000000000003;}}}'),
(1379, 112, '_wp_attached_file', '2016/11/main.jpg'),
(1380, 112, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3100;s:6:"height";i:1640;s:4:"file";s:16:"2016/11/main.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"main-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"main-300x159.jpg";s:5:"width";i:300;s:6:"height";i:159;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"main-768x406.jpg";s:5:"width";i:768;s:6:"height";i:406;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"main-1024x542.jpg";s:5:"width";i:1024;s:6:"height";i:542;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:16:"main-992x525.jpg";s:5:"width";i:992;s:6:"height";i:525;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:17:"main-1200x635.jpg";s:5:"width";i:1200;s:6:"height";i:635;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-lg";a:4:{s:4:"file";s:17:"main-1600x846.jpg";s:5:"width";i:1600;s:6:"height";i:846;s:9:"mime-type";s:10:"image/jpeg";}s:22:"section-background-xxl";a:4:{s:4:"file";s:18:"main-1920x1016.jpg";s:5:"width";i:1920;s:6:"height";i:1016;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1461, 112, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.0412934075178208;s:5:"bytes";i:16486;s:11:"size_before";i:542072;s:10:"size_after";i:525586;s:4:"time";d:0.52000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:8:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:4.0499999999999998;s:5:"bytes";i:232;s:11:"size_before";i:5723;s:10:"size_after";i:5491;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:3.1400000000000001;s:5:"bytes";i:283;s:11:"size_before";i:8999;s:10:"size_after";i:8716;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:2.8999999999999999;s:5:"bytes";i:1154;s:11:"size_before";i:39752;s:10:"size_after";i:38598;s:4:"time";d:0.029999999999999999;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:2.8700000000000001;s:5:"bytes";i:1802;s:11:"size_before";i:62737;s:10:"size_after";i:60935;s:4:"time";d:0.10000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:2.9500000000000002;s:5:"bytes";i:1765;s:11:"size_before";i:59851;s:10:"size_after";i:58086;s:4:"time";d:0.070000000000000007;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:2.8199999999999998;s:5:"bytes";i:2269;s:11:"size_before";i:80550;s:10:"size_after";i:78281;s:4:"time";d:0.059999999999999998;}s:21:"section-background-lg";O:8:"stdClass":5:{s:7:"percent";d:2.96;s:5:"bytes";i:3647;s:11:"size_before";i:123211;s:10:"size_after";i:119564;s:4:"time";d:0.11;}s:22:"section-background-xxl";O:8:"stdClass":5:{s:7:"percent";d:3.3100000000000001;s:5:"bytes";i:5334;s:11:"size_before";i:161249;s:10:"size_after";i:155915;s:4:"time";d:0.13;}}}'),
(1469, 138, '_wp_attached_file', '2016/11/logo-domalux.png'),
(1470, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:404;s:6:"height";i:120;s:4:"file";s:24:"2016/11/logo-domalux.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"logo-domalux-150x120.png";s:5:"width";i:150;s:6:"height";i:120;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"logo-domalux-300x89.png";s:5:"width";i:300;s:6:"height";i:89;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1471, 139, '_wp_attached_file', '2016/11/logo-dotpay.png'),
(1472, 139, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:181;s:6:"height";i:54;s:4:"file";s:23:"2016/11/logo-dotpay.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"logo-dotpay-150x54.png";s:5:"width";i:150;s:6:"height";i:54;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1473, 140, '_wp_attached_file', '2016/11/logo-tba.png'),
(1474, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:68;s:4:"file";s:20:"2016/11/logo-tba.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1475, 139, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:31.899981057018373;s:5:"bytes";i:1684;s:11:"size_before";i:5279;s:10:"size_after";i:3595;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:2:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:24.300000000000001;s:5:"bytes";i:494;s:11:"size_before";i:2033;s:10:"size_after";i:1539;s:4:"time";d:0.02;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:36.659999999999997;s:5:"bytes";i:1190;s:11:"size_before";i:3246;s:10:"size_after";i:2056;s:4:"time";d:0.02;}}}'),
(1476, 138, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:25.958337016411797;s:5:"bytes";i:8810;s:11:"size_before";i:33939;s:10:"size_after";i:25129;s:4:"time";d:0.26000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:23.329999999999998;s:5:"bytes";i:1257;s:11:"size_before";i:5389;s:10:"size_after";i:4132;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:29.02;s:5:"bytes";i:4597;s:11:"size_before";i:15840;s:10:"size_after";i:11243;s:4:"time";d:0.080000000000000002;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:23.260000000000002;s:5:"bytes";i:2956;s:11:"size_before";i:12710;s:10:"size_after";i:9754;s:4:"time";d:0.14999999999999999;}}}'),
(1477, 140, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:33.14805457301668;s:5:"bytes";i:1312;s:11:"size_before";i:3958;s:10:"size_after";i:2646;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:1:{s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:33.149999999999999;s:5:"bytes";i:1312;s:11:"size_before";i:3958;s:10:"size_after";i:2646;s:4:"time";d:0.029999999999999999;}}}'),
(1478, 52, 'content_5_section_bg', 'red'),
(1479, 52, '_content_5_section_bg', 'field_5836c16c062d1'),
(1480, 52, 'content_5_section_intro', '<h1>Dostarczamy pod Twój dom w <del>całej Polsce!<ins>Warszawie!</ins></del></h1>\r\nJuż wkrótce wygodna dostawa kurierem do każdej miejscowości na terenie całego kraju już w 24 godziny*.\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone size-full wp-image-253" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa-warszawa.png" alt="mapa-warszawa" width="622" height="602" />\r\n\r\n&nbsp;'),
(1481, 52, '_content_5_section_intro', 'field_5835d59224057'),
(1482, 52, 'content_5_layout_0_contact_0_icon', 'email'),
(1483, 52, '_content_5_layout_0_contact_0_icon', 'field_583710110e6cd'),
(1484, 52, 'content_5_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(1485, 52, '_content_5_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(1486, 52, 'content_5_layout_0_contact_1_icon', 'phone'),
(1487, 52, '_content_5_layout_0_contact_1_icon', 'field_583710110e6cd'),
(1488, 52, 'content_5_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(1489, 52, '_content_5_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(1490, 52, 'content_5_layout_0_contact_2_icon', 'pin'),
(1491, 52, '_content_5_layout_0_contact_2_icon', 'field_583710110e6cd'),
(1492, 52, 'content_5_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(1493, 52, '_content_5_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(1494, 52, 'content_5_layout_0_contact', '3'),
(1495, 52, '_content_5_layout_0_contact', 'field_583710060e6cc'),
(1496, 52, 'content_5_layout_0_partners_label', 'Nasz partner:'),
(1497, 52, '_content_5_layout_0_partners_label', 'field_583710740e6cf'),
(1498, 52, 'content_5_layout_0_partners_0_img', '138'),
(1499, 52, '_content_5_layout_0_partners_0_img', 'field_583710df0e6d2'),
(1500, 52, 'content_5_layout_0_partners', '1'),
(1501, 52, '_content_5_layout_0_partners', 'field_583710830e6d0'),
(1502, 52, 'content_5_layout_0_cooperators_label', 'Współpracujemy z:'),
(1503, 52, '_content_5_layout_0_cooperators_label', 'field_583710f20e6d3'),
(1504, 52, 'content_5_layout_0_cooperators_0_img', '140'),
(1505, 52, '_content_5_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(1506, 52, 'content_5_layout_0_cooperators_1_img', '139'),
(1507, 52, '_content_5_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(1508, 52, 'content_5_layout_0_cooperators', '2'),
(1509, 52, '_content_5_layout_0_cooperators', 'field_583711200e6d4'),
(1510, 52, 'content_5_layout', 'a:1:{i:0;s:4:"none";}'),
(1511, 52, '_content_5_layout', 'field_5835d59b24058'),
(1626, 145, '_menu_item_type', 'post_type'),
(1627, 145, '_menu_item_menu_item_parent', '0'),
(1628, 145, '_menu_item_object_id', '61'),
(1629, 145, '_menu_item_object', 'page'),
(1630, 145, '_menu_item_target', ''),
(1631, 145, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1632, 145, '_menu_item_xfn', ''),
(1633, 145, '_menu_item_url', ''),
(1635, 142, '_edit_last', '1'),
(1636, 142, '_wp_page_template', 'default'),
(1637, 142, 'content', ''),
(1638, 142, '_content', 'field_5835d51f24055'),
(1641, 142, '_edit_lock', '1480004480:1'),
(1642, 143, '_edit_last', '1'),
(1643, 143, '_wp_page_template', 'default'),
(1644, 143, 'content', ''),
(1645, 143, '_content', 'field_5835d51f24055'),
(1648, 143, '_edit_lock', '1480004481:1'),
(1649, 144, '_edit_last', '1'),
(1650, 144, '_wp_page_template', 'default'),
(1651, 144, 'content', ''),
(1652, 144, '_content', 'field_5835d51f24055'),
(1655, 144, '_edit_lock', '1480004438:1'),
(1656, 149, '_edit_last', '1'),
(1657, 149, '_wp_page_template', 'default'),
(1658, 149, 'content', ''),
(1659, 149, '_content', 'field_5835d51f24055'),
(1662, 149, '_edit_lock', '1480004482:1'),
(1663, 151, '_menu_item_type', 'post_type'),
(1664, 151, '_menu_item_menu_item_parent', '0'),
(1665, 151, '_menu_item_object_id', '149'),
(1666, 151, '_menu_item_object', 'page'),
(1667, 151, '_menu_item_target', ''),
(1668, 151, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1669, 151, '_menu_item_xfn', ''),
(1670, 151, '_menu_item_url', ''),
(1672, 152, '_menu_item_type', 'post_type'),
(1673, 152, '_menu_item_menu_item_parent', '0'),
(1674, 152, '_menu_item_object_id', '144'),
(1675, 152, '_menu_item_object', 'page'),
(1676, 152, '_menu_item_target', ''),
(1677, 152, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1678, 152, '_menu_item_xfn', ''),
(1679, 152, '_menu_item_url', ''),
(1681, 153, '_menu_item_type', 'post_type'),
(1682, 153, '_menu_item_menu_item_parent', '0'),
(1683, 153, '_menu_item_object_id', '143'),
(1684, 153, '_menu_item_object', 'page'),
(1685, 153, '_menu_item_target', ''),
(1686, 153, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1687, 153, '_menu_item_xfn', ''),
(1688, 153, '_menu_item_url', ''),
(1690, 154, '_menu_item_type', 'post_type'),
(1691, 154, '_menu_item_menu_item_parent', '0'),
(1692, 154, '_menu_item_object_id', '142'),
(1693, 154, '_menu_item_object', 'page'),
(1694, 154, '_menu_item_target', ''),
(1695, 154, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1696, 154, '_menu_item_xfn', ''),
(1697, 154, '_menu_item_url', ''),
(1927, 52, 'content_1_section_bg_img', '112') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1928, 52, '_content_1_section_bg_img', 'field_5836c1c4062d2'),
(1929, 52, 'content_1_layout_0_content', '<h1>Tani\r\nwynajem od\r\n<strong>50zł\r\n</strong>za dobę</h1>\r\n<span class="btn btn--primary btn--fill">Sprawdź ceny</span>'),
(1930, 52, '_content_1_layout_0_content', 'field_5836ca8eb9160'),
(1931, 52, 'content_2_layout_0_step_0_icon', 'myszka'),
(1932, 52, '_content_2_layout_0_step_0_icon', 'field_5836c4722cf71'),
(1933, 52, 'content_2_layout_0_step_0_label', 'Zarezerwuj online'),
(1934, 52, '_content_2_layout_0_step_0_label', 'field_5836c4db2cf72'),
(1935, 52, 'content_2_layout_0_step_1_icon', 'wozek'),
(1936, 52, '_content_2_layout_0_step_1_icon', 'field_5836c4722cf71'),
(1937, 52, 'content_2_layout_0_step_1_label', 'Kurier dostarczy cykliniarkę pod Twój dom'),
(1938, 52, '_content_2_layout_0_step_1_label', 'field_5836c4db2cf72'),
(1939, 52, 'content_2_layout_0_step_2_icon', 'cykliniarka'),
(1940, 52, '_content_2_layout_0_step_2_icon', 'field_5836c4722cf71'),
(1941, 52, 'content_2_layout_0_step_2_label', 'Wycyklinuj podłogę'),
(1942, 52, '_content_2_layout_0_step_2_label', 'field_5836c4db2cf72'),
(1943, 52, 'content_2_layout_0_step_3_icon', 'ciezarowka'),
(1944, 52, '_content_2_layout_0_step_3_icon', 'field_5836c4722cf71'),
(1945, 52, 'content_2_layout_0_step_3_label', 'Kurier odbierze cykliniarkę od Ciebie'),
(1946, 52, '_content_2_layout_0_step_3_label', 'field_5836c4db2cf72'),
(1947, 52, 'content_2_layout_0_step', '4'),
(1948, 52, '_content_2_layout_0_step', 'field_5836c4452cf70'),
(1949, 52, 'content_3_layout_0_image', ''),
(1950, 52, '_content_3_layout_0_image', 'field_5835d5b924059'),
(1951, 52, 'content_3_layout_0_iframe', '<iframe width="640" height="360" src="https://www.youtube.com/embed/TDjpSRnKElA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>'),
(1952, 52, '_content_3_layout_0_iframe', 'field_5835d5ea2405b'),
(1953, 52, 'content_3_layout_0_caption', ''),
(1954, 52, '_content_3_layout_0_caption', 'field_5835d5ca2405a'),
(1955, 52, 'content_5_layout_0_image', '80'),
(1956, 52, '_content_5_layout_0_image', 'field_5835d5b924059'),
(1957, 52, 'content_5_layout_0_iframe', ''),
(1958, 52, '_content_5_layout_0_iframe', 'field_5835d5ea2405b'),
(1959, 52, 'content_5_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(1960, 52, '_content_5_layout_0_caption', 'field_5835d5ca2405a'),
(1961, 52, 'content_6_section_bg', 'white'),
(1962, 52, '_content_6_section_bg', 'field_5836c16c062d1'),
(1963, 52, 'content_6_section_intro', '<h1>Zaoszczędzisz\r\nnawet 400zł*</h1>\r\nSamodzielny remont podłogi oznacza,\r\nże sporo pieniędzy zostanie w Twojej kieszeni.\r\nCyklinowanie przez fachowaca pokoju\r\no powierzchni 10 m<sup>2</sup> to koszt ok. 400 zł.'),
(1964, 52, '_content_6_section_intro', 'field_5835d59224057'),
(1965, 52, 'content_6_layout_0_contact_0_icon', 'email'),
(1966, 52, '_content_6_layout_0_contact_0_icon', 'field_583710110e6cd'),
(1967, 52, 'content_6_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(1968, 52, '_content_6_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(1969, 52, 'content_6_layout_0_contact_1_icon', 'phone'),
(1970, 52, '_content_6_layout_0_contact_1_icon', 'field_583710110e6cd'),
(1971, 52, 'content_6_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(1972, 52, '_content_6_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(1973, 52, 'content_6_layout_0_contact_2_icon', 'pin'),
(1974, 52, '_content_6_layout_0_contact_2_icon', 'field_583710110e6cd'),
(1975, 52, 'content_6_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(1976, 52, '_content_6_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(1977, 52, 'content_6_layout_0_contact', '3'),
(1978, 52, '_content_6_layout_0_contact', 'field_583710060e6cc'),
(1979, 52, 'content_6_layout_0_partners_label', 'Nasz partner:'),
(1980, 52, '_content_6_layout_0_partners_label', 'field_583710740e6cf'),
(1981, 52, 'content_6_layout_0_partners_0_img', '138'),
(1982, 52, '_content_6_layout_0_partners_0_img', 'field_583710df0e6d2'),
(1983, 52, 'content_6_layout_0_partners', '1'),
(1984, 52, '_content_6_layout_0_partners', 'field_583710830e6d0'),
(1985, 52, 'content_6_layout_0_cooperators_label', 'Współpracujemy z:'),
(1986, 52, '_content_6_layout_0_cooperators_label', 'field_583710f20e6d3'),
(1987, 52, 'content_6_layout_0_cooperators_0_img', '140'),
(1988, 52, '_content_6_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(1989, 52, 'content_6_layout_0_cooperators_1_img', '139'),
(1990, 52, '_content_6_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(1991, 52, 'content_6_layout_0_cooperators', '2'),
(1992, 52, '_content_6_layout_0_cooperators', 'field_583711200e6d4'),
(1993, 52, 'content_6_layout', 'a:1:{i:0;s:5:"media";}'),
(1994, 52, '_content_6_layout', 'field_5835d59b24058'),
(2723, 52, 'content_0_layout_0_input_label', 'Wpisz miasto lub kod pocztowy...'),
(2724, 52, '_content_0_layout_0_input_label', 'field_583837c3a4e29'),
(2725, 52, 'content_0_layout_0_search_label', 'Sprawdź dostępność'),
(2726, 52, '_content_0_layout_0_search_label', 'field_583837cfa4e2a'),
(2913, 165, '_wp_attached_file', '2016/11/opinie.jpg'),
(2914, 165, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3100;s:6:"height";i:1608;s:4:"file";s:18:"2016/11/opinie.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"opinie-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"opinie-300x156.jpg";s:5:"width";i:300;s:6:"height";i:156;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"opinie-768x398.jpg";s:5:"width";i:768;s:6:"height";i:398;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:19:"opinie-1024x531.jpg";s:5:"width";i:1024;s:6:"height";i:531;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:18:"opinie-992x515.jpg";s:5:"width";i:992;s:6:"height";i:515;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:19:"opinie-1200x622.jpg";s:5:"width";i:1200;s:6:"height";i:622;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-lg";a:4:{s:4:"file";s:19:"opinie-1600x830.jpg";s:5:"width";i:1600;s:6:"height";i:830;s:9:"mime-type";s:10:"image/jpeg";}s:22:"section-background-xxl";a:4:{s:4:"file";s:19:"opinie-1920x996.jpg";s:5:"width";i:1920;s:6:"height";i:996;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2915, 165, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.9658033693738997;s:5:"bytes";i:20592;s:11:"size_before";i:258505;s:10:"size_after";i:237913;s:4:"time";d:0.56999999999999995;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:8:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.0599999999999996;s:5:"bytes";i:184;s:11:"size_before";i:3633;s:10:"size_after";i:3449;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";i:4;s:5:"bytes";i:231;s:11:"size_before";i:5782;s:10:"size_after";i:5551;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:3.3700000000000001;s:5:"bytes";i:679;s:11:"size_before";i:20163;s:10:"size_after";i:19484;s:4:"time";d:0.02;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:5.2300000000000004;s:5:"bytes";i:1575;s:11:"size_before";i:30090;s:10:"size_after";i:28515;s:4:"time";d:0.059999999999999998;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:5.0599999999999996;s:5:"bytes";i:1463;s:11:"size_before";i:28932;s:10:"size_after";i:27469;s:4:"time";d:0.029999999999999999;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:6.4299999999999997;s:5:"bytes";i:2396;s:11:"size_before";i:37236;s:10:"size_after";i:34840;s:4:"time";d:0.059999999999999998;}s:21:"section-background-lg";O:8:"stdClass":5:{s:7:"percent";d:9.2400000000000002;s:5:"bytes";i:5272;s:11:"size_before";i:57037;s:10:"size_after";i:51765;s:4:"time";d:0.16;}s:22:"section-background-xxl";O:8:"stdClass":5:{s:7:"percent";d:11.619999999999999;s:5:"bytes";i:8792;s:11:"size_before";i:75632;s:10:"size_after";i:66840;s:4:"time";d:0.22;}}}'),
(2916, 166, '_wp_attached_file', '2016/11/ocena.png'),
(2917, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:310;s:6:"height";i:74;s:4:"file";s:17:"2016/11/ocena.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ocena-150x74.png";s:5:"width";i:150;s:6:"height";i:74;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"ocena-300x72.png";s:5:"width";i:300;s:6:"height";i:72;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(2918, 166, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:27.085417824717435;s:5:"bytes";i:7309;s:11:"size_before";i:26985;s:10:"size_after";i:19676;s:4:"time";d:0.12000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:25.149999999999999;s:5:"bytes";i:1018;s:11:"size_before";i:4047;s:10:"size_after";i:3029;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:27.260000000000002;s:5:"bytes";i:4248;s:11:"size_before";i:15584;s:10:"size_after";i:11336;s:4:"time";d:0.040000000000000001;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:27.780000000000001;s:5:"bytes";i:2043;s:11:"size_before";i:7354;s:10:"size_after";i:5311;s:4:"time";d:0.070000000000000007;}}}'),
(2919, 52, 'content_4_section_bg_img', '165'),
(2920, 52, '_content_4_section_bg_img', 'field_5836c1c4062d2'),
(2921, 52, 'content_6_layout_0_image', '80'),
(2922, 52, '_content_6_layout_0_image', 'field_5835d5b924059'),
(2923, 52, 'content_6_layout_0_iframe', ''),
(2924, 52, '_content_6_layout_0_iframe', 'field_5835d5ea2405b'),
(2925, 52, 'content_6_layout_0_caption', '* Symulacja dla pokoju 10m<sup>2</sup>'),
(2926, 52, '_content_6_layout_0_caption', 'field_5835d5ca2405a'),
(2927, 52, 'content_7_section_bg', 'black'),
(2928, 52, '_content_7_section_bg', 'field_5836c16c062d1'),
(2929, 52, 'content_7_section_intro', '<h1>FAQ</h1>'),
(2930, 52, '_content_7_section_intro', 'field_5835d59224057'),
(2931, 52, 'content_7_layout_0_contact_0_icon', 'email'),
(2932, 52, '_content_7_layout_0_contact_0_icon', 'field_583710110e6cd'),
(2933, 52, 'content_7_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(2934, 52, '_content_7_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(2935, 52, 'content_7_layout_0_contact_1_icon', 'telefon'),
(2936, 52, '_content_7_layout_0_contact_1_icon', 'field_583710110e6cd'),
(2937, 52, 'content_7_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(2938, 52, '_content_7_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(2939, 52, 'content_7_layout_0_contact_2_icon', 'pinezka'),
(2940, 52, '_content_7_layout_0_contact_2_icon', 'field_583710110e6cd'),
(2941, 52, 'content_7_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2942, 52, '_content_7_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(2943, 52, 'content_7_layout_0_contact', '3'),
(2944, 52, '_content_7_layout_0_contact', 'field_583710060e6cc'),
(2945, 52, 'content_7_layout_0_partners_label', 'Nasz partner:'),
(2946, 52, '_content_7_layout_0_partners_label', 'field_583710740e6cf'),
(2947, 52, 'content_7_layout_0_partners_0_img', '138'),
(2948, 52, '_content_7_layout_0_partners_0_img', 'field_583710df0e6d2'),
(2949, 52, 'content_7_layout_0_partners', '1'),
(2950, 52, '_content_7_layout_0_partners', 'field_583710830e6d0'),
(2951, 52, 'content_7_layout_0_cooperators_label', 'Współpracujemy z:'),
(2952, 52, '_content_7_layout_0_cooperators_label', 'field_583710f20e6d3'),
(2953, 52, 'content_7_layout_0_cooperators_0_img', '140'),
(2954, 52, '_content_7_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(2955, 52, 'content_7_layout_0_cooperators_1_img', '139'),
(2956, 52, '_content_7_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(2957, 52, 'content_7_layout_0_cooperators', '2'),
(2958, 52, '_content_7_layout_0_cooperators', 'field_583711200e6d4'),
(2959, 52, 'content_7_layout', 'a:1:{i:0;s:3:"faq";}'),
(2960, 52, '_content_7_layout', 'field_5835d59b24058'),
(3645, 176, '_wp_attached_file', '2016/11/photo.jpg'),
(3646, 176, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:200;s:6:"height";i:200;s:4:"file";s:17:"2016/11/photo.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"photo-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(3647, 176, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:5.8220293000542593;s:5:"bytes";i:1073;s:11:"size_before";i:18430;s:10:"size_after";i:17357;s:4:"time";d:0.059999999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:2:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9399999999999999;s:5:"bytes";i:194;s:11:"size_before";i:4923;s:10:"size_after";i:4729;s:4:"time";d:0.040000000000000001;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:6.5099999999999998;s:5:"bytes";i:879;s:11:"size_before";i:13507;s:10:"size_after";i:12628;s:4:"time";d:0.02;}}}'),
(3648, 52, 'content_4_layout_0_reviews_0_img', '176'),
(3649, 52, '_content_4_layout_0_reviews_0_img', 'field_58385679f0113'),
(3650, 52, 'content_4_layout_0_reviews_0_content', 'Chcialbym podziekować za wynajem cykliniraki. Byłem bardzo zadwolony z maszyn oraz ze współpracy z państwem. W najblizszym czasie ponownie się zgłoszę.'),
(3651, 52, '_content_4_layout_0_reviews_0_content', 'field_58385681f0114'),
(3652, 52, 'content_4_layout_0_reviews_0_name', 'Marcin Filipek'),
(3653, 52, '_content_4_layout_0_reviews_0_name', 'field_58385693f0115'),
(3654, 52, 'content_4_layout_0_reviews_1_img', '176'),
(3655, 52, '_content_4_layout_0_reviews_1_img', 'field_58385679f0113'),
(3656, 52, 'content_4_layout_0_reviews_1_content', 'Chciałam Pańswu bardzo podziękować za wynajem sprzętu. Spełniło ono swoje zadanie należycie. Z pewnością będę korzystała z Państwa usług.'),
(3657, 52, '_content_4_layout_0_reviews_1_content', 'field_58385681f0114'),
(3658, 52, 'content_4_layout_0_reviews_1_name', 'Lucyna Piechowska'),
(3659, 52, '_content_4_layout_0_reviews_1_name', 'field_58385693f0115'),
(3660, 52, 'content_4_layout_0_reviews_2_img', '176'),
(3661, 52, '_content_4_layout_0_reviews_2_img', 'field_58385679f0113'),
(3662, 52, 'content_4_layout_0_reviews_2_content', 'Bardzo dziękuję za możliwość skorzystania z Państwa oferty. Cykliniarka spisała się bardzo dobrze dzięki czemu cała praca przebiegła wyjatkowo szybko i przyjemnie. '),
(3663, 52, '_content_4_layout_0_reviews_2_content', 'field_58385681f0114'),
(3664, 52, 'content_4_layout_0_reviews_2_name', 'Janusz Nowakowski'),
(3665, 52, '_content_4_layout_0_reviews_2_name', 'field_58385693f0115'),
(3666, 52, 'content_4_layout_0_reviews', '3'),
(3667, 52, '_content_4_layout_0_reviews', 'field_5838565af0112'),
(3668, 52, 'content_4_layout_0_content', '<a class="btn btn--primary" href="#">Zobacz wszystkie opinie</a>'),
(3669, 52, '_content_4_layout_0_content', 'field_5838569ff0116'),
(4420, 52, 'content_7_layout_0_faqs_0_question', 'Jak długo muszę czekać na maszynę po rezerwacji?'),
(4421, 52, '_content_7_layout_0_faqs_0_question', 'field_583863f942729'),
(4422, 52, 'content_7_layout_0_faqs_0_answear', 'Po instruktażu i otrzymaniu stosownych porad praktycznych KAŻDY jest w stanie w ciągu dwóch dni, po kilku godzinach pracy, do 200 złotych doprowadzić swoją podłogę o powierzchni około 50 m<sup>2</sup> do perfekcji.  Nie musisz wynajmować żadnych cykliniarzy, firm,  nikt nie będzie łazil Ci po Twoim mieszkaniu, nie musisz parzyć nikomu kawy, sprzątać po kimś ubikacji - wszystko to zrobisz sam. Obsługa tych cykliniarek to przyjemność. '),
(4423, 52, '_content_7_layout_0_faqs_0_answear', 'field_583863ff4272a'),
(4424, 52, 'content_7_layout_0_faqs_1_question', 'Lorem ipsum?'),
(4425, 52, '_content_7_layout_0_faqs_1_question', 'field_583863f942729'),
(4426, 52, 'content_7_layout_0_faqs_1_answear', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.'),
(4427, 52, '_content_7_layout_0_faqs_1_answear', 'field_583863ff4272a'),
(4428, 52, 'content_7_layout_0_faqs', '2'),
(4429, 52, '_content_7_layout_0_faqs', 'field_583863ae42723'),
(4430, 52, 'content_7_layout_0_content', '<a class="btn btn--primary" href="#">Zobacz wszystkie pytania</a>'),
(4431, 52, '_content_7_layout_0_content', 'field_583863ae42727'),
(4432, 52, 'content_8_section_bg', 'white'),
(4433, 52, '_content_8_section_bg', 'field_5836c16c062d1'),
(4434, 52, 'content_8_section_intro', '<h1>U nas kupisz wszystko</h1>\r\nNie trać czasu na chodzenie po marketach. Zaopatrzymy Ciebie w niezbędne papiery ścierne, lakiery i wałki.\r\nSprzedajemy wszystkie produkty potrzebne do odswieżenia podlogi.'),
(4435, 52, '_content_8_section_intro', 'field_5835d59224057'),
(4436, 52, 'content_8_layout_0_contact_0_icon', 'email'),
(4437, 52, '_content_8_layout_0_contact_0_icon', 'field_583710110e6cd'),
(4438, 52, 'content_8_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(4439, 52, '_content_8_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(4440, 52, 'content_8_layout_0_contact_1_icon', 'telefon'),
(4441, 52, '_content_8_layout_0_contact_1_icon', 'field_583710110e6cd'),
(4442, 52, 'content_8_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(4443, 52, '_content_8_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(4444, 52, 'content_8_layout_0_contact_2_icon', 'pinezka'),
(4445, 52, '_content_8_layout_0_contact_2_icon', 'field_583710110e6cd'),
(4446, 52, 'content_8_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(4447, 52, '_content_8_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(4448, 52, 'content_8_layout_0_contact', '3'),
(4449, 52, '_content_8_layout_0_contact', 'field_583710060e6cc'),
(4450, 52, 'content_8_layout_0_partners_label', 'Nasz partner:'),
(4451, 52, '_content_8_layout_0_partners_label', 'field_583710740e6cf'),
(4452, 52, 'content_8_layout_0_partners_0_img', '138'),
(4453, 52, '_content_8_layout_0_partners_0_img', 'field_583710df0e6d2'),
(4454, 52, 'content_8_layout_0_partners', '1'),
(4455, 52, '_content_8_layout_0_partners', 'field_583710830e6d0'),
(4456, 52, 'content_8_layout_0_cooperators_label', 'Współpracujemy z:'),
(4457, 52, '_content_8_layout_0_cooperators_label', 'field_583710f20e6d3'),
(4458, 52, 'content_8_layout_0_cooperators_0_img', '140'),
(4459, 52, '_content_8_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(4460, 52, 'content_8_layout_0_cooperators_1_img', '139'),
(4461, 52, '_content_8_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(4462, 52, 'content_8_layout_0_cooperators', '2'),
(4463, 52, '_content_8_layout_0_cooperators', 'field_583711200e6d4'),
(4464, 52, 'content_8_layout', 'a:1:{i:0;s:4:"none";}'),
(4465, 52, '_content_8_layout', 'field_5835d59b24058'),
(5358, 52, 'content_9_layout_0_contact_0_icon', 'email'),
(5359, 52, '_content_9_layout_0_contact_0_icon', 'field_583710110e6cd'),
(5360, 52, 'content_9_layout_0_contact_0_label', '<a href="mailto:info@wypozyczalniacykliniarek.com">info@wypozyczalnia\r\ncykliniarek.com</a>'),
(5361, 52, '_content_9_layout_0_contact_0_label', 'field_5837103d0e6ce'),
(5362, 52, 'content_9_layout_0_contact_1_icon', 'telefon'),
(5363, 52, '_content_9_layout_0_contact_1_icon', 'field_583710110e6cd'),
(5364, 52, 'content_9_layout_0_contact_1_label', '<a href="tel:+48668387017">668 387 017</a>'),
(5365, 52, '_content_9_layout_0_contact_1_label', 'field_5837103d0e6ce'),
(5366, 52, 'content_9_layout_0_contact_2_icon', 'pinezka'),
(5367, 52, '_content_9_layout_0_contact_2_icon', 'field_583710110e6cd') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(5368, 52, 'content_9_layout_0_contact_2_label', 'Radzymińska 98\r\n03-574 Warszawa'),
(5369, 52, '_content_9_layout_0_contact_2_label', 'field_5837103d0e6ce'),
(5370, 52, 'content_9_layout_0_contact', '3'),
(5371, 52, '_content_9_layout_0_contact', 'field_583710060e6cc'),
(5372, 52, 'content_9_layout_0_partners_label', 'Nasz partner:'),
(5373, 52, '_content_9_layout_0_partners_label', 'field_583710740e6cf'),
(5374, 52, 'content_9_layout_0_partners_0_img', '138'),
(5375, 52, '_content_9_layout_0_partners_0_img', 'field_583710df0e6d2'),
(5376, 52, 'content_9_layout_0_partners', '1'),
(5377, 52, '_content_9_layout_0_partners', 'field_583710830e6d0'),
(5378, 52, 'content_9_layout_0_cooperators_label', 'Współpracujemy z:'),
(5379, 52, '_content_9_layout_0_cooperators_label', 'field_583710f20e6d3'),
(5380, 52, 'content_9_layout_0_cooperators_0_img', '140'),
(5381, 52, '_content_9_layout_0_cooperators_0_img', 'field_583711290e6d5'),
(5382, 52, 'content_9_layout_0_cooperators_1_img', '139'),
(5383, 52, '_content_9_layout_0_cooperators_1_img', 'field_583711290e6d5'),
(5384, 52, 'content_9_layout_0_cooperators', '2'),
(5385, 52, '_content_9_layout_0_cooperators', 'field_583711200e6d4'),
(5718, 52, 'content_0_show', '1'),
(5719, 52, '_content_0_show', 'field_58397608ccf61'),
(5720, 52, 'content_1_show', '1'),
(5721, 52, '_content_1_show', 'field_58397608ccf61'),
(5722, 52, 'content_2_show', '1'),
(5723, 52, '_content_2_show', 'field_58397608ccf61'),
(5724, 52, 'content_3_show', '1'),
(5725, 52, '_content_3_show', 'field_58397608ccf61'),
(5726, 52, 'content_4_show', '1'),
(5727, 52, '_content_4_show', 'field_58397608ccf61'),
(5728, 52, 'content_5_show', '1'),
(5729, 52, '_content_5_show', 'field_58397608ccf61'),
(5730, 52, 'content_6_show', '1'),
(5731, 52, '_content_6_show', 'field_58397608ccf61'),
(5732, 52, 'content_7_show', '1'),
(5733, 52, '_content_7_show', 'field_58397608ccf61'),
(5734, 52, 'content_8_show', '0'),
(5735, 52, '_content_8_show', 'field_58397608ccf61'),
(6088, 194, '_edit_last', '1'),
(6089, 194, '_edit_lock', '1480949345:1'),
(6090, 194, '_visibility', 'visible'),
(6091, 194, '_stock_status', 'instock'),
(6092, 194, 'total_sales', '2'),
(6093, 194, '_downloadable', 'no'),
(6094, 194, '_virtual', 'no'),
(6095, 194, '_tax_status', 'taxable'),
(6096, 194, '_tax_class', ''),
(6097, 194, '_purchase_note', ''),
(6098, 194, '_featured', 'no'),
(6099, 194, '_weight', ''),
(6100, 194, '_length', ''),
(6101, 194, '_width', ''),
(6102, 194, '_height', ''),
(6103, 194, '_sku', ''),
(6104, 194, '_product_attributes', 'a:2:{s:12:"pa_pre-price";a:6:{s:4:"name";s:12:"pa_pre-price";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:0;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";i:0;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(6105, 194, '_regular_price', '40'),
(6106, 194, '_sale_price', ''),
(6107, 194, '_sale_price_dates_from', ''),
(6108, 194, '_sale_price_dates_to', ''),
(6109, 194, '_price', '40'),
(6110, 194, '_sold_individually', ''),
(6111, 194, '_manage_stock', 'no'),
(6112, 194, '_backorders', 'no'),
(6113, 194, '_stock', ''),
(6114, 194, '_upsell_ids', 'a:0:{}'),
(6115, 194, '_crosssell_ids', 'a:0:{}'),
(6116, 194, '_product_version', '2.6.8'),
(6117, 194, '_product_image_gallery', '195,80,165'),
(6118, 194, '_wc_rating_count', 'a:0:{}'),
(6119, 194, '_wc_review_count', '0'),
(6120, 194, '_wc_average_rating', '0'),
(6121, 195, '_wp_attached_file', '2016/11/produkt-podklad.jpg'),
(6122, 195, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:392;s:6:"height";i:392;s:4:"file";s:27:"2016/11/produkt-podklad.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"produkt-podklad-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"produkt-podklad-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"produkt-podklad-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"produkt-podklad-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(6123, 195, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.3030439138997334;s:5:"bytes";i:1912;s:11:"size_before";i:57886;s:10:"size_after";i:55974;s:4:"time";d:0.15999999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:5:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9300000000000002;s:5:"bytes";i:162;s:11:"size_before";i:4126;s:10:"size_after";i:3964;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:3.54;s:5:"bytes";i:371;s:11:"size_before";i:10475;s:10:"size_after";i:10104;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.9199999999999999;s:5:"bytes";i:208;s:11:"size_before";i:5302;s:10:"size_after";i:5094;s:4:"time";d:0.029999999999999999;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:10104;s:10:"size_after";i:10104;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:4.2000000000000002;s:5:"bytes";i:1171;s:11:"size_before";i:27879;s:10:"size_after";i:26708;s:4:"time";d:0.089999999999999997;}}}'),
(6124, 194, '_thumbnail_id', '195'),
(6125, 191, '_edit_lock', '1480182618:1'),
(6126, 200, '_wp_attached_file', '2016/11/KT-DX-CAPON-EXTRA-02-2010.pdf'),
(6127, 194, 'content_0_show', '1'),
(6128, 194, '_content_0_show', 'field_58397608ccf61'),
(6129, 194, 'content_0_section_bg', 'black-pure'),
(6130, 194, '_content_0_section_bg', 'field_5836c16c062d1'),
(6131, 194, 'content_0_section_intro', '<h1>Szczegółowe informacje</h1>'),
(6132, 194, '_content_0_section_intro', 'field_5835d59224057'),
(6133, 194, 'content_0_layout_0_col-left', '<h2>Zalety</h2>\r\n<ul>\r\n 	<li>Neutralizuje olejki eteryczne, garbniki i żywice</li>\r\n 	<li>Podkreśla strukturę drewna</li>\r\n 	<li>Nieznacznie wpływa na barwę drewna</li>\r\n 	<li>Szybkoschnący</li>\r\n</ul>\r\n<h2>Dane techniczne</h2>\r\n<ul>\r\n 	<li>Ilość wartstw: 1-2</li>\r\n 	<li>Wydajność z litra: ok. 10m<sup>2</sup> - pędzel, wałek; 30-40m<sup>2</sup> - szpachla parkieciarska</li>\r\n 	<li>Nakładanie warstwy po: 2-4h</li>\r\n 	<li>Rozcieńczalnik: do wyrobów nitrocelulozowych lub poliuretanowych</li>\r\n 	<li>Opakowanie: 5L</li>\r\n</ul>'),
(6134, 194, '_content_0_layout_0_col-left', 'field_583c065c0a8f3'),
(6135, 194, 'content_0_layout_0_col-right', '<h2>Sposób przygotowania</h2>\r\n<strong>Etap 1 - Przygotowanie podłoża</strong>\r\n\r\nPowierzchnia przeznaczona do lakierowania powinna być czysta, sucha, bez pozostałości np. past woskowych i środków nabłyszczających. Stare powłoki lakierowe należy usunąć. Rysy i szczeliny wypełnić szpachlą. Przed lakierowaniem podłoga powinna być wyszlifowana papierem ściernym o granulacji 100 -120 a następnie dokładnie oczyszczona z pyłu.\r\n\r\n<strong>Etap 2 - Lakierowanie</strong>\r\n\r\nPrzed użyciem podkład dokładnie wymieszaj w opakowaniu. Wyrób nie wymaga rozcieńczania. Podkład nakładaj 1-krotnie przy pomocy pędzla lub wałka, 2-krotnie przy pomocy szpachli parkieciarskiej. W przypadku lakierowania drewna egzotycznego i gatunków problematycznych, aby uzyskać odpowiedni poziom odcięcia, zaleca się aplikację wałkiem lub pędzlem z nakładem 100-120 g/m<sup>2</sup>. W przypadku podłóg wykonanych z drewna bukowego, do aplikacji podkładu zalecane jest użycie wałka. Po wyschnięciu (2-4h) można nakładać lakiery nawierzchniowe bez konieczności matowienia.'),
(6136, 194, '_content_0_layout_0_col-right', 'field_583c06a10a8f4'),
(6137, 194, 'content_0_layout', 'a:1:{i:0;s:22:"szczegolowe-informacje";}'),
(6138, 194, '_content_0_layout', 'field_5835d59b24058'),
(6139, 194, 'content_1_show', '1'),
(6140, 194, '_content_1_show', 'field_58397608ccf61'),
(6141, 194, 'content_1_section_bg', 'black-pure'),
(6142, 194, '_content_1_section_bg', 'field_5836c16c062d1'),
(6143, 194, 'content_1_section_intro', '<h1>Do pobrania</h1>'),
(6144, 194, '_content_1_section_intro', 'field_5835d59224057'),
(6145, 194, 'content_1_layout_0_files_0_file', '200'),
(6146, 194, '_content_1_layout_0_files_0_file', 'field_583c06e00a8f7'),
(6147, 194, 'content_1_layout_0_files_1_file', '195'),
(6148, 194, '_content_1_layout_0_files_1_file', 'field_583c06e00a8f7'),
(6149, 194, 'content_1_layout_0_files', '2'),
(6150, 194, '_content_1_layout_0_files', 'field_583c06ce0a8f6'),
(6151, 194, 'content_1_layout', 'a:1:{i:0;s:11:"do-pobrania";}') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(6152, 194, '_content_1_layout', 'field_5835d59b24058'),
(6153, 194, 'content', '2'),
(6154, 194, '_content', 'field_5835d51f24055'),
(6155, 201, '_edit_lock', '1480606245:1'),
(6156, 201, '_edit_last', '1'),
(6502, 60, 'content', ''),
(6503, 60, '_content', 'field_5835d51f24055'),
(6506, 244, '_edit_last', '1'),
(6507, 244, '_edit_lock', '1480528923:1'),
(6508, 244, '_visibility', 'visible'),
(6509, 244, '_stock_status', 'instock'),
(6510, 244, '_downloadable', 'no'),
(6511, 244, '_virtual', 'no'),
(6512, 244, '_tax_status', 'taxable'),
(6513, 244, '_tax_class', ''),
(6514, 244, '_purchase_note', ''),
(6515, 244, '_featured', 'no'),
(6516, 244, '_weight', ''),
(6517, 244, '_length', ''),
(6518, 244, '_width', ''),
(6519, 244, '_height', ''),
(6520, 244, '_product_attributes', 'a:1:{s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(6521, 244, '_regular_price', '40'),
(6522, 244, '_sale_price', ''),
(6523, 244, '_sale_price_dates_from', ''),
(6524, 244, '_sale_price_dates_to', ''),
(6525, 244, '_price', '40'),
(6526, 244, '_sold_individually', ''),
(6527, 244, '_manage_stock', 'no'),
(6528, 244, '_backorders', 'no'),
(6529, 244, '_stock', ''),
(6530, 244, '_upsell_ids', 'a:0:{}'),
(6531, 244, '_crosssell_ids', 'a:0:{}'),
(6532, 244, '_product_version', '2.6.8'),
(6533, 244, '_product_image_gallery', '195,80,165'),
(6534, 244, '_thumbnail_id', '195'),
(6535, 244, 'content_0_show', '1'),
(6536, 244, '_content_0_show', 'field_58397608ccf61'),
(6537, 244, 'content_0_section_bg', 'black-pure'),
(6538, 244, '_content_0_section_bg', 'field_5836c16c062d1'),
(6539, 244, 'content_0_section_intro', '<h1>Szczegółowe informacje</h1>'),
(6540, 244, '_content_0_section_intro', 'field_5835d59224057'),
(6541, 244, 'content_0_layout_0_col-left', '<h2>Zalety</h2>\r\n<ul>\r\n 	<li>Neutralizuje olejki eteryczne, garbniki i żywice</li>\r\n 	<li>Podkreśla strukturę drewna</li>\r\n 	<li>Nieznacznie wpływa na barwę drewna</li>\r\n 	<li>Szybkoschnący</li>\r\n</ul>\r\n<h2>Dane techniczne</h2>\r\n<ul>\r\n 	<li>Ilość wartstw: 1-2</li>\r\n 	<li>Wydajność z litra: ok. 10m<sup>2</sup> - pędzel, wałek; 30-40m<sup>2</sup> - szpachla parkieciarska</li>\r\n 	<li>Nakładanie warstwy po: 2-4h</li>\r\n 	<li>Rozcieńczalnik: do wyrobów nitrocelulozowych lub poliuretanowych</li>\r\n 	<li>Opakowanie: 5L</li>\r\n</ul>'),
(6542, 244, '_content_0_layout_0_col-left', 'field_583c065c0a8f3'),
(6543, 244, 'content_0_layout_0_col-right', '<h2>Sposób przygotowania</h2>\r\n<strong>Etap 1 - Przygotowanie podłoża</strong>\r\n\r\nPowierzchnia przeznaczona do lakierowania powinna być czysta, sucha, bez pozostałości np. past woskowych i środków nabłyszczających. Stare powłoki lakierowe należy usunąć. Rysy i szczeliny wypełnić szpachlą. Przed lakierowaniem podłoga powinna być wyszlifowana papierem ściernym o granulacji 100 -120 a następnie dokładnie oczyszczona z pyłu.\r\n\r\n<strong>Etap 2 - Lakierowanie</strong>\r\n\r\nPrzed użyciem podkład dokładnie wymieszaj w opakowaniu. Wyrób nie wymaga rozcieńczania. Podkład nakładaj 1-krotnie przy pomocy pędzla lub wałka, 2-krotnie przy pomocy szpachli parkieciarskiej. W przypadku lakierowania drewna egzotycznego i gatunków problematycznych, aby uzyskać odpowiedni poziom odcięcia, zaleca się aplikację wałkiem lub pędzlem z nakładem 100-120 g/m<sup>2</sup>. W przypadku podłóg wykonanych z drewna bukowego, do aplikacji podkładu zalecane jest użycie wałka. Po wyschnięciu (2-4h) można nakładać lakiery nawierzchniowe bez konieczności matowienia.'),
(6544, 244, '_content_0_layout_0_col-right', 'field_583c06a10a8f4'),
(6545, 244, 'content_0_layout', 'a:1:{i:0;s:22:"szczegolowe-informacje";}'),
(6546, 244, '_content_0_layout', 'field_5835d59b24058'),
(6547, 244, 'content_1_show', '1'),
(6548, 244, '_content_1_show', 'field_58397608ccf61'),
(6549, 244, 'content_1_section_bg', 'black-pure'),
(6550, 244, '_content_1_section_bg', 'field_5836c16c062d1'),
(6551, 244, 'content_1_section_intro', '<h1>Do pobrania</h1>'),
(6552, 244, '_content_1_section_intro', 'field_5835d59224057'),
(6553, 244, 'content_1_layout_0_files_0_file', '200'),
(6554, 244, '_content_1_layout_0_files_0_file', 'field_583c06e00a8f7'),
(6555, 244, 'content_1_layout_0_files_1_file', '195'),
(6556, 244, '_content_1_layout_0_files_1_file', 'field_583c06e00a8f7'),
(6557, 244, 'content_1_layout_0_files', '2'),
(6558, 244, '_content_1_layout_0_files', 'field_583c06ce0a8f6'),
(6559, 244, 'content_1_layout', 'a:1:{i:0;s:11:"do-pobrania";}'),
(6560, 244, '_content_1_layout', 'field_5835d59b24058'),
(6561, 244, 'content', '2'),
(6562, 244, '_content', 'field_5835d51f24055'),
(6569, 244, 'total_sales', '1'),
(6570, 244, '_sku', ''),
(6913, 52, 'content_0_content_after', ''),
(6914, 52, '_content_0_content_after', 'field_58400d1d4d1e5'),
(6915, 52, 'content_1_content_after', ''),
(6916, 52, '_content_1_content_after', 'field_58400d1d4d1e5'),
(6917, 52, 'content_2_content_after', ''),
(6918, 52, '_content_2_content_after', 'field_58400d1d4d1e5'),
(6919, 52, 'content_3_content_after', ''),
(6920, 52, '_content_3_content_after', 'field_58400d1d4d1e5'),
(6921, 52, 'content_4_content_after', '<a class="btn btn--primary" href="#">Zobacz wszystkie opinie</a>'),
(6922, 52, '_content_4_content_after', 'field_58400d1d4d1e5'),
(6923, 52, 'content_5_content_after', '<a class="btn--primary btn" href="#">Szczegóły dostawy</a>\r\n<p class="xsmall">* czas dostawy do mniejszych miejscowości może wynieść do 48h</p>\r\n<p class="xsmall">Dostawa kurierem Tba EXPRESS wraz ze zwrotem w cenie od 200 do 220 zł.</p>'),
(6924, 52, '_content_5_content_after', 'field_58400d1d4d1e5'),
(6925, 52, 'content_6_content_after', ''),
(6926, 52, '_content_6_content_after', 'field_58400d1d4d1e5'),
(6927, 52, 'content_7_content_after', '<a class="btn btn--primary" href="#">Zobacz wszystkie pytania</a>'),
(6928, 52, '_content_7_content_after', 'field_58400d1d4d1e5'),
(6929, 52, 'content_8_content_after', ''),
(6930, 52, '_content_8_content_after', 'field_58400d1d4d1e5'),
(6931, 52, 'content_9_show', '1'),
(6932, 52, '_content_9_show', 'field_58397608ccf61'),
(6933, 52, 'content_9_section_bg', 'white'),
(6934, 52, '_content_9_section_bg', 'field_5836c16c062d1'),
(6935, 52, 'content_9_section_intro', '<h1>U nas kupisz wszystko</h1>\r\nNie trać czasu na chodzenie po marketach. Zaopatrzymy Ciebie w niezbędne papiery ścierne, lakiery i wałki. Sprzedajemy wszystkie produkty potrzebne do odswieżenia podlogi.'),
(6936, 52, '_content_9_section_intro', 'field_5835d59224057'),
(6937, 52, 'content_9_layout_0_products_0_product', '194'),
(6938, 52, '_content_9_layout_0_products_0_product', 'field_58400cc8fde33'),
(6939, 52, 'content_9_layout_0_products_1_product', '244'),
(6940, 52, '_content_9_layout_0_products_1_product', 'field_58400cc8fde33'),
(6941, 52, 'content_9_layout_0_products', '2'),
(6942, 52, '_content_9_layout_0_products', 'field_58400ca2fde31'),
(6943, 52, 'content_9_layout', 'a:1:{i:0;s:8:"produkty";}'),
(6944, 52, '_content_9_layout', 'field_5835d59b24058'),
(6945, 52, 'content_9_content_after', '<a class="btn btn--primary btn--fill" href="#">Zobacz pozostałe produkty</a>'),
(6946, 52, '_content_9_content_after', 'field_58400d1d4d1e5') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(8075, 253, '_wp_attached_file', '2016/11/mapa-warszawa.png'),
(8076, 253, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:622;s:6:"height";i:602;s:4:"file";s:25:"2016/11/mapa-warszawa.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"mapa-warszawa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:25:"mapa-warszawa-300x290.png";s:5:"width";i:300;s:6:"height";i:290;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"mapa-warszawa-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"mapa-warszawa-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:11:"shop_single";a:4:{s:4:"file";s:25:"mapa-warszawa-600x600.png";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(8077, 253, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:30.82600095031728;s:5:"bytes";i:91474;s:11:"size_before";i:296743;s:10:"size_after";i:205269;s:4:"time";d:1.5599999999999998;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:6:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:30.940000000000001;s:5:"bytes";i:4099;s:11:"size_before";i:13250;s:10:"size_after";i:9151;s:4:"time";d:0.040000000000000001;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:35.399999999999999;s:5:"bytes";i:14663;s:11:"size_before";i:41422;s:10:"size_after";i:26759;s:4:"time";d:0.17999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:32.5;s:5:"bytes";i:5727;s:11:"size_before";i:17621;s:10:"size_after";i:11894;s:4:"time";d:0.080000000000000002;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:34.159999999999997;s:5:"bytes";i:14433;s:11:"size_before";i:42250;s:10:"size_after";i:27817;s:4:"time";d:0.35999999999999999;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:33.210000000000001;s:5:"bytes";i:45733;s:11:"size_before";i:137715;s:10:"size_after";i:91982;s:4:"time";d:0.62;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:15.33;s:5:"bytes";i:6819;s:11:"size_before";i:44485;s:10:"size_after";i:37666;s:4:"time";d:0.28000000000000003;}}}'),
(9206, 257, '_edit_lock', '1480944615:1'),
(9207, 257, '_edit_last', '1'),
(9208, 276, '_edit_lock', '1480955379:1'),
(9209, 276, '_edit_last', '1'),
(9210, 5, 'content_0_show', '1'),
(9211, 5, '_content_0_show', 'field_5845620a97f9d'),
(9212, 5, 'content_0_layout_0_content', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\n<p class="medium"><strong>Bona jest firmą rodzinną, założoną w 1919 roku. Nasza siedziba znajduje się w Szwecji, a dzięki licznej sieci dystrybucji jesteśmy obecni w ponad 70 krajach na całym świecie. Dostarczamy produkty do instalacji, renowacji i konserwacji podłóg drewnianych.</strong></p>\r\n<iframe src="//www.youtube.com/embed/TDjpSRnKElA?rel=0&amp;showinfo=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe>\r\n\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.\r\n\r\n<img class="alignnone wp-image-35 size-full" src="http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg" alt="cykliniarki" width="825" height="455" />\r\n<h2>Ogólne warunki umowy najmu/dzierżawy</h2>\r\n<h3>II. Postanowienia ogólne</h3>\r\n<strong> §2</strong>\r\n\r\nKlient oświadcza, że znany jest mu stan techniczny przedmiotu umowy, nie wnosi co do niego zastrzeżeń, a ponadto uznaje, że w chwili przekazania przedmiot umowy nadaje się do realizowania celu, do którego jest przeznaczony.\r\n\r\n<strong>§8 (wypowiedzenie z winy Klienta)</strong>\r\n\r\n1. LIBRA 2 ma prawo wypowiedzieć umowę ze skutkiem natychmiastowym, w przypadku gdy Klient:\r\n\r\na) używa przedmiot umowy w sposób sprzeczny z jego przeznaczeniem narażając go na utratę lub uszkodzenie\r\n\r\nb) oddaje przedmiot umowy do używania osobom trzecim bez zgody LIBRA 2\r\n<h2>Główne zalety</h2>\r\n<ul>\r\n 	<li>Silnik o dużej mocy</li>\r\n 	<li>Składana prowadnica zmniejsza zajmowane miejsce podczas transportu</li>\r\n 	<li>Duże koła ułatwiają <a href="#">transport</a></li>\r\n 	<li>Kompatybilna z Bona Power Drive</li>\r\n</ul>\r\n<a class="btn btn--primary btn--fill" href="#">Button</a>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Model:</strong></td>\r\n<td>Bona FlexiSand 1.5</td>\r\n<td>Bona FlexiSand 1.9</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Waga:</strong></td>\r\n<td>38 kg</td>\r\n<td>50 kg</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Moc:</strong></td>\r\n<td>1,5 kW</td>\r\n<td>1,9 kW</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="disclaimer"><em>Jej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.</em></p>'),
(9213, 5, '_content_0_layout_0_content', 'field_584562c22e459'),
(9214, 5, 'content_0_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9215, 5, '_content_0_layout', 'field_584562a62e458'),
(9216, 5, 'content', '1'),
(9217, 5, '_content', 'field_5845620a7c5f4'),
(9226, 315, '_edit_last', '1'),
(9227, 315, '_edit_lock', '1480957753:1'),
(9228, 315, '_visibility', 'visible'),
(9229, 315, '_stock_status', 'instock'),
(9230, 315, 'pricing_type', 'general_pricing'),
(9231, 315, 'general_price', '100'),
(9232, 315, 'hourly_price', '100'),
(9233, 315, 'redq_rental_availability', 'a:2:{i:0;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:17:"Grudzień 1, 2016";s:2:"to";s:18:"Grudzień 31, 2017";s:8:"rentable";s:2:"no";}i:1;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:10:"12/06/2016";s:2:"to";s:10:"12/15/2016";s:8:"rentable";s:2:"no";}}'),
(9234, 315, 'redq_attributes', 'a:0:{}'),
(9235, 315, 'redq_all_data', 'a:6:{s:12:"pricing_type";s:15:"general_pricing";s:15:"general_pricing";s:3:"100";s:14:"hourly_pricing";s:3:"100";s:18:"block_rental_dates";N;s:19:"rental_availability";a:1:{i:0;a:4:{s:4:"type";s:11:"custom_date";s:4:"from";s:17:"Grudzień 1, 2016";s:2:"to";s:18:"Grudzień 31, 2017";s:8:"rentable";s:2:"no";}}s:10:"attributes";a:0:{}}'),
(9236, 315, 'total_sales', '1'),
(9237, 315, '_downloadable', 'no'),
(9238, 315, '_virtual', 'no'),
(9239, 315, '_tax_status', 'taxable'),
(9240, 315, '_tax_class', ''),
(9241, 315, '_purchase_note', ''),
(9242, 315, '_featured', 'no'),
(9243, 315, '_weight', ''),
(9244, 315, '_length', ''),
(9245, 315, '_width', ''),
(9246, 315, '_height', ''),
(9247, 315, '_sku', ''),
(9248, 315, '_product_attributes', 'a:2:{s:18:"pa_jednostka-miary";a:6:{s:4:"name";s:18:"pa_jednostka-miary";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:12:"pa_pre-price";a:6:{s:4:"name";s:12:"pa_pre-price";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(9249, 315, '_regular_price', '100'),
(9250, 315, '_sale_price', ''),
(9251, 315, '_sale_price_dates_from', ''),
(9252, 315, '_sale_price_dates_to', ''),
(9253, 315, '_price', '100'),
(9254, 315, '_sold_individually', ''),
(9255, 315, '_manage_stock', 'yes'),
(9256, 315, '_backorders', 'no'),
(9257, 315, '_stock', '10.000000'),
(9258, 315, '_upsell_ids', 'a:0:{}'),
(9259, 315, '_crosssell_ids', 'a:0:{}'),
(9260, 315, '_product_version', '2.6.8'),
(9261, 315, '_product_image_gallery', ''),
(9262, 315, 'content', ''),
(9263, 315, '_content', 'field_5835d51f24055'),
(9264, 316, '_order_key', 'wc_order_58457d93a10d7'),
(9265, 316, '_order_currency', 'PLN'),
(9266, 316, '_prices_include_tax', 'yes'),
(9267, 316, '_customer_ip_address', '::1'),
(9268, 316, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'),
(9269, 316, '_customer_user', '1'),
(9270, 316, '_created_via', 'checkout'),
(9271, 316, '_cart_hash', '4cf8390ba49364020d1de8cb10f85c6e'),
(9272, 316, '_order_version', '2.6.8'),
(9273, 194, 'redq_rental_availability', ''),
(9274, 244, 'redq_rental_availability', ''),
(9275, 316, '_billing_first_name', 'Test'),
(9276, 316, '_billing_last_name', 'Test'),
(9277, 316, '_billing_company', ''),
(9278, 316, '_billing_email', 'patryk@visibee.pl'),
(9279, 316, '_billing_phone', '608187167'),
(9280, 316, '_billing_country', 'PL'),
(9281, 316, '_billing_address_1', 'Test'),
(9282, 316, '_billing_address_2', ''),
(9283, 316, '_billing_city', 'Test'),
(9284, 316, '_billing_state', ''),
(9285, 316, '_billing_postcode', '81-377'),
(9286, 316, '_shipping_first_name', 'Test'),
(9287, 316, '_shipping_last_name', 'Test'),
(9288, 316, '_shipping_company', ''),
(9289, 316, '_shipping_country', 'PL'),
(9290, 316, '_shipping_address_1', 'Test'),
(9291, 316, '_shipping_address_2', ''),
(9292, 316, '_shipping_city', 'Test'),
(9293, 316, '_shipping_state', ''),
(9294, 316, '_shipping_postcode', '81-377'),
(9295, 316, '_payment_method', 'bacs'),
(9296, 316, '_payment_method_title', 'Direct Bank Transfer'),
(9297, 316, '_order_shipping', '0'),
(9298, 316, '_cart_discount', '0'),
(9299, 316, '_cart_discount_tax', '-2.8421709430404E-14'),
(9300, 316, '_order_tax', '209.4309'),
(9301, 316, '_order_shipping_tax', '0'),
(9302, 316, '_order_total', '1120'),
(9303, 316, '_recorded_sales', 'yes'),
(9304, 316, '_order_stock_reduced', '1'),
(9305, 316, '_edit_lock', '1480949117:1'),
(9306, 318, '_wp_attached_file', '2016/11/cykliniarki-1.jpg'),
(9307, 318, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1550;s:6:"height";i:827;s:4:"file";s:25:"2016/11/cykliniarki-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"cykliniarki-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"cykliniarki-1-300x160.jpg";s:5:"width";i:300;s:6:"height";i:160;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:25:"cykliniarki-1-768x410.jpg";s:5:"width";i:768;s:6:"height";i:410;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:26:"cykliniarki-1-1024x546.jpg";s:5:"width";i:1024;s:6:"height";i:546;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"cykliniarki-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"cykliniarki-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:25:"cykliniarki-1-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-xs";a:4:{s:4:"file";s:25:"cykliniarki-1-768x410.jpg";s:5:"width";i:768;s:6:"height";i:410;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:25:"cykliniarki-1-992x529.jpg";s:5:"width";i:992;s:6:"height";i:529;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:26:"cykliniarki-1-1200x640.jpg";s:5:"width";i:1200;s:6:"height";i:640;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9308, 318, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.2828893468508298;s:5:"bytes";i:18192;s:11:"size_before";i:554146;s:10:"size_after";i:535954;s:4:"time";d:0.43000000000000005;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:10:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.3300000000000001;s:5:"bytes";i:402;s:11:"size_before";i:7544;s:10:"size_after";i:7142;s:4:"time";d:0.01;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:4.5;s:5:"bytes";i:586;s:11:"size_before";i:13017;s:10:"size_after";i:12431;s:4:"time";d:0.029999999999999999;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:3.77;s:5:"bytes";i:2321;s:11:"size_before";i:61512;s:10:"size_after";i:59191;s:4:"time";d:0.050000000000000003;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:3.6499999999999999;s:5:"bytes";i:3553;s:11:"size_before";i:97376;s:10:"size_after";i:93823;s:4:"time";d:0.040000000000000001;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.04;s:5:"bytes";i:505;s:11:"size_before";i:10022;s:10:"size_after";i:9517;s:4:"time";d:0.01;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:4.0800000000000001;s:5:"bytes";i:911;s:11:"size_before";i:22343;s:10:"size_after";i:21432;s:4:"time";d:0.02;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:3.2599999999999998;s:5:"bytes";i:2161;s:11:"size_before";i:66262;s:10:"size_after";i:64101;s:4:"time";d:0.040000000000000001;}s:21:"section-background-xs";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:59191;s:10:"size_after";i:59191;s:4:"time";d:0.10000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:3.6000000000000001;s:5:"bytes";i:3334;s:11:"size_before";i:92497;s:10:"size_after";i:89163;s:4:"time";d:0.080000000000000002;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:3.5499999999999998;s:5:"bytes";i:4419;s:11:"size_before";i:124382;s:10:"size_after";i:119963;s:4:"time";d:0.050000000000000003;}}}'),
(9309, 320, '_wp_attached_file', '2016/11/bona-1.jpg'),
(9310, 320, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-1-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-1-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9311, 321, '_wp_attached_file', '2016/11/bona-2.jpg'),
(9312, 321, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-2-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-2-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9313, 320, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.3711911357340725;s:5:"bytes";i:1610;s:11:"size_before";i:25270;s:10:"size_after";i:23660;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.4400000000000004;s:5:"bytes";i:404;s:11:"size_before";i:6272;s:10:"size_after";i:5868;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.6900000000000004;s:5:"bytes";i:393;s:11:"size_before";i:6911;s:10:"size_after";i:6518;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:6.7300000000000004;s:5:"bytes";i:813;s:11:"size_before";i:12087;s:10:"size_after";i:11274;s:4:"time";d:0.01;}}}'),
(9314, 322, '_wp_attached_file', '2016/11/bona-3.jpg'),
(9315, 322, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-3-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-3-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9316, 321, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.981246552675124;s:5:"bytes";i:1447;s:11:"size_before";i:18130;s:10:"size_after";i:16683;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3200000000000003;s:5:"bytes";i:293;s:11:"size_before";i:4633;s:10:"size_after";i:4340;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.9900000000000002;s:5:"bytes";i:345;s:11:"size_before";i:4934;s:10:"size_after";i:4589;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:9.4499999999999993;s:5:"bytes";i:809;s:11:"size_before";i:8563;s:10:"size_after";i:7754;s:4:"time";d:0.01;}}}'),
(9317, 323, '_wp_attached_file', '2016/11/bona-4.jpg'),
(9318, 323, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-4-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-4-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9319, 322, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7966089413810593;s:5:"bytes";i:1876;s:11:"size_before";i:27602;s:10:"size_after";i:25726;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3099999999999996;s:5:"bytes";i:430;s:11:"size_before";i:6819;s:10:"size_after";i:6389;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3799999999999999;s:5:"bytes";i:472;s:11:"size_before";i:7401;s:10:"size_after";i:6929;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.2800000000000002;s:5:"bytes";i:974;s:11:"size_before";i:13382;s:10:"size_after";i:12408;s:4:"time";d:0.01;}}}'),
(9320, 324, '_wp_attached_file', '2016/11/bona-5.jpg'),
(9321, 324, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-5-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-5-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9322, 323, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7761053128441811;s:5:"bytes";i:1292;s:11:"size_before";i:19067;s:10:"size_after";i:17775;s:4:"time";d:0.050000000000000003;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4199999999999999;s:5:"bytes";i:260;s:11:"size_before";i:4794;s:10:"size_after";i:4534;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4699999999999998;s:5:"bytes";i:282;s:11:"size_before";i:5157;s:10:"size_after";i:4875;s:4:"time";d:0.029999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.2300000000000004;s:5:"bytes";i:750;s:11:"size_before";i:9116;s:10:"size_after";i:8366;s:4:"time";d:0.01;}}}'),
(9323, 325, '_wp_attached_file', '2016/11/bona-6.jpg'),
(9324, 325, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:195;s:6:"height";i:130;s:4:"file";s:18:"2016/11/bona-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bona-6-150x130.jpg";s:5:"width";i:150;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"bona-6-180x130.jpg";s:5:"width";i:180;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9325, 324, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7911532045417857;s:5:"bytes";i:1262;s:11:"size_before";i:18583;s:10:"size_after";i:17321;s:4:"time";d:0.050000000000000003;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4299999999999997;s:5:"bytes";i:241;s:11:"size_before";i:4442;s:10:"size_after";i:4201;s:4:"time";d:0.02;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.0099999999999998;s:5:"bytes";i:245;s:11:"size_before";i:4893;s:10:"size_after";i:4648;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.3900000000000006;s:5:"bytes";i:776;s:11:"size_before";i:9248;s:10:"size_after";i:8472;s:4:"time";d:0.02;}}}'),
(9326, 325, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.9683315351907087;s:5:"bytes";i:1098;s:11:"size_before";i:15757;s:10:"size_after";i:14659;s:4:"time";d:0.040000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.7800000000000002;s:5:"bytes";i:198;s:11:"size_before";i:3423;s:10:"size_after";i:3225;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.1200000000000001;s:5:"bytes";i:217;s:11:"size_before";i:4236;s:10:"size_after";i:4019;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.4299999999999997;s:5:"bytes";i:683;s:11:"size_before";i:8098;s:10:"size_after";i:7415;s:4:"time";d:0.02;}}}'),
(9327, 326, '_wp_attached_file', '2016/11/bona.jpg'),
(9328, 326, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1550;s:6:"height";i:582;s:4:"file";s:16:"2016/11/bona.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"bona-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"bona-300x113.jpg";s:5:"width";i:300;s:6:"height";i:113;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"bona-768x288.jpg";s:5:"width";i:768;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"bona-1024x384.jpg";s:5:"width";i:1024;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"bona-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:16:"bona-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:16:"bona-600x582.jpg";s:5:"width";i:600;s:6:"height";i:582;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-xs";a:4:{s:4:"file";s:16:"bona-768x288.jpg";s:5:"width";i:768;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-sm";a:4:{s:4:"file";s:16:"bona-992x372.jpg";s:5:"width";i:992;s:6:"height";i:372;s:9:"mime-type";s:10:"image/jpeg";}s:21:"section-background-md";a:4:{s:4:"file";s:17:"bona-1200x451.jpg";s:5:"width";i:1200;s:6:"height";i:451;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9329, 326, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.9277077631327804;s:5:"bytes";i:22430;s:11:"size_before";i:571071;s:10:"size_after";i:548641;s:4:"time";d:0.45000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:10:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.1299999999999999;s:5:"bytes";i:489;s:11:"size_before";i:7974;s:10:"size_after";i:7485;s:4:"time";d:0.029999999999999999;}s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:7.4299999999999997;s:5:"bytes";i:1060;s:11:"size_before";i:14267;s:10:"size_after";i:13207;s:4:"time";d:0.01;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:5.4500000000000002;s:5:"bytes";i:3461;s:11:"size_before";i:63500;s:10:"size_after";i:60039;s:4:"time";d:0.029999999999999999;}s:5:"large";O:8:"stdClass":5:{s:7:"percent";d:4.4699999999999998;s:5:"bytes";i:4452;s:11:"size_before";i:99635;s:10:"size_after";i:95183;s:4:"time";d:0.11;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.6399999999999997;s:5:"bytes";i:605;s:11:"size_before";i:10727;s:10:"size_after";i:10122;s:4:"time";d:0.029999999999999999;}s:12:"shop_catalog";O:8:"stdClass":5:{s:7:"percent";d:4.6100000000000003;s:5:"bytes";i:1062;s:11:"size_before";i:23039;s:10:"size_after";i:21977;s:4:"time";d:0.01;}s:11:"shop_single";O:8:"stdClass":5:{s:7:"percent";d:3.2400000000000002;s:5:"bytes";i:2215;s:11:"size_before";i:68450;s:10:"size_after";i:66235;s:4:"time";d:0.040000000000000001;}s:21:"section-background-xs";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:60039;s:10:"size_after";i:60039;s:4:"time";d:0.040000000000000001;}s:21:"section-background-sm";O:8:"stdClass":5:{s:7:"percent";d:4.3600000000000003;s:5:"bytes";i:4153;s:11:"size_before";i:95225;s:10:"size_after";i:91072;s:4:"time";d:0.080000000000000002;}s:21:"section-background-md";O:8:"stdClass":5:{s:7:"percent";d:3.8500000000000001;s:5:"bytes";i:4933;s:11:"size_before";i:128215;s:10:"size_after";i:123282;s:4:"time";d:0.070000000000000007;}}}'),
(9330, 327, '_wp_attached_file', '2016/11/cyk-1.jpg'),
(9331, 327, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-1-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-1-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9332, 328, '_wp_attached_file', '2016/11/cyk-2.jpg'),
(9333, 328, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-2-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-2-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9334, 329, '_wp_attached_file', '2016/11/cyk-3.jpg'),
(9335, 329, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-3-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-3-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9336, 327, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.4370171170017505;s:5:"bytes";i:1508;s:11:"size_before";i:23427;s:10:"size_after";i:21919;s:4:"time";d:0.080000000000000002;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.21;s:5:"bytes";i:282;s:11:"size_before";i:5411;s:10:"size_after";i:5129;s:4:"time";d:0.029999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4400000000000004;s:5:"bytes";i:328;s:11:"size_before";i:6028;s:10:"size_after";i:5700;s:4:"time";d:0.029999999999999999;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.4900000000000002;s:5:"bytes";i:898;s:11:"size_before";i:11988;s:10:"size_after";i:11090;s:4:"time";d:0.02;}}}'),
(9337, 328, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.1406680968433962;s:5:"bytes";i:1864;s:11:"size_before";i:26104;s:10:"size_after";i:24240;s:4:"time";d:0.060000000000000005;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.1699999999999999;s:5:"bytes";i:369;s:11:"size_before";i:5983;s:10:"size_after";i:5614;s:4:"time";d:0.040000000000000001;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.96;s:5:"bytes";i:398;s:11:"size_before";i:6682;s:10:"size_after";i:6284;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.1600000000000001;s:5:"bytes";i:1097;s:11:"size_before";i:13439;s:10:"size_after";i:12342;s:4:"time";d:0.01;}}}'),
(9338, 330, '_wp_attached_file', '2016/11/cyk-4.jpg'),
(9339, 330, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-4-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-4-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9340, 329, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.1048221932601008;s:5:"bytes";i:1908;s:11:"size_before";i:26855;s:10:"size_after";i:24947;s:4:"time";d:0.10000000000000001;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";i:6;s:5:"bytes";i:365;s:11:"size_before";i:6083;s:10:"size_after";i:5718;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.0099999999999998;s:5:"bytes";i:412;s:11:"size_before";i:6857;s:10:"size_after";i:6445;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.1300000000000008;s:5:"bytes";i:1131;s:11:"size_before";i:13915;s:10:"size_after";i:12784;s:4:"time";d:0.080000000000000002;}}}'),
(9341, 331, '_wp_attached_file', '2016/11/cyk-5.jpg'),
(9342, 331, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-5-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-5-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9343, 330, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.9226737815045976;s:5:"bytes";i:1453;s:11:"size_before";i:20989;s:10:"size_after";i:19536;s:4:"time";d:0.029999999999999999;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.4500000000000002;s:5:"bytes";i:268;s:11:"size_before";i:4921;s:10:"size_after";i:4653;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.5;s:5:"bytes";i:292;s:11:"size_before";i:5313;s:10:"size_after";i:5021;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.3000000000000007;s:5:"bytes";i:893;s:11:"size_before";i:10755;s:10:"size_after";i:9862;s:4:"time";d:0.01;}}}'),
(9344, 332, '_wp_attached_file', '2016/11/cyk-6.jpg'),
(9345, 332, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:230;s:6:"height";i:146;s:4:"file";s:17:"2016/11/cyk-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"cyk-6-150x146.jpg";s:5:"width";i:150;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:17:"cyk-6-180x146.jpg";s:5:"width";i:180;s:6:"height";i:146;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9346, 331, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:6.7720289599152395;s:5:"bytes";i:1534;s:11:"size_before";i:22652;s:10:"size_after";i:21118;s:4:"time";d:0.090000000000000011;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.0999999999999996;s:5:"bytes";i:320;s:11:"size_before";i:5249;s:10:"size_after";i:4929;s:4:"time";d:0.01;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:5.5899999999999999;s:5:"bytes";i:323;s:11:"size_before";i:5777;s:10:"size_after";i:5454;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:7.6600000000000001;s:5:"bytes";i:891;s:11:"size_before";i:11626;s:10:"size_after";i:10735;s:4:"time";d:0.070000000000000007;}}}'),
(9347, 332, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:7.5010158472165784;s:5:"bytes";i:1846;s:11:"size_before";i:24610;s:10:"size_after";i:22764;s:4:"time";d:0.070000000000000007;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:3:{s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.3700000000000001;s:5:"bytes";i:382;s:11:"size_before";i:6001;s:10:"size_after";i:5619;s:4:"time";d:0.029999999999999999;}s:14:"shop_thumbnail";O:8:"stdClass":5:{s:7:"percent";d:6.5599999999999996;s:5:"bytes";i:433;s:11:"size_before";i:6596;s:10:"size_after";i:6163;s:4:"time";d:0.01;}s:4:"full";O:8:"stdClass":5:{s:7:"percent";d:8.5800000000000001;s:5:"bytes";i:1031;s:11:"size_before";i:12013;s:10:"size_after";i:10982;s:4:"time";d:0.029999999999999999;}}}'),
(9348, 59, 'content_0_show', '1'),
(9349, 59, '_content_0_show', 'field_5845620a97f9d'),
(9350, 59, 'content_0_layout_0_content', '<p class="intro">Wielofunkcyjna maszyna, do której można dopasować różne talerze by wykonała wiele rozmaitych prac związanych z przygotowaniem podłogi.</p>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.'),
(9351, 59, '_content_0_layout_0_content', 'field_584562c22e459'),
(9352, 59, 'content_0_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9353, 59, '_content_0_layout', 'field_584562a62e458'),
(9354, 59, 'content_1_show', '1'),
(9355, 59, '_content_1_show', 'field_5845620a97f9d'),
(9356, 59, 'content_1_layout_0_image', '318'),
(9357, 59, '_content_1_layout_0_image', 'field_584562f32e45b'),
(9358, 59, 'content_1_layout_0_capiton', ''),
(9359, 59, '_content_1_layout_0_capiton', 'field_584563072e45c'),
(9360, 59, 'content_1_layout', 'a:1:{i:0;s:7:"zdjecie";}'),
(9361, 59, '_content_1_layout', 'field_584562a62e458'),
(9362, 59, 'content_2_show', '1'),
(9363, 59, '_content_2_show', 'field_5845620a97f9d'),
(9364, 59, 'content_2_layout_0_images_0_image', '320'),
(9365, 59, '_content_2_layout_0_images_0_image', 'field_584564252e45f'),
(9366, 59, 'content_2_layout_0_images_0_caption', 'Large wheels for easy transportation'),
(9367, 59, '_content_2_layout_0_images_0_caption', 'field_584564322e460'),
(9368, 59, 'content_2_layout_0_images_1_image', '321'),
(9369, 59, '_content_2_layout_0_images_1_image', 'field_584564252e45f'),
(9370, 59, 'content_2_layout_0_images_1_caption', 'Thermal and power overload fuse ensures operational safety'),
(9371, 59, '_content_2_layout_0_images_1_caption', 'field_584564322e460'),
(9372, 59, 'content_2_layout_0_images_2_image', '322'),
(9373, 59, '_content_2_layout_0_images_2_image', 'field_584564252e45f'),
(9374, 59, 'content_2_layout_0_images_2_caption', 'Folding and stepless shaft adjustment for easy handling'),
(9375, 59, '_content_2_layout_0_images_2_caption', 'field_584564322e460'),
(9376, 59, 'content_2_layout_0_images_3_image', '323'),
(9377, 59, '_content_2_layout_0_images_3_image', 'field_584564252e45f'),
(9378, 59, 'content_2_layout_0_images_3_caption', 'Fuly sealed system keeps indoor air perfectly clean'),
(9379, 59, '_content_2_layout_0_images_3_caption', 'field_584564322e460'),
(9380, 59, 'content_2_layout_0_images_4_image', '324'),
(9381, 59, '_content_2_layout_0_images_4_image', 'field_584564252e45f'),
(9382, 59, 'content_2_layout_0_images_4_caption', 'Pressure release valve for easy filter clearing'),
(9383, 59, '_content_2_layout_0_images_4_caption', 'field_584564322e460'),
(9384, 59, 'content_2_layout_0_images_5_image', '325'),
(9385, 59, '_content_2_layout_0_images_5_image', 'field_584564252e45f'),
(9386, 59, 'content_2_layout_0_images_5_caption', 'Unique 2-step cyclonic filter separation process'),
(9387, 59, '_content_2_layout_0_images_5_caption', 'field_584564322e460'),
(9388, 59, 'content_2_layout_0_images', '6'),
(9389, 59, '_content_2_layout_0_images', 'field_584564172e45e'),
(9390, 59, 'content_2_layout', 'a:1:{i:0;s:13:"galeria-zdjec";}'),
(9391, 59, '_content_2_layout', 'field_584562a62e458'),
(9392, 59, 'content_3_show', '1'),
(9393, 59, '_content_3_show', 'field_5845620a97f9d'),
(9394, 59, 'content_3_layout_0_content', 'Bona FlexiSand Pro System is our innovative answer to a simpler and smoother working process – one flexible solution for all flooring challenges. Whether it’s precleaning, sanding, oiling or polishing, the all-purpose system offers a dynamic solution with exceptional quality and performance. One powerful solution is at hand in one versatile system.\r\n\r\nNow Bona introduces even more innovations into the FlexiSand Pro System, including the Bona FlexiSand 1.9. With a strong and reliable 1.9 kW motor, it delivers high performance direction-free sanding for even the toughest surfaces such as concrete.'),
(9395, 59, '_content_3_layout_0_content', 'field_584562c22e459'),
(9396, 59, 'content_3_layout', 'a:1:{i:0;s:5:"tekst";}'),
(9397, 59, '_content_3_layout', 'field_584562a62e458'),
(9436, 59, 'content', '6'),
(9437, 59, '_content', 'field_5845620a7c5f4'),
(9528, 59, 'content_2_layout_0_content', 'Bona FlexiSand Pro System is our innovative answer to a simpler and smoother working process – one flexible solution for all flooring challenges. Whether it’s precleaning, sanding, oiling or polishing, the all-purpose system offers a dynamic solution with exceptional quality and performance. One powerful solution is at hand in one versatile system.\r\n\r\nNow Bona introduces even more innovations into the FlexiSand Pro System, including the Bona FlexiSand 1.9. With a strong and reliable 1.9 kW motor, it delivers high performance direction-free sanding for even the toughest surfaces such as concrete.'),
(9529, 59, '_content_2_layout_0_content', 'field_584562c22e459'),
(9530, 59, 'content_3_layout_0_image', '326'),
(9531, 59, '_content_3_layout_0_image', 'field_584562f32e45b'),
(9532, 59, 'content_3_layout_0_capiton', ''),
(9533, 59, '_content_3_layout_0_capiton', 'field_584563072e45c'),
(9540, 59, 'content_4_show', '1'),
(9541, 59, '_content_4_show', 'field_5845620a97f9d'),
(9542, 59, 'content_4_layout_0_image', '326'),
(9543, 59, '_content_4_layout_0_image', 'field_584562f32e45b'),
(9544, 59, 'content_4_layout_0_capiton', '') ;
INSERT INTO `wpwyp2810161505_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(9545, 59, '_content_4_layout_0_capiton', 'field_584563072e45c'),
(9546, 59, 'content_4_layout', 'a:1:{i:0;s:7:"zdjecie";}'),
(9547, 59, '_content_4_layout', 'field_584562a62e458'),
(9548, 59, 'content_5_show', '1'),
(9549, 59, '_content_5_show', 'field_5845620a97f9d'),
(9550, 59, 'content_5_layout_0_images_0_image', '327'),
(9551, 59, '_content_5_layout_0_images_0_image', 'field_584564252e45f'),
(9552, 59, 'content_5_layout_0_images_0_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9553, 59, '_content_5_layout_0_images_0_caption', 'field_584564322e460'),
(9554, 59, 'content_5_layout_0_images_1_image', '328'),
(9555, 59, '_content_5_layout_0_images_1_image', 'field_584564252e45f'),
(9556, 59, 'content_5_layout_0_images_1_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9557, 59, '_content_5_layout_0_images_1_caption', 'field_584564322e460'),
(9558, 59, 'content_5_layout_0_images_2_image', '329'),
(9559, 59, '_content_5_layout_0_images_2_image', 'field_584564252e45f'),
(9560, 59, 'content_5_layout_0_images_2_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9561, 59, '_content_5_layout_0_images_2_caption', 'field_584564322e460'),
(9562, 59, 'content_5_layout_0_images_3_image', '330'),
(9563, 59, '_content_5_layout_0_images_3_image', 'field_584564252e45f'),
(9564, 59, 'content_5_layout_0_images_3_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9565, 59, '_content_5_layout_0_images_3_caption', 'field_584564322e460'),
(9566, 59, 'content_5_layout_0_images_4_image', '331'),
(9567, 59, '_content_5_layout_0_images_4_image', 'field_584564252e45f'),
(9568, 59, 'content_5_layout_0_images_4_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9569, 59, '_content_5_layout_0_images_4_caption', 'field_584564322e460'),
(9570, 59, 'content_5_layout_0_images_5_image', '332'),
(9571, 59, '_content_5_layout_0_images_5_image', 'field_584564252e45f'),
(9572, 59, 'content_5_layout_0_images_5_caption', '<ul>\r\n 	<li>Bare wood sanding</li>\r\n 	<li>Great with Bona FlexiSand</li>\r\n</ul>\r\n<h4>Bona Power Drive</h4>\r\nA breakthrough development specially for the Bona FlexiSand 1.5 and 1.9, the Bona Power Drive delivers optimal performance in bare wood sanding. Equipped with geared rotating discs, its unique design ensures direction-free sanding and high power. It is also easy to operate in narrow areas and corners.'),
(9573, 59, '_content_5_layout_0_images_5_caption', 'field_584564322e460'),
(9574, 59, 'content_5_layout_0_images', '6'),
(9575, 59, '_content_5_layout_0_images', 'field_584564172e45e'),
(9576, 59, 'content_5_layout', 'a:1:{i:0;s:13:"galeria-zdjec";}'),
(9577, 59, '_content_5_layout', 'field_584562a62e458'),
(9770, 59, 'content_1_layout_0_caption', '<h2>The Power of Flexibility</h2>\r\nJej potężny silnik 1,5 kW i solidna budowa zapewniają stabilność nawet przy dużym obciążeniu. Bona FlexiSand 1.5 jest kompatybilna z Bona Power Drive, unikalnym talerzem pozwalającym na silne szlifowanie do surowego drewna, nieograniczone kierunkiem szlifowania. Aby optymalnie zredukować wydalanie pyłu, Bona FlexiSand 1.5 można podłączyć do Bona DCS 70.'),
(9771, 59, '_content_1_layout_0_caption', 'field_584563072e45c'),
(9772, 59, 'content_4_layout_0_caption', ''),
(9773, 59, '_content_4_layout_0_caption', 'field_584563072e45c'),
(9874, 315, '_booking_min', ''),
(9875, 315, '_booking_max', ''),
(9876, 315, '_booking_dates', 'global'),
(9877, 315, '_booking_duration', 'global'),
(9878, 315, '_custom_booking_duration', ''),
(9879, 315, '_first_available_date', ''),
(9880, 315, '_booking_option', 'yes'),
(9881, 337, '_edit_last', '1'),
(9882, 337, 'discount_type', 'percent_product'),
(9883, 337, 'coupon_amount', '50'),
(9884, 337, 'individual_use', 'no'),
(9885, 337, 'product_ids', '315'),
(9886, 337, 'exclude_product_ids', ''),
(9887, 337, 'usage_limit', ''),
(9888, 337, 'usage_limit_per_user', ''),
(9889, 337, 'limit_usage_to_x_items', ''),
(9890, 337, 'expiry_date', ''),
(9891, 337, 'free_shipping', 'no'),
(9892, 337, 'exclude_sale_items', 'no'),
(9893, 337, 'product_categories', 'a:0:{}'),
(9894, 337, 'exclude_product_categories', 'a:0:{}'),
(9895, 337, 'minimum_amount', ''),
(9896, 337, 'maximum_amount', ''),
(9897, 337, 'customer_email', 'a:0:{}'),
(9898, 337, '_edit_lock', '1480957874:1') ;

#
# End of data contents of table `wpwyp2810161505_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_posts`
#

DROP TABLE IF EXISTS `wpwyp2810161505_posts`;


#
# Table structure of table `wpwyp2810161505_posts`
#

CREATE TABLE `wpwyp2810161505_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_posts`
#
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2016-10-28 13:10:22', '2016-10-28 13:10:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2016-10-28 13:10:22', '2016-10-28 13:10:22', '', 0, 'http://wypozyczalniacykliniarek.com/?p=1', 0, 'post', '', 1),
(5, 1, '2016-11-03 16:13:12', '2016-11-03 16:13:12', '', 'Podstrona opisowa', '', 'publish', 'closed', 'closed', '', 'strona-opisowa', '', '', '2016-12-05 12:59:39', '2016-12-05 12:59:39', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=5', 0, 'page', '', 0),
(35, 1, '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 'cykliniarki', '', 'inherit', 'open', 'closed', '', 'cykliniarki', '', '', '2016-11-04 17:30:40', '2016-11-04 17:30:40', '', 5, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 'Strona o ciasteczkach', '', 'publish', 'closed', 'closed', '', 'strona-o-ciasteczkach', '', '', '2016-11-05 11:29:20', '2016-11-05 11:29:20', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=45', 0, 'page', '', 0),
(48, 1, '2016-11-07 17:22:44', '2016-11-07 17:22:44', '', 'Kalkulator', '', 'publish', 'closed', 'closed', '', 'kalkulator', '', '', '2016-11-07 17:24:08', '2016-11-07 17:24:08', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=48', 0, 'page', '', 0),
(52, 1, '2016-11-23 10:00:37', '2016-11-23 10:00:37', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2016-12-01 13:41:20', '2016-12-01 13:41:20', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=52', 0, 'page', '', 0),
(54, 1, '2016-11-23 12:30:01', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:01', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2016-11-23 12:30:03', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:03', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=55', 1, 'nav_menu_item', '', 0),
(56, 1, '2016-11-23 12:30:05', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-11-23 12:30:05', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 'O nas', '', 'publish', 'closed', 'closed', '', 'o-nas', '', '', '2016-11-23 12:30:26', '2016-11-23 12:30:26', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=57', 0, 'page', '', 0),
(59, 1, '2016-11-23 12:30:45', '2016-11-23 12:30:45', '', 'Nasze cykliniarki', '', 'publish', 'closed', 'closed', '', 'nasze-cykliniarki', '', '', '2016-12-05 16:08:13', '2016-12-05 16:08:13', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=59', 0, 'page', '', 0),
(60, 1, '2016-11-23 12:30:51', '2016-11-23 12:30:51', '', 'Nasze produkty', '', 'publish', 'closed', 'closed', '', 'produkty', '', '', '2016-11-30 13:01:42', '2016-11-30 13:01:42', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=60', 0, 'page', '', 0),
(61, 1, '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 'Kontakt', '', 'publish', 'closed', 'closed', '', 'kontakt', '', '', '2016-11-23 12:30:59', '2016-11-23 12:30:59', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=61', 0, 'page', '', 0),
(62, 1, '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 'Zarezerwuj', '', 'publish', 'closed', 'closed', '', 'zarezerwuj', '', '', '2016-11-23 12:31:05', '2016-11-23 12:31:05', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=62', 0, 'page', '', 0),
(67, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=67', 6, 'nav_menu_item', '', 0),
(68, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=68', 5, 'nav_menu_item', '', 0),
(69, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=69', 4, 'nav_menu_item', '', 0),
(70, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=70', 3, 'nav_menu_item', '', 0),
(71, 1, '2016-11-23 12:31:53', '2016-11-23 12:31:53', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2016-11-23 12:31:53', '2016-11-23 12:31:53', '', 0, 'http://wypozyczalniacykliniarek.com/?p=71', 2, 'nav_menu_item', '', 0),
(72, 1, '2016-11-23 12:31:52', '2016-11-23 12:31:52', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2016-11-23 12:31:52', '2016-11-23 12:31:52', '', 0, 'http://wypozyczalniacykliniarek.com/?p=72', 1, 'nav_menu_item', '', 0),
(73, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:7:{s:8:"location";a:2:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:28:"page-templates/home-page.php";}}i:1;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:7:"product";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:6:{i:0;s:11:"the_content";i:1;s:7:"excerpt";i:2;s:8:"comments";i:3;s:6:"format";i:4;s:15:"page_attributes";i:5;s:14:"featured_image";}s:11:"description";s:0:"";}', 'Main', 'main', 'publish', 'closed', 'closed', '', 'group_5835d5033efe3', '', '', '2016-12-05 12:47:14', '2016-12-05 12:47:14', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=73', 0, 'acf-field-group', '', 0),
(74, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:19:"field_5836c16c062d1";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5835d51f24055', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 73, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=74', 0, 'acf-field', '', 0),
(75, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_5835d59224057', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=75', 3, 'acf-field', '', 0),
(76, 1, '2016-11-23 17:47:23', '2016-11-23 17:47:23', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:10:{i:0;a:6:{s:3:"key";s:13:"5835d5a00eafe";s:5:"label";s:5:"Media";s:4:"name";s:5:"media";s:7:"display";s:5:"table";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"583707ef9372e";s:5:"label";s:4:"None";s:4:"name";s:4:"none";s:7:"display";s:5:"table";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:2;a:6:{s:3:"key";s:13:"5836c41b2cf6f";s:5:"label";s:15:"Jak to działa?";s:4:"name";s:13:"jak-to-dziala";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:3;a:6:{s:3:"key";s:13:"5836ca79b915f";s:5:"label";s:12:"Tani wynajem";s:4:"name";s:12:"tani-wynajem";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:4;a:6:{s:3:"key";s:13:"583837b5a4e28";s:5:"label";s:22:"Sprawdź dostępność";s:4:"name";s:18:"sprawdz-dostepnosc";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:5;a:6:{s:3:"key";s:13:"58385655f0111";s:5:"label";s:6:"Opinie";s:4:"name";s:6:"opinie";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:6;a:6:{s:3:"key";s:13:"583863ae42722";s:5:"label";s:3:"FAQ";s:4:"name";s:3:"faq";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:7;a:6:{s:3:"key";s:13:"583c06360a8f2";s:5:"label";s:24:"Szczegółowe informacje";s:4:"name";s:22:"szczegolowe-informacje";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:8;a:6:{s:3:"key";s:13:"583c06c70a8f5";s:5:"label";s:11:"Do pobrania";s:4:"name";s:11:"do-pobrania";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:9;a:6:{s:3:"key";s:13:"58400ca2fde30";s:5:"label";s:8:"Produkty";s:4:"name";s:8:"produkty";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_5835d59b24058', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=76', 4, 'acf-field', '', 0),
(77, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:16:{s:4:"type";s:5:"image";s:12:"instructions";s:17:"Dodaj zdjęcie...";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5835d5b924059', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=77', 0, 'acf-field', '', 0),
(78, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:34:"... lub kod iframe do pliku wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Iframe', 'iframe', 'publish', 'closed', 'closed', '', 'field_5835d5ea2405b', '', '', '2016-11-23 17:47:24', '2016-11-23 17:47:24', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=78', 1, 'acf-field', '', 0),
(79, 1, '2016-11-23 17:47:24', '2016-11-23 17:47:24', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:27:"Podpis pod zdjęciem/wideo.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5835d5a00eafe";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_5835d5ca2405a', '', '', '2016-11-24 10:46:23', '2016-11-24 10:46:23', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=79', 2, 'acf-field', '', 0),
(80, 1, '2016-11-23 17:49:26', '2016-11-23 17:49:26', '', 'fig', '', 'inherit', 'open', 'closed', '', 'fig', '', '', '2016-12-01 11:36:44', '2016-12-01 11:36:44', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/fig.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_5836c16c062d1', '', '', '2016-11-28 10:42:11', '2016-11-28 10:42:11', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=89', 1, 'acf-field', '', 0),
(90, 1, '2016-11-24 10:33:08', '2016-11-24 10:33:08', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_5836c16c062d1";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_5836c16c062d1";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_5836c1c4062d2', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=90', 2, 'acf-field', '', 0),
(94, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836c41b2cf6f";s:9:"collapsed";s:0:"";s:3:"min";i:4;s:3:"max";i:4;s:6:"layout";s:3:"row";s:12:"button_label";s:8:"Add Step";}', 'Step', 'step', 'publish', 'closed', 'closed', '', 'field_5836c4452cf70', '', '', '2016-11-25 16:11:12', '2016-11-25 16:11:12', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=94', 0, 'acf-field', '', 0),
(95, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:16:{s:11:"znak-cytatu";s:11:"znak-cytatu";s:12:"strzalka-rog";s:12:"strzalka-rog";s:9:"logo-znak";s:9:"logo-znak";s:13:"strzalka-lewo";s:13:"strzalka-lewo";s:14:"strzalka-prawo";s:14:"strzalka-prawo";s:11:"cykliniarka";s:11:"cykliniarka";s:5:"email";s:5:"email";s:6:"myszka";s:6:"myszka";s:6:"olowek";s:6:"olowek";s:7:"telefon";s:7:"telefon";s:7:"pinezka";s:7:"pinezka";s:4:"sejf";s:4:"sejf";s:8:"gwiazdka";s:8:"gwiazdka";s:5:"wozek";s:5:"wozek";s:10:"ciezarowka";s:10:"ciezarowka";s:16:"ciezarowka-pusta";s:16:"ciezarowka-pusta";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_5836c4722cf71', '', '', '2016-11-25 16:11:12', '2016-11-25 16:11:12', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=95', 0, 'acf-field', '', 0),
(96, 1, '2016-11-24 10:46:24', '2016-11-24 10:46:24', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";i:2;s:9:"new_lines";s:2:"br";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_5836c4db2cf72', '', '', '2016-11-24 10:54:45', '2016-11-24 10:54:45', '', 94, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=96', 1, 'acf-field', '', 0),
(101, 1, '2016-11-24 11:10:28', '2016-11-24 11:10:28', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"5836ca79b915f";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5836ca8eb9160', '', '', '2016-11-24 11:10:28', '2016-11-24 11:10:28', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=101', 0, 'acf-field', '', 0),
(107, 1, '2016-11-24 15:29:03', '2016-11-24 15:29:03', '', 'mapa', '', 'inherit', 'open', 'closed', '', 'mapa', '', '', '2016-11-24 15:29:03', '2016-11-24 15:29:03', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa.png', 0, 'attachment', 'image/png', 0),
(112, 1, '2016-11-24 15:46:02', '2016-11-24 15:46:02', '', 'main', '', 'inherit', 'open', 'closed', '', 'main', '', '', '2016-11-24 15:46:08', '2016-11-24 15:46:08', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/main.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2016-11-24 16:16:10', '2016-11-24 16:16:10', '', 'logo-domalux', '', 'inherit', 'open', 'closed', '', 'logo-domalux', '', '', '2016-12-01 15:31:49', '2016-12-01 15:31:49', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-domalux.png', 0, 'attachment', 'image/png', 0),
(139, 1, '2016-11-24 16:16:12', '2016-11-24 16:16:12', '', 'logo-dotpay', '', 'inherit', 'open', 'closed', '', 'logo-dotpay', '', '', '2016-12-01 15:31:59', '2016-12-01 15:31:59', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-dotpay.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2016-11-24 16:16:14', '2016-11-24 16:16:14', '', 'logo-tba', '', 'inherit', 'open', 'closed', '', 'logo-tba', '', '', '2016-11-24 16:16:14', '2016-11-24 16:16:14', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/logo-tba.png', 0, 'attachment', 'image/png', 0),
(142, 1, '2016-11-24 16:22:48', '2016-11-24 16:22:48', '', 'Regulamin', '', 'publish', 'closed', 'closed', '', 'regulamin', '', '', '2016-11-24 16:22:48', '2016-11-24 16:22:48', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=142', 0, 'page', '', 0),
(143, 1, '2016-11-24 16:22:53', '2016-11-24 16:22:53', '', 'FAQ', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2016-11-24 16:22:53', '2016-11-24 16:22:53', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=143', 0, 'page', '', 0),
(144, 1, '2016-11-24 16:22:58', '2016-11-24 16:22:58', '', 'Cennik', '', 'publish', 'closed', 'closed', '', 'cennik', '', '', '2016-11-24 16:22:58', '2016-11-24 16:22:58', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=144', 0, 'page', '', 0),
(145, 1, '2016-11-24 16:23:18', '2016-11-24 16:23:18', ' ', '', '', 'publish', 'closed', 'closed', '', '145', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=145', 1, 'nav_menu_item', '', 0),
(149, 1, '2016-11-24 16:23:12', '2016-11-24 16:23:12', '', 'Materiały szkoleniowe', '', 'publish', 'closed', 'closed', '', 'materialy-szkoleniowe', '', '', '2016-11-24 16:23:12', '2016-11-24 16:23:12', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=149', 0, 'page', '', 0),
(151, 1, '2016-11-24 16:23:40', '2016-11-24 16:23:40', ' ', '', '', 'publish', 'closed', 'closed', '', '151', '', '', '2016-11-24 16:23:40', '2016-11-24 16:23:40', '', 0, 'http://wypozyczalniacykliniarek.com/?p=151', 5, 'nav_menu_item', '', 0),
(152, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '152', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=152', 4, 'nav_menu_item', '', 0),
(153, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '153', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=153', 3, 'nav_menu_item', '', 0),
(154, 1, '2016-11-24 16:23:39', '2016-11-24 16:23:39', ' ', '', '', 'publish', 'closed', 'closed', '', '154', '', '', '2016-11-24 16:23:39', '2016-11-24 16:23:39', '', 0, 'http://wypozyczalniacykliniarek.com/?p=154', 2, 'nav_menu_item', '', 0),
(162, 1, '2016-11-25 13:08:44', '2016-11-25 13:08:44', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583837b5a4e28";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Input label', 'input_label', 'publish', 'closed', 'closed', '', 'field_583837c3a4e29', '', '', '2016-11-25 13:08:44', '2016-11-25 13:08:44', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=162', 0, 'acf-field', '', 0),
(163, 1, '2016-11-25 13:08:44', '2016-11-25 13:08:44', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583837b5a4e28";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Search label', 'search_label', 'publish', 'closed', 'closed', '', 'field_583837cfa4e2a', '', '', '2016-11-25 13:08:44', '2016-11-25 13:08:44', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=163', 1, 'acf-field', '', 0),
(165, 1, '2016-11-25 14:56:19', '2016-11-25 14:56:19', '', 'opinie', '', 'inherit', 'open', 'closed', '', 'opinie', '', '', '2016-11-25 14:56:25', '2016-11-25 14:56:25', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/opinie.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2016-11-25 14:58:07', '2016-11-25 14:58:07', '', 'ocena', '', 'inherit', 'open', 'closed', '', 'ocena', '', '', '2016-11-25 14:58:07', '2016-11-25 14:58:07', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/ocena.png', 0, 'attachment', 'image/png', 0),
(170, 1, '2016-11-25 15:20:20', '2016-11-25 15:20:20', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";s:0:"";s:17:"conditional_logic";s:0:"";s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"58385655f0111";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:0:"";s:12:"button_label";s:10:"Add Review";}', 'Reviews', 'reviews', 'publish', 'closed', 'closed', '', 'field_5838565af0112', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=170', 0, 'acf-field', '', 0),
(171, 1, '2016-11-25 15:20:20', '2016-11-25 15:20:20', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_58385679f0113', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=171', 0, 'acf-field', '', 0),
(172, 1, '2016-11-25 15:20:21', '2016-11-25 15:20:21', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:7:"wpautop";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_58385681f0114', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=172', 1, 'acf-field', '', 0),
(173, 1, '2016-11-25 15:20:21', '2016-11-25 15:20:21', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Name', 'name', 'publish', 'closed', 'closed', '', 'field_58385693f0115', '', '', '2016-11-25 15:29:37', '2016-11-25 15:29:37', '', 170, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=173', 2, 'acf-field', '', 0),
(176, 1, '2016-11-25 15:26:31', '2016-11-25 15:26:31', '', 'photo', '', 'inherit', 'open', 'closed', '', 'photo', '', '', '2016-11-25 15:26:42', '2016-11-25 15:26:42', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/photo.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583863ae42722";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:12:"Add Question";}', 'FAQs', 'faqs', 'publish', 'closed', 'closed', '', 'field_583863ae42723', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=180', 0, 'acf-field', '', 0),
(181, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Question', 'question', 'publish', 'closed', 'closed', '', 'field_583863f942729', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 180, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=181', 0, 'acf-field', '', 0),
(182, 1, '2016-11-25 16:18:03', '2016-11-25 16:18:03', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:2:"br";}', 'Answear', 'answear', 'publish', 'closed', 'closed', '', 'field_583863ff4272a', '', '', '2016-11-25 16:18:03', '2016-11-25 16:18:03', '', 180, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=182', 1, 'acf-field', '', 0),
(188, 1, '2016-11-26 11:46:47', '2016-11-26 11:46:47', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_58397608ccf61', '', '', '2016-11-26 11:46:47', '2016-11-26 11:46:47', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=188', 0, 'acf-field', '', 0),
(190, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 0, 'http://wypozyczalniacykliniarek.com/shop/', 0, 'page', '', 0),
(191, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 0, 'http://wypozyczalniacykliniarek.com/cart/', 0, 'page', '', 0),
(192, 1, '2016-11-26 12:48:05', '2016-11-26 12:48:05', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2016-11-26 12:48:05', '2016-11-26 12:48:05', '', 0, 'http://wypozyczalniacykliniarek.com/checkout/', 0, 'page', '', 0),
(193, 1, '2016-11-26 12:48:06', '2016-11-26 12:48:06', '[woocommerce_my_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2016-11-26 12:48:06', '2016-11-26 12:48:06', '', 0, 'http://wypozyczalniacykliniarek.com/my-account/', 0, 'page', '', 0),
(194, 1, '2016-11-26 12:57:20', '2016-11-26 12:57:20', 'Podkład odcinający AL jest poliwinylowym lakierem podkładowym na bazie alkoholu. Idealnie nadaje się do lakierowania drewna wewnątrz pomieszczeń, a w szczególności podłóg drewnianych: parkietów, mozaiki parkietowej, podłóg przemysłowych i desek z drewna europejskiego i egzotycznego. Minimalizuje wypłukiwanie olejków i garbników oraz stabilizuje luźne cząsteczki drewna. Dzięki właściwościom odcinającym (zamyka pory drewna) polecany na żywicznych i oleistych gatunkach drewna oraz do renowacji starych podłóg drewnianych, w tym wcześniej olejowanych. Zapewnia bardzo dobrą przyczepność lakierów nawierzchniowych oraz zwiększa ich wydajność.', 'Podkład odcinający AL. Domalux', 'To jest Product Short Description', 'publish', 'open', 'closed', '', 'podklad-odcinajacy-al-domalux', '', '', '2016-11-30 18:03:38', '2016-11-30 18:03:38', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&#038;p=194', 0, 'product', '', 0),
(195, 1, '2016-11-26 12:59:00', '2016-11-26 12:59:00', '', 'Atest', '', 'inherit', 'open', 'closed', '', 'produkt-podklad', '', '', '2016-11-28 13:22:03', '2016-11-28 13:22:03', '', 194, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/produkt-podklad.jpg', 0, 'attachment', 'image/jpeg', 0),
(196, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06360a8f2";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Left Column', 'col-left', 'publish', 'closed', 'closed', '', 'field_583c065c0a8f3', '', '', '2016-11-28 10:29:25', '2016-11-28 10:29:25', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=196', 0, 'acf-field', '', 0),
(197, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06360a8f2";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Right Column', 'col-right', 'publish', 'closed', 'closed', '', 'field_583c06a10a8f4', '', '', '2016-11-28 10:29:25', '2016-11-28 10:29:25', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=197', 1, 'acf-field', '', 0),
(198, 1, '2016-11-28 10:29:25', '2016-11-28 10:29:25', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583c06c70a8f5";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add File";}', 'Files', 'files', 'publish', 'closed', 'closed', '', 'field_583c06ce0a8f6', '', '', '2016-11-28 14:11:30', '2016-11-28 14:11:30', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=198', 0, 'acf-field', '', 0),
(199, 1, '2016-11-28 10:29:26', '2016-11-28 10:29:26', 'a:10:{s:4:"type";s:4:"file";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:7:"library";s:3:"all";s:8:"min_size";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'File', 'file', 'publish', 'closed', 'closed', '', 'field_583c06e00a8f7', '', '', '2016-11-28 14:11:30', '2016-11-28 14:11:30', '', 198, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=199', 0, 'acf-field', '', 0),
(200, 1, '2016-11-28 10:37:04', '2016-11-28 10:37:04', '', 'Karta producenta', '', 'inherit', 'open', 'closed', '', 'kt-dx-capon-extra-02-2010', '', '', '2016-11-28 13:21:50', '2016-11-28 13:21:50', '', 194, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/KT-DX-CAPON-EXTRA-02-2010.pdf', 0, 'attachment', 'application/pdf', 0),
(201, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'group_583c0ba522a74', '', '', '2016-12-01 15:29:59', '2016-12-01 15:29:59', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=201', 0, 'acf-field-group', '', 0),
(202, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content--footer', 'publish', 'closed', 'closed', '', 'field_583c0ba55100f', '', '', '2016-12-01 15:29:59', '2016-12-01 15:29:59', '', 201, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=202', 0, 'acf-field', '', 0),
(203, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_583c0ba566b90', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=203', 0, 'acf-field', '', 0),
(204, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_583c0ba566f83', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=204', 1, 'acf-field', '', 0),
(205, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_583c0ba566f83";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_583c0ba566f83";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_583c0ba56736b', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=205', 2, 'acf-field', '', 0),
(206, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_583c0ba56774c', '', '', '2016-11-28 10:49:09', '2016-11-28 10:49:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=206', 3, 'acf-field', '', 0),
(207, 1, '2016-11-28 10:49:09', '2016-11-28 10:49:09', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:1:{i:0;a:6:{s:3:"key";s:13:"583710010e6cb";s:5:"label";s:6:"Footer";s:4:"name";s:6:"footer";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_583c0ba567b49', '', '', '2016-11-28 10:49:51', '2016-11-28 10:49:51', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=207', 4, 'acf-field', '', 0),
(215, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";i:3;s:6:"layout";s:5:"table";s:12:"button_label";s:11:"Add Contact";}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed167', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=215', 0, 'acf-field', '', 0),
(216, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:16:{s:11:"znak-cytatu";s:11:"znak-cytatu";s:12:"strzalka-rog";s:12:"strzalka-rog";s:9:"logo-znak";s:9:"logo-znak";s:13:"strzalka-lewo";s:13:"strzalka-lewo";s:14:"strzalka-prawo";s:14:"strzalka-prawo";s:11:"cykliniarka";s:11:"cykliniarka";s:5:"email";s:5:"email";s:6:"myszka";s:6:"myszka";s:6:"olowek";s:6:"olowek";s:7:"telefon";s:7:"telefon";s:7:"pinezka";s:7:"pinezka";s:4:"sejf";s:4:"sejf";s:8:"gwiazdka";s:8:"gwiazdka";s:5:"wozek";s:5:"wozek";s:10:"ciezarowka";s:10:"ciezarowka";s:16:"ciezarowka-pusta";s:16:"ciezarowka-pusta";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_583c0ba6c5ecd', '', '', '2016-11-28 10:49:10', '2016-11-28 10:49:10', '', 215, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=216', 0, 'acf-field', '', 0),
(217, 1, '2016-11-28 10:49:10', '2016-11-28 10:49:10', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:5:"basic";s:12:"media_upload";i:0;}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_583c0ba6c629f', '', '', '2016-11-28 10:49:10', '2016-11-28 10:49:10', '', 215, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=217', 1, 'acf-field', '', 0),
(218, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:13:"Nasz partner:";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Partners Label', 'partners_label', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed552', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=218', 1, 'acf-field', '', 0),
(219, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add Logo";}', 'Partners', 'partners', 'publish', 'closed', 'closed', '', 'field_583c0ba5ed955', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=219', 2, 'acf-field', '', 0),
(220, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_583c0ba7362fa', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 219, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=220', 0, 'acf-field', '', 0),
(221, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:19:"Współpracujemy z:";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Cooperators Label', 'cooperators_label', 'publish', 'closed', 'closed', '', 'field_583c0ba5edd30', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=221', 3, 'acf-field', '', 0),
(222, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:8:"Add Logo";}', 'Cooperators', 'cooperators', 'publish', 'closed', 'closed', '', 'field_583c0ba5ee119', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 207, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=222', 4, 'acf-field', '', 0),
(223, 1, '2016-11-28 10:49:11', '2016-11-28 10:49:11', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'img', 'publish', 'closed', 'closed', '', 'field_583c0ba7849e0', '', '', '2016-11-28 10:49:11', '2016-11-28 10:49:11', '', 222, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=223', 0, 'acf-field', '', 0),
(243, 1, '2016-11-30 13:04:14', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-11-30 13:04:14', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?p=243', 0, 'post', '', 0),
(244, 1, '2016-11-30 13:15:27', '2016-11-30 13:15:27', 'Podkład odcinający AL jest poliwinylowym lakierem podkładowym na bazie alkoholu. Idealnie nadaje się do lakierowania drewna wewnątrz pomieszczeń, a w szczególności podłóg drewnianych: parkietów, mozaiki parkietowej, podłóg przemysłowych i desek z drewna europejskiego i egzotycznego. Minimalizuje wypłukiwanie olejków i garbników oraz stabilizuje luźne cząsteczki drewna. Dzięki właściwościom odcinającym (zamyka pory drewna) polecany na żywicznych i oleistych gatunkach drewna oraz do renowacji starych podłóg drewnianych, w tym wcześniej olejowanych. Zapewnia bardzo dobrą przyczepność lakierów nawierzchniowych oraz zwiększa ich wydajność.', 'Podkład odcinający AL. Domalux (Kopia)', 'To jest Product Short Description', 'publish', 'open', 'closed', '', 'podklad-odcinajacy-al-domalux-kopia', '', '', '2016-11-30 18:04:16', '2016-11-30 18:04:16', '', 0, 'http://wypozyczalniacykliniarek.com/produkt/podklad-odcinajacy-al-domalux-kopia/', 0, 'product', '', 0),
(246, 1, '2016-12-01 11:44:12', '2016-12-01 11:44:12', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"58400ca2fde30";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:11:"Add Product";}', 'Products', 'products', 'publish', 'closed', 'closed', '', 'field_58400ca2fde31', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 76, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=246', 0, 'acf-field', '', 0),
(247, 1, '2016-12-01 11:44:12', '2016-12-01 11:44:12', 'a:11:{s:4:"type";s:11:"post_object";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:1:{i:0;s:7:"product";}s:8:"taxonomy";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:13:"return_format";s:6:"object";s:2:"ui";i:1;}', 'Product', 'product', 'publish', 'closed', 'closed', '', 'field_58400cc8fde33', '', '', '2016-12-01 11:44:12', '2016-12-01 11:44:12', '', 246, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=247', 0, 'acf-field', '', 0),
(248, 1, '2016-12-01 11:45:11', '2016-12-01 11:45:11', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_58400d1d4d1e5', '', '', '2016-12-01 11:45:11', '2016-12-01 11:45:11', '', 74, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=248', 5, 'acf-field', '', 0),
(250, 1, '2016-12-01 12:04:09', '2016-12-01 12:04:09', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_584011ac7b85c', '', '', '2016-12-01 12:04:09', '2016-12-01 12:04:09', '', 202, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=250', 5, 'acf-field', '', 0),
(253, 1, '2016-12-01 13:31:11', '2016-12-01 13:31:11', '', 'mapa-warszawa', '', 'inherit', 'open', 'closed', '', 'mapa-warszawa', '', '', '2016-12-01 13:31:11', '2016-12-01 13:31:11', '', 52, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/mapa-warszawa.png', 0, 'attachment', 'image/png', 0),
(257, 1, '2016-12-01 15:34:01', '2016-12-01 15:34:01', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Header', 'header', 'publish', 'closed', 'closed', '', 'group_584042e9c9a0d', '', '', '2016-12-05 13:31:40', '2016-12-05 13:31:40', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=257', 0, 'acf-field-group', '', 0),
(258, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content--header', 'publish', 'closed', 'closed', '', 'field_584042ea01135', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 257, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=258', 0, 'acf-field', '', 0),
(259, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_584042ea1aba6', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=259', 0, 'acf-field', '', 0) ;
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(260, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:6:{s:5:"white";s:5:"white";s:5:"black";s:5:"black";s:10:"black-pure";s:10:"black-pure";s:3:"red";s:3:"red";s:3:"img";s:3:"img";s:8:"img-dark";s:8:"img-dark";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:13:"return_format";s:5:"value";s:11:"placeholder";s:0:"";}', 'Section Background', 'section_bg', 'publish', 'closed', 'closed', '', 'field_584042ea1afed', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=260', 1, 'acf-field', '', 0),
(261, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:17:"conditional_logic";a:2:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_584042ea1afed";s:8:"operator";s:2:"==";s:5:"value";s:3:"img";}}i:1;a:1:{i:0;a:3:{s:5:"field";s:19:"field_584042ea1afed";s:8:"operator";s:2:"==";s:5:"value";s:8:"img-dark";}}}}', 'Section Background Image', 'section_bg_img', 'publish', 'closed', 'closed', '', 'field_584042ea1b3a7', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=261', 2, 'acf-field', '', 0) ;
INSERT INTO `wpwyp2810161505_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(262, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Section Intro', 'section_intro', 'publish', 'closed', 'closed', '', 'field_584042ea1b7c5', '', '', '2016-12-01 15:34:02', '2016-12-01 15:34:02', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=262', 3, 'acf-field', '', 0),
(263, 1, '2016-12-01 15:34:02', '2016-12-01 15:34:02', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";s:0:"";s:3:"max";i:1;s:7:"layouts";a:2:{i:0;a:6:{s:3:"key";s:13:"583710010e6cb";s:5:"label";s:12:"Notification";s:4:"name";s:12:"notification";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"58404491994e7";s:5:"label";s:4:"None";s:4:"name";s:4:"none";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_584042ea1bb70', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=263', 4, 'acf-field', '', 0),
(273, 1, '2016-12-01 15:34:03', '2016-12-01 15:34:03', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";}', 'Content After', 'content_after', 'publish', 'closed', 'closed', '', 'field_584042ea1bf50', '', '', '2016-12-01 15:34:03', '2016-12-01 15:34:03', '', 258, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=273', 5, 'acf-field', '', 0),
(274, 1, '2016-12-01 15:41:55', '2016-12-01 15:41:55', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";s:0:"";s:17:"conditional_logic";s:0:"";s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"583710010e6cb";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";s:0:"";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_58404456994e6', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 263, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=274', 0, 'acf-field', '', 0),
(275, 1, '2016-12-01 15:41:55', '2016-12-01 15:41:55', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'field_584044ab994e9', '', '', '2016-12-01 15:41:55', '2016-12-01 15:41:55', '', 257, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=275', 1, 'acf-field', '', 0),
(276, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:31:"page-templates/content-page.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:6:{i:0;s:11:"the_content";i:1;s:7:"excerpt";i:2;s:8:"comments";i:3;s:6:"format";i:4;s:15:"page_attributes";i:5;s:14:"featured_image";}s:11:"description";s:0:"";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'group_5845620a498bb', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field-group&#038;p=276', 0, 'acf-field-group', '', 0),
(277, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";i:1;s:3:"max";s:0:"";s:6:"layout";s:5:"block";s:12:"button_label";s:11:"Add Section";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5845620a7c5f4', '', '', '2016-12-05 13:00:51', '2016-12-05 13:00:51', '', 276, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=277', 0, 'acf-field', '', 0),
(278, 1, '2016-12-05 12:48:10', '2016-12-05 12:48:10', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Show?', 'show', 'publish', 'closed', 'closed', '', 'field_5845620a97f9d', '', '', '2016-12-05 12:48:10', '2016-12-05 12:48:10', '', 277, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=278', 0, 'acf-field', '', 0),
(306, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:9:{s:4:"type";s:16:"flexible_content";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:12:"button_label";s:10:"Add Layout";s:3:"min";i:1;s:3:"max";i:1;s:7:"layouts";a:3:{i:0;a:6:{s:3:"key";s:13:"584562b7e8352";s:5:"label";s:5:"Tekst";s:4:"name";s:5:"tekst";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:1;a:6:{s:3:"key";s:13:"584562d02e45a";s:5:"label";s:8:"Zdjęcie";s:4:"name";s:7:"zdjecie";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}i:2;a:6:{s:3:"key";s:13:"584563412e45d";s:5:"label";s:15:"Galeria zdjęć";s:4:"name";s:13:"galeria-zdjec";s:7:"display";s:5:"block";s:3:"min";s:0:"";s:3:"max";s:0:"";}}}', 'Layout', 'layout', 'publish', 'closed', 'closed', '', 'field_584562a62e458', '', '', '2016-12-05 15:23:12', '2016-12-05 15:23:12', '', 277, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=306', 1, 'acf-field', '', 0),
(307, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562b7e8352";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_584562c22e459', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=307', 0, 'acf-field', '', 0),
(308, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:16:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562d02e45a";s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_584562f32e45b', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=308', 0, 'acf-field', '', 0),
(309, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:10:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584562d02e45a";s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_584563072e45c', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=309', 1, 'acf-field', '', 0),
(310, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:11:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"parent_layout";s:13:"584563412e45d";s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:12:"Dodaj wiersz";}', 'Images', 'images', 'publish', 'closed', 'closed', '', 'field_584564172e45e', '', '', '2016-12-05 16:07:36', '2016-12-05 16:07:36', '', 306, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&#038;p=310', 0, 'acf-field', '', 0),
(311, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_584564252e45f', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 310, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=311', 0, 'acf-field', '', 0),
(312, 1, '2016-12-05 12:58:43', '2016-12-05 12:58:43', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Caption', 'caption', 'publish', 'closed', 'closed', '', 'field_584564322e460', '', '', '2016-12-05 12:58:43', '2016-12-05 12:58:43', '', 310, 'http://wypozyczalniacykliniarek.com/?post_type=acf-field&p=312', 1, 'acf-field', '', 0),
(314, 1, '2016-12-05 14:36:48', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-12-05 14:36:48', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&p=314', 0, 'product', '', 0),
(315, 1, '2016-12-05 14:41:00', '2016-12-05 14:41:00', '', 'Duża i mała cykliniarka (zestaw)', '', 'publish', 'open', 'closed', '', 'duza-i-mala-cykliniarka-zestaw', '', '', '2016-12-05 16:41:45', '2016-12-05 16:41:45', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=product&#038;p=315', 0, 'product', '', 0),
(316, 1, '2016-12-05 14:45:40', '2016-12-05 14:45:40', '', 'Order &ndash; Grudzień 5, 2016 @ 02:45 PM', '', 'wc-on-hold', 'open', 'closed', 'order_58457d93979ce', 'zamowienie-dec-05-2016-o-0245-pm', '', '', '2016-12-05 14:45:40', '2016-12-05 14:45:40', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=shop_order&#038;p=316', 0, 'shop_order', '', 1),
(317, 1, '2016-12-05 14:51:52', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-12-05 14:51:52', '0000-00-00 00:00:00', '', 0, 'http://wypozyczalniacykliniarek.com/?page_id=317', 0, 'page', '', 0),
(318, 1, '2016-12-05 14:54:06', '2016-12-05 14:54:06', '', 'cykliniarki', '', 'inherit', 'open', 'closed', '', 'cykliniarki-2', '', '', '2016-12-05 14:54:12', '2016-12-05 14:54:12', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cykliniarki-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(320, 1, '2016-12-05 15:01:06', '2016-12-05 15:01:06', '', 'bona-1', '', 'inherit', 'open', 'closed', '', 'bona-1', '', '', '2016-12-05 15:01:27', '2016-12-05 15:01:27', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(321, 1, '2016-12-05 15:01:09', '2016-12-05 15:01:09', '', 'bona-2', '', 'inherit', 'open', 'closed', '', 'bona-2', '', '', '2016-12-05 15:01:09', '2016-12-05 15:01:09', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(322, 1, '2016-12-05 15:01:12', '2016-12-05 15:01:12', '', 'bona-3', '', 'inherit', 'open', 'closed', '', 'bona-3', '', '', '2016-12-05 15:01:12', '2016-12-05 15:01:12', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(323, 1, '2016-12-05 15:01:15', '2016-12-05 15:01:15', '', 'bona-4', '', 'inherit', 'open', 'closed', '', 'bona-4', '', '', '2016-12-05 15:01:15', '2016-12-05 15:01:15', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(324, 1, '2016-12-05 15:01:19', '2016-12-05 15:01:19', '', 'bona-5', '', 'inherit', 'open', 'closed', '', 'bona-5', '', '', '2016-12-05 15:01:19', '2016-12-05 15:01:19', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(325, 1, '2016-12-05 15:01:22', '2016-12-05 15:01:22', '', 'bona-6', '', 'inherit', 'open', 'closed', '', 'bona-6', '', '', '2016-12-05 15:25:47', '2016-12-05 15:25:47', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(326, 1, '2016-12-05 15:02:51', '2016-12-05 15:02:51', '', 'bona', '', 'inherit', 'open', 'closed', '', 'bona', '', '', '2016-12-05 15:02:55', '2016-12-05 15:02:55', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/bona.jpg', 0, 'attachment', 'image/jpeg', 0),
(327, 1, '2016-12-05 15:07:37', '2016-12-05 15:07:37', '', 'cyk-1', '', 'inherit', 'open', 'closed', '', 'cyk-1', '', '', '2016-12-05 15:07:59', '2016-12-05 15:07:59', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(328, 1, '2016-12-05 15:07:40', '2016-12-05 15:07:40', '', 'cyk-2', '', 'inherit', 'open', 'closed', '', 'cyk-2', '', '', '2016-12-05 15:07:40', '2016-12-05 15:07:40', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(329, 1, '2016-12-05 15:07:43', '2016-12-05 15:07:43', '', 'cyk-3', '', 'inherit', 'open', 'closed', '', 'cyk-3', '', '', '2016-12-05 15:07:43', '2016-12-05 15:07:43', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(330, 1, '2016-12-05 15:07:46', '2016-12-05 15:07:46', '', 'cyk-4', '', 'inherit', 'open', 'closed', '', 'cyk-4', '', '', '2016-12-05 15:07:46', '2016-12-05 15:07:46', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(331, 1, '2016-12-05 15:07:49', '2016-12-05 15:07:49', '', 'cyk-5', '', 'inherit', 'open', 'closed', '', 'cyk-5', '', '', '2016-12-05 15:07:49', '2016-12-05 15:07:49', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(332, 1, '2016-12-05 15:07:52', '2016-12-05 15:07:52', '', 'cyk-6', '', 'inherit', 'open', 'closed', '', 'cyk-6', '', '', '2016-12-05 15:26:49', '2016-12-05 15:26:49', '', 59, 'http://wypozyczalniacykliniarek.com/wp-content/uploads/2016/11/cyk-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(337, 1, '2016-12-05 17:13:25', '2016-12-05 17:13:25', '', 'cyk50', '', 'publish', 'closed', 'closed', '', 'cyk50', '', '', '2016-12-05 17:13:25', '2016-12-05 17:13:25', '', 0, 'http://wypozyczalniacykliniarek.com/?post_type=shop_coupon&#038;p=337', 0, 'shop_coupon', '', 0) ;

#
# End of data contents of table `wpwyp2810161505_posts`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_relationships`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_relationships`;


#
# Table structure of table `wpwyp2810161505_term_relationships`
#

CREATE TABLE `wpwyp2810161505_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_relationships`
#
INSERT INTO `wpwyp2810161505_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(67, 2, 0),
(68, 2, 0),
(69, 2, 0),
(70, 2, 0),
(71, 2, 0),
(72, 2, 0),
(145, 3, 0),
(151, 3, 0),
(152, 3, 0),
(153, 3, 0),
(154, 3, 0),
(194, 4, 0),
(194, 8, 0),
(194, 10, 0),
(244, 4, 0),
(244, 9, 0),
(315, 4, 0),
(315, 10, 0),
(315, 11, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_term_taxonomy`
#

DROP TABLE IF EXISTS `wpwyp2810161505_term_taxonomy`;


#
# Table structure of table `wpwyp2810161505_term_taxonomy`
#

CREATE TABLE `wpwyp2810161505_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_term_taxonomy`
#
INSERT INTO `wpwyp2810161505_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'nav_menu', '', 0, 5),
(4, 4, 'product_type', '', 0, 3),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(7, 7, 'product_type', '', 0, 0),
(8, 8, 'pa_jednostka-miary', '', 0, 1),
(9, 9, 'pa_jednostka-miary', '', 0, 1),
(10, 10, 'pa_pre-price', '', 0, 2),
(11, 11, 'pa_jednostka-miary', '', 0, 1),
(12, 12, 'product_type', '', 0, 0) ;

#
# End of data contents of table `wpwyp2810161505_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_termmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_termmeta`;


#
# Table structure of table `wpwyp2810161505_termmeta`
#

CREATE TABLE `wpwyp2810161505_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_termmeta`
#
INSERT INTO `wpwyp2810161505_termmeta` ( `meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 8, 'order_pa_jednostka-miary', '0'),
(2, 9, 'order_pa_jednostka-miary', '0'),
(3, 10, 'order_pa_pre-price', '0'),
(4, 11, 'order_pa_jednostka-miary', '0') ;

#
# End of data contents of table `wpwyp2810161505_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_terms`
#

DROP TABLE IF EXISTS `wpwyp2810161505_terms`;


#
# Table structure of table `wpwyp2810161505_terms`
#

CREATE TABLE `wpwyp2810161505_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_terms`
#
INSERT INTO `wpwyp2810161505_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Header', 'header', 0),
(3, 'Footer', 'footer', 0),
(4, 'simple', 'simple', 0),
(5, 'grouped', 'grouped', 0),
(6, 'variable', 'variable', 0),
(7, 'external', 'external', 0),
(8, 'szt.', 'szt', 0),
(9, 'l', 'l', 0),
(10, 'od', 'od', 0),
(11, 'doba', 'doba', 0),
(12, 'redq_rental', 'redq_rental', 0) ;

#
# End of data contents of table `wpwyp2810161505_terms`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_usermeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_usermeta`;


#
# Table structure of table `wpwyp2810161505_usermeta`
#

CREATE TABLE `wpwyp2810161505_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_usermeta`
#
INSERT INTO `wpwyp2810161505_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin_wypozycczalnia'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wpwyp2810161505_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wpwyp2810161505_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'black_studio_tinymce_widget'),
(13, 1, 'show_welcome_panel', '1'),
(15, 1, 'wpwyp2810161505_dashboard_quick_press_last_post_id', '243'),
(16, 1, 'wpwyp2810161505_user-settings', 'editor=tinymce&libraryContent=browse&imgsize=full'),
(17, 1, 'wpwyp2810161505_user-settings-time', '1480606972'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'nav_menu_recently_edited', '3'),
(21, 1, 'session_tokens', 'a:2:{s:64:"aa57f493d92a48619a50e126985e37ac1fbd9880a383fbb94f04246e3b187ecc";a:4:{s:10:"expiration";i:1481131709;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";s:5:"login";i:1479922109;}s:64:"6f07e4d64d4fe40c733ae3cb0f33af1147b3c91285cfda820924763d837d17a8";a:4:{s:10:"expiration";i:1482150877;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36";s:5:"login";i:1480941277;}}'),
(22, 1, 'acf_user_settings', 'a:2:{s:29:"collapsed_field_5835d51f24055";s:17:"0,1,2,3,4,6,7,8,9";s:29:"collapsed_field_584042ea1bb70";s:1:"0";}'),
(23, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(25, 1, 'closedpostboxes_product', 'a:1:{i:0;s:11:"postexcerpt";}'),
(26, 1, 'metaboxhidden_product', 'a:7:{i:0;s:12:"postimagediv";i:1;s:23:"acf-group_5845620a498bb";i:2;s:23:"acf-group_583c0ba522a74";i:3;s:23:"acf-group_584042e9c9a0d";i:4;s:10:"postcustom";i:5;s:7:"slugdiv";i:6;s:11:"postexcerpt";}'),
(27, 1, 'closedpostboxes_toplevel_page_acf-options', 'a:1:{i:0;s:23:"acf-group_583c0ba522a74";}'),
(28, 1, 'metaboxhidden_toplevel_page_acf-options', 'a:0:{}'),
(29, 1, 'DOPBSP_backend_language', 'en'),
(30, 1, 'billing_first_name', 'Test'),
(31, 1, 'billing_last_name', 'Test'),
(32, 1, 'billing_company', ''),
(33, 1, 'billing_email', 'patryk@visibee.pl'),
(34, 1, 'billing_phone', '608187167'),
(35, 1, 'billing_country', 'PL'),
(36, 1, 'billing_address_1', 'Test'),
(37, 1, 'billing_address_2', ''),
(38, 1, 'billing_city', 'Test'),
(39, 1, 'billing_state', ''),
(40, 1, 'billing_postcode', '81-377'),
(41, 1, 'meta-box-order_product', 'a:4:{s:15:"acf_after_title";s:0:"";s:4:"side";s:84:"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images";s:6:"normal";s:163:"woocommerce-product-data,acf-group_5845620a498bb,acf-group_583c0ba522a74,acf-group_584042e9c9a0d,acf-group_5835d5033efe3,postcustom,slugdiv,postexcerpt,commentsdiv";s:8:"advanced";s:0:"";}'),
(42, 1, 'screen_layout_product', '2'),
(43, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:1:{s:32:"051fcd864e119b297c37ca97f2ad1737";a:15:{s:14:"_booking_price";s:3:"850";s:17:"_booking_duration";i:17;s:11:"_start_date";s:16:"6 grudzień 2016";s:9:"_end_date";s:17:"22 grudzień 2016";s:10:"_ebs_start";s:10:"2016-12-06";s:8:"_ebs_end";s:10:"2016-12-22";s:10:"product_id";i:315;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:8;s:10:"line_total";d:2764.2276000000002;s:8:"line_tax";d:635.77239999999995;s:13:"line_subtotal";d:5528.4552999999996;s:17:"line_subtotal_tax";d:1271.5446999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:635.77239999999995;}s:8:"subtotal";a:1:{i:1;d:1271.5446999999999;}}}}}') ;

#
# End of data contents of table `wpwyp2810161505_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_users`
#

DROP TABLE IF EXISTS `wpwyp2810161505_users`;


#
# Table structure of table `wpwyp2810161505_users`
#

CREATE TABLE `wpwyp2810161505_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_users`
#
INSERT INTO `wpwyp2810161505_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin_wypozyczalnia', '$P$BqCt3f5NgafK1P8Ci3qrqDTmBA1bAP0', 'admin_wypozyczalnia', 'patryk@visibee.pl', '', '2016-10-28 13:10:21', '', 0, 'admin_wypozycczalnia') ;

#
# End of data contents of table `wpwyp2810161505_users`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_api_keys`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_api_keys`;


#
# Table structure of table `wpwyp2810161505_woocommerce_api_keys`
#

CREATE TABLE `wpwyp2810161505_woocommerce_api_keys` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `permissions` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8_unicode_ci,
  `truncated_key` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  PRIMARY KEY (`key_id`),
  KEY `consumer_key` (`consumer_key`),
  KEY `consumer_secret` (`consumer_secret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_api_keys`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_api_keys`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_attribute_taxonomies`;


#
# Table structure of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#

CREATE TABLE `wpwyp2810161505_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_label` longtext COLLATE utf8_unicode_ci,
  `attribute_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_orderby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
#
INSERT INTO `wpwyp2810161505_woocommerce_attribute_taxonomies` ( `attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'jednostka-miary', 'Jednostka miary', 'select', 'menu_order', 0),
(2, 'pre-price', 'Przed ceną', 'select', 'menu_order', 0) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_attribute_taxonomies`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_downloadable_product_permissions`;


#
# Table structure of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

CREATE TABLE `wpwyp2810161505_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `download_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_downloadable_product_permissions`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_order_itemmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_order_itemmeta`;


#
# Table structure of table `wpwyp2810161505_woocommerce_order_itemmeta`
#

CREATE TABLE `wpwyp2810161505_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_order_itemmeta`
#
INSERT INTO `wpwyp2810161505_woocommerce_order_itemmeta` ( `meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_qty', '2'),
(2, 1, '_tax_class', ''),
(3, 1, '_product_id', '194'),
(4, 1, '_variation_id', '0'),
(5, 1, '_line_subtotal', '65.0407'),
(6, 1, '_line_total', '65.0407'),
(7, 1, '_line_subtotal_tax', '14.9593'),
(8, 1, '_line_tax', '14.9593'),
(9, 1, '_line_tax_data', 'a:2:{s:5:"total";a:1:{i:1;s:7:"14.9593";}s:8:"subtotal";a:1:{i:1;s:7:"14.9593";}}'),
(10, 2, '_qty', '1'),
(11, 2, '_tax_class', ''),
(12, 2, '_product_id', '244'),
(13, 2, '_variation_id', '0'),
(14, 2, '_line_subtotal', '32.5203'),
(15, 2, '_line_total', '32.5203'),
(16, 2, '_line_subtotal_tax', '7.4797'),
(17, 2, '_line_tax', '7.4797'),
(18, 2, '_line_tax_data', 'a:2:{s:5:"total";a:1:{i:1;s:6:"7.4797";}s:8:"subtotal";a:1:{i:1;s:6:"7.4797";}}'),
(19, 3, '_qty', '1'),
(20, 3, '_tax_class', ''),
(21, 3, '_product_id', '315'),
(22, 3, '_variation_id', '0'),
(23, 3, '_line_subtotal', '813.0081'),
(24, 3, '_line_total', '813.0081'),
(25, 3, '_line_subtotal_tax', '186.9919'),
(26, 3, '_line_tax', '186.9919'),
(27, 3, '_line_tax_data', 'a:2:{s:5:"total";a:1:{i:1;s:8:"186.9919";}s:8:"subtotal";a:1:{i:1;s:8:"186.9919";}}'),
(28, 3, 'Pickup Date', '12/06/2016'),
(29, 3, 'Pickup Time', '15:45'),
(30, 3, 'Drop-off Date', '12/16/2016'),
(31, 3, 'Drop-off Time', '16:10'),
(32, 3, 'Total Days', '5'),
(33, 4, 'rate_id', '1'),
(34, 4, 'label', 'VAT'),
(35, 4, 'compound', '0'),
(36, 4, 'tax_amount', '209.4309'),
(37, 4, 'shipping_tax_amount', '0') ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_order_itemmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_order_items`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_order_items`;


#
# Table structure of table `wpwyp2810161505_woocommerce_order_items`
#

CREATE TABLE `wpwyp2810161505_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_order_items`
#
INSERT INTO `wpwyp2810161505_woocommerce_order_items` ( `order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Podkład odcinający AL. Domalux', 'line_item', 316),
(2, 'Podkład odcinający AL. Domalux (Kopia)', 'line_item', 316),
(3, 'Duża i mała cykliniarka (zestaw)', 'line_item', 316),
(4, 'PL-VAT-1', 'tax', 316) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_order_items`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_payment_tokenmeta`;


#
# Table structure of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

CREATE TABLE `wpwyp2810161505_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `payment_token_id` (`payment_token_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_payment_tokenmeta`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_payment_tokens`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_payment_tokens`;


#
# Table structure of table `wpwyp2810161505_woocommerce_payment_tokens`
#

CREATE TABLE `wpwyp2810161505_woocommerce_payment_tokens` (
  `token_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_payment_tokens`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_payment_tokens`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_sessions`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_sessions`;


#
# Table structure of table `wpwyp2810161505_woocommerce_sessions`
#

CREATE TABLE `wpwyp2810161505_woocommerce_sessions` (
  `session_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_key` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `session_expiry` bigint(20) NOT NULL,
  PRIMARY KEY (`session_key`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_sessions`
#
INSERT INTO `wpwyp2810161505_woocommerce_sessions` ( `session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(91, '1', 'a:22:{s:21:"removed_cart_contents";s:637:"a:1:{s:32:"745be145c17c1d3e1bde2bf4a6ac954b";a:15:{s:14:"_booking_price";s:4:"1700";s:17:"_booking_duration";i:17;s:11:"_start_date";s:16:"6 grudzień 2016";s:9:"_end_date";s:17:"22 grudzień 2016";s:10:"_ebs_start";s:10:"2016-12-06";s:8:"_ebs_end";s:10:"2016-12-22";s:10:"product_id";i:315;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:1382.1138000000001;s:8:"line_tax";d:317.88619999999997;s:13:"line_subtotal";d:1382.1138000000001;s:17:"line_subtotal_tax";d:317.88619999999997;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:317.88619999999997;}s:8:"subtotal";a:1:{i:1;d:317.88619999999997;}}}}";s:14:"shipping_total";N;s:10:"wc_notices";N;s:21:"chosen_payment_method";s:4:"bacs";s:8:"customer";s:407:"a:14:{s:8:"postcode";s:6:"81-377";s:4:"city";s:4:"Test";s:9:"address_1";s:4:"Test";s:9:"address_2";s:0:"";s:5:"state";s:0:"";s:7:"country";s:2:"PL";s:17:"shipping_postcode";s:6:"81-377";s:13:"shipping_city";s:4:"Test";s:18:"shipping_address_1";s:4:"Test";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:0:"";s:16:"shipping_country";s:2:"PL";s:13:"is_vat_exempt";b:0;s:19:"calculated_shipping";b:1;}";s:7:"booking";s:6:"a:0:{}";s:4:"cart";s:636:"a:1:{s:32:"051fcd864e119b297c37ca97f2ad1737";a:15:{s:14:"_booking_price";s:3:"850";s:17:"_booking_duration";i:17;s:11:"_start_date";s:16:"6 grudzień 2016";s:9:"_end_date";s:17:"22 grudzień 2016";s:10:"_ebs_start";s:10:"2016-12-06";s:8:"_ebs_end";s:10:"2016-12-22";s:10:"product_id";i:315;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:8;s:10:"line_total";d:2764.2276000000002;s:8:"line_tax";d:635.77239999999995;s:13:"line_subtotal";d:5528.4552999999996;s:17:"line_subtotal_tax";d:1271.5446999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:635.77239999999995;}s:8:"subtotal";a:1:{i:1;d:1271.5446999999999;}}}}";s:15:"applied_coupons";s:22:"a:1:{i:0;s:5:"cyk50";}";s:23:"coupon_discount_amounts";s:39:"a:1:{s:5:"cyk50";d:2764.2280000000001;}";s:27:"coupon_discount_tax_amounts";s:39:"a:1:{s:5:"cyk50";d:635.77200000000005;}";s:19:"cart_contents_total";d:2764.2276000000002;s:5:"total";d:3400;s:8:"subtotal";d:6800;s:15:"subtotal_ex_tax";d:5528.4552999999996;s:9:"tax_total";d:635.77239999999995;s:5:"taxes";s:31:"a:1:{i:1;d:635.77239999999995;}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";d:2764.2280000000001;s:17:"discount_cart_tax";d:635.77200000000005;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";}', 1481114086) ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_sessions`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zone_locations`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`),
  KEY `location_type` (`location_type`),
  KEY `location_type_code` (`location_type`,`location_code`(90))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zone_locations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zone_methods`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `method_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method_order` bigint(20) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zone_methods`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_shipping_zones`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_shipping_zones`;


#
# Table structure of table `wpwyp2810161505_woocommerce_shipping_zones`
#

CREATE TABLE `wpwyp2810161505_woocommerce_shipping_zones` (
  `zone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zone_order` bigint(20) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_shipping_zones`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_shipping_zones`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_tax_rate_locations`;


#
# Table structure of table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

CREATE TABLE `wpwyp2810161505_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type` (`location_type`),
  KEY `location_type_code` (`location_type`,`location_code`(90))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_tax_rate_locations`
#

#
# End of data contents of table `wpwyp2810161505_woocommerce_tax_rate_locations`
# --------------------------------------------------------



#
# Delete any existing table `wpwyp2810161505_woocommerce_tax_rates`
#

DROP TABLE IF EXISTS `wpwyp2810161505_woocommerce_tax_rates`;


#
# Table structure of table `wpwyp2810161505_woocommerce_tax_rates`
#

CREATE TABLE `wpwyp2810161505_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`(191)),
  KEY `tax_rate_state` (`tax_rate_state`(191)),
  KEY `tax_rate_class` (`tax_rate_class`(191)),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wpwyp2810161505_woocommerce_tax_rates`
#
INSERT INTO `wpwyp2810161505_woocommerce_tax_rates` ( `tax_rate_id`, `tax_rate_country`, `tax_rate_state`, `tax_rate`, `tax_rate_name`, `tax_rate_priority`, `tax_rate_compound`, `tax_rate_shipping`, `tax_rate_order`, `tax_rate_class`) VALUES
(1, 'PL', '', '23.0000', 'VAT', 1, 0, 1, 0, '') ;

#
# End of data contents of table `wpwyp2810161505_woocommerce_tax_rates`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

